UPDATE lecturas_silos SET porcentaje = 40 WHERE id_entidad = 483 AND fecha BETWEEN '2020-10-27 02:33:36' AND '2020-11-11 19:31:00';
UPDATE lecturas_silos SET id_entidad = null WHERE fecha < '2020-10-26 20:33:25' AND id_entidad = 483;
UPDATE lecturas_silos SET kilos = (porcentaje * (SELECT capacidad FROM entidades_recipientes WHERE id_entidad = 483)) / 100 WHERE id_entidad = 483;
UPDATE lecturas_silos SET altura = 305 WHERE id_entidad = 483 AND porcentaje = 40;

SELECT DATE_FORMAT(fecha, "%d-%m-%Y %H:%i:%s") fecha, porcentaje, altura distancia FROM lecturas_silos WHERE id_entidad = 483 AND fecha BETWEEN '2020-11-08' AND '2020-12-17' ORDER BY lecturas_silos.fecha ASC



-- Naves
-- Interior 378
-- Exterior 379
SELECT DATE_FORMAT(fecha, "%d-%m-%Y %H:%i:%s") fecha, temperatura, humedad FROM entidades_naves_historico WHERE id_entidad = 378 AND fecha BETWEEN '2020-07-28 00:00:00' AND '2020-12-17 00:00:00' ORDER BY entidades_naves_historico.fecha ASC;


SELECT DATE_FORMAT(fecha, "%d-%m-%Y %H:%i:%s") fecha, min(temperatura), min(humedad) FROM entidades_naves_historico WHERE id_entidad = 378 AND fecha BETWEEN '2020-07-28 00:00:00' AND '2020-12-17 00:00:00' GROUP BY DATE_FORMAT(fecha, "%d-%m-%Y %H:") ORDER BY entidades_naves_historico.fecha ASC;

SELECT DATE_FORMAT(N.fecha, "%d-%m-%Y %H") fecha, min(temperatura), min(humedad) FROM entidades_naves_historico N WHERE id_entidad = 378 AND fecha BETWEEN '2020-07-28 00:00:00' AND '2020-12-17 00:00:00' GROUP BY DATE_FORMAT(N.fecha, "%d-%m-%Y %H") ORDER BY min(fecha);