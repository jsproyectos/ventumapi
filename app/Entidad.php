<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
	public $timestamps = false;
	protected $table = 'entidades';

	protected $fillable = ["id", "nombre", "codigo", "fake", "descripcion", "latitud", "longitud", "id_tipo", "id_explotacion", "fechaAlta", "fechaBaja"];
}
