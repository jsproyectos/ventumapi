<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertaSensor extends Model
{
	public $timestamps = false;
	protected $table = 'alertas_sensores';

	protected $fillable = ['id', 'id_sensor', 'id_alerta', 'id_usuario_add', 'id_usuario', 'parametros', 'activo', 'ultima'];
}
