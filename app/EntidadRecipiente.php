<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntidadRecipiente extends Model
{
	public $timestamps = false;
	protected $table = 'entidades_recipientes';
	protected $primaryKey = 'id_entidad';

	protected $fillable = ["id_entidad", "id_modelo", "id_sensor", "id_ganado", "animales", "fecha", "capacidad"];
}
