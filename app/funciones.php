<?php

define('VALOR_MAXIMO_BATERIA', 3.8);
define('VALOR_MEDIO_BATERIA', 3.4); // Entre VALOR_BAJO_BATERIA y VALOR_MEDIO_BATERIA se avisa Warning
define('VALOR_BAJO_BATERIA', 3.2); //Por debajo de esto la bateria está BAJA
define('VALOR_MUY_BAJO_BATERIA', 3.1); //Por debajo de esto la bateria está BAJA
define('VALOR_MINIMO_BATERIA', 3); //Por debajo de esto la bateria está MUERTA

define('VALOR_MAXIMO_3_PILAS', 4500); // Valor máximo con 3 pilas de 1.5 V
define('VALOR_MINIMO_3_PILAS', 2500); // Valor máximo con 3 pilas de 1.5 V

define('INTERVALO_VALOR_MAXIMO_BATERIA', VALOR_MAXIMO_BATERIA - VALOR_MINIMO_BATERIA); // 0.8
define('INTERVALO_VALOR_MEDIO_BATERIA', VALOR_MEDIO_BATERIA - VALOR_MINIMO_BATERIA); // 0.4
define('INTERVALO_VALOR_BAJO_BATERIA', VALOR_BAJO_BATERIA - VALOR_MINIMO_BATERIA); // 0.2

define('VALOR_MAXIMO_SIGNAL', 30);
define('VALOR_MEDIO_SIGNAL', 15);	// Entre VALOR_MINIMO_SIGNAL y VALOR_MEDIO_SIGNAL se avisa Warning
define('VALOR_MINIMO_SIGNAL', 8);	//Por debajo de esto la señal es BAJA

// Valores booleanos de la base de datos
define('ACTIVO', 1);
define('NO_ACTIVO', 0);

// Sensores
define('SENSOR_SILO', 1);
define('SENSOR_TEMPERATURA', 2);
define('SENSOR_GPS', 3);
define('SENSOR_PUERTA', 4);
define('SENSOR_SIN_TIPO', 6);
define('SENSOR_ESTACION', 7);
define('SENSOR_MACHOS', 8);
define('SENSOR_TEMP_HUM_LUZ', 10);
define('SENSOR_AGUA_REBOSE', 11);
define('SENSOR_CO2', 15);
define('SENSOR_CAUDALIMETRO', 16);
define('SENSOR_GASES', 17);

// Entidades
define('ENTIDAD_SILO', 1);
define('ENTIDAD_LOCALIZACION', 3);
define('ENTIDAD_PUERTA', 4);
define('ENTIDAD_AGUA_DEPOSITOS', 5);
define('ENTIDAD_ESTACION', 7);
define('ENTIDAD_MACHOS', 8);
define('ENTIDAD_LECHE', 9);
define('ENTIDAD_NAVES', 12);
define('ENTIDAD_NEVERAS', 13);
define('ENTIDAD_CELDA_NAVE', 14);
define('ENTIDAD_ANIMAL', 15);
define('ENTIDAD_TOLVA', 16);

//Tipos de depósito
define('DEPOSITO_SILO', 1);
define('DEPOSITO_AGUA', 2);
define('DEPOSITO_LECHE', 3);

// Constantes de tipos de usuario
define('USUARIO_SUPERADMINISTRADOR', 1);
define('USUARIO_ADMINISTRADOR', 2);
define('USUARIO_TST', 3);
define('USUARIO_NORMAL', 4);
define('USUARIO_PEDRO', 5);
define('USUARIO_AVANZADO', 6);
define('USUARIO_COOPERATIVA_PIENSO', 10);
define('USUARIO_MIEMBRO_COOPERATIVA_PIENSO', 11);

// Tipos de ganado
define('GANADO_OVINO', 1);
define('GANADO_BOVINO', 2);
define('GANADO_PORCINO', 3);
define('GANADO_CAPRINO', 4);
define('GANADO_EQUINO', 5);
define('GANADO_POLLOS', 6);
define('GANADO_GALLLINAS_PONEDORAS', 7);

define('ICONO_GANADO_OVINO', 'oveja_rojo.png');
define('ICONO_GANADO_BOVINO', 'oveja_rojo.png');
define('ICONO_GANADO_PORCINO', 'oveja_rojo.png');
define('ICONO_GANADO_CAPRINO', 'oveja_rojo.png');
define('ICONO_GANADO_EQUINO', 'oveja_rojo.png');
define('ICONO_GANADO_POLLOS', 'oveja_rojo.png');
define('ICONO_GANADO_GALLLINAS_PONEDORAS', 'oveja_rojo.png');

define('SERVIDOR', 'http://clientes.ventumidc.es');
define('RUTA_ICONOS', '/icons/');

define('PUERTA_CERRADA', 0); // Indica el valor de la puerta cerrada para el sensor de puertas
define('PUERTA_ABIERTA', 1); // Indica el valor de la puerta abierta para el sensor de puertas
define('PUERTA_BATERIA', 2); // Indica que la información es un keepalive de la batería

// Estación meteorológica
define('ESTACION_CONFIG_NORMAL', 0);
define('ESTACION_CONFIG_EXCEPCIONAL', 1);
define('ESTACION_EXCEPCION_LUVIA', 1); // Indica que se refiere a una trama que contiene lluvia
define('ESTACION_EXCEPCION_VIENTO', 2); // Indica que se refiere a una trama que contiene una alerta de viento huracanado

//Alarmas
define('ALARMA_BATERIA_BAJA', 101);
define('ALARMA_SNR_BAJA', 102);
define('VALOR_SIN_VALOR', '*');

// Alarmas Silos
define('ALARMA_SILOS_CAPACIDAD_BAJA', 20101);
define('ALARMA_SILOS_CAPACIDAD_ALTA', 20102);
define('ALARMA_SILOS_DESNIVEL', 20103);
define('ALARMA_SILOS_RECEPCION', 20104); // Envía información nada más recibirla

// Alarmas Temperatura Humedad
define('ALARMA_TEMPERATURA_BAJA', 20201);
define('ALARMA_TEMPERATURA_ALTA', 20202);
define('ALARMA_HUMEDAD_BAJA', 20203);
define('ALARMA_HUMEDAD_ALTA', 20204);

// Alarmas GPS
define('ALARMA_GPS_SALIDA_ANIMAL', 20301);
define('ALARMA_GPS_ESTANCAMIENTO', 20302);

// Alarmas Puertas
define('ALARMA_PUERTAS_APERTURA', 20401);
define('ALARMA_PUERTAS_APERTURA_PROLONGADA', 20402);
define('ALARMA_PUERTAS_CIERRE', 20403);
define('ALARMA_PUERTAS_CIERRE_PROLONGADO', 20404);

// Alerta monta macho
define('ALARMA_MONTA_MACHO', 20801);

// Alarmas Luminosidad
define('ALARMA_LUMINOSIDAD_BAJA', 21001);
define('ALARMA_LUMINOSIDAD_ALTA', 21002);

// Alarma de estrés térmico prolongado
define('ALERTA_ETERMICO_PROLONGADO', 31201);
// Alerta de IHT Peligroso
define('ALERTA_IHT_PELIGROSO', 31202);

define('NOTIFICACION_NO_ENVIAR_MAIL', '1970-01-01 02:00:00');

//define('API_KEY_ONE_SIGNAL', 'c01a56bb-f12a-4575-b0dd-f790e1e27128');
define('API_KEY_ONE_SIGNAL', 'bdfcb28a-cec3-449b-823e-bc3ef9ea497f');
define('REST_API_KEY', 'OGJhM2MxY2ItY2U2Yy00NzAyLWE4ZGQtNzY1ZWY5Nzg4NGEx');
define('TITULO_NOTIFICACIONES', 'Gavilán Control');

define('ENTIDAD', 0);
define('SENSOR', 1);

// Devuelve true en caso de que la tapa se encuentre cerrada
function tapaCerrada($aceX, $aceY, $aceZ)
{
	if (($aceX > -10 && $aceX < 10) && ($aceY > -10 && $aceY < 10) && ($aceZ > -60 && $aceZ < -70))
		return true;
	return false;
}

/*function calcularPorcentajeBateriaBarra($valorBateria)
{
	$porcentaje = calcularPorcentajeBateria($valorBateria);
	$color = "bg-success"; // Por defecto

	if ($porcentaje < 15)
	{
		$color = "bg-danger";
	}
	else
	{
		if ($porcentaje < 30)
		{
			$color = "bg-warning";
		}
	}

	return (["porcentaje" => $porcentaje, "color" => $color]);
}

function calcularColorBateria($porcentaje)
{
	if ($porcentaje < 15)
	{
		return "bg-danger";
	}
	else
	{
		if ($porcentaje < 30)
			return $color = "bg-warning";
		else
			return $color = "bg-success";
	}

}*/

// Calcula el porcentaje de batería
function calcularPorcentajeBateria($valorBateria)
{
	/*// Hago esta resta puesto que una batería por debajo de VALOR_MINIMO_BATERÍA es inservible. Luego lo hago para que valores por encima de 3.8 V sea 3.8V
	$valorBateriaUsable = min(max($valorBateria - VALOR_MINIMO_BATERIA, 0), INTERVALO_VALOR_MAXIMO_BATERIA);

	return ($valorBateriaUsable * 100) / INTERVALO_VALOR_MAXIMO_BATERIA; //Porcentaje real de batería.*/

	return 100;
}

function calcularPorcentajeSignalBarra($valorSignal)
{
	$porcentaje = round((($valorSignal * 100) / VALOR_MAXIMO_SIGNAL));
	$color = "bg-success";
	$texto = "Buena";

	if ($valorSignal < VALOR_MINIMO_SIGNAL)
	{
		$color = "bg-danger";
		$texto = "Baja";
	}
	else
	{
		if ($valorSignal < VALOR_MEDIO_SIGNAL)
		{
			$color = "bg-warning";
			$texto = "Regular";
		}
	}

	return (["porcentaje" => $porcentaje, "color" => $color, "texto" => $texto]);
}

function diferenciaDias($fecha1, $fecha2)
{
	// Diferencia entre dos fechas
	$diff = date_diff(date_create($fecha1), date_create($fecha2));

	$dias = (float) $diff->format("%a");
	$horas = $diff->h / 24;

	return $dias + $horas;
}

function bindec2($bin)
{
	if (strlen($bin) == 32 && $bin[0] == '1')
	{
		for ($i = 0; $i < 32; $i++) {
			$bin[$i] = $bin[$i] == '1' ? '0' : '1';
		}
		return (bindec($bin) + 1) * -1;
	}
	return bindec($bin);
}

function bindec8($bin)
{
	if (strlen($bin) == 8 && $bin[0] == '1') {
		for ($i = 0; $i < 8; $i++) {
			$bin[$i] = $bin[$i] == '1' ? '0' : '1';
		}

		return (bindec($bin) + 1) * -1;
	}
	return bindec($bin);
}

function bindecComplemento($bin, $bits = 8)
{
	if (strlen($bin) == $bits && $bin[0] == '1') {
		for ($i = 0; $i < $bits; $i++) {
			$bin[$i] = $bin[$i] == '1' ? '0' : '1';
		}

		return (bindec($bin) + 1) * -1;
	}
	return bindec($bin);
}

function incrementarUnDia($fecha) { return date('Y/m/d H:i:s',strtotime($fecha . "+1 days")); }

function reducirUnDia($fecha) { return date('Y-m-d H:i:s',strtotime($fecha . "-1 days")); }

function likear($cadena) { return "%" . $cadena . "%"; }

function homeSensorNoContratado($activo)
{
	return $activo != '1' ? " homeDeshabilitado" : "";
}

function homeEnlaceSensor($id, $contratado)
{
	return $contratado == '1' ? url('/sensores/general/') . "/"  . $id : "#";
}

function esSensorAutorizado($autorizado)
{
	return $autorizado != "0" ? "sensorAutorizado" : "sensorPropio";
}

function SILOS_obtenerImagen($porcentaje)
{
	$segmento = (intval(intval($porcentaje) / 10) % 10);

	return "/icons/icono_sensor_silo/icono_sensor_silo_" . $segmento . "0.png";
}

// Funciones para pruebas y randomización
function generarCoordenadasAleatorias()
{
	$latMax = 43;
	$latMin = 37;
	$lngMax = -8.5;
	$lngMin = 1;

	$latRango = $latMax - $latMin;
	$lat = $latMin + $latRango * (rand(1,getrandmax()) / getrandmax());

	$lngRango = $lngMax - $lngMin;
	$lng = $lngMin + $lngRango * (rand(1,getrandmax()) / getrandmax());

	return ["lat" => $lat, "lng" => $lng];
}

function obtenerLlenadoAleatorio()
{
	return rand(1, 100);
}

function obtenerTemperaturaAleatoria()
{
	return rand(20, 40);
}

function obtenerBateriaAleatoria()
{
	return rand(2200, 4000);
}

function obtenerSnrAleatoria()
{
	return (float) rand(5, 40);
}

function obtenerValorAcelerometro($eje)
{
	$probabilidadError = rand(1, 100);
	$valorEje = 0;

	switch ($eje)
	{
		case 'x':
			$valorEje = ($probabilidadError < 5) ? rand(1, 255) : rand(0, 15);
		break;
		case 'y':
			$valorEje = ($probabilidadError < 5) ? rand(1, 255) : rand(0, 15);
		break;
		case 'z':
			$valorEje = ($probabilidadError < 5) ? rand(1, 255) : rand(240, 255);
		break;
		
		default:
			$valorEje = rand(1, 255);
		break;
	}

	return $valorEje;
}

function piensos_obtenerColorFondo($porcentaje)
{
	if ($porcentaje > 40)
		return "lleno";
	else
	{
		if ($porcentaje > 10)
			return "medio";
		else
			return "vacio";
	}
}

function obtenerTiposUsuario()
{
	return
	[
		"Administrador" => USUARIO_ADMINISTRADOR,
		"TsT" => USUARIO_TST,
		"Normal" => USUARIO_NORMAL,
		"Cooperativa de pienso" => USUARIO_COOPERATIVA_PIENSO,
		"Miembro Cooperativa de pienso" => USUARIO_MIEMBRO_COOPERATIVA_PIENSO
	];
}

function obtenerInterrogacionesSQL($array)
{
	$cuantos = count($array);
		
	if($cuantos == 0)
		return "(null)";
	else
	{
		$in = "(?";
		for ($i = 1; $i < $cuantos; $i++)
		{
			$in .= ",?";
		}
		$in .= ")";
		return $in;
	}
}

// Función que coloca un checked="checked" si el valor pasado es 1
function ponerChecked($activo)
{
	return $activo == 1 ? 'checked="checked"' : '';
}

// Función que coloca el valor por defecto de un select tag
function ponerSelected($elQueTiene, $elQueDeberiaTener)
{
	return $elQueTiene == $elQueDeberiaTener ? 'selected="selected"' : '';	
}

function concatenarErrores($errores)
{
	$erroresTexto = "";

	while (count($errores) > 1)
	{
		$erroresTexto .= $errores[0] . ", "; array_shift($errores);
	}
	$erroresTexto .= $errores[0];

	return $erroresTexto;
}

// Devuelve un número formateado sin decimales y con puntos
function formatearNumeroSinDecimales($numero)
{
	return intval(number_format($numero, 0, ",", "."));
}

function erroresFormulario($errores)
{
	$indice = 0;
	$mensaje = "Se han encontrado errores: ";

	foreach ($errores as $error)
	{
		$mensaje .=  $error . " ";
	}

	return $mensaje;
}

function erroresFormularioJSON($errores)
{
	$indice = 0;
	$mensaje = "Se han encontrado errores: ";

	foreach ($errores as $error)
	{
		$mensaje .=  $error . " ";
	}

	return ["mensaje" => $mensaje];
}

function obtenerIn($elementos)
{
	// Creamos un string que será lo que vaya en el WHERE de la consulta, de esta forma lo hacemos todo en una operación atómica y rápida
	$cuantosTiposSeleccionados = count($elementos);
	$in = "(";

	switch ($cuantosTiposSeleccionados)
	{
		case 0:
			$in .= "null)";
			break;
		case 1:
			$in .= $elementos[0] . ")";
			break;
		default:
			$in .= $elementos[0];
			for ($i = 1; $i < $cuantosTiposSeleccionados; $i++)
			{
				$in .= "," . $elementos[$i];
			}
			$in .= ")";
			break;
	}

	return $in;
}

// 23-12-2019 Función temporal que convierte cada elemento del array en un String. Usada mientras Sinfony tenga un fallo con PHP 4
function stringearArray($array)
{
	$array = (Array) $array;
	foreach ($array as $key => $value) { $array[$key] = (string) $value; }
	return $array;
}

function esAdmin($tipoUsuario){ return $tipoUsuario == USUARIO_ADMINISTRADOR || $tipoUsuario == USUARIO_SUPERADMINISTRADOR ? true : false; }

?>
