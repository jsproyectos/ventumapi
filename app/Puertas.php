<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puertas extends Model
{
	public $timestamps = false;
	protected $table = 'sensores_puertas';
	protected $primaryKey = 'id_sensor';

	protected $fillable = ["id_sensor", "estado", "fecha"];
}
