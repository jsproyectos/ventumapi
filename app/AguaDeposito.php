<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AguaDeposito extends Model
{
	public $timestamps = false;
	protected $table = 'sensores_depositos_agua';
	protected $primaryKey = 'id_sensor';

	protected $fillable = ["id_sensor", "altura", "ultimaLectura", "litros", "temperatura", "consumo"];
}
