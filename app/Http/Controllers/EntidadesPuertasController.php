<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class EntidadesPuertasController extends EntidadesController
{
	// Crea una nueva entrada en la tabla sensores_temperatura_humedad
	/*public function inicializarResumen($idSensor)
	{
		if (DB::insert("INSERT INTO sensores_puertas (id_sensor) VALUES (?)", [$idSensor]) == 1)
			return true;
		return false;
	}*/

	/*private function obtenerEstadoPuerta($id_sensor)
	{
		return DB::select("SELECT estado, fecha FROM sensores_puertas WHERE id_sensor = ?", [$id_sensor]);
	}*/

	/*public function ExportarHistoricoExcel($id_entidad, $fechaInicio, $fechaFin)
	{
		$criterios = ["fecha", "tipo"];
		$informacion = $this->obtenerInformacionParaExcel($id_entidad, $fechaInicio, incrementarUnDia($fechaFin), "ASC");

		$this->exportarExcel($id_entidad, $criterios, $informacion, $fechaInicio, $fechaFin);
	}*/

	/*public function obtenerInformacionParaExcel($id_entidad, $fechaInicio, $fechaFin, $orden)
	{
		$informacion = $this->obtenerInformacionParaTablas($id_entidad, $fechaInicio, $fechaFin, $orden);

		foreach ($informacion as $info)
		{
			$info->tipo = $info->tipo == PUERTA_ABIERTA ? "Apuertura" : "Cierre";
		}

		return $informacion;
	}*/

	public function actualizar($idEntidad, $fecha, $estado)
	{
		try
		{
			DB::update("UPDATE entidades_puertas SET estado = ?, fecha = ? WHERE id_entidad = ?", [$estado, $fecha, $idEntidad]);

			// Creamos la cola para alertas de la entidad
			$job = new \App\Jobs\alertasEntidadPuerta($idEntidad, $fecha, $estado);
			dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}			
	}
	
	public function obtenerResumen(Request $request)
	{
		try
		{
			$orden = $this->ordenMostrar($request['orden']);

			$entidades = DB::select('SELECT entidades.id id, entidades.nombre nombre, tiempo, entidades.latitud, entidades.longitud, explotaciones.nombre nombreExplotacion, entidades.codigo codigo, entidades.descripcion descripcion, entidades_puertas.fecha, estado, autorizado, notificaciones FROM entidades JOIN entidades_puertas ON entidades.id = entidades_puertas.id_entidad LEFT JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad LEFT JOIN explotaciones ON entidades.id_explotacion = explotaciones.id WHERE id_usuario = ? AND entidades_usuarios.fechaBaja IS NULL AND entidades.fechaBaja IS NULL ORDER BY ' . $orden, [Auth::user()['id']]);

			// Convertimos el autorizado en un booleano para indicar si la entidad es autorizada o propia y se convierten los valores string de humedad y temperatura en floats.
			foreach ($entidades as $entidad)
			{
				$entidad->propio = $entidad->autorizado == 0 ? true : false; unset ($entidad->autorizado);
				$entidad->latitud = floatval($entidad->latitud);
				$entidad->longitud = floatval($entidad->longitud);
			}

			return response()->json(['estado' => true, 'datos' => $entidades], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	private function ordenMostrar($orden)
	{
		switch ($orden)
		{
			case 'nombre': return 'nombre ASC'; break;
			case 'abiertas': return 'estado ASC, nombre ASC'; break;
			case 'cerradas': return 'estado DESC, nombre ASC'; break;
			default: return 'nombre ASC'; break;
		}
	}

	// Función utilizada desde la llamada HTTP o desde actualizar una entidad
	public function obtenerDetallesEntidad($idEntidad)
	{
		$entidad = DB::select('SELECT entidades.id id, entidades.nombre nombre, tiempo, id_sensor, entidades.descripcion descripcion, entidades.latitud latitud, entidades.longitud longitud, entidades.id_tipo id_tipo, id_explotacion, explotaciones.nombre nombreExplotacion, fichero, notificaciones, entidades_puertas.fecha, estado, autorizado FROM entidades JOIN entidades_puertas ON entidades.id = entidades_puertas.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad LEFT JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades.id = ? AND id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL', [$idEntidad, Auth::user()['id']])[0];

		$entidad->propio = $entidad->autorizado == 0 ? true : false; unset ($entidad->autorizado);
		$entidad->latitud = floatval($entidad->latitud);
		$entidad->longitud = floatval($entidad->longitud);

		return $entidad;
	}

	// Función utilizada para las peticiciones HTTP
	public function obtenerDetalles($idEntidad)
	{
		try
		{
			return response()->json(['estado' => true, 'datos' => $this->obtenerDetallesEntidad($idEntidad)], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerHistorico($idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			if (esAdmin(Auth::user()['role']) || $this->puedeVer($idEntidad, Auth::user()['id']))
			{
				// El <> 2 es para que no obtenga la información del estado de batería, que no interesa en la entidad
				$historico = DB::select("SELECT fecha, tipo FROM lecturas_puertas WHERE id_entidad = ? AND fecha BETWEEN ? AND ? AND tipo <> 2 ORDER BY fecha DESC", [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);

				return response()->json(['estado' => true, 'datos' => $historico], 200);
			}
			else
			{
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permisos pera ver esta información'], 200);
			}
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER HISTORICO PUERTAS] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}
