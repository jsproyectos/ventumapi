<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use Validator;
use Exception;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

define('LONGITUD_TRAMA_TRACKER_TST', 12);

class SensoresGpsController extends Controller
{
	function MostrarPantallaGPS()
	{
		$tipoUsuario = Auth::user()["role"];
		$nombrePagina = "principal";

		return view('home', ["tipoUsuario" => $tipoUsuario, "nombrePagina" => $nombrePagina]);
	}

	// Devuelve en JSON los datos del GPS, para exportaciones e importaciones de bases de datos
	function obtenerInformacionJsonGET($id_sensor, $fechaInicio, $fechaFin)
	{
		$orden = "DESC";
		return json_encode($this->obtenerInformacion($id_sensor, $fechaInicio, $fechaFin, $orden));
	}

	public function obtenerDatosGPSPorIdYFechas($id, $fechaInicio, $fechaFin)
	{
		return DB::select("SELECT fecha, snr, latitud, longitud, bateria, flag_configuracion, flag_movimiento, flag_alarma_digital, flag_entrada_digital, flag_cobertura, flag_satelites, velocidad FROM lecturas_gps WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC", [$id, $fechaInicio, $fechaFin]);
	}

	// Obtiene información para localizaciones
	public function obtenerInformacion($id_sensor, $fechaInicio, $fechaFin, $orden)
	{
		return DB::select("SELECT fecha, latitud, longitud, bateria, snr FROM lecturas_gps WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha $orden", [$id_sensor, $fechaInicio, $fechaFin]);
	}

	public function obtenerInformacionJson(Request $request)
	{
		$fechaFin = incrementarUnDia($request["fechaFin"]);
		$orden = "DESC";

		$mensaje["estado"] = "OK";
		$mensaje["datos"] = $this->obtenerInformacion($request["id_sensor"], $request["fechaInicio"], $fechaFin, $orden);

		return json_encode($mensaje);
	}

	public function decodificarTramaGPS($payload)
	{
		// Antes rellenamos con 0s los huecos para que se pueda convertir correctamente
		$trama = "";

		for ($i = 0; $i < LONGITUD_TRAMA_TRACKER_TST; $i ++)
		{
			$posicion = $i * 2;
			$subCadenaAConvertir = substr($payload, $posicion, 2);

			// Convertido el hexadecimal a binario
			$subCadenaBinaria = base_convert ($subCadenaAConvertir, 16, 2);

			//Ahora rellenamos de 0s a la izquierdapara que todos sean de 8 bits:
			$trama .= str_pad($subCadenaBinaria, 8, "0", STR_PAD_LEFT);
		}

		$NSEQ_CONFIG = bindec(substr($trama, 0, 2));
		$IRQ_ACC_FLAG = bindec(substr($trama, 2, 1));
		$IRQ_DI_FLAG = bindec(substr($trama, 3, 1));
		$DI_VALUE = bindec(substr($trama, 4, 1));
		$LAST_GOOD_POSITION_FLAG = bindec(substr($trama, 5, 1));
		$BATTERY = (bindec(substr($trama, 8, 7) . "00000") * 3.9) / 4095;
		$LATITUDE = $this->obtenerLatitud(substr($trama, 15, 28));
		$LONGITUDE = $this->obtenerLongitud(substr($trama, 43, 29));
		$NUMBER_OF_SATELLITES = bindec(substr($trama, 77, 3));
		$SPEED_IN_KMPH = bindec(substr($trama, 80, 8)) * 1.852;

		return [
			"NSEQ_CONFIG" => $NSEQ_CONFIG,
			"IRQ_ACC_FLAG" => $IRQ_ACC_FLAG,
			"IRQ_DI_FLAG" => $IRQ_DI_FLAG,
			"DI_VALUE" => $DI_VALUE,
			"LAST_GOOD_POSITION_FLAG" => $LAST_GOOD_POSITION_FLAG,
			"BATTERY" => $BATTERY,
			"LATITUDE" => $LATITUDE,
			"LONGITUDE" => $LONGITUDE,
			"NUMBER_OF_SATELLITES" => $NUMBER_OF_SATELLITES,
			"SPEED_IN_KMPH" => $SPEED_IN_KMPH
		];
	}

	public function insertarLectura($idSensor, $idEntidad, $bateria, $bp, $rssi, $snr, $fecha, $latitud, $longitud, $nseq, $irqAccFlag, $irqDiFlag, $diValue, $lastGood, $satelites, $velocidad)
	{
		try
		{
			// Insertar en histórico
			DB::insert("INSERT INTO lecturas_gps (id_sensor, id_entidad, fecha, snr, rssi, latitud, longitud, bateria, bp, flag_configuracion, flag_movimiento, flag_alarma_digital, flag_entrada_digital, flag_cobertura, flag_satelites, velocidad) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [$idSensor, $idEntidad, $fecha, $snr, $rssi, $latitud, $longitud, $bateria, $bp, $nseq, $irqAccFlag, $irqDiFlag, $diValue, $lastGood, $satelites, $velocidad]);

			// Este sensor no tiene resumen, ya que su posición la guardamos en la tabla sensores.
			DB::update("UPDATE sensores SET bateria = ?, bp = ?, snr = ?, rssi = ?, latitud = ?, longitud = ? WHERE id = ?", [$bateria, $bp, $snr, $rssi, $latitud, $longitud, $idSensor]);
			return true;

			// Creamos una cola para las alertas relacionadas con ese sensor
			/* $job = new \App\Jobs\alertasGPS($idSensor, $fecha, $trama["BATTERY"], $snr, $trama["LATITUDE"], $trama["LONGITUDE"]);
			dispatch($job); */
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
		return false;
	}

	private function obtenerLatitud($coordenadaEnBinario)
	{
		// Se pregunta si el primer bit es 1, lo cual quiere decir que es un número negativo. Si es cierto, se rellenan con 1s hasta ser de 32 bits
		if ($coordenadaEnBinario[0] == 1)
			return $this->convertirCoordenada(bindec2("1111" . $coordenadaEnBinario));
		else
			return $this->convertirCoordenada(bindec($coordenadaEnBinario));
	}

	private function obtenerLongitud($coordenadaEnBinario)
	{
		// Se pregunta si el primer bit es 1, lo cual quiere decir que es un número negativo. Si es cierto, se rellenan con 1s hasta ser de 32 bits
		if ($coordenadaEnBinario[0] == 1)
			return $this->convertirCoordenada(bindec2("111" . $coordenadaEnBinario));
		else
			return $this->convertirCoordenada(bindec($coordenadaEnBinario));
	}

	private function convertirCoordenada($coordenada)
	{
		$coordenada = $coordenada / 10000;
		$parte_entera = intdiv($coordenada , 100);
		$parte_decimal = ($coordenada - ($parte_entera * 100)) / 60;
		return($parte_entera + $parte_decimal);
	}

	public function obtenerResumen()
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = DB::select("SELECT sensores.id as id, sensores.nombre AS nombre, sensores.descripcion AS descripcion, latitud, longitud, bateria, snr, '0' AS autorizado, animales.id_tipo AS tipo, crotal, animales.nombre AS nombreAnimal FROM sensores LEFT JOIN sensores_animales ON sensores.id = sensores_animales.id_sensor LEFT JOIN animales ON sensores_animales.id_animal = animales.id WHERE sensores.id_tipo = ?", [SENSOR_GPS]);
		}
		else
		{
			$id_usuario = Auth::user()["id"];
			$sensores = DB::select("SELECT sensores.id as id, sensores.nombre AS nombre, sensores.descripcion AS descripcion, latitud, longitud, bateria, snr, '0' AS autorizado, animales.id_tipo AS tipo, crotal, animales.nombre AS nombreAnimal FROM sensores LEFT JOIN sensores_animales ON sensores.id = sensores_animales.id_sensor JOIN animales ON sensores_animales.id_animal = animales.id WHERE sensores.id IN (SELECT id_sensor FROM sensores_usuarios WHERE id_usuario = ?) UNION SELECT sensores.id as id, sensores.nombre AS nombre, sensores.descripcion AS descripcion, latitud, longitud, bateria, snr, '1' AS autorizado, animales.id_tipo AS tipo, crotal, animales.nombre AS nombreAnimal FROM sensores LEFT JOIN sensores_animales ON sensores.id = sensores_animales.id_sensor JOIN animales ON sensores_animales.id_animal = animales.id WHERE sensores.id IN (SELECT id_sensor FROM sensores_usuarios_autorizados WHERE id_usuario = ?)", [$id_usuario, $id_usuario]);
		}

		foreach ($sensores as $sensor)
		{
			$bateria = calcularPorcentajeBateriaBarra($sensor->bateria);

			$sensor->icono = $this->obtenerIconoSegunAnimal($sensor->tipo);
			$sensor->bateria = $bateria["porcentaje"];
			$sensor->colorBateria = $bateria["color"];
		}

		return $sensores;
	}

	public function obtenerResumenSensoresEspecificosPorAdministrador($sensores)
	{
		$interrogaciones = obtenerInterrogacionesSQL($sensores);

		return DB::select("SELECT sensores.id as id, sensores.nombre AS nombre, sensores.descripcion AS descripcion, latitud, longitud, bateria, snr, '0' AS autorizado, animales.id_tipo AS tipo, crotal, animales.nombre AS nombreAnimal FROM sensores LEFT JOIN sensores_animales ON sensores.id = sensores_animales.id_sensor JOIN animales ON sensores_animales.id_animal = animales.id WHERE sensores.id IN $interrogaciones", $sensores);
	}

	public function obtenerResumenSensoresEspecificosPorUsuario($id_usuario, $sensores)
	{
		return DB::select("SELECT sensores.id as id, sensores.nombre AS nombre, sensores.descripcion AS descripcion, latitud, longitud, bateria, snr, '0' AS autorizado, animales.id_tipo AS tipo, crotal, animales.nombre AS nombreAnimal FROM sensores LEFT JOIN sensores_animales ON sensores.id = sensores_animales.id_sensor JOIN animales ON sensores_animales.id_animal = animales.id WHERE sensores.id IN (SELECT id_sensor FROM sensores_usuarios WHERE id_usuario = ? AND id_sensor IN ()) UNION SELECT sensores.id as id, sensores.nombre AS nombre, sensores.descripcion AS descripcion, latitud, longitud, bateria, snr, '1' AS autorizado, animales.id_tipo AS tipo, crotal, animales.nombre AS nombreAnimal FROM sensores LEFT JOIN sensores_animales ON sensores.id = sensores_animales.id_sensor JOIN animales ON sensores_animales.id_animal = animales.id WHERE sensores.id IN (SELECT id_sensor FROM sensores_usuarios_autorizados WHERE id_usuario = ? AND id_sensor IN ()", [$id_usuario, $sensores, $id_usuario, $sensores]);
	}

	public function obtenerResumenAJAX(Request $request)
	{
		$sensoresABuscar = json_decode($request["sensores"]);

		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = $this->obtenerResumenSensoresEspecificosPorAdministrador($sensoresABuscar);
		}
		else
		{
			$id_usuario = Auth::user()["id"];
			$sensores = $this->obtenerResumenSensoresEspecificosPorUsuario($id_usuario, $sensoresABuscar);
		}

		foreach ($sensores as $sensor)
		{
			$bateria = calcularPorcentajeBateriaBarra($sensor->bateria);

			$sensor->icono = $this->obtenerIconoSegunAnimal($sensor->tipo);
			$sensor->bateria = $bateria["porcentaje"];
			$sensor->colorBateria = $bateria["color"];
		}

		return json_encode(["estado" => "OK", "mensaje" => $sensores]);

	}

	public function obtenerIconoSegunAnimal($tipoAnimal)
	{
		switch ($tipoAnimal)
		{
			case GANADO_OVINO:
				return RUTA_ICONOS . ICONO_GANADO_OVINO;
				break;
			case GANADO_BOVINO:
				return RUTA_ICONOS . ICONO_GANADO_BOVINO;
				break;
			case GANADO_PORCINO:
				return RUTA_ICONOS . ICONO_GANADO_PORCINO;
				break;
			case GANADO_CAPRINO:
				return RUTA_ICONOS . GANADO_CAPRINO;
				break;
			case GANADO_EQUINO:
				return RUTA_ICONOS . ICONO_GANADO_EQUINO;
				break;
			case GANADO_POLLOS:
				return RUTA_ICONOS . ICONO_GANADO_POLLOS;
				break;
			case GANADO_GALLLINAS_PONEDORAS:
				return RUTA_ICONOS . GANADO_GALLLINAS_PONEDORAS;
				break;
			
			default:
				return RUTA_ICONOS . ICONO_GANADO_OVINO;
				break;
		}
	}

	public function obtenerZonasHabituales(Request $request)
	{
		try
		{
			$idExplotacion = $request["explotacion"];
			$tipoUsuario = Auth::user()["role"];

			if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
			{
				$lecturas = DB::select("SELECT latitud AS lat, longitud AS lon, '0' AS autorizado FROM lecturas_gps WHERE id_entidad IN (SELECT id FROM entidades WHERE id_tipo = ? and id_explotacion = ? AND fechaBaja IS NULL)", [ENTIDAD_ANIMAL, $idExplotacion]);
			}
			else
			{
				$lecturas = DB::select("SELECT latitud AS lat, longitud AS lon, autorizado FROM lecturas_gps JOIN entidades_usuarios ON lecturas_gps.id_entidad = entidades_usuarios.id_entidad WHERE id_usuario = ? AND entidades_usuarios.id_entidad IN (SELECT id FROM entidades WHERE id_tipo = ? AND id_explotacion = ? AND fechaBaja IS NULL) AND entidades_usuarios.fechaBaja IS NULL", [Auth::user()["id"], ENTIDAD_ANIMAL, $idExplotacion]);
			}

			return json_encode(["estado" => "OK", "mensaje" => $lecturas]);
		}
		catch (Exception $e)
		{
			return json_encode(["estado" => "ERROR", "mensaje" => $e->getMessage()]);
		}

	}

	// Función que devuelve los trackers que no tienen ningún animal asignado
	public function obtenerSensoresSinAsignar()
	{
		return DB::select("SELECT id, nombre FROM sensores WHERE id_tipo = ? AND id NOT IN (SELECT id FROM sensores_animales)", [SENSOR_GPS]);
	}
}
