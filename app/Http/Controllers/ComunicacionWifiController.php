<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class ComunicacionWifiController extends ComunicacionController
{
	private function todoBien($device)
	{
		return response()->json(['estado' => 'true', 'mensaje' => 'OK', 'fecha' => date('H:i:s d-m-Y')], 200);
	}

	private function todoBienCallback($device, $mensaje)
	{
		return response()->json([
			'estado' => true,
			'mensaje' => 'OK',
			'fecha' => date('H:i:s d-m-Y'),
			'callback' => $mensaje
		], 200);
	}

	private function todoBienV2($device, $tiempo = null, $debug = false)
	{
		$mensaje = [
			'estado' => true,
			'mensaje' => 'OK',
			'fecha' => date('H:i:s d-m-Y'),
		];
		
		if ($debug)
			$mensaje['debug'] = true;

		if ($tiempo)
			$mensaje['callback'] = $tiempo;

		return response()->json($mensaje, 200);
	}

	// Devuelve un mensaje de error ya formado cuando algo ha ido mal, con código de error 500
	private function devolverErrorWifi($device, $mensaje)
	{
		LogController::errores("[$device] $mensaje");
		return response()->json(['estado' => 'ERROR', 'dispositvo' => $device, 'mensaje' => $mensaje], 500);
	}

	// ventumapilocal.es/api/d/ultrasonidos?device=ESP8266MXWIFI1&distancia=50
	/*public function registrarUltrasonidos(Request $request)
	{
		try
		{
			$device = $request["device"];
			$distancia = $request["distancia"];
			$fecha = date("Y-m-d H:i:s");
			$bateria = $request["bateria"];

			$sensor = $this->obtenerInfoSensorSilo($device);

			DB::insert("INSERT INTO lecturas_desarrollo (id_sensor, payload, fecha, distancia, temperatura, humedad, bateria) VALUES (?, ?, ?, ?, ?, ?, ?)", [$sensor->id_sensor, null, $fecha, $distancia, 0, 0, $bateria]);
			DB::update("UPDATE sensores_desarrollo SET distancia = ?, fecha = ? WHERE id_sensor = ?", [$distancia, $fecha, $sensor->id_sensor]);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}*/

	// ventumapilocal.es/api/wifi/silos/FIREWIFI01/50
	public function registrarUltrasonidos($device, $data, $bateria = VALOR_MAXIMO_3_PILAS, $rssi = null, $intentos = 0)
	{
		try
		{
			$fecha = date('Y-m-d H:i:s');
			$sensor = $this->obtenerInfoSensorSilo($device);

			$bp = app('App\Http\Controllers\SensoresController')->calcularBateriaPilas45($bateria);
			$bateria = $bateria / 1000;
			$snr = null;
			$temperatura = 0; $acelerometroX = 0; $acelerometroY = 0; $acelerometroZ = 0;

			// return $bateria;

			$altura = intval($data) + intval($sensor->offset);
			$altura = $altura > $sensor->altura ? $sensor->ultimaLectura : $altura;

			$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $altura);
			$kilos = $porcentaje * $sensor->capacidad / 100;

			// Ahora se procede a su inserción y a insertar las alertas sobre las entidades
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $fecha, $bateria, $bp, $rssi, $snr, $intentos, $altura, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

			// Si hay callback activo, es la primera vez que se encuentra el sensor o hace mas de X tiempo programado que hizo un callback, lo devolvemos
			if ($sensor->callback || $sensor->fechaCallback == null || $this->diferenciaTiempoCallback($fecha, $sensor->fechaCallback))
			{
				// Indicamos que la siguiente vez no habrá callback
				$this->deshabilitarCallback($sensor->id_sensor, $fecha);
				return $this->todoBienCallback($device, $sensor->tiempo);
			}
			else
				return $this->todoBienV2($sensor->id_sensor, null, $sensor->debug);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}

	// ventumapilocal.es/api/wifi/silos/FIREWIFI01/150/32.45/-110/1
	public function registrarUltrasonidosTemperatura($device, $distancia, $temperatura, $bateria = VALOR_MAXIMO_3_PILAS, $rssi = null, $intentos = 0)
	{
		try
		{
			$fecha = date('Y-m-d H:i:s');
			$sensor = $this->obtenerInfoSensorSilo($device);
			$bp = app('App\Http\Controllers\SensoresController')->calcularBateriaPilas45($bateria);
			$bateria = $bateria / 1000;
			$snr = null;
			$acelerometroX = 0; $acelerometroY = 0; $acelerometroZ = 0;

			$altura = intval($distancia);
			$altura = $altura > $sensor->altura ? $sensor->ultimaLectura : $altura;

			$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $altura);
			$kilos = $porcentaje * $sensor->capacidad / 100;

			// Simulamos el payload, juntando la información de distancia y temperatura
			$data = $distancia . $temperatura;

			// Ahora se procede a su inserción y a insertar las alertas sobre las entidades
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $fecha, $bateria, $bp, $rssi, $snr, $intentos, $altura, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

			// return $this->todoBien($sensor->id_sensor, $device, false);
			return $this->todoBienV2($sensor->id_sensor, null, $sensor->debug);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}

	// ventumapilocal.es/api/d/machos/tst/0005M/30/-100/3.7
	public function registrarMachosTST($device, $snr, $rssi, $bateria, Request $request)
	{
		try
		{
			$fecha = date('Y/m/d H:i:s');
			$sensor = $this->obtenerInfoSensor($device);

			// Si existe el sensor, insertamos la información
			if ($sensor->id_sensor != null && $sensor->id_tipo_sensor == SENSOR_MACHOS)
			{
				$bp = app('App\Http\Controllers\SensoresController')->calcularBateriaPorcentaje3($bateria);
				// Insertar en la tabla de historicos $idSensor, $idEntidad, $fecha, $bateria, $bp, $snr, $rssi
				app('App\Http\Controllers\SensoresMachosController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $fecha, $bateria, $bp, $snr, $rssi);
				// Actualiza la tabla de entidades machos
				app('App\Http\Controllers\EntidadesMachosController')->actualizar($sensor->id_entidad, $fecha);

				$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

				return $this->todoBien($device, $fecha);
			}
			else
			{
				return response()->json(['mensaje' => 'El sensor $device no se encuentra o no es un sensor de machos'], 404);
			}
		}
		catch (Exception $e)
		{
			return $this->devolverErrorWifi($device, $e->getMessage());
		}
	}

	// ventumapilocal.es/api/wifi/temperatura/FIREWIFI03/20/58
	public function registrarTemperaturaVentum($device, $temperatura, $humedad, $rssi = null, $intentos = 1)
	{
		try
		{
			$fecha = date('Y-m-d H:i:s');
			$sensor = $this->obtenerInfoSensorSilo($device);
			$bateria = 3.7; $bp = 100; $snr = null;
			$temperatura = floatval($temperatura);
			$humedad = floatval($humedad);
			$luminosidad = 0;
			$etermico = app('App\Http\Controllers\SensoresTempHumController')->calcularEstressTermico($temperatura, $humedad);
			$data  = $temperatura . $humedad;

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $bateria, $bp, $rssi, $snr, $fecha, $temperatura, $humedad, $luminosidad, $etermico);
			app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico);

			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

			// Si hay callback activo, es la primera vez que se encuentra el sensor o hace mas de X tiempo programado que hizo un callback, lo devolvemos
			if ($sensor->callback || $sensor->fechaCallback == null || $this->diferenciaTiempoCallback($fecha, $sensor->fechaCallback))
			{
				// Indicamos que la siguiente vez no habrá callback
				$this->deshabilitarCallback($sensor->id_sensor, $fecha);
				return $this->todoBienCallback($device, $sensor->tiempo);
			}
			else
				return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}

	// ventumapilocal.es/api/wifi/co2/ventum/C1D3BF/25/78/450
	public function registrarCO2Ventum($device, $temperatura, $humedad, $co2, $rssi = null, $intentos = 1, $calibrar = false)
	{
		try
		{
			$fecha = date('Y-m-d H:i:s');
			$sensor = $this->obtenerInfoSensorSilo($device);
			$bateria = 3.7; $bp = 100; $snr = null;
			$temperatura = floatval($temperatura);
			$humedad = floatval($humedad);
			$co2 = floatval($co2);
			$etermico = app('App\Http\Controllers\SensoresTempHumController')->calcularEstressTermico($temperatura, $humedad);
			$data  = $temperatura . $humedad;

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresCO2Controller')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $bateria, $bp, $rssi, $snr, $fecha, $temperatura, $humedad, $etermico, $co2);

			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}

	public function registrarNanolike(Request $request)
	{
		try
		{
			$codigo = $request['deviceId'];
			$fecha = date('Y-m-d H:m:s', intval($request['timestamp']) / 1000);
			$porcentaje = intval($request['level_percent']);
			$kilos = intval($request['level_kg']);

			$idEntidad = DB::select('SELECT id FROM entidades WHERE codigo = ?', [$codigo])[0]->id;

			// ($idSensor, $idEntidad, $data, $fecha, $bateria, $bp, $rssi, $snr, $altura, $porcentaje, $kilos, $temperatura, $aX, $aY, $aZ)
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura(null, $idEntidad, null, $fecha, 3.7, 100, 0, 0, 1, 0, $porcentaje, $kilos, 0, 0, 0, 0);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($idEntidad, ENTIDAD_SILO, $fecha, $porcentaje, $kilos, 0, 0, 0, 0);

			return response()->json(['status' => true, 'message' => 'OK'], 200);
		}
		catch (Exception $e)
		{
			return $this->devolverError($codigo, $e->getMessage());
		}
	}

	// ventumapilocal.es/api/wifi/caudalimetro/DISPOSITIVO/10000/10
	public function registrarCaudalimetroVentum($device, $mililitros = 0, $caudal = 0, $rssi = null, $intentos = 1)
	{
		try
		{
			$fecha = date('Y-m-d H:i:s');
			$sensor = $this->obtenerInfoSensor($device);
			$bateria = 3.7; $bp = 100; $snr = null;
			$litros = $mililitros / 1000;

			// Ahora se procede a su inserción y a insertar las alertas sobre las entidades
			app('App\Http\Controllers\SensoresCaudalimetroController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, null, $fecha, $bateria, $bp, $rssi, $snr, $mililitros, $litros, $caudal);

			// app('App\Http\Controllers\SensoresCaudalimetroController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}
}
