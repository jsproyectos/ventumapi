<?php

namespace App\Http\Controllers;

use Storage;
use Auth;
use Illuminate\Http\Request;
use Mail;

define("TITULO", "[Ventum Alertas] ");
define("TIEMPO_AVISOS_BATERIA_BAJA", 24); // Tiempo en horas entre avisos de batería baja (para no saturar de mensajes de batería baja)
define("TIEMPO_CORTESIA", 5); // Tiempo en minutos de cortesía para el tiempo de avisos de batería baja

define("LOG_ERRORES", "errores.log");
define("LOG_ERRORES_FIWARE", "errores_Fiware.log");
define("LOG_MENSAJES", "registro.log");
define('LOG_MENSAJES_LORA', 'registro_lora.log');

class LogController extends Controller
{
	public static function errores($mensaje)
	{
		$fechaTexto = date('Y/m/d H:i:s');
		$usuario = Auth::user() ? Auth::user()["id"] . " : " .  Auth::user()["email"] : "[SIN USUARIO]";

		//Storage::append(LOG_ERRORES, "IP: " . LogController::obtenerIP());
		Storage::append(LOG_ERRORES, $fechaTexto . " : " . $usuario . " : " . $mensaje);
	}

	public static function erroresFiware($mensaje)
	{
		$fechaTexto = date('d/m/Y H:i:s');

		//Storage::append(LOG_ERRORES, "IP: " . LogController::obtenerIP());
		Storage::append(LOG_ERRORES_FIWARE, $fechaTexto . " : " . $mensaje);
	}

	public static function mensaje($mensaje)
	{
		$fechaTexto = date('Y/m/d H:i:s');
		Storage::append(LOG_MENSAJES, $fechaTexto . " : " . $mensaje);	
	}

	public static function mensajeLora($mensaje)
	{
		$fechaTexto = date('Y/m/d H:i:s');
		Storage::append(LOG_MENSAJES_LORA, $fechaTexto . " : " . $mensaje);	
	}

	public static function mensajeFichero($mensaje, $fichero)
	{
		$fechaTexto = date('Y/m/d H:i:s');
		Storage::append($fichero, $fechaTexto . " : " . $mensaje);	
	}

	private static function obtenerIP()
	{
		if(!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			//ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			//ip pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	public static function enviarEmail($email, $vista, $datos, $asunto)
	{
		try
		{
			Mail::send($vista, $datos, function ($message) use($email, $asunto)
			{
				$message->to($email)->subject(TITULO . $asunto);
			});
			LogController::mensaje("Email enviado a $email");
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}

	public static function GET_LOG_MENSAJES() { return LOG_MENSAJES; }
	public static function GET_LOG_ERRORES() { return LOG_ERRORES; }
	public static function GET_LOG_ERRORES_FIWARE() { return LOG_ERRORES_FIWARE; }

	public static function borrar_log_mensajes(){ try { Storage::delete(LOG_MENSAJES); } catch (Exception $e) { LogController::errores($e->getMessage()); }}
	public static function borrar_log_errores(){ try { Storage::delete(LOG_ERRORES); } catch (Exception $e) { LogController::errores($e->getMessage()); }}
	public static function borrar_log_errores_fiware(){ try { Storage::delete(LOG_ERRORES_FIWARE); } catch (Exception $e) { LogController::errores($e->getMessage()); }}
}