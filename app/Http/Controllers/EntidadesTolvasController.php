<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

define ('TRAMO_1_TOLVA', 125);
define ('TRAMO_2_TOLVA', 26);
define ('TRAMO_3_TOLVA', 11);
define ('TAM_TOLVA', TRAMO_1_TOLVA + TRAMO_2_TOLVA + TRAMO_3_TOLVA);

class EntidadesTolvasController extends EntidadesRecipientesController
{
	// Estamos teniendo en cuenta únicamente el porcentaje de las TOLVAS de Valdesequera
	public function CalcularPorcentajeTolvaValdesequera($distancia)
	{
		try
		{
			$volumenTotal = 100 * TAM_TOLVA * 200;

			// Factor corrector de la distancia, para no tener volumenes negativos
			$distancia = $distancia > TAM_TOLVA ? TAM_TOLVA : $distancia;
			$volumenocupado = $volumenTotal - (100 * 200 * $distancia);

			$porcentaje = round(($volumenocupado / $volumenTotal) * 100);
			$porcentaje = max(min($porcentaje, 100), 0);

			return $porcentaje;
		}
		catch (Exception $e)
		{
			LogController::errores('[ENTIDADES RECIPIENTES CALCULAR PORCENTAJE TOLVA] ' . $e->getMessage());
			return 100;
		}
	}

	public function obtenerResumen(Request $request)
	{
		try
		{
			// Medida de seguridad
			$orden = $request['orden'] ? $request['orden'] : null;
			$nombre = $request['nombre'] ? $request['nombre'] : null;
			$explotacion = $request['explotacion'] ? $request['explotacion'] : null;
			$falsas = $request['falsas'] ? true : false;
			$bajas = $request['bajas'] ? true : false;

			$entidades = $this->obtenerResumenRecipientes(ENTIDAD_TOLVA, $orden, $nombre, $explotacion, $falsas, $bajas);

			return response()->json(['estado' => true, 'datos' => $entidades], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 200);
		}
	}
}