<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class MapaController extends Controller
{
	public function obtenerMenu(Request $request)
	{
		try
		{
			if (esAdmin(Auth::user()["role"]))
			{
				// $explotaciones = DB::select('SELECT id, nombre, codigo, fichero FROM explotaciones');
				// $tipos = DB::select('SELECT DISTINCT entidades_tipos.id, entidades_tipos.nombre FROM entidades_tipos JOIN entidades ON entidades_tipos.id = entidades.id_tipo WHERE entidades_tipos.id <> 14'); // El 14 es para evitar las celdas*/

				$explotaciones = DB::select('SELECT t.id, t.nombre, t.codigo, fichero, id_tipo, entidades_tipos.nombre tipo FROM (SELECT explotaciones.id, explotaciones.nombre, explotaciones.codigo, fichero, id_tipo FROM explotaciones JOIN entidades ON explotaciones.id = entidades.id_explotacion GROUP BY explotaciones.id, explotaciones.nombre, explotaciones.codigo, fichero, id_tipo) t JOIN entidades_tipos ON t.id_tipo = entidades_tipos.id WHERE entidades_tipos.id <> 14 ORDER BY t.id, t.id_tipo');
			}
			else
			{
				// $explotaciones = DB::select('SELECT id, nombre, codigo, fichero FROM explotaciones WHERE id IN (SELECT id_explotacion FROM entidades WHERE id IN (SELECT id_entidad FROM entidades_usuarios WHERE id_usuario = ?))', [Auth::user()['id']]);
				// $tipos = DB::select('SELECT id, nombre FROM entidades_tipos WHERE id IN (SELECT id_tipo FROM entidades WHERE id IN (SELECT id_entidad FROM entidades_usuarios WHERE id_usuario = ?))', [Auth::user()['id']]);
				$explotaciones = DB::select('SELECT t.id, t.nombre, t.codigo, fichero, id_tipo, entidades_tipos.nombre tipo FROM (SELECT explotaciones.id, explotaciones.nombre, explotaciones.codigo, fichero, id_tipo FROM explotaciones JOIN entidades ON explotaciones.id = entidades.id_explotacion JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE id_usuario = ? GROUP BY explotaciones.id, explotaciones.nombre, explotaciones.codigo, fichero, id_tipo) t JOIN entidades_tipos ON t.id_tipo = entidades_tipos.id WHERE entidades_tipos.id <> 14 ORDER BY t.id, t.id_tipo', [Auth::user()["id"]]);
			}

			// Procedemos a agrupar las explotaciones
			if (!empty($explotaciones))
			{
				$explotacionesProcesadas = [];
				$explotacionActual = ['id' => $explotaciones[0]->id, 'nombre' => $explotaciones[0]->nombre, 'codigo' => $explotaciones[0]->codigo, 'fichero' => $explotaciones[0]->fichero];
				$tiposExplotacionActual = [];

				foreach ($explotaciones as $explotacion)
				{
					if ($explotacion->id == $explotacionActual['id'])
					{
						$tiposExplotacionActual[] = ['id' => intval($explotacion->id_tipo), 'nombre' => $explotacion->tipo];
					}
					else
					{
						$explotacionActual['tipos'] = $tiposExplotacionActual;
						$explotacionesProcesadas[] = $explotacionActual;

						// Vaciamos almacenes y empezamos una nueva explotación
						$tiposExplotacionActual = [];
						$explotacionActual = ['id' => $explotacion->id, 'nombre' => $explotacion->nombre, 'codigo' => $explotacion->codigo, 'fichero' => $explotacion->fichero];

						$tiposExplotacionActual[] = ['id' => intval($explotacion->id_tipo), 'nombre' => $explotacion->tipo];
					}
				}

				$explotacionActual['tipos'] = $tiposExplotacionActual;
				$explotacionesProcesadas[] = $explotacionActual;
			}

			return response()->json(['estado' => true, 'explotaciones' => $explotacionesProcesadas], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MAPA CONTROLLER OBTENER MENU] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerEntidades(Request $request)
	{
		$data = json_decode($request['data']);

		try
		{
			if (esAdmin(Auth::user()["role"]))
			{
				// Con obtenerInterrgogaciones obtenemos las (?,?,?) que concatenamos y luego el merge es para juntar los dos arrays que se usarán para dichas interrogaciones (tipos de entidad y explotaciones)
				$entidades = DB::select('SELECT id, nombre, codigo, fake, descripcion, latitud, longitud, id_tipo, id_explotacion FROM entidades WHERE fechaBaja IS NULL AND id_tipo IN ' . obtenerInterrogacionesSQL($data->tipos) . ' AND id_explotacion = ?', array_merge($data->tipos, [$data->explotacion]));
			}
			else
			{
				$entidades = DB::select('SELECT id, nombre, codigo, fake, descripcion, latitud, longitud, id_tipo, id_explotacion FROM entidades INNER JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL and id_tipo IN ' . obtenerInterrogacionesSQL($data->tipos) . ' AND id_explotacion = ?', array_merge([Auth::user()["id"]], $data->tipos, [$data->explotacion]));
			}

			foreach ($entidades as $entidad)
			{
				$entidad->id_tipo = intval($entidad->id_tipo);
				$entidad->latitud = floatval($entidad->latitud);
				$entidad->longitud = floatval($entidad->longitud);
			}

			return response()->json(['estado' => true, 'data' => $entidades], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MAPA CONTROLLER OBTENER ENTIDADES] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}
