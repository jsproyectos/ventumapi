<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

define('MINUTOS_CALLBACK', 720);	// Tiempo en minutos para renovar callback. 720 minutos = 12 horas.

class ComunicacionController extends Controller
{
	public function __construct(){}

	// Devuelve un mensaje de error ya formado cuando algo ha ido mal
	protected function devolverError($device, $mensaje)
	{
		LogController::errores('[' . $device . "] $mensaje");
		return json_encode(['estado' => 'ERROR', 'dispositvo' => $device, 'mensaje' => $mensaje]);
	}

	// Obtiene el id del sensor del cual nos llega la información
	protected function obtenerIDSensor($eui)
	{
		$id = DB::select('SELECT id from sensores where dispositivo = ?', [$eui]);

		// Si no está vacío devolvemos el primero que será el correcto
		if (!empty($id))
			return $id[0]->id;
		else
		{
			// Si no hay, se registra y se obtiene el id
			DB::insert('INSERT INTO sensores (dispositivo, id_emision) values (?, "1")', [$eui]);
			$idSensor = $this->obtenerIDSensor($eui);
		}
	}

	// Devuelve una entidad nula, para indicar que el sensor no tiene entidad asociada
	protected function sinEntidad()
	{
		$entidad = new \stdClass(); $entidad->id = null; $entidad->id_tipo = null; $entidad->capacidad = 0;
		return $entidad;
	}

	protected function deshabilitarCallback($idSensor, $fecha)
	{
		DB::update('UPDATE sensores SET callback = 0, fechaCallback = ? WHERE id = ?', [$fecha, $idSensor]);
	}

	// 17-06-2021 Ya no es necesaria, se utilizan las otras que ofrecen mas detalles en una sola consulta
	// Obtiene el id de la entidad que estuviera activa al sensor asociado si hubiera
	/*protected function obtenerIdYTipoEntidad($idSensor)
	{
		$entidad = DB::select('SELECT id, id_tipo FROM entidades WHERE id = (SELECT id_entidad FROM entidades_sensores WHERE id_sensor = ? AND fechaBaja IS NULL) AND fechaBaja IS NULL', [$idSensor]);

		return empty($entidad) ? $this->sinEntidad() : $entidad[0];
	}*/

	protected function obtenerInfoSensor($dispositivo)
	{
		$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, bateria, bp, tiempo, debug, callback, fechaCallback, ES.id id_entidad, ES.id_tipo id_tipo_entidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.id_sensor = (SELECT id FROM sensores WHERE dispositivo = ? AND fechaBaja IS NULL) AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE dispositivo = ? AND fechaBaja IS NULL', [$dispositivo, $dispositivo]);

		if (!empty($sensor))
		{
			$sensor[0]->callback = $sensor[0]->callback == 1 ? true : false;
			return $sensor[0];
		}
		return false;
	}

	protected function obtenerInfoSensorSilo($dispositivo)
	{
		$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, altura, ultimaLectura, offset, tiempo, debug, callback, fechaCallback, ES.id id_entidad, ES.id_tipo id_tipo_entidad, capacidad FROM sensores LEFT JOIN sensores_silos ON sensores.id = sensores_silos.id_sensor LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre, capacidad FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad WHERE entidades_sensores.id_sensor = (SELECT id FROM sensores WHERE dispositivo = ? AND fechaBaja IS NULL) AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE dispositivo = ? AND fechaBaja IS NULL', [$dispositivo, $dispositivo]);

		if (!empty($sensor))
		{
			$sensor[0]->callback = $sensor[0]->callback == 1 ? true : false;
			return $sensor[0];
		}
		return false;
	}

	protected function obtenerBateria($idSensor)
	{
		$bateria = DB::select('SELECT bateria, bp FROM sensores WHERE id = ?', [$idSensor]);

		if (!empty($bateria))
			return $bateria[0];
		return ['bateria' => 3,7, 'bp' => 100]; // Si está vacío, devolvemos batería llena
	}

	protected function diferenciaTiempoCallback($fechaActual, $fechaCallback)
	{
			$diferencia = date_diff(date_create($fechaActual), date_create($fechaCallback));

			return ($diferencia->days * 24 * 90 + $diferencia->h * 60 + $diferencia->i) > MINUTOS_CALLBACK;
	}

	public function actualizarInformacionSensor($idSensor, $bateria, $bateriaPorcentaje, $snr = 0, $rssi= 0, $latitud = null, $longitud = null)
	{
		$update = 'UPDATE sensores SET snr = ?, rssi = ?';
		$parametros = [$snr, $rssi];

		if ($bateria != null)
		{
			$update .= ', bateria = ?';
			$parametros[] = $bateria;
		}

		if ($bateriaPorcentaje != null)
		{
			$update .= ', bp = ?';
			$parametros[] = $bateriaPorcentaje;
		}

		if ($latitud != null)
		{
			$update .= ', latitud = ?';
			$parametros[] = $latitud;
		}

		if ($longitud != null)
		{
			$update .= ', longitud = ?';
			$parametros[] = $longitud;
		}

		$update .= ' WHERE id = ?';
		$parametros[] = $idSensor;

		if (DB::update($update, $parametros) > 0)
			return true;
		else
			return false;
	}

	/* Función que convierte una cadena hexadecimal a binaria. Usada para decodificar tramas */
	public function binarizarCadena($cadena)
	{
		$tamCadena = strlen($cadena); $binario = "";

		for ($i = 0; $i < $tamCadena; $i+=2)
			$binario .= str_pad(base_convert($cadena[$i] . $cadena[$i + 1], 16, 2), 8, '0', STR_PAD_LEFT);

		return $binario;
	}

	public function pruebas()
	{
		return 'No tienes permiso';
	}
}


/*UPDATE sensores SET finContrato = '2020-07-22 08:37:52' WHERE id IN (1, 2, 3);
UPDATE sensores SET finContrato = '2020-07-23 08:37:52' WHERE id IN (4, 5, 6, 7, 8);
UPDATE sensores SET finContrato = '2020-07-24 08:37:52' WHERE id IN (9);
UPDATE sensores SET finContrato = '2020-07-25 08:37:52' WHERE id IN (10, 11, 12);
UPDATE sensores SET finContrato = '2020-07-26 08:37:52' WHERE id IN (13, 14, 15);
UPDATE sensores SET finContrato = '2020-07-27 08:37:52' WHERE id IN (16, 17);
UPDATE sensores SET finContrato = '2020-07-28 08:37:52' WHERE id IN (18, 19, 20, 21, 22, 23, 24);
UPDATE sensores SET finContrato = '2020-07-29 08:37:52' WHERE id IN (25, 26, 27, 28);
UPDATE sensores SET finContrato = '2020-07-30 08:37:52' WHERE id IN (29);
UPDATE sensores SET finContrato = '2020-07-31 08:37:52' WHERE id IN (30, 31, 32);
UPDATE sensores SET finContrato = '2020-08-01 08:37:52' WHERE id IN (33, 34, 35, 36, 37);*/