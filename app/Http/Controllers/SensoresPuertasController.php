<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SensoresPuertasController extends SensoresController
{
	// Crea una nueva entrada en la tabla sensores_temperatura_humedad
	public function inicializarResumen($idSensor)
	{
		if (DB::insert("INSERT INTO sensores_puertas (id_sensor) VALUES (?)", [$idSensor]) == 1)
			return true;
		return false;
	}

	private function obtenerEstadoPuerta($id_sensor)
	{
		return DB::select("SELECT estado, fecha FROM sensores_puertas WHERE id_sensor = ?", [$id_sensor]);
	}

	public function obtenerInformacionParaGraficos()
	{
		return null;
	}

	private function calcularBateriaPorcentaje($bateria)
	{
		// Las baterías de litio funcionan con una tensión mínima de 2.5V y un máximo de 3.6 o 3.7 (en las recargables incluso 4)
		return min(Round(($bateria * 100) / 3.7), 100);
	}

	public function insertarLectura($idSensor, $idEntidad, $fecha, $tipo, $bateria, $bp, $snr, $rssi)
	{
		try
		{
			// Insertamos en el histórico
			DB::insert("INSERT INTO lecturas_puertas (id_sensor, id_entidad, fecha, tipo, bateria, bp, snr, rssi) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [$idSensor, $idEntidad, $fecha, $tipo, $bateria, $bp, $snr, $rssi]);

			// Actualizamos el resumen
			DB::update("UPDATE sensores_puertas SET estado = ?, fecha = ? WHERE id_sensor = ?", [$tipo, $fecha, $idSensor]);

			// Creamos la cola para alertas del sensor
			$jobSensor = new \App\Jobs\alertasSensoresPuertas($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $tipo);
			dispatch($jobSensor);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}

	// Obtenemos los sensores y la entidad a la que está asociada
	public function obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		$consulta = 'SELECT sensores.id, dispositivo, id_tipo, seguimiento, sensores.nombre, e.nombre nombreEntidad, bateria, bp, snr, id_emision, sensores_tipos_emision.nombre comunicacion, estado, sensores.fechaAlta, sensores.fechaBaja, fecha, tiempo, autorizado, notificaciones FROM sensores JOIN sensores_puertas ON sensores.id = sensores_puertas.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_puertas.id_sensor = e.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_tipo = ' . SENSOR_PUERTA . ' AND id_usuario = ?';

		$sensores = $this->completarConsultaYEjecutar($consulta, Auth::user()["id"], $nombre, $dispositivo, $comunicacion, $fake, $bajas);

		foreach ($sensores as $sensor)
		{
			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			unset ($sensor->autorizado);
			$sensor->bateria = floatval($sensor->bateria);
		}

		return $sensores;
	}


	public function obtenerDetallesSensor($id)
	{
		$sensor = DB::select('SELECT id, sensores.nombre nombre, sensores_puertas.fecha, tiempo, sensores.fechaAlta, sensores.fechaBaja, id_tipo, dispositivo, descripcion, latitud, longitud, bateria, bp, snr, rssi, id_emision, estado, autorizado, e.nombre nombreEntidad FROM sensores JOIN sensores_puertas ON sensores.id = sensores_puertas.id_sensor JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_puertas.id_sensor = e.id_sensor WHERE sensores.id = ? AND id_usuario = ?', [$id, Auth::user()["id"]])[0];

		$sensor->propio = $sensor->autorizado == 0 ? true : false;
		unset ($sensor->autorizado);
		$sensor->latitud = floatval($sensor->latitud);
		$sensor->longitud = floatval($sensor->longitud);
		$sensor->bateria = floatval($sensor->bateria);
		$sensor->snr = floatval($sensor->snr);
		$sensor->rssi = floatval($sensor->rssi);
		$sensor->estado = intval($sensor->estado);

		return $sensor;
	}

	// Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico($idSensor, $fechaInicio, $fechaFin)
	{
		try
		{
			if ($this->puedeVer($idSensor))
			{
				$historico = DB::select('SELECT fecha, tipo, bateria, bp, snr, rssi FROM lecturas_puertas WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC', [$idSensor, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			// Ahora hacemos el tratamiento de al tapa
			foreach ($historico as $lectura)
			{
				$lectura->bateria = floatval($lectura->bateria);
				$lectura->rssi = floatval($lectura->rssi);
				$lectura->snr = floatval($lectura->snr);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function insertarValorFalso($id, Request $request)
	{
		try
		{
			if (!esAdmin(Auth::user()['role']))
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 403);

			$fecha = date('Y-m-d H:i:s');

			// Obtenemos datos del sensor, SIMILAR  a la función obtenerInfoSensorSilo de ComunicacionController
			$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, ES.id id_entidad, ES.id_tipo id_tipo_entidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.id_sensor = ? AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE sensores.id = ? AND fechaBaja IS NULL', [$id, $id])[0];

			$bp = $this->calcularBateriaPorcentaje($request['bateria']);

			$this->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $fecha, $request['accion'], $request['bateria'], $bp, $request['snr'], $request['rssi']);
			app('App\Http\Controllers\EntidadesPuertasController')->actualizar($sensor->id_entidad, $fecha, $request['accion']);

			// Actualizar la batería y la señal
			app('App\Http\Controllers\ComunicacionController')->actualizarInformacionSensor($id, $request['bateria'], $bp, $request['snr'], $request['rssi']);

			return response()->json(['estado' => true, 'mensaje' => 'Se ha insertado correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA FALSA SILOS] ' . $e->getMessage());
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}
