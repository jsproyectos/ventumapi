<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComunicacionGPRSController extends ComunicacionController
{
	// Devuelve un mensaje indicando que la operación de inserción ha resultado correcta
	private function todoBien($device)
	{
		return json_encode(['device' => $device, 'status' => 'OK']);
	}

	// ventumapilocal.es/api/gprs/llenado/tst/352353081971498/1594366703/150/f1000140769859e51dbf00d60007000005b40000127d000025070400c05dae331a
	// ventumapilocal.es/api/gprs/llenado/tst/352353082513711/1594366703/150/f1000140769851992fc300d600010000d9190000e6890100c610effc3f5e18191d
	// Problemático:
	// ventumapilocal.es/api/gprs/llenado/tst/352353083077062/1594366703/150/f100014076985a31c6bb00d600070000025c0000089c010393030200c15e277d2a
	//public function registrarLlenadoTST3($device, $time, $snr, $data
	// Entidad 13 (Silo 2 Agustín)
	// ventumapilocal.es/api/gprs/llenado/tst/352353083077062/1594366703/150/f1000140769859e51dbf00d60007000005b40000127d000025070400c05dae331a
	// ventumapilocal.es/api/gprs/llenado/tst/352353083077062/1594366703/-69/f100014076985a31c6bb00d600070000025c0000089c01ffff200200c15ec11c4d

	public function registrarLlenadoTST($device, $time, $snr, $data)
	{
		try
		{
			$sensor = $this->obtenerInfoSensorSilo($device);

			// Esto se hace puesto que algunas veces los sensores GPRS dan una fecha errónea. Entones uso la del servidor
			$fecha = $time < 1500000000 ? date('Y/m/d H:i:s') : date('Y/m/d H:i:s', $time);

			// Nivel de batería: 0 (muy bueno) hasta 3 (muy malo)
			$payload = null;
			$bateria = hexdec(substr($data, 44, 2));
			$rssi = bindec8(decbin(hexdec(substr($data, 18, 2))));
			$bp = app('App\Http\Controllers\SensoresNivelLlenadoController')->transformarBateria($bateria);
			$altura = bindecComplemento(decbin(hexdec(substr($data, 46, 4))), 16);
			$temperatura = bindec8(decbin(hexdec(substr($data, 50, 2))));
			$acelerometroX = bindec8(decbin(hexdec(substr($data, 52, 2))));
			$acelerometroY = bindec8(decbin(hexdec(substr($data, 54, 2))));
			$acelerometroZ = bindec8(decbin(hexdec(substr($data, 56, 2))));

			// Nos aseguramos que los valores son correctos
			if ($altura == -1)
			{
				$altura = $sensor->ultimaLectura; $payload = 'error detect';
			}
			else if ($altura == -2)
			{
				$altura = $sensor->ultimaLectura; $payload = 'error medida';
			}

			$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $altura);
			$kilos = $porcentaje * $sensor->capacidad / 100;

			// Ahora se procede a su inserción
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $payload, $fecha, $bateria, $bp, $rssi, 0, 1, $altura, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);
			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr);

			return $this->todoBien($device);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}

	public function _registrarLlenadoTST()
	{
		$silos = [
			"1588981278#2020-05-09 01:41:18#352353083057437#f1000140769859e51dbd00d60007000005b40000127d0100ba0903ffc15eb5ee1e",
		];

		// Una request vacía
		$request = new Request();

		foreach ($silos as $silo)
		{
			$campos = explode("#", $silo);
			$device = $campos[2];
			$time = $campos[0];
			$data = $campos[3];
			$snr = bindec8(decbin(hexdec(substr($data, 18, 2))));

			//$time = DateTime::createFromFormat('d/m/Y H:i', $fecha)->getTimestamp();
			//$this->registrarLlenadoTST($device, $time, $data, false, $request);
			$this->registrarLlenadoTST($device, $time, $snr, $data, $request);
		}

		return "fin";
	}

	// ventumapilocal.es/api/gprs/temperatura/tst/1595490073/b'|3|352353082516201|1598422953|2836|1598426553|2842|1598430153|2890|1598433753|2965|1598437353|3026|1598440953|2466|1598444553|2466|1598448153|2924|1598451753|2646|1598455353|2510|1598458953|2418|1598462553|2232|'
	public function registrarTemperaturaTSTVarilla($time, $data)
	{
		try
		{
			$data = explode('|', $data);

			// Eliminamos el primero y el último, y nos quedamos con el tipo de emisión y el IMEI del dispositivo
			array_shift($data); 					// Eliminamos el primero, que corresponde con el símbolo de cadena binaria de python
			$configuracion = array_shift($data); 	// Solo nos interesan los que tengan de configuración 3

			if ($configuracion != '3')
			{
				if ($configuracion == '2')
					return $this->devolverError($data[1], 'La trama no es válida');
				return $this->devolverError('[DISPOSITIVO DESCONOCIDO]', 'La trama no es válida');
			}

			$device = array_shift($data);			// Debe corresponder con el imei del dispositivo
			array_pop($data);						// Eliminamos el último elemento, que corresponde con el fin de cadena binaria de python

			// Obtenemos la información del sensor
			$sensor = $this->obtenerInfoSensor($device);

			if ($sensor == false)
				return $this->devolverError($device, 'El dispositivo no existe');

			// Recorremos para insertar las temperaturas
			$tam = count($data) / 2;

			for ($i = 0; $i < $tam; $i++)
			{
				$fecha = date('Y/m/d H:i:s', $data[$i * 2]);
				$temperatura = $data[($i * 2) + 1] / 100;

				// Insertar en la tabla de historicos
				app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, '', $sensor->bateria, $sensor->bp, null, null, $fecha, $temperatura, 0, 0, 0);
				app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $temperatura, 0, 0, 0);
			}

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $sensor->bateria, $sensor->bp, 0, 0);

			return $this->todoBien($device);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, $e->getMessage());
		}
	}

	// ventumapilocal.es/api/gprs/tracker/ventum/GPSDEV1/63.5752/8.6589/5
	public function registrarGpsVentum($device, $latitud, $longitud, $satelites = 0, $bateria = 3700, $bp = 100)
	{
		try
		{
			$sensor = $this->obtenerInfoSensor($device);

			if ($sensor)
			{
				$fecha = date('Y/m/d H:i:s');
				$snr = null; $rssi = null;// ; $bp = 100; $bateria = 3.7;
				$bateria = $bateria / 1000;

				app('App\Http\Controllers\SensoresGpsController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $bateria, $bp, $rssi, $snr, $fecha, $latitud, $longitud, 0, 0, 0, 0, 0, $satelites, 0);

				// Si tiene entidad, la actualizamos
				if ($sensor->id_entidad)
					DB::update('UPDATE entidades SET latitud = ?, longitud = ? WHERE id = ?', [ $latitud,  $longitud, $sensor->id_entidad]);

				return $this->todoBien($sensor->id_sensor, $device, false);
			}
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[GPRS GPS VENTUM] ' . $e->getMessage());
		}
	}
}
