<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use Validator;
use App\Sensor;
use App\SensorSilo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SensoresNivelLlenadoController extends SensoresController
{
	public function inicializarResumen($idSensor)
	{
		if (DB::insert("INSERT INTO sensores_silos (id_sensor) VALUES (?)", [$idSensor]) == 1)
			return false;
		return false;
	}

	private function sinEntidad()
	{
		$entidad = new \stdClass(); $entidad->id = null; $entidad->id_tipo = null; $entidad->capacidad = 0;
		return $entidad;
	}

	// Obtiene el id de la entidad que estuviera activa al sensor asociado si hubiera
	public function obtenerIdYTipoEntidad($idSensor)
	{
		$entidad = DB::select('SELECT id, id_tipo, capacidad FROM entidades JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad WHERE id = (SELECT id_entidad FROM entidades_sensores WHERE id_sensor = ? AND fechaBaja IS NULL) AND fechaBaja IS NULL', [$idSensor]);

		return empty($entidad) ? $this->sinEntidad() : $entidad[0];
	}

	public function decodificarTramaLoraVentum($data)
	{
		$trama = new \stdClass();

		$trama->distancia = $data[0] * 256 + $data[1];
		$trama->bateria = ($data[2] * 256 + $data[3]) / 1000;
		$trama->bp = $this->calcularBateriaPilas45($trama->bateria);
		$trama->temperatura = 0;
		$trama->acelerometroX = 0;
		$trama->acelerometroY = 0;
		$trama->acelerometroZ = 0;

		return $trama;
	}

	public function insertarLectura($idSensor, $idEntidad, $data, $fecha, $bateria, $bp, $rssi, $snr, $intentos = 1 , $altura, $porcentaje, $kilos, $temperatura, $aX, $aY, $aZ)
	{
		try
		{
			// Insertar en histórico
			DB::insert('INSERT INTO lecturas_silos (id_sensor, id_entidad, payload, fecha, bateria, bp, rssi, snr, intentos, altura, porcentaje, kilos, temperatura, acelerometroX, acelerometroY, acelerometroZ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$idSensor, $idEntidad, $data, $fecha, $bateria, $bp, $rssi, $snr, $intentos, $altura, $porcentaje, $kilos, $temperatura, $aX, $aY, $aZ]);

			// Insertar en resumen
			if ($idSensor != null)
				DB::update('UPDATE sensores_silos SET ultimaLectura = ?, temperatura = ?, fecha = ? WHERE id_sensor = ?', [$altura, $temperatura, $fecha, $idSensor]);

			// Ahora se lanza la cola para las alertas del sensor de llenado de silos
			$job = new \App\Jobs\alertasSensoresLlenado($idSensor, $fecha, $bateria, $rssi, $snr, $altura, $temperatura, $aX, $aY, $aZ);
			dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			//return false;
			dd($e->getMessage());
		}
	}

	public function transformarBateria($bateria)
	{
		switch ($bateria)
		{
			case 0: return 100; break;
			case 1: return 75; break;
			case 2: return 40; break;
			default: return 5; 	break; //3 u otro
		}
	}

	// Obtenemos los sensores y la entidad a la que está asociada
	public function obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		$consulta = 'SELECT sensores.id, id_tipo, fake, seguimiento, dispositivo, sensores.nombre, e.nombre nombreEntidad, bateria, bp, enchufe, snr, id_emision, sensores_tipos_emision.nombre comunicacion, tiempo, sensores.fechaAlta, sensores.fechaBaja, fecha, ultimaLectura, temperatura, autorizado, notificaciones FROM sensores JOIN sensores_silos ON sensores.id = sensores_silos.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja IS NULL) e ON sensores_silos.id_sensor = e.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_tipo = 1 AND id_usuario = ?';

		$sensores = $this->completarConsultaYEjecutar($consulta, Auth::user()['id'], $nombre, $dispositivo, $comunicacion, $fake, $bajas);

		foreach ($sensores as $sensor)
		{
			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			unset ($sensor->autorizado);
			$sensor->ultimaLectura = intval($sensor->ultimaLectura);
			$sensor->bateria = floatval($sensor->bateria);
			$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
		}

		return $sensores;
	}

	public function obtenerDetallesSensor($id)
	{
		try
		{
			$sensor = DB::select('SELECT sensores.id, id_tipo, sensores.nombre, dispositivo, fake, seguimiento, sensores.descripcion, sensores_silos.fecha, sensores.longitud, sensores.latitud, bateria, bp, enchufe, snr, rssi, altura, ultimaLectura, temperatura, autorizado, notificaciones, id_emision, sensores.id_emision, sensores_tipos_emision.nombre comunicacion, tiempo, sensores.fechaAlta, sensores.fechaBaja, id_fab FROM sensores JOIN sensores_silos ON sensores.id = sensores_silos.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores_silos.id_sensor = sensores_usuarios.id_sensor WHERE sensores.id = ? AND id_usuario = ?', [$id, Auth::user()['id']])[0];

			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			unset ($sensor->autorizado);
			$sensor->fake = $sensor->fake == 1 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			$sensor->latitud = floatval($sensor->latitud);
			$sensor->longitud = floatval($sensor->longitud);
			$sensor->bateria = floatval($sensor->bateria);
			$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
			$sensor->snr = floatval($sensor->snr);

			return $sensor;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	// Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico($idSensor, $fechaInicio, $fechaFin, $factor = false)
	{
		try
		{
			if ($this->puedeVer($idSensor))
			{
				$historico = DB::select('SELECT fecha, payload, altura, IFNULL(temperatura, 0) temperatura, porcentaje, kilos, bateria, bp, rssi, snr, acelerometroX, acelerometroY, acelerometroZ FROM lecturas_silos WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC', [$idSensor, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			// Ahora hacemos el tratamiento de al tapa
			foreach ($historico as $lectura)
			{
				$lectura->tapa = tapaCerrada($lectura->acelerometroX, $lectura->acelerometroY, $lectura->acelerometroZ);
				$lectura->rssi = floatval($lectura->rssi);
				$lectura->snr = floatval($lectura->snr);
				// $lectura->bateria = floatval($lectura->bateria);
			}

			if ($factor)
				$historico = $this->corregirConFactor($historico, $factory);

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	/* FUNCIONES COOPERATIVAS PIENSO */
	private function piensos_obtenerNota($idSensor)
	{
		if (rand(0, 100) < 40)
			return "Este sensor está colocado en un punto de dificil acceso. Dejar para último en repartir";
		else
			return "";
	}

	private function piensos_obtenerIconoSilo($porcentaje)
	{
		if ($porcentaje > 50)
		{
			return "/icons/piensos/verde.png";
		}
		else
		{
			if ($porcentaje > 30)
			{
				return "/icons/piensos/amarillo.png";
			}
			else
			{
				if ($porcentaje > 10)
				{
					return "/icons/piensos/naranja.png";
				}
				else
				{
					return "/icons/piensos/rojo.png";
				}
			}
		}
	}

	private function piensos_obtenerSilosDesdeBD()
	{
		return DB::select("SELECT id, nombre, codigo, fake, descripcion, latitud, longitud, id_explotacion, porcentaje, temperatura, consumo, truncate(porcentaje / consumo, 0) prevision FROM entidades JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad WHERE id_tipo = ?", [ENTIDAD_SILO]);
	}

	public function piensos_obtenerSilos(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR || $tipoUsuario == USUARIO_COOPERATIVA_PIENSO)
		{
			$entidades = $this->piensos_obtenerSilosDesdeBD();
		}

		foreach ($entidades as $entidad)
		{
			$entidad->icono = $this->piensos_obtenerIconoSilo($entidad->porcentaje);
			$entidad->imagen = SILOS_obtenerImagen($entidad->porcentaje);
			$entidad->nota = $this->piensos_obtenerNota($entidad->id);

			//Errores
			if (rand(0, 100) < 2)
			{
				$entidad->estado = "ERROR";
				$entidad->mensaje = "Hay un error con el sensor. Es posible que se haya caído o averiado, por favor, compruebelo físicamente";
			}
			else
			{
				$entidad->estado = "OK";
				$entidad->mensaje = "El sensor funciona correctamente";
			}
		}

		return json_encode(["estado" => "OK", "mensaje" => $entidades]);
	}

	public function piensos_obtenerSilo(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR || $tipoUsuario == USUARIO_COOPERATIVA_PIENSO)
		{
			$idSensor = $request["idSensor"];

			$historico = DB::select("SELECT fecha, round((((SELECT altura FROM sensores_silos WHERE id_sensor = ?) - altura) / (SELECT altura FROM sensores_silos WHERE id_sensor = ?)) * 100) AS porcentaje, temperatura FROM lecturas_silos WHERE id_sensor = ? ORDER BY fecha DESC LIMIT 24;", [$idSensor, $idSensor, $idSensor]); // Cada 8 horas, 24 es la última semana
		}

		$fechas = [];
		$porcentajes = [];
		$temperaturas = [];

		$tam = count($historico);

		for($i = ($tam - 1); $i > 0; $i--)
		{
			$fechas[] = $historico[$i]->fecha;
			$porcentajes[] = $historico[$i]->porcentaje;
			$temperaturas[] = $historico[$i]->temperatura;
		}

		return json_encode(["estado" => "OK", "fechas" => $fechas, "porcentajes" => $porcentajes, "temperaturas" => $temperaturas]);
	}

	public function piensos_estadisticas()
	{
		$sensores = $this->piensos_obtenerSilosDesdeBD();

		$tipoUsuario = Auth::user()["role"];
		$nombrePagina = "estadisticas";

		return view("pienso.estadisticas", ["tipoUsuario" => $tipoUsuario, "nombrePagina" => $nombrePagina, "sensores" => $sensores]);
	}

	public function insertarValorFalso($id, Request $request)
	{
		try
		{
			if (!esAdmin(Auth::user()['role']))
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 403);

			$fecha = date('Y-m-d H:i:s');

			// Obtenemos datos del sensor, SIMILAR  a la función obtenerINfoSensorSilo de ComunicacionController
			$sensor = DB::select('SELECT sensores.id, ES.id id_entidad, ES.id_tipo, capacidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre, capacidad FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad WHERE entidades_sensores.id_sensor = ? AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE sensores.id = ? AND sensores.fechaBaja IS NULL', [$id, $id])[0];

			$bateriaPorcentaje = $this->transformarBateria($request['bateria']);
			$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $request['distancia']);
			$kilos = $porcentaje * $sensor->capacidad / 100;

			$this->insertarLectura($id, $sensor->id_entidad, 'FAKE', $fecha, $request['bateria'], $bateriaPorcentaje, $request['rssi'], $request['snr'], $request['distancia'], $porcentaje, $kilos, $request['temperatura'], $request['ax'], $request['ay'], $request['az']);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo, $fecha, $porcentaje, $kilos, $request['temperatura'], $request['ax'], $request['ay'], $request['az']);

			// Actualizar la batería y la señal
			app('App\Http\Controllers\ComunicacionController')->actualizarInformacionSensor($id, $request['bateria'], $bateriaPorcentaje, $request['snr'], $request['rssi']);

			return response()->json(['estado' => true, 'mensaje' => 'Se ha insertado correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA FALSA SILOS] ' . $e->getMessage());
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function corregirConFactor($historico, $factor)
	{
		if (!empty($historico))
		{
			$distancia = $historico[0]->altura;
			$porcentaje = $historico[0]->porcentaje;
			$kilos = $historico[0]->kilos;
			$tam = count($historico);
			$factor = intval($factor);

			for($i = 1; $i < $tam; $i++)
			{
				if (($historico[$i]->altura + $factor) <= $distancia)
				{
					$distancia = $historico[$i]->altura;
					$porcentaje = $historico[$i]->porcentaje;
					$kilos = $historico[$i]->kilos;
				}
				else
				{
					// Se corrige la distancia y resto de parámetros
					$historico[$i]->altura = $distancia;
					$historico[$i]->porcentaje = $porcentaje;
					$historico[$i]->kilos = $kilos;
				}
			}
		}

		return $historico;
	}
}
