<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use Validator;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class SensoresGasesController extends SensoresController
{
public function calcularEstressTermico($temperatura, $humedad)
	{
		return (0.8 * $temperatura + (($humedad / 100) * $temperatura - 14.3) + 46.4);
	}

	// Calcula el voltaje de carga de las baterías
	/*private function calcularBateria($bateria)
	{
		return Round(($bateria * 8.25) / 4096, 2);
	}*/

	// Obtiene el porcentaje de carga de las baterías en función del voltaje de carga.
	private function calcularBateriaPorcentaje($bateria)
	{
		// Las baterías de litio funcionan con una tensión mínima de 2.5V y un máximo de 3.6 o 3.7 (en las recargables incluso 4)
		// En este caso, nuestros sensores de temperatura sigfox de TST utilizan dos baterías, cuyo voltaje mínimo sería 5 y voltaje máximo 7.2 (3.6 * 2)
		return min(Round(($bateria * 100) / 3.7), 100);
	}

	private function hexToDec($valor)
	{
		switch($valor)
		{
			case "\x00": return '0'; break;
			case "\x01": return '1'; break;
			case "\x02": return '2'; break;
			case "\x03": return '3'; break;
			case "\x04": return '4'; break;
			case "\x05": return '5'; break;
			case "\x06": return '6'; break;
			case "\x07": return '7'; break;
			case "\x08": return '8'; break;
			case "\x09": return '9'; break;
			default: return '';
		}
	}

	private function convertirCadenaHexadecimal($cadena)
	{
		$traducido = '';

		for ($i = 0; $i < strlen($cadena); $i++)
			$traducido .= $this->hexToDec($cadena[$i]);

		return $traducido;
	}

	public function decodificarTramaLibelum($data)
	{
		$data = explode('#', base64_decode($data));

		$trama = new \stdClass();

		$trama->bateria = 3.7;
		$trama->bp = $this->calcularBateriaPorcentaje($trama->bateria);
		$trama->co2 = intval($this->convertirCadenaHexadecimal($data[0]));
		$trama->nh3 = intval($this->convertirCadenaHexadecimal($data[1]));
		$trama->ch4 = intval($this->convertirCadenaHexadecimal($data[2]));
		$trama->temperatura = floatval($this->convertirCadenaHexadecimal($data[3])) / 100 - 40;
		$trama->humedad = intval($this->convertirCadenaHexadecimal($data[4])) / 100;
		$trama->etermico = 0;
		$trama->presion = intval($this->convertirCadenaHexadecimal($data[5]));
		$trama->luz = intval($this->convertirCadenaHexadecimal($data[6]));

		return $trama;
	}

	public function insertarLectura($idSensor, $idEntidad, $payload, $bateria, $bp, $rssi, $snr, $fecha, $co2, $nh3, $ch4, $temp, $hum, $etermico, $presion, $luz, $alerta = true)
	{
		try
		{
			// Insertar en histórico
			// 05-11-2021 Elimino el payload, porque de momento no lo guardo, pero en el futuro estaría bien guardarlo por si acaso
			// DB::insert('INSERT INTO lecturas_gases (id_sensor, id_entidad, payload, rssi, snr, bateria, bp, fecha, co2, nh3, ch4, temperatura, humedad, etermico, presion, luz) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [$idSensor, $idEntidad, $payload, $rssi, $snr, $bateria, $bp, $fecha, $co2, $nh3, $ch4, $temp, $hum, $etermico, $presion, $luz]);

			DB::insert('INSERT INTO lecturas_gases (id_sensor, id_entidad, rssi, snr, bateria, bp, fecha, co2, nh3, ch4, temperatura, humedad, etermico, presion, luz) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [$idSensor, $idEntidad, $rssi, $snr, $bateria, $bp, $fecha, $co2, $nh3, $ch4, $temp, $hum, $etermico, $presion, $luz]);

			DB::update('UPDATE sensores_gases SET co2 = ?, nh3 = ?, ch4 = ?, temperatura = ?, humedad = ?, etermico = ?, presion = ?, luz = ?, fecha = ? WHERE id_sensor = ?', [$co2, $nh3, $ch4, $temp, $hum, $etermico, $presion, $luz, $fecha, $idSensor]);

			/*if ($alerta)
			{
				// Si se indica alerta por parámetro, la creamos en la cola para alertas del sensor. Si no está activada, es por inserciones en masa.
				$job = new \App\Jobs\alertasSensoresTemperatura($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $temp, $hum, $luz, $etermico);
				dispatch($job);
			}*/

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}

	// Obtenemos los sensores y la entidad a la que está asociada
	public function obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		$consulta = 'SELECT sensores.id, dispositivo, fake, seguimiento, id_tipo, sensores.nombre, e.nombre nombreEntidad, bateria, bp, enchufe, snr, co2, nh3, ch4, temperatura, humedad, etermico, presion, luz, id_emision, sensores_tipos_emision.nombre comunicacion, sensores.fechaAlta, sensores.fechaBaja, fecha, tiempo, autorizado, notificaciones FROM sensores JOIN sensores_gases ON sensores.id = sensores_gases.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_gases.id_sensor = e.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_usuario = ?';

		$sensores = $this->completarConsultaYEjecutar($consulta, Auth::user()['id'], $nombre, $dispositivo, $comunicacion, $fake, $bajas);

		foreach ($sensores as $sensor)
		{
			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			unset ($sensor->autorizado);
			$sensor->fake = $sensor->fake == 1 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			$sensor->co2 = intval($sensor->co2);
			$sensor->nh3 = intval($sensor->nh3);
			$sensor->ch4 = intval($sensor->ch4);
			$sensor->temperatura = floatval($sensor->temperatura);
			$sensor->humedad = floatval($sensor->humedad);
			$sensor->presion = intval($sensor->presion);
			$sensor->luz = intval($sensor->luz);
			$sensor->bateria = floatval($sensor->bateria);
			$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
			$sensor->snr = floatval($sensor->snr);
		}

		return $sensores;
	}

	public function obtenerDetallesSensor($id)
	{
		$sensor = DB::select('SELECT id, sensores.nombre nombre, sensores_gases.fecha, tiempo, sensores.fechaAlta, sensores.fechaBaja, id_tipo, dispositivo, descripcion, latitud, longitud, bateria, bp, enchufe, snr, rssi, id_emision, co2, nh3, ch4, temperatura, humedad, presion, luz, autorizado, e.nombre nombreEntidad FROM sensores JOIN sensores_gases ON sensores.id = sensores_gases.id_sensor JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_gases.id_sensor = e.id_sensor WHERE sensores.id = ? AND id_usuario = ?', [$id, Auth::user()['id']])[0];

		$sensor->propio = $sensor->autorizado == 0 ? true : false;
		unset ($sensor->autorizado);
		$sensor->latitud = floatval($sensor->latitud);
		$sensor->longitud = floatval($sensor->longitud);
		$sensor->bateria = floatval($sensor->bateria);
		$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
		$sensor->snr = floatval($sensor->snr);
		$sensor->rssi = floatval($sensor->rssi);
		$sensor->co2 = intval($sensor->co2);
		$sensor->nh3 = intval($sensor->nh3);
		$sensor->ch4 = intval($sensor->ch4);
		$sensor->temperatura = floatval($sensor->temperatura);
		$sensor->humedad = floatval($sensor->humedad);
		$sensor->presion = intval($sensor->presion);
		$sensor->luz = intval($sensor->luz);

		return $sensor;
	}

	// Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico($idSensor, $fechaInicio, $fechaFin)
	{
		try
		{
			if ($this->puedeVer($idSensor))
			{
				$historico = DB::select('SELECT fecha, co2, nh3, ch4, temperatura, humedad, etermico, presion, luz, bateria, bp, snr, rssi FROM lecturas_gases WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC', [$idSensor, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			// Ahora hacemos el tratamiento de al tapa
			foreach ($historico as $lectura)
			{
				$lectura->co2 = intval($lectura->co2);
				$lectura->nh3 = intval($lectura->nh3);
				$lectura->ch4 = intval($lectura->ch4);
				$lectura->temperatura = floatval($lectura->temperatura);
				$lectura->humedad = floatval($lectura->humedad);
				$lectura->etermico = floatval($lectura->etermico);
				$lectura->presion = intval($lectura->presion);
				$lectura->luz = intval($lectura->luz);
				$lectura->bateria = floatval($lectura->bateria);
				$lectura->rssi = floatval($lectura->rssi);
				$lectura->snr = floatval($lectura->snr);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function insertarValorFalso($id, Request $request)
	{
		/*try
		{
			if (!esAdmin(Auth::user()['role']))
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 403);

			$fecha = date('Y-m-d H:i:s');

			// Obtenemos datos del sensor, SIMILAR  a la función obtenerInfoSensorSilo de ComunicacionController
			$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, ES.id id_entidad, ES.id_tipo id_tipo_entidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.id_sensor = ? AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE sensores.id = ? AND fechaBaja IS NULL', [$id, $id])[0];

			$etermico = $this->calcularEstressTermico($request['temperatura'], $request['humedad']);
			$bp = $this->calcularBateriaPorcentaje($request['bateria']);

			// Insertar en la tabla de historicos
			$this->insertarLectura($id, $sensor->id_entidad, 'FAKE', $request['bateria'], $bp, $request['rssi'], $request['snr'], $fecha, $request['temperatura'], $request['humedad'], $etermico, $request['co2']);
			// app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $request['temperatura'], $request['humedad'], $luminosidad, $etermico);

			// Actualizar la batería y la señal
			app('App\Http\Controllers\ComunicacionController')->actualizarInformacionSensor($id, $request['bateria'], $bp, $request['snr'], $request['rssi']);

			return response()->json(['estado' => true, 'mensaje' => 'Se ha insertado correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA FALSA CO2] ' . $e->getMessage());
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}*/
	}
}
