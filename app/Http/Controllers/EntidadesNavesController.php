<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

define('ALERTA_IHT_MAX', 78);
define('ALERTA_RANGO_MAX', 15);
define('UMBRAL_TIEMPO', 7200); // Tiempo en segundos los cuales una temperatura se considerará no válida

class EntidadesNavesController extends EntidadesController
{
	public function obtenerResumen(Request $request)
	{
		try
		{
			$orden = $this->ordenMostrar($request["orden"]);

			$entidades = DB::select("SELECT entidades.id id, entidades.nombre nombre, tiempo, codigo, descripcion, entidades_naves.fecha, temperatura, humedad, luminosidad, etermico, imagen, autorizado, notificaciones FROM entidades JOIN entidades_naves ON entidades.id = entidades_naves.id_entidad LEFT JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE id_usuario = ? ORDER BY $orden", [Auth::user()['id']]);

			foreach ($entidades as $entidad)
			{
				$entidad->propio = $entidad->autorizado == 0 ? true : false;
				unset ($entidad->autorizado);
				$entidad->temperatura = floatval($entidad->temperatura);
				$entidad->humedad = floatval($entidad->humedad);
				$entidad->luminosidad = floatval($entidad->luminosidad);
				$entidad->etermico = floatval($entidad->etermico);
			}

			return response()->json(['estado' => true, 'datos' => $entidades], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	private function ordenMostrar($orden)
	{
		switch ($orden)
		{
			case 'nombre': return "nombre ASC"; break;
			case 'temperatura': return "temperatura ASC"; break;
			case 'humedad': return "humedad ASC"; break;
			case 'etermico': return "etermico ASC"; break;
			default: return "nombre ASC"; break;
		}
	}

	public function obtenerDetallesCeldas($idNave)
	{
		if ($this->puedeVer($idNave, Auth::user()['id']))
		{
			$celdas = DB::select('SELECT entidades.id id_entidad, entidades.nombre nombre, ES.id_sensor, ES.fecha fecha, IFNULL(ES.temperatura, 0) temperatura, IFNULL(ES.humedad, 0) humedad, IFNULL(ES.luminosidad, 0) luminosidad, IFNULL(ES.etermico, 0) etermico, ES.id_tipo, activo, entidades_naves_celdas.fechaBaja FROM entidades_naves_celdas JOIN entidades ON entidades_naves_celdas.id_celda = entidades.id LEFT JOIN (SELECT entidades_sensores.id_sensor, sensores.id_tipo, entidades_sensores.id_entidad, temperatura, humedad, luminosidad, etermico, sensores_temperatura_humedad_luz.fecha FROM entidades_sensores JOIN sensores_temperatura_humedad_luz ON entidades_sensores.id_sensor = sensores_temperatura_humedad_luz.id_sensor JOIN sensores ON sensores_temperatura_humedad_luz.id_sensor = sensores.id WHERE entidades_sensores.fechaBaja IS NULL AND sensores.fechaBaja IS NULL) ES on entidades.id = ES.id_entidad WHERE id_nave = ? ORDER BY activo DESC, fechaBaja ASC, nombre ASC', [$idNave]);
		}

		/*if (esAdmin(Auth::user()['role']))
		{
			$celdas = DB::select('SELECT entidades.id id_entidad, entidades.nombre nombre, sensores.id id_sensor, sensores_temperatura_humedad_luz.fecha fecha, temperatura, humedad, luminosidad, etermico, sensores.id_tipo id_tipo, activo FROM entidades_naves_celdas JOIN entidades_sensores ON id_celda = entidades_sensores.id_entidad LEFT JOIN sensores_temperatura_humedad_luz ON entidades_sensores.id_sensor = sensores_temperatura_humedad_luz.id_sensor JOIN sensores ON entidades_sensores.id_sensor = sensores.id JOIN entidades ON entidades_naves_celdas.id_celda = entidades.id WHERE id_nave = ? AND entidades_naves_celdas.fechaBaja IS NULL AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL ORDER BY activo DESC, nombre ASC', [$idNave]);
		}
		else
		{
			$celdas = DB::select('SELECT entidades.id id_entidad, entidades.nombre nombre, sensores.id id_sensor, sensores_temperatura_humedad_luz.fecha fecha, temperatura, humedad, luminosidad, etermico, sensores.id_tipo id_tipo, activo FROM entidades_naves_celdas JOIN entidades_sensores ON id_celda = entidades_sensores.id_entidad LEFT JOIN sensores_temperatura_humedad_luz ON entidades_sensores.id_sensor = sensores_temperatura_humedad_luz.id_sensor JOIN sensores ON entidades_sensores.id_sensor = sensores.id JOIN entidades ON entidades_naves_celdas.id_celda = entidades.id WHERE id_nave = ? AND entidades_naves_celdas.fechaBaja IS NULL AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL AND id_nave IN (SELECT id_entidad FROM entidades_usuarios WHERE id_usuario = ?) ORDER BY activo DESC, nombre ASC', [$idNave, Auth::user()['id']]);
		}*/

		foreach ($celdas as $celda)
		{
			$celda->temperatura = floatval($celda->temperatura);
			$celda->humedad = floatval($celda->humedad);
			$celda->luminosidad = floatval($celda->luminosidad);
			$celda->etermico = floatval($celda->etermico);
			$celda->activo = $celda->activo == '1' && $celda->fechaBaja == null ? true : false;
		}

		return $celdas;
	}

	public function obtenerDetallesNave($idEntidad)
	{

		$entidad = DB::select('SELECT entidades.id, id_tipo, tiempo, entidades.codigo, entidades.descripcion, entidades.nombre, entidades.latitud, entidades.longitud, explotaciones.nombre nombreExplotacion, fichero, entidades_naves.fecha, temperatura, humedad, luminosidad, etermico, notificaciones FROM entidades_naves JOIN entidades ON entidades_naves.id_entidad = entidades.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id WHERE entidades_usuarios.id_entidad = ? AND entidades_usuarios.id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL', [$idEntidad, Auth::user()['id']])[0];

		$entidad->latitud = floatval($entidad->latitud);
		$entidad->longitud = floatval($entidad->longitud);
		$entidad->temperatura = floatval($entidad->temperatura);
		$entidad->humedad = floatval($entidad->humedad);
		$entidad->luminosidad = floatval($entidad->luminosidad);
		$entidad->etermico = floatval($entidad->etermico);

		return $entidad;
	}

	public function obtenerDetalles($id)
	{
		try
		{
			return response()->json(['estado' => true, 'nave' => $this->obtenerDetallesNave($id), 'celdas' => $this->obtenerDetallesCeldas($id)], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerHistorico($idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			if (esAdmin(Auth::user()['role']) || $this->puedeVer($idEntidad, Auth::user()['id']))
			{
				$historico = DB::select("SELECT fecha, temperatura, humedad, luminosidad, etermico FROM entidades_naves_historico WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY entidades_naves_historico.fecha DESC", [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			return response()->json(['estado' => true, 'mensaje' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerHistoricoCelda($idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			if (esAdmin(Auth::user()['role']))
			{
				$historico = DB::select('SELECT fecha, temperatura, humedad, luminosidad, etermico FROM lecturas_temperatura_humedad WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC', [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
			{
				$historico = DB::select('SELECT fecha, temperatura, humedad, luminosidad, etermico FROM lecturas_temperatura_humedad WHERE id_entidad IN (SELECT id_entidad FROM entidades_usuarios WHERE id_usuario = ? AND id_entidad = ? AND fechaBaja IS NULL) AND fecha BETWEEN ? AND ? ORDER BY fecha DESC', [Auth::user()['id'], $idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
			}

			return response()->json(['estado' => true, 'mensaje' => $historico], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER HISTORICO CELDA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerHistoricoGlobal($parametro, $idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			switch ($parametro)
			{
				case 't': $dato = 'temperatura'; break;
				case 'h': $dato = 'humedad'; break;
				case 'l': $dato = 'luminosidad'; break;
				case 'e': $dato = 'etermico'; break;
				default: $dato = 'temperatura'; break;
			}

			$historico = DB::select("SELECT UNIX_TIMESTAMP(fecha) fecha, $dato dato, id_entidad, 'Nave' nombre FROM entidades_naves_historico WHERE id_entidad = ? AND fecha BETWEEN ? AND ? UNION SELECT UNIX_TIMESTAMP(lecturas_temperatura_humedad.fecha) fecha, $dato dato, lecturas_temperatura_humedad.id_entidad id_entidad, nombre FROM lecturas_temperatura_humedad JOIN entidades ON lecturas_temperatura_humedad.id_entidad = entidades.id WHERE id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE id_nave = ? AND fechaBaja IS NULL) AND lecturas_temperatura_humedad.fecha BETWEEN ? AND ? AND entidades.fechaBaja IS NULL ORDER BY id_entidad ASC, fecha ASC", [$idEntidad, $fechaInicio, $fechaFin, $idEntidad, $fechaInicio, $fechaFin]);

			// Una vez obtenidos, procedemos a formatear los datos para que estén preparados para el gráfico
			// Donde se almacenan los resultados
			$datosGrafico = [];
			$datosPorZona = [];
			$totalFechas = []; // Para el filtrado de fechas, el cual haremos que no haya repetidas para optimizar el gráfico

			// Empezamos a filtrar y separar por naves y celdas
			$datosPorZona = [];
			if (!empty($historico))
			{
				$entidad = $historico[0]->id_entidad;
				$nombre = $historico[0]->nombre;

				foreach ($historico as $datoHistorico)
				{
					// Ya no es necesario, puesto que obtenemos el timestamp directamente desde la BD
					//$timeStampLecturaDato = strtotime($datoHistorico->fecha);
					$timeStampLecturaDato = $datoHistorico->fecha;
					$totalFechas[] = $timeStampLecturaDato;

					if ($datoHistorico->id_entidad == $entidad)
					{
						$datosPorZona[] = ['x' => $timeStampLecturaDato, 'y' => $datoHistorico->dato];
					}
					else
					{
						$datosGrafico[] = ['entidad' => $entidad, 'historico' => $datosPorZona, 'nombre' => $nombre];
						// Nuevo entidad, vaciamos los datos anteriores
						$datosPorZona = [];
						$entidad = $datoHistorico->id_entidad;
						$nombre = $datoHistorico->nombre;
						$datosPorZona[] = ['x' => $timeStampLecturaDato, 'y' => $datoHistorico->dato];
					}
				}
				// Guardamos el último
				$datosGrafico[] = ['entidad' => $entidad, 'historico' => $datosPorZona, 'nombre' => $nombre];
			}

			//return response()->json(['estado' => true, 'mensaje' => $datosGrafico], 200);

			// Ahora procedemos a normalizar el gráfico. Filtramos la lista de fechas para que quede ordenada y sin repetidos
			$totalFechas = array_unique($totalFechas, SORT_NUMERIC);
			sort($totalFechas, SORT_NUMERIC);
			$nFechas = count($totalFechas);

			// Se empieza a normalizar
			$datosGraficoNormalizado = [];

			foreach ($datosGrafico as $datoGrafico)
			{
				$datosPorZona = [];

				foreach ($totalFechas as $fecha)
				{
					if (count($datoGrafico['historico']) != 0 && $fecha == $datoGrafico['historico'][0]['x'])
					{
							$datosPorZona[] = ['x' => $datoGrafico['historico'][0]['x'], 'y' => $datoGrafico['historico'][0]['y']];
							array_shift($datoGrafico['historico']);
					}
					else
						$datosPorZona[] = ['x' => $fecha, 'y' => null];
				}

				$datosGraficoNormalizado[] = ['entidad' => $datoGrafico['entidad'], 'historico' => $datosPorZona, 'nombre' => $datoGrafico['nombre']];
			}

			return response()->json(['estado' => true, 'mensaje' => $datosGraficoNormalizado], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER HISTORICO GLOBAL NAVES] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerMinMaxMed($id_entidad, $fechaInicio, $fechaFin)
	{
		try
		{
			$medidas = DB::select('SELECT DATE(fecha) fecha, min(temperatura) minTemperatura, ROUND(avg(temperatura), 2) medTemperatura, max(temperatura) maxTemperatura, min(humedad) minHumedad, ROUND(avg(humedad), 2) medHumedad, max(humedad) maxHumedad FROM entidades_naves_historico WHERE id_entidad = ? and fecha BETWEEN ? AND ? GROUP BY DATE(fecha) ORDER BY fecha ASC', [$id_entidad, $fechaInicio, incrementarUnDia($fechaFin)]);

			// Una vez obtenidas las mínimas, medias y máximas, procedemos a calcular su IHT
			foreach ($medidas as $medida)
			{
				$medida->IHTmin = round($this->calculoIHT($medida->minTemperatura, $medida->minHumedad), 2);
				$medida->IHTmedia = round($this->calculoIHT($medida->medTemperatura, $medida->medHumedad), 2);
				$medida->IHTmax = round($this->calculoIHT($medida->maxTemperatura, $medida->maxHumedad), 2);
				$medida->rango = $medida->IHTmax - $medida->IHTmin;
				$medida->IHTpeligro = $this->alertaIHT($medida->IHTmax, $medida->rango) == true ? 'Si' : 'No';
			}

			return response()->json(['estado' => true, 'mensaje' => $medidas], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER MIN MAX MEDIA NAVE] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 200);
		}
	}

	public function obtenerIHTNaves($idNaves, $fecha)
	{
		$fechaInicio = "$fecha 00:00:00";
		$fechaFin = "$fecha 23:59:59";
		$parametros = [$fechaInicio, $fechaFin];

		foreach ($idNaves as $idEntidad)
			$parametros[] = $idEntidad;

		$medidas = DB::select('SELECT id_entidad, DATE(fecha) fecha, min(temperatura) minTemperatura, ROUND(avg(temperatura), 2) medTemperatura, max(temperatura) maxTemperatura, min(humedad) minHumedad, ROUND(avg(humedad), 2) medHumedad, max(humedad) maxHumedad FROM entidades_naves_historico WHERE fecha BETWEEN ? AND ? AND id_entidad IN ' . obtenerIn($idNaves) . ' GROUP BY DATE(fecha), id_entidad ORDER BY id_entidad, fecha ASC', $parametros);

		// Una vez obtenidas las mínimas, medias y máximas, procedemos a calcular su IHT
		foreach ($medidas as $medida)
		{
			$medida->IHTmin = round($this->calculoIHT($medida->minTemperatura, $medida->minHumedad), 2);
			$medida->IHTmedia = round($this->calculoIHT($medida->medTemperatura, $medida->medHumedad), 2);
			$medida->IHTmax = round($this->calculoIHT($medida->maxTemperatura, $medida->maxHumedad), 2);
			$medida->rango = $medida->IHTmax - $medida->IHTmin;
			$medida->IHTpeligro = $this->alertaIHT($medida->IHTmax, $medida->rango);
		}

		return $medidas;
	}

	private function calculoIHT($temperatura, $humedad)
	{
		return (0.81 * $temperatura) + $humedad / 100 * ($temperatura - 14.4) + 46.4;
	}

	private function alertaIHT($IHT, $rango)
	{
		return ($IHT > ALERTA_IHT_MAX && $rango > ALERTA_RANGO_MAX);
	}

	public function calculoEstresTermicoProlongado($idNave, $eTermicoProlongado, $eTermicoReciente)
	{
		$dias = 14; $diasIntenso = 3;

		$fechaInicio = date('Y-m-d', strtotime('-' . ($dias - 1) . ' day'));
		$fechaFin = date('Y-m-d', strtotime('+1 day'));

		$eTermico = DB::SELECT("SELECT DATE(fecha) fecha, ROUND(avg(etermico), 2) etermico, ROUND(min(etermico), 2) minEtermico FROM entidades_naves_historico WHERE id_entidad = ? and fecha BETWEEN ? AND ? GROUP BY DATE(fecha) ORDER BY fecha DESC", [$idNave, $fechaInicio, $fechaFin]);

		foreach ($eTermico as $dia)
		{
			if ($dia->etermico < $eTermicoProlongado)
				return false;
		}

		$tam = count($eTermico);
		for  ($i = 0; ($i < 3) && ($i < $tam); $i++)
		{
			if ($eTermico[$i]->minEtermico < $eTermicoReciente)
				return false;
		}

		return true;
	}

	public function calculoEstresTermicoProlongadoCelda($idEntidad, $eTermicoProlongado, $eTermicoReciente)
	{
		$dias = 14; $diasIntenso = 3;

		$fechaInicio = date('Y-m-d', strtotime('-' . ($dias - 1) . ' day')); $fechaFin = date('Y-m-d', strtotime('+1 day'));

		$eTermico = DB::SELECT("SELECT DATE(fecha) fecha, ROUND(avg(etermico), 2) etermico, ROUND(min(etermico), 2) minEtermico FROM lecturas_temperatura_humedad WHERE id_entidad = ? and fecha BETWEEN ? AND ? GROUP BY DATE(fecha) ORDER BY fecha DESC", [$idEntidad, $fechaInicio, $fechaFin]);

		foreach ($eTermico as $dia)
		{
			if ($dia->etermico < $eTermicoProlongado)
				return false;
		}

		$tam = count($eTermico);
		for  ($i = 0; ($i < 3) && ($i < $tam); $i++)
		{
			if ($eTermico[$i]->minEtermico < $eTermicoReciente)
				return false;
		}

		return true;
	}

	public function actualizarCelda($idEntidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico)
	{
		// OJO, Entidad corresponde a una celda, no a una nave
		// En caso de ser null es que no hay entidad asociada, pro lo tanto, no se inserta en ninguna nave
		try
		{
			// Primero, creamos la alerta para las celdas
			$jobCelda = new \App\Jobs\alertasEntidadCelda($idEntidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico);
			dispatch($jobCelda);

			// Obtenemos el id de la nave que está asociada a la celda, que usaremos para mas consultas posteriores
			$idNave = DB::select('SELECT id_nave FROM entidades_naves_celdas WHERE id_celda = ? AND fechaBaja IS NULL', [$idEntidad])[0]->id_nave;

			// Segundo, se obtiene la media del último valor de los sensores actualmente asociados a las celdas que posee la nave. Si los datos corresponden a valores con una diferencia de más de 2 horas, no se toman por buenos
			$resumen = DB::select("SELECT ROUND(avg(temperatura), 2) temperatura, ROUND(avg(humedad), 2) humedad, ROUND(avg(luminosidad), 2) luminosidad, ROUND(avg(etermico), 2) etermico FROM sensores_temperatura_humedad_luz WHERE id_sensor IN (SELECT id_sensor FROM entidades_sensores WHERE id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE id_nave = ? AND fechaBaja IS NULL AND activo = 1) and fechaBaja IS NULL AND fecha BETWEEN ? AND ?)", [$idNave, date('Y-m-d H:i:s', strtotime($fecha . '-' . UMBRAL_TIEMPO . ' seconds')), $fecha])[0];

			// Actualizamos el estado de la nave con el resumen obtenido anteriormente
			DB::update("UPDATE entidades_naves SET temperatura = ?, humedad = ?, luminosidad = ?, etermico = ?, fecha = ? WHERE id_entidad = ?", [$resumen->temperatura, $resumen->humedad, $resumen->luminosidad, $resumen->etermico, $fecha, $idNave]);

			// Se inserta en el histórico
			DB::insert("INSERT INTO entidades_naves_historico (id_entidad, fecha, temperatura, humedad, luminosidad, etermico) SELECT id_entidad, fecha, temperatura, humedad, luminosidad, etermico FROM entidades_naves WHERE id_entidad = ?", [$idNave]);

			// Una vez actualizados los datos de las naves, se procede a lanzar las alertas
			$job = new \App\Jobs\alertasEntidadNave($idNave, $fecha, $resumen->temperatura, $resumen->humedad, $resumen->luminosidad, $resumen->etermico);
			dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores('[ACTUALIZAR CELDA] ' . $e->getMessage());
			return false;
		}
	}

	public function actualizarCeldaIOTA($idEntidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico)
	{
		// OJO, Entidad corresponde a una celda, no a una nave
		// En caso de ser null es que no hay entidad asociada, pro lo tanto, no se inserta en ninguna nave
		try
		{
			// Primero, creamos la alerta para las celdas
			$jobCelda = new \App\Jobs\alertasEntidadCelda($idEntidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico);
			dispatch($jobCelda);

			// Para las medias, ya que no uso un resumen creado directamente por una consulta
			$mediaTemperatura = 0; $mediaHumedad= 0; $mediaLuminosidad = 0; $mediaEtermico = 0;

			// Obtenemos el id de la nave que está asociada a la celda, que usaremos para mas consultas posteriores
			$idNave = DB::select('SELECT id_nave FROM entidades_naves_celdas WHERE id_celda = ? AND fechaBaja IS NULL', [$idEntidad])[0]->id_nave;

			// Obtenemos el estado de las celdas de esa nave a la hora de la medición de temperatura, con un margen de 2 horas (mas tiempo la temperatura se considera desfasada y no se tiene en cuenta)
			$estadoCeldasHastaElMomento = DB::select('SELECT lth.id_entidad, lth.fecha, lth.temperatura, lth.humedad, lth.luminosidad, lth.etermico FROM lecturas_temperatura_humedad lth INNER JOIN (SELECT id_entidad, MAX(fecha) fecha FROM lecturas_temperatura_humedad WHERE fecha BETWEEN ? AND ? AND id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE id_nave = ? AND fechaBaja IS NULL AND activo = 1) GROUP BY id_entidad) maximas ON lth.id_entidad = maximas.id_entidad AND lth.fecha = maximas.fecha', [date('Y-m-d H:i:s',strtotime($fecha . '-' . UMBRAL_TIEMPO . ' seconds')), $fecha, $idNave]);

			// Ahora que tenemos el estado de las celdas hasta el momento, comprobamos cuantos resultados hay. Si hay solo uno, significa que corresponde a esta misma celda y por tanto no hace falta recalcular medias
			if (count($estadoCeldasHastaElMomento) == 1)
			{
				$mediaTemperatura = $temperatura;
				$mediaHumedad = $humedad;
				$mediaLuminosidad = $luminosidad;
				$mediaEtermico = $etermico;
				// En este caso, como solo hay una insertamos los valores actuales en la nave
				$this->insertarLecturaNave($idNave, $fecha, $temperatura, $humedad, $luminosidad, $etermico);
			}
			else if (count($estadoCeldasHastaElMomento) > 1)
			{
				// Contadores para las medias
				$cuantasCeldas = count($estadoCeldasHastaElMomento);

				// Primero, insertamos en el histórico de naves la media de las celdas hasta el momento
				foreach ($estadoCeldasHastaElMomento as $lecturaCelda)
				{
					$mediaTemperatura += $lecturaCelda->temperatura;
					$mediaHumedad += $lecturaCelda->humedad;
					$mediaLuminosidad += $lecturaCelda->luminosidad;
					$mediaEtermico += $lecturaCelda->etermico;
				}

				$mediaTemperatura = $mediaTemperatura / $cuantasCeldas;
				$mediaHumedad = $mediaHumedad / $cuantasCeldas;
				$mediaLuminosidad = $mediaLuminosidad / $cuantasCeldas;
				$mediaEtermico = $mediaEtermico / $cuantasCeldas;

				// Una vez calculadas las medias, procedemos a insertar los valores
				// En este caso, como solo hay una insertamos los valores actuales en la nave
				$this->insertarLecturaNave($idNave, $fecha, $mediaTemperatura, $mediaHumedad, $mediaLuminosidad, $mediaEtermico);

				// Una vez insertado el valor, procedemos a recuperar las lecturas del histórico e ir actualizando de forma retroActiva con las lecturas de los otros sensores en el intervalo de tiempo desde ahora, hasta la siguiente lectura del sensor de IOTA
				// Es decir, los sensores de IOTA emiten cada 15 minutos, entonces los siguientes quince minutos de IOTA es posible que haya emisión de otros sensores y tengan medias erróneas por no haber estado actualizado a tiempo los sensores de IOTA.

				// Buscamos las lecturas entre la fecha actual + 1 segundo y la fecha + 899 segundos (15 minutos - 1 segundo)
				$fechaInicio = date('Y-m-d H:i:s',strtotime($fecha . "-1 second"));
				$fechaFin = date('Y-m-d H:i:s',strtotime($fecha . "+899 seconds"));
				$lecturasSensor = DB::select('SELECT lth.id_entidad, lth.fecha, lth.temperatura, lth.humedad, lth.luminosidad, lth.etermico FROM lecturas_temperatura_humedad lth WHERE id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE id_nave = ? AND fechaBaja IS NULL AND activo = 1) AND fecha BETWEEN ? AND ? ORDER BY lth.fecha ASC', [$idNave, $fechaInicio, $fechaFin]);

				// Ahora comprobamos si hay lecturas del sensor. Si no hay lecturas entre esos 15 minutos, terminamos. Si hay lecturas, entonces obtenemos las lecturas del histórico de naves, que deberá haber tantas como lecturas de sensor hay (ya que por cada lectura de sensor, se actualiza la nave)
				if (!empty($lecturasSensor))
				{
					$lecturasHistoricoNave = DB::select('SELECT id, temperatura, humedad, luminosidad, etermico FROM entidades_naves_historico WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY fecha ASC', [$idNave, $fechaInicio, $fechaFin]);

					$contador = 0;
					$cuantasCeldas = count($estadoCeldasHastaElMomento);

					foreach ($lecturasHistoricoNave as $lecturaNave)
					{
						// Contadores para las medias
						$mediaTemperatura = 0; $mediaHumedad= 0; $mediaLuminosidad = 0; $mediaEtermico = 0;

						// Agregamos la lectura del sensor al estado actual de Celdas. Para ello recorremos el array con los datos actuales de celdas y cuando encontremos la celda con la lectura
						foreach ($estadoCeldasHastaElMomento as $lecturaCelda)
						{
							if ($lecturaCelda->id_entidad == $lecturasSensor[$contador]->id_entidad)
							{
								$lecturaCelda->temperatura = $lecturasSensor[$contador]->temperatura;
								$lecturaCelda->humedad = $lecturasSensor[$contador]->humedad;
								$lecturaCelda->luminosidad = $lecturasSensor[$contador]->luminosidad;
								$lecturaCelda->etermico = $lecturasSensor[$contador]->etermico;
							}

							$mediaTemperatura += $lecturaCelda->temperatura;
							$mediaHumedad += $lecturaCelda->humedad;
							$mediaLuminosidad += $lecturaCelda->luminosidad;
							$mediaEtermico += $lecturaCelda->etermico;
						}

						$mediaTemperatura = $mediaTemperatura / $cuantasCeldas;
						$mediaHumedad = $mediaHumedad / $cuantasCeldas;
						$mediaLuminosidad = $mediaLuminosidad / $cuantasCeldas;
						$mediaEtermico = $mediaEtermico / $cuantasCeldas;

						// Actualizamos la lectura del histórico de naves
						DB::update('UPDATE entidades_naves_historico SET temperatura = ?, humedad = ?, luminosidad = ?, etermico = ? WHERE id = ?', [$mediaTemperatura, $mediaHumedad, $mediaLuminosidad, $mediaEtermico, $lecturaNave->id]);

						$contador++;
					}
				}
			}

			// Actualizamos el estado de la nave con el resumen obtenido anteriormente
			DB::update("UPDATE entidades_naves SET temperatura = ?, humedad = ?, luminosidad = ?, etermico = ?, fecha = ? WHERE id_entidad = ?", [$mediaTemperatura, $mediaHumedad, $mediaLuminosidad, $mediaEtermico, $fecha, $idNave]);

			// Una vez actualizados los datos de las naves, se procede a lanzar las alertas
			$job = new \App\Jobs\alertasEntidadNave($idNave, $fecha, $mediaTemperatura, $mediaHumedad, $mediaLuminosidad, $mediaEtermico);
			dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores('[ACTUALIZAR CELDA IOTA] ' . $e->getMessage());
			return false;
		}
	}

	// Función auxiliar para las naves con sensor de IOTA y no repetir código
	private function insertarLecturaNave($idEntidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico)
	{
		DB::insert('INSERT INTO entidades_naves_historico (id_entidad, fecha, temperatura, humedad, luminosidad, etermico) VALUES(?, ?, ?, ?, ?, ?)', [$idEntidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico]);
	}

	public function calculoRetroActivo($id_entidad = 0, $fechaInicio, $fechaFin)
	{
		// 0 = TODAS
		if ($id_entidad == 0)
		{
			// Eliminamos primero todas los valores historicos entre las dos fechas
			DB::delete("DELETE FROM entidades_naves_historico WHERE fecha BETWEEN ? AND ?", [$fechaInicio, $fechaFin]);

			// Primero obtenemos el estado de las celdas antes de la fecha de Inicio
			$entidadesYFechas = DB::select("SELECT max(fecha) fecha, id_entidad FROM lecturas_temperatura_humedad WHERE fecha < ? AND id_entidad IS NOT NULL AND id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE fechaBaja IS NULL) GROUP BY id_entidad", [$fechaInicio]);

			$medias = [];

			foreach ($entidadesYFechas as $entidadYFecha)
			{
				$ultimaLecturaAntes = DB::select("SELECT id_entidad, temperatura, humedad, luminosidad, etermico, id_nave FROM lecturas_temperatura_humedad LEFT JOIN entidades_naves_celdas ON lecturas_temperatura_humedad.id_entidad = entidades_naves_celdas.id_celda WHERE id_entidad = ? AND fecha = ?", [$entidadYFecha->id_entidad, $entidadYFecha->fecha]);

				$ultimaLectura = new \stdClass();
				$ultimaLectura->temperatura = $ultimaLecturaAntes[0]->temperatura;
				$ultimaLectura->humedad = $ultimaLecturaAntes[0]->humedad;
				$ultimaLectura->luminosidad = $ultimaLecturaAntes[0]->luminosidad;
				$ultimaLectura->etermico = $ultimaLecturaAntes[0]->etermico;
				$ultimaLectura->id_nave = $ultimaLecturaAntes[0]->id_nave;

				$medias[$ultimaLecturaAntes[0]->id_entidad] = $ultimaLectura;
			}

			// Ahora cogemos el histórico entre las dos fechas
			$lecturas = DB::select("SELECT id_entidad, fecha, temperatura, humedad, luminosidad, etermico, id_nave FROM lecturas_temperatura_humedad LEFT JOIN entidades_naves_celdas ON lecturas_temperatura_humedad.id_entidad = entidades_naves_celdas.id_celda WHERE id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE fechaBaja IS NULL) AND fecha BETWEEN ? AND ? ORDER BY fecha ASC", [$fechaInicio, $fechaFin]);

			// Ahora recorremos, calculamos la media entre las medidas e insertamos en el histórico
			foreach ($lecturas as $lectura)
			{
				$ultimaLectura = new \stdClass();
				$ultimaLectura->temperatura = $lectura->temperatura;
				$ultimaLectura->humedad = $lectura->humedad;
				$ultimaLectura->luminosidad = $lectura->luminosidad;
				$ultimaLectura->etermico = $lectura->etermico;
				$ultimaLectura->id_nave = $lectura->id_nave;

				$medias[$lectura->id_entidad] = $ultimaLectura;

				$datosNave = $this->calcularMediaNaves($lectura->id_nave, $medias);

				try
				{
					DB::insert("INSERT INTO entidades_naves_historico (id_entidad, fecha, temperatura, humedad, luminosidad, etermico) VALUES (?, ?, ?, ?, ?, ?)", [$lectura->id_nave, $lectura->fecha, $datosNave["temperatura"], $datosNave["humedad"], $datosNave["luminosidad"], $datosNave["etermico"]]);
				}
				catch (Exception $e)
				{
					dump($e->getMessage());
				}
			}

			// En caso de que sea un calculo retroactivo completo (es decir, hasta ahora, en lugar de entre dos fechas concretas), actualizamos los valores en la tabla de naves
			if (strtotime($fechaFin) > strtotime("now"))
			{
				$entidadesYFechas = DB::select("SELECT max(fecha) fecha, id_entidad FROM entidades_naves_historico WHERE fecha < ? GROUP BY id_entidad", [$fechaFin]);

				foreach ($entidadesYFechas as $entidadYFecha)
				{
					// Para que sea mas fácil de leer
					$e = $entidadYFecha->id_entidad;
					$f = $entidadYFecha->fecha;

					DB::update("UPDATE entidades_naves SET
						temperatura = (SELECT temperatura FROM entidades_naves_historico WHERE id_entidad = ? AND fecha = ?),
						humedad = (SELECT humedad FROM entidades_naves_historico WHERE id_entidad = ? AND fecha = ?),
						luminosidad = (SELECT luminosidad FROM entidades_naves_historico WHERE id_entidad = ? AND fecha = ?),
						etermico = (SELECT etermico FROM entidades_naves_historico WHERE id_entidad = ? AND fecha = ?),
						fecha = ? WHERE id_entidad = ?", [$e, $f, $e, $f, $e, $f, $e, $f, $f, $e]);
				}
			}
		}
		else
		{
			$estadoCeldasHastaElMomento = DB::select('SELECT lth.id_entidad, lth.fecha, lth.temperatura, lth.humedad, lth.luminosidad, lth.etermico FROM lecturas_temperatura_humedad lth INNER JOIN (SELECT id_entidad, MAX(fecha) fecha FROM lecturas_temperatura_humedad WHERE fecha < ? AND id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE id_nave = ? AND fechaBaja IS NULL) GROUP BY id_entidad) maximas ON lth.id_entidad = maximas.id_entidad AND lth.fecha = maximas.fecha', [$fechaInicio, $id_entidad]);

			$lecturas = DB::select('SELECT fecha, id_entidad, temperatura, humedad, luminosidad, etermico FROM lecturas_temperatura_humedad WHERE fecha BETWEEN ? AND ? AND id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE id_nave = ? AND fechaBaja IS NULL) ORDER BY fecha ASC', [$fechaInicio, $fechaFin, $id_entidad]);

			// Borramos los valores que vamos a actualizar
			DB::delete('DELETE FROM entidades_naves_historico WHERE id_entidad = ? AND fecha BETWEEN ? AND ?', [$id_entidad, $fechaInicio, $fechaFin]);

			$cuantasCeldas = count($estadoCeldasHastaElMomento);

			foreach ($lecturas as $lectura)
			{
				$mediaTemperatura = 0; $mediaHumedad= 0; $mediaLuminosidad = 0; $mediaEtermico = 0;
				$cuantasCeldas = 0;

				$fechaLimite = strtotime($lectura->fecha) - UMBRAL_TIEMPO;

				foreach ($estadoCeldasHastaElMomento as $lecturaCelda)
				{
					if ($lecturaCelda->id_entidad == $lectura->id_entidad)
					{
						$lecturaCelda->temperatura = $lectura->temperatura;
						$lecturaCelda->humedad = $lectura->humedad;
						$lecturaCelda->luminosidad = $lectura->luminosidad;
						$lecturaCelda->etermico = $lectura->etermico;
						$lecturaCelda->fecha = $lectura->fecha;
					}

					// Ahora, si la fecha de la lectura de celda es inferior a 2 horas con respecto a la actual, no la tenemos en cuenta
					if ($fechaLimite <= strtotime($lecturaCelda->fecha))
					{
						$mediaTemperatura += $lecturaCelda->temperatura;
						$mediaHumedad += $lecturaCelda->humedad;
						$mediaLuminosidad += $lecturaCelda->luminosidad;
						$mediaEtermico += $lecturaCelda->etermico;
						$cuantasCeldas++;
					}
				}

				$mediaTemperatura = $mediaTemperatura / $cuantasCeldas;
				$mediaHumedad = $mediaHumedad / $cuantasCeldas;
				$mediaLuminosidad = $mediaLuminosidad / $cuantasCeldas;
				$mediaEtermico = $mediaEtermico / $cuantasCeldas;

				$this->insertarLecturaNave($id_entidad, $lectura->fecha, $mediaTemperatura, $mediaHumedad, $mediaLuminosidad, $mediaEtermico);
			}
		}
	}

	// Usada para el cálculo retroactivo
	private function calcularMediaNaves($id_entidad, $datos)
	{
		$temperaturas = []; $humedades = []; $etermicos = []; $luminosidades = [];

		foreach ($datos as $dato)
		{
			if ($dato->id_nave == $id_entidad)
			{
				$temperaturas[] = $dato->temperatura;
				$humedades[] = $dato->humedad;
				$etermicos[] = $dato->etermico;
				$luminosidades[] = $dato->luminosidad;
			}
		}

		return
		[
			'temperatura' => array_sum($temperaturas)/count($temperaturas),
			'humedad' => array_sum($humedades)/count($humedades),
			'etermico' => array_sum($etermicos)/count($etermicos),
			'luminosidad' => array_sum($luminosidades)/count($luminosidades)
		];
	}

	public function obtenerAlertasIHT()
	{
		// Obtiene las naves que tienen alerta de IHT y que no han saltado este mismo día
		$alertas = DB::select('SELECT alertas_entidades.id, id_alerta, parametros, users.id id_usuario, users.name nombreUsuario, users.email email, user_id_ionic, id_entidad, entidades.nombre nombreEntidad, codigo, id_tipo, email_instantaneo, email_alerta, notificaciones_movil, ultima FROM alertas_entidades JOIN entidades ON alertas_entidades.id_entidad = entidades.id JOIN users ON alertas_entidades.id_usuario = users.id JOIN configuracion on users.id = configuracion.id_usuario WHERE entidades.fechaBaja IS NULL AND alertas_entidades.activo = 1 AND id_alerta = 31202 AND ultima NOT BETWEEN \'' . date('Y-m-d 00:00:00') . '\' AND \'' . date('Y-m-d 23:59:59') . '\'');

		// Convertimos las banderas de la BD en booleanos que son mas fáciles de trabajar
		foreach ($alertas as $alerta)
		{
			$alerta->email_instantaneo = $alerta->email_instantaneo == ACTIVO ? true : false;
			$alerta->email_alerta = $alerta->email_alerta == ACTIVO ? true : false;
			$alerta->notificaciones_movil = $alerta->notificaciones_movil == ACTIVO ? true : false;
		}

		return $alertas;
	}
}
