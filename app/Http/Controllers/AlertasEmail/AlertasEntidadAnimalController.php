<?php

namespace App\Http\Controllers\AlertasEmail;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class AlertasEntidadAnimalController extends Controller
{
	public function montaMacho($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $codigo)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'crotal' => $codigo,
				'idTipo' => ENTIDAD_ANIMAL,
				'titulo' => 'Alerta por monta detectada'
			];
			LogController::enviarEmail($email, 'alertas.entidad.animal.monta', $data, "Monta detectada en $nombreEntidad ($codigo)");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL ANIMAL] ' . $e->getMessage());
		}
	}

	/*

	public function enviarAlertaMail($idAlerta, $idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta)
	{
		switch ($idAlerta)
		{
			case ALARMA_TEMPERATURA_BAJA:
				$this->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_TEMPERATURA_ALTA:
				$this->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_BAJA:
				$this->humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_ALTA:
				$this->humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_BAJA:
				$this->luminosidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_ALTA:
				$this->luminosidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS ENTIDAD MAIL CELDA] TIPO ALERTA NO ENCONTRADA : $idEntidad, $nombreEntidad, $nombreUsuario, $email, 4fecha, $parametroNotificacion, $parametroAlerta");
				break;
		}
	}

	*/

	public function enviarAlertaMail($idAlerta, $idEntidad, $nombreEntidad, $codigo, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta)
	{
		switch ($idAlerta)
		{
			case ALARMA_MONTA_MACHO:
				$this->montaMacho($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $codigo); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS ENTIDAD MAIL ANIMAL] TIPO ALERTA NO ENCONTRADA : $idEntidad, $nombreEntidad, $nombreUsuario, $email, 4fecha, $parametrosNotificacion, $parametrosAlerta");
				break;
		}
	}
}
