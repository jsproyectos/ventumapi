<?php

namespace App\Http\Controllers\AlertasEmail;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class AlertasEntidadDepositoAguaController extends Controller
{
	public function temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.deposito_agua.temperatura_baja', $data, "Temperatura baja en Depósito de agua $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL DEPOSITO AGUA] ' . $e->getMessage());
		}
	}

	public function temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.deposito_agua.temperatura_alta', $data, "Temperatura alta en Depósito de agua $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL DEPOSITO AGUA] ' . $e->getMessage());
		}
	}

	public function capacidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $capacidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'capacidad' => $capacidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por capacidad baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.deposito_agua.capacidad_baja', $data, "Capacidad baja en Depósito de agua $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL DEPOSITO AGUA] ' . $e->getMessage());
		}
	}

	public function capacidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $capacidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'capacidad' => $capacidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por capacidad alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.deposito_agua.capacidad_alta', $data, "Capacidad alta en Depósito de agua $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL DEPOSITO AGUA] ' . $e->getMessage());
		}
	}
	
	public function recepcion($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $capacidad)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'capacidad' => $capacidad,
				'titulo' => 'Capacidad actualizada'
			];
			LogController::enviarEmail($email, 'alertas.entidad.deposito_agua.recepcion', $data, "Capacidad actualizada en Depósito de agua $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL DEPOSITO AGUA] ' . $e->getMessage());
		}
	}

	public function desnivel($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'titulo' => 'Sensor desnivelado'
			];
			LogController::enviarEmail($email, 'alertas.entidad.deposito_agua.desnivel', $data, "Sensor desnivelado en Depósito de agua $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL DEPOSITO AGUA] ' . $e->getMessage());
		}
	}

	public function enviarAlertaMail($idAlerta, $idEntidad, $nombreEntidad, $codigo, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta)
	{
		switch ($idAlerta)
		{
			case ALARMA_TEMPERATURA_BAJA:
				$this->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_TEMPERATURA_ALTA:
				$this->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_SILOS_CAPACIDAD_BAJA:
				$this->capacidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_SILOS_CAPACIDAD_ALTA:
				$this->capacidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_SILOS_DESNIVEL:
				$this->desnivel($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha);
				break;
			case ALARMA_SILOS_RECEPCION:
				$this->recepcion($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS ENTIDAD MAIL DEPÓSITO AGUA] TIPO ALERTA NO ENCONTRADA : $idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta");
				break;
		}
	}
}
