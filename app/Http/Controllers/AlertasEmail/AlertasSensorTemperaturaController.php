<?php

namespace App\Http\Controllers\AlertasEmail;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class AlertasSensorTemperaturaController extends Controller
{
	public function temperaturaBaja($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idSensor' => $idSensor,
				'nombreSensor' => $nombreSensor,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura baja'
			];
			LogController::enviarEmail($email, 'alertas.sensor.temperatura.temperatura_baja', $data, "Temperatura baja en sensor $nombreSensor");
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL SENSOR TEMPERATURA] ' . $e->getMessage());
		}
	}

	public function temperaturaAlta($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idSensor' => $idSensor,
				'nombreSensor' => $nombreSensor,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura alta'
			];
			LogController::enviarEmail($email, 'alertas.sensor.temperatura.temperatura_alta', $data, "Temperatura alta en sensor $nombreSensor");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL SENSOR TEMPERATURA] ' . $e->getMessage());
		}
	}

	public function humedadBaja($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idSensor' => $idSensor,
				'nombreSensor' => $nombreSensor,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad baja'
			];
			LogController::enviarEmail($email, 'alertas.sensor.temperatura.humedad_baja', $data, "Humedad baja en sensor $nombreSensor");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL SENSOR TEMPERATURA] ' . $e->getMessage());
		}
	}

	public function humedadAlta($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idSensor' => $idSensor,
				'nombreSensor' => $nombreSensor,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad alta'
			];
			LogController::enviarEmail($email, 'alertas.sensor.temperatura.humedad_alta', $data, "Humedad alta en sensor $nombreSensor");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL SENSOR TEMPERATURA] ' . $e->getMessage());
		}
	}

	public function luminosidadBaja($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idSensor' => $idSensor,
				'nombreSensor' => $nombreSensor,
				'luminosidad' => $luminosidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por luminosidad baja'
			];
			LogController::enviarEmail($email, 'alertas.sensor.temperatura.luminosidad_baja', $data, "Luminosidad baja en sensor $nombreSensor");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL SENSOR TEMPERATURA] ' . $e->getMessage());
		}
	}

	public function luminosidadAlta($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idSensor' => $idSensor,
				'nombreSensor' => $nombreSensor,
				'luminosidad' => $luminosidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por luminosidad alta'
			];
			LogController::enviarEmail($email, 'alertas.sensor.temperatura.luminosidad_alta', $data, "Luminosidad alta en sensor $nombreSensor");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL SENSOR TEMPERATURA] ' . $e->getMessage());
		}
	}

	public function enviarAlertaMail($idAlerta, $idSensor, $nombreSensor, $codigo, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta)
	{
		switch ($idAlerta)
		{
			case ALARMA_TEMPERATURA_BAJA:
				$this->temperaturaBaja($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_TEMPERATURA_ALTA:
				$this->temperaturaAlta($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_BAJA:
				$this->humedadBaja($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_ALTA:
				$this->humedadAlta($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_BAJA:
				$this->luminosidadBaja($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_ALTA:
				$this->luminosidadAlta($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALERTA_IHT_PELIGROSO:
				$this->IHTPeligroso($idSensor, $nombreSensor, $nombreUsuario, $email, $fecha, $parametroNotificacion); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS SENSOR MAIL TEMPERATURA] TIPO ALERTA NO ENCONTRADA : $idSensor, $nombreSensor, $nombreUsuario, $email, 4fecha, $parametroNotificacion, $parametroAlerta");
				break;
		}
	}
}
