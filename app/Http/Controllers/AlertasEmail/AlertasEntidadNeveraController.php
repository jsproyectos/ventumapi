<?php

namespace App\Http\Controllers\AlertasEmail;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class AlertasEntidadNeveraController extends Controller
{
	public function temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nevera.temperatura_baja', $data, "Temperatura baja en nevera $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NEVERA] ' . $e->getMessage());
		}
	}

	public function temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nevera.temperatura_alta', $data, "Temperatura alta en nevera $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NEVERA] ' . $e->getMessage());
		}
	}

	public function humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nevera.humedad_baja', $data, "Humedad baja en nevera $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NEVERA] ' . $e->getMessage());
		}
	}

	public function humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nevera.humedad_alta', $data, "Humedad alta en nevera $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NEVERA] ' . $e->getMessage());
		}
	}

	public function enviarAlertaMail($idAlerta, $idEntidad, $nombreEntidad, $codigo, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta)
	{
		switch ($idAlerta)
		{
			case ALARMA_TEMPERATURA_BAJA:
				$this->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_TEMPERATURA_ALTA:
				$this->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_BAJA:
				$this->humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_ALTA:
				$this->humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS ENTIDAD MAIL NEVERA] TIPO ALERTA NO ENCONTRADA : $idEntidad, $nombreEntidad, $nombreUsuario, $email, 4fecha, $parametroNotificacion, $parametroAlerta");
				break;
		}
	}
}
