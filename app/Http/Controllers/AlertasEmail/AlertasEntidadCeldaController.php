<?php

namespace App\Http\Controllers\AlertasEmail;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class AlertasEntidadCeldaController extends Controller
{
	public function temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.celda.temperatura_baja', $data, "Temperatura baja en celda $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.celda.temperatura_alta', $data, "Temperatura alta en celda $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL CELDA] ' . $e->getMessage());
		}
	}

	public function humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.celda.humedad_baja', $data, "Humedad baja en celda $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL CELDA] ' . $e->getMessage());
		}
	}

	public function humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.celda.humedad_alta', $data, "Humedad alta en celda $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL CELDA] ' . $e->getMessage());
		}
	}

	public function luminosidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'luminosidad' => $luminosidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por luminosidad baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.celda.luminosidad_baja', $data, "Luminosidad baja en celda $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL CELDA] ' . $e->getMessage());
		}
	}

	public function luminosidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'luminosidad' => $luminosidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por luminosidad alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.celda.luminosidad_alta', $data, "Luminosidad alta en celda $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL CELDA] ' . $e->getMessage());
		}
	}

	public function estresTermicoProlongado($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $etermico, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por estrés térmico prolongado'
			];
			LogController::enviarEmail($email, 'alertas.entidad.celda.eTermicoProlongado"', $data, "Estrés térmico prolongado en celda $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL CELDA] ' . $e->getMessage());
		}
	}

	public function enviarAlertaMail($idAlerta, $idEntidad, $nombreEntidad, $codigo, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta)
	{
		switch ($idAlerta)
		{
			case ALARMA_TEMPERATURA_BAJA:
				$this->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_TEMPERATURA_ALTA:
				$this->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_BAJA:
				$this->humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_ALTA:
				$this->humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_BAJA:
				$this->luminosidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_ALTA:
				$this->luminosidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS ENTIDAD MAIL CELDA] TIPO ALERTA NO ENCONTRADA : $idEntidad, $nombreEntidad, $nombreUsuario, $email, 4fecha, $parametroNotificacion, $parametroAlerta");
				break;
		}
	}
}
