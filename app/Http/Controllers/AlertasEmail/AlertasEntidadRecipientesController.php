<?php

namespace App\Http\Controllers\AlertasEmail;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class AlertasEntidadRecipientesController extends Controller
{
	// Tipo quiere decir si es un silo, depósito de agua o tanque de leche
	public function temperaturaBaja($idEntidad, $nombreEntidad, $tipo, $ruta, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura baja'
			];
			LogController::enviarEmail($email, "alertas.entidad.$ruta.temperatura_baja", $data, "Temperatura baja en $tipo $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL RECIPIENTES] ' . $e->getMessage());
		}
	}

	// Tipo quiere decir si es un silo, depósito de agua o tanque de leche
	public function temperaturaAlta($idEntidad, $nombreEntidad, $tipo, $ruta, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura alta'
			];
			LogController::enviarEmail($email, "alertas.entidad.$ruta.temperatura_alta", $data, "Temperatura alta en $tipo $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL RECIPIENTES] ' . $e->getMessage());
		}
	}

	public function capacidadBaja($idEntidad, $nombreEntidad, $tipo, $ruta, $nombreUsuario, $email, $fecha, $capacidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'capacidad' => $capacidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por capacidad baja'
			];
			LogController::enviarEmail($email, "alertas.entidad.$ruta.capacidad_baja", $data, "Capacidad baja en $tipo $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL RECIPIENTES] ' . $e->getMessage());
		}
	}

	public function capacidadAlta($idEntidad, $nombreEntidad, $tipo, $ruta, $nombreUsuario, $email, $fecha, $capacidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'capacidad' => $capacidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por capacidad alta'
			];
			LogController::enviarEmail($email, "alertas.entidad.$ruta.capacidad_alta", $data, "Capacidad alta en $tipo $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL RECIPIENTES] ' . $e->getMessage());
		}
	}
	
	public function recepcion($idEntidad, $nombreEntidad, $tipo, $ruta, $nombreUsuario, $email, $fecha, $capacidad)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'capacidad' => $capacidad,
				'titulo' => 'Capacidad actualizada'
			];
			LogController::enviarEmail($email, "alertas.entidad.$ruta.recepcion", $data, "Capacidad actualizada en $tipo $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL RECIPIENTES] ' . $e->getMessage());
		}
	}

	public function desnivel($idEntidad, $nombreEntidad, $tipo, $ruta, $nombreUsuario, $email, $fecha)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'titulo' => 'Sensor desnivelado'
			];
			LogController::enviarEmail($email, "alertas.entidad.$ruta.desnivel", $data, "Sensor desnivelado en $tipo $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL RECIPIENTES] ' . $e->getMessage());
		}
	}

	public function enviarAlertaMail($idAlerta, $idEntidad, $idTipo, $nombreEntidad, $codigo, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta)
	{
		// Id tipo de la entidad
		$ruta = null;
		$tipoEntidad = null;

		switch ($idTipo)
		{
			case ENTIDAD_SILO:
				$ruta = 'silo';
				$tipoEntidad = 'silo';
				$rutaEntidad = '/app/principal/silos/';
				break;
			case ENTIDAD_AGUA_DEPOSITOS:
				$ruta = 'deposito_agua';
				$tipoEntidad = 'depósito de agua';
				$rutaEntidad = '/app/principal/depositos/';
				break;
			
			default:
				$ruta = 'silo';
				$tipoEntidad = 'silo';
				$rutaEntidad = '';
				break;
		}

		switch ($idAlerta)
		{
			case ALARMA_TEMPERATURA_BAJA:
				$this->temperaturaBaja($idEntidad, $nombreEntidad, $tipoEntidad, $ruta, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_TEMPERATURA_ALTA:
				$this->temperaturaAlta($idEntidad, $nombreEntidad, $tipoEntidad, $ruta, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_SILOS_CAPACIDAD_BAJA:
				$this->capacidadBaja($idEntidad, $nombreEntidad, $tipoEntidad, $ruta, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_SILOS_CAPACIDAD_ALTA:
				$this->capacidadAlta($idEntidad, $nombreEntidad, $tipoEntidad, $ruta, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta);
				break;
			case ALARMA_SILOS_DESNIVEL:
				$this->desnivel($idEntidad, $nombreEntidad, $tipoEntidad, $ruta, $nombreUsuario, $email, $fecha);
				break;
			case ALARMA_SILOS_RECEPCION:
				$this->recepcion($idEntidad, $nombreEntidad, $tipoEntidad, $ruta, $nombreUsuario, $email, $fecha, $parametroNotificacion); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS ENTIDAD MAIL RECIPIENTE] TIPO ALERTA NO ENCONTRADA : $idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta");
				break;
		}
	}
}
