<?php

namespace App\Http\Controllers\AlertasEmail;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class AlertasEntidadNaveController extends Controller
{
	public function temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.temperatura_baja', $data, "Temperatura baja en nave $nombreEntidad");
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'temperatura' => $temperatura,
				'parametros' => $parametros,
				'titulo' => 'Alerta por temperatura alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.temperatura_alta', $data, "Temperatura alta en nave $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.humedad_baja', $data, "Humedad baja en nave $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'humedad' => $humedad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por humedad alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.humedad_alta', $data, "Humedad alta en nave $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function luminosidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'luminosidad' => $luminosidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por luminosidad baja'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.luminosidad_baja', $data, "Luminosidad baja en nave $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function luminosidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'luminosidad' => $luminosidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por luminosidad alta'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.luminosidad_alta', $data, "Luminosidad alta en nave $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function estresTermicoProlongado($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $etermico, $parametros)
	{
		try
		{
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'parametros' => $parametros,
				'titulo' => 'Alerta por estrés térmico prolongado'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.eTermicoProlongado', $data, "Estrés térmico prolongado en nave $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function IHTPeligroso($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion/*, No hay parámetros alerta*/)
	{
		try
		{
			$parametroNotificacion = explode('#', $parametroNotificacion);
			$data = [
				'nombre' => $nombreUsuario,
				'fecha' => $fecha,
				'idEntidad' => $idEntidad,
				'nombreEntidad' => $nombreEntidad,
				'IHTMax' => $parametroNotificacion[0],
				'rango' => $parametroNotificacion[1],
				'titulo' => 'Alerta por IHT elevado'
			];
			LogController::enviarEmail($email, 'alertas.entidad.nave.iht_elevado', $data, "IHT elevado en nave $nombreEntidad");	
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTA EMAIL NAVES] ' . $e->getMessage());
		}
	}

	public function enviarAlertaMail($idAlerta, $idEntidad, $nombreEntidad, $codigo, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta)
	{
		switch ($idAlerta)
		{
			case ALARMA_TEMPERATURA_BAJA:
				$this->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_TEMPERATURA_ALTA:
				$this->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_BAJA:
				$this->humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_HUMEDAD_ALTA:
				$this->humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_BAJA:
				$this->luminosidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALARMA_LUMINOSIDAD_ALTA:
				$this->luminosidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion, $parametroAlerta); break;
			case ALERTA_IHT_PELIGROSO:
				$this->IHTPeligroso($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametroNotificacion); break;
			default:
				LogController::errores("[ALERTAS PERIODICAS ENTIDAD MAIL NAVE] TIPO ALERTA NO ENCONTRADA : $idEntidad, $nombreEntidad, $nombreUsuario, $email, 4fecha, $parametroNotificacion, $parametroAlerta");
				break;
		}
	}
}
