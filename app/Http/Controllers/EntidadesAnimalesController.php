<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use App\EntidadNevera;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class EntidadesAnimalesController extends EntidadesController
{
	public function obtenerResumen(Request $request)
	{
		try
		{
			$orden = $this->ordenMostrar($request['orden']);

			$explotaciones = DB::select('SELECT id_explotacion id, explotaciones.nombre, fichero, count(*) animales, explotaciones.latitud, explotaciones.longitud, zoom, sum(notificaciones) notificaciones FROM entidades JOIN entidades_animales ON entidades.id = entidades_animales.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE id_usuario = ? AND entidades_animales.tracker = 1 GROUP BY id_explotacion, explotaciones.nombre, fichero, explotaciones.latitud, explotaciones.longitud, zoom', [Auth::user()['id']]);

			foreach ($explotaciones as $explotacion)
			{
				$explotacion->notificaciones = intval($explotacion->notificaciones);
				$explotacion->latitud = floatval($explotacion->latitud);
				$explotacion->longitud = floatval($explotacion->longitud);
				$explotacion->zoom = floatval($explotacion->zoom);
			}

			return response()->json(['estado' => true, 'datos' => $explotaciones], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	private function ordenMostrar($orden)
	{
		switch ($orden)
		{
			case 'nombre': return "nombre ASC"; break;
			case 'temperatura': return "temperatura ASC"; break;
			case 'humedad': return "humedad ASC"; break;
			case 'etermico': return "etermico ASC"; break;
			default: return "nombre ASC"; break;
		}
	}

	public function obtenerAnimales($idExplotacion)
	{
		// Obtiene los animales de la finca seleccionada
		$animales = DB::select('SELECT entidades.id, S.id_sensor, entidades.nombre nombre, entidades.codigo, entidades.latitud, entidades.longitud, entidades.descripcion, autorizado, notificaciones, entidades_animales.id_tipo tipoAnimal FROM entidades JOIN entidades_animales ON entidades.id = entidades_animales.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad LEFT JOIN (SELECT id_entidad, id_sensor FROM entidades_sensores JOIN sensores ON entidades_sensores.id_sensor = sensores.id WHERE id_tipo = 3 AND entidades_sensores.fechaBaja IS NULL AND sensores.fechaBaja IS NULL) S ON entidades.id = S.id_entidad WHERE entidades_animales.tracker = 1 AND entidades.id_explotacion = ? AND id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL', [$idExplotacion, Auth::user()['id']]);

		foreach ($animales as $animal)
		{
			$animal->propio = $animal->autorizado == 0 ? true : false;
			unset ($animal->autorizado);
			$animal->latitud = floatval($animal->latitud);
			$animal->longitud = floatval($animal->longitud);
		}

		return $animales;
	}

	public function obtenerAnimalesPorExplotacion($idExplotacion)
	{
		try
		{
			return response()->json(['estado' => true, 'datos' => $this->obtenerAnimales($idExplotacion)], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerHistorico($idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			if (esAdmin(Auth::user()['role']) || $this->puedeVer($idEntidad, Auth::user()['id']))
			{
				$historico = DB::select('SELECT fecha, latitud, longitud FROM lecturas_gps WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY fecha ASC', [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			foreach ($historico as $registro)
			{
				$registro->latitud = floatval($registro->latitud);
				$registro->longitud = floatval($registro->longitud);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER HISTORICO NEVERA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}
