<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
// use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntidadesSilosController extends EntidadesRecipientesController
{
	private function calcularVolumenTotal($idModelo)
	{
		$tam = DB::SELECT("SELECT aconosup, acilindro, aconoinf, rconosup, rcilindro, rconoinf FROM entidades_modelos_recipientes WHERE id = ?", [$idModelo])[0];

		$volumenConoInferior = (pi() * $tam->aconoinf * (pow($tam->rconoinf, 2) + pow($tam->rcilindro, 2) + $tam->rconoinf * $tam->rcilindro)) / 3;
		$volumenCilindro = pi() * pow($tam->rconoinf, 2) * $tam->acilindro;
		$volumenConoSuperior = (pi() * $tam->aconosup * (pow($tam->rcilindro, 2) + pow($tam->rconosup, 2) + $tam->rcilindro * $tam->rconosup)) / 3;
		return round($volumenConoInferior + $volumenCilindro + $volumenConoSuperior);
	}

	public function obtenerResumen(Request $request)
	{
		try
		{
			// Medida de seguridad
			$orden = $request['orden'] ? $request['orden'] : null;
			$nombre = $request['nombre'] ? $request['nombre'] : null;
			$explotacion = $request['explotacion'] ? $request['explotacion'] : null;
			$falsas = $request['falsas'] ? true : false;
			$bajas = $request['bajas'] ? true : false;

			$entidades = $this->obtenerResumenRecipientes(ENTIDAD_SILO, $orden, $nombre, $explotacion, $falsas, $bajas);

			return response()->json(['estado' => true, 'datos' => $entidades], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}
