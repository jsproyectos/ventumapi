<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Sensor;
use App\AguaDeposito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntidadesDepositosAguaController extends EntidadesRecipientesController
{
	// Devuelve un número formateado con decimales y puntos
	private function formatearNumeroConDecimales($numero)
	{
		return number_format($numero, 2, ",", ".");
	}

	public function obtenerResumen(Request $request)
	{
		try
		{
			// Medida de seguridad
			$orden = $request['orden'] ? $request['orden'] : null;
			$nombre = $request['nombre'] ? $request['nombre'] : null;
			$explotacion = $request['explotacion'] ? $request['explotacion'] : null;
			$falsas = $request['falsas'] ? true : false;
			$bajas = $request['bajas'] ? true : false;

			$entidades = $this->obtenerResumenRecipientes(ENTIDAD_AGUA_DEPOSITOS, $orden, $nombre, $explotacion, $falsas, $bajas);

			return response()->json(['estado' => true, 'datos' => $entidades], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}
