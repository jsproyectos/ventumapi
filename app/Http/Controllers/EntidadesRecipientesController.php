<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use App\EntidadRecipiente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

define ('DISTANCIA_LLENADO', 50);

class EntidadesRecipientesController extends EntidadesController
{
    // Actualiza el consumo de un recipiente
	public function actualizarConsumoAcumuladoRecipiente($id_entidad)
	{
		// Primero obtenemos la entidad de la base de datos
		// Luego, actualizamos su consumo con el acumulado actual
		// Luego guardamos las entidad obtenida previamente con el consumo actual

		$entidad = EntidadRecipiente::find($id_entidad);
		$entidad->consumo = $this->obtenerConsumoPorDiaAcumulado($id_entidad);

		if ($entidad->save())
			return true;
		else
			return false;
	}

	// Función que inserta los consumos en un histórico.
	public function insertarConsumosEnHistorico()
	{
		DB::insert("INSERT INTO entidades_recipientes_consumo_historico (id_entidad, fecha, acumulado, pacumulado, diario, pdiario, animales, id_ganado, id_modelo, capacidad) SELECT id_entidad, now() fecha, consumo acumulado, (consumo / capacidad * 100) pacumulado, consumoD diario, (consumoD / capacidad * 100) pdiario, animales, id_ganado, id_modelo, capacidad FROM entidades_recipientes JOIN entidades ON entidades_recipientes.id_entidad = entidades.id WHERE entidades.fechaBaja IS NULL");
	}

	// Función que cuando se cambia el número de animales, el tipo de recipiente, la capacidad del recipiente, etc, se inserta en un histórico y ademas guarda la información del usuario que lo hizo
	public function insertarCambioEnHistorico($id_entidad)
	{
		try
		{
			DB::insert("INSERT INTO entidades_recipientes_historico (id_entidad, id_ganado, id_modelo, animales, capacidad, id_usuario) SELECT id_entidad, id_ganado, id_modelo, animales, capacidad, ? id_usuario FROM entidades_recipientes WHERE id_entidad = ?", [Auth::user()["id"], $id_entidad]);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}

	public function obtenerConsumoDia($id_entidad)
	{
		$lecturasDiaria = DB::select("SELECT DISTINCT altura, porcentaje, kilos, fecha FROM lecturas_silos WHERE id_entidad = ? AND fecha > ? ORDER BY fecha ASC", [$id_entidad, date("Y-m-d")]);

		return $this->obtenerConsumo($lecturasDiaria);
	}

	public function obtenerConsumoPorDiaAcumulado($id_entidad)
	{
		// SELECT altura, porcentaje, kilos, fecha FROM lecturas_silos WHERE id_entidad = 11 AND fecha BETWEEN (SELECT fecha_reinicio FROM entidades_recipientes WHERE id_entidad = 11) AND now() ORDER BY fecha ASC
		$lecturasSemanal = DB::select("SELECT altura, porcentaje, kilos, fecha FROM lecturas_silos WHERE id_entidad = ? AND fecha BETWEEN (SELECT fecha_reinicio FROM entidades_recipientes WHERE id_entidad = ?) AND now() ORDER BY fecha ASC", [$id_entidad, $id_entidad]);

		return $this->obtenerConsumo($lecturasSemanal);
	}

	public function obtenerConsumo($lecturas)
	{
		if (empty($lecturas))
			return 0;
		try
		{
			// Inicializamos las variables necesarias
			$consumoAcumulado = []; // Array donde se establecerán los consumos así como el tiempo que han estado. De esta forma podremos hacer un cálculo correcto cuando haya un llenado del silo
			$diasAcumulado = 0;

			$distanciaAnterior = $lecturas[0]->altura;
			$capacidadLimiteI = $lecturas[0]->kilos;
			$fechaLimiteI = $lecturas[0]->fecha;
			$capacidadLimiteD = $lecturas[0]->kilos;
			$fechaLimiteD = $lecturas[0]->fecha;

			foreach ($lecturas as $lectura)
			{
				// Comprobamos si ha habido un llenado del silo
				if ($lectura->altura < $distanciaAnterior - DISTANCIA_LLENADO)
				{
					$dias = diferenciaDias($fechaLimiteI, $fechaLimiteD);

					// Para evitar divisiones por 0
					if ($dias > 0)
					{
						$consumoAcumulado[] = [
							'consumo' => abs(($capacidadLimiteI - $capacidadLimiteD)) / $dias,
							'dias' => $dias
						];

						$diasAcumulado += $dias;
					}

					$capacidadLimiteI = $lectura->kilos;
					$fechaLimiteI = $lectura->fecha;
				}
				else
				{
					$capacidadLimiteD = $lectura->kilos;
					$fechaLimiteD = $lectura->fecha;
				}

				$distanciaAnterior = $lectura->altura;
			}

			$dias = diferenciaDias($fechaLimiteI, $fechaLimiteD);

			// Para evitar divisiones por 0
			if ($dias > 0)
			{
				$consumoAcumulado[] = [
					'consumo' => abs(($capacidadLimiteI - $capacidadLimiteD)) / $dias,
					'dias' => $dias
				];

				$diasAcumulado += $dias;
			}

			// Ahora realizamos la mediaPonderada
			$consumoTotal = 0;

			if ($diasAcumulado != 0)
			{
				foreach ($consumoAcumulado as $consumo)
					$consumoTotal += $consumo['consumo'] * ($consumo['dias'] / $diasAcumulado);
			}

			return $consumoTotal;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return 0;
		}
	}

	public function actualizar($idEntidad, $idTipo, $fecha, $porcentaje, $kilos, $temperatura, $aceX = 0, $aceY = 0, $aceZ = 0)
	{
		try
		{
			if($idEntidad != null)
			{
				// Utilizo consultas directas para que sea mas eficiente
				DB::update('UPDATE entidades_recipientes SET porcentaje = ?, kilos = ?, temperatura = ?, fecha = ?, consumoD = ? WHERE id_entidad = ?', [$porcentaje, $kilos, $temperatura, $fecha, $this->obtenerConsumoDia($idEntidad), $idEntidad]);
				
				//DB::update("UPDATE entidades_recipientes SET consumoD = ?", []);

				// Ahora se lanza la cola para las alertas de los recipientes
				$job = null;

				if ($idTipo == ENTIDAD_SILO)
					$job = new \App\Jobs\alertasEntidadSilo($idEntidad, $porcentaje, $kilos, $fecha, $temperatura, $aceX, $aceY, $aceZ);

				if ($idTipo == ENTIDAD_AGUA_DEPOSITOS)
					$job = new \App\Jobs\alertasEntidadDepositoAgua($idEntidad, $porcentaje, $kilos, $fecha, $temperatura, $aceX, $aceY, $aceZ);

				if ($job)
					dispatch($job);

				// Ahora creamos una alerta para enviar información en caso de que el cliente tenga fiware contratado
				// $jobFiware = new \App\Jobs\FiwareRecipientes($idEntidad, $fecha);
				// dispatch($jobFiware);
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
		return true;
	}

	protected function obtenerResumenRecipientes($tipo, $orden, $nombre, $explotacion, $falsas, $bajas)
	{
		switch ($orden)
		{
			case 'nombre': $orden = ' ORDER BY nombre'; break;
			case 'porcentaje': $orden = ' ORDER BY porcentaje'; break;
			case 'consumo': $orden = ' ORDER BY consumo'; break;
			case 'estimacion': $orden = ' ORDER BY prevision ASC'; break;
			default: $orden = ' ORDER BY nombre'; break;
		}

		$consulta = "SELECT entidades.id, entidades.nombre nombre, tiempo, id_ganado, capacidad, porcentaje, temperatura, consumo, autorizado, id_explotacion, explotaciones.nombre nombreExplotacion, notificaciones, ROUND((capacidad * porcentaje / 100), 0) kilos, IFNULL(ROUND((kilos / consumo)), 0) prevision FROM entidades_recipientes JOIN entidades ON entidades_recipientes.id_entidad = entidades.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id WHERE entidades.id_tipo = ? AND id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL";

		$parametros = [$tipo, Auth::user()['id']];

		if ($nombre)
		{
			$consulta .= ' AND entidades.nombre LIKE ?';
			$parametros[] = likear($nombre);
		}

		if ($explotacion)
		{
			$consulta .= ' AND id_explotacion IN (SELECT id FROM explotaciones WHERE nombre LIKE ?)';
			$parametros[] = likear($explotacion);
		}

		if (esAdmin(Auth::user()['role']))
		{
			if (!$falsas)
				$consulta .= ' AND fake = 0';

			if (!$bajas)
				$consulta .= ' AND entidades.fechaBaja IS null';
		}

		// Concatenamos con el orden y ejecutamos la consulta con los parámetros indicados
		$entidades = DB::select($consulta . $orden, $parametros);

		foreach ($entidades as $entidad)
		{
			$entidad->consumoDiario = $entidad->consumo;
			$entidad->kilos = $entidad->capacidad * $entidad->porcentaje / 100;
			$entidad->prevision = $entidad->consumoDiario == 0 ? 0 : intval($entidad->kilos / $entidad->consumo);
			$entidad->propio = $entidad->autorizado == 0 ? true : false;
			unset ($entidad->autorizado);
		}

		return $entidades;
	}

	protected function obtenerDetallesEntidad($id)
	{
		try
		{
			$entidad = DB::select('SELECT entidades.id id, entidades.nombre, entidades.id_tipo id_tipo, tiempo, SA.id_tipo id_tipo_sensor, id_sensor, entidades.descripcion, entidades.longitud, entidades.latitud, porcentaje, kilos, capacidad, id_ganado tipo_ganado, animales, consumo consumoAcumulado, consumoD consumoDiario, fecha_reinicio, autorizado, explotaciones.nombre nombreExplotacion, fichero, notificaciones FROM entidades JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad LEFT JOIN (SELECT id_entidad, id id_sensor, id_tipo FROM entidades_sensores JOIN sensores ON entidades_sensores.id_sensor = sensores.id WHERE entidades_sensores.fechaBaja IS NULL AND id_entidad = ?) SA ON entidades.id = SA.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades_recipientes.id_entidad = entidades_usuarios.id_entidad WHERE entidades.id = ? AND id_usuario = ? AND entidades_usuarios.fechaBaja IS NULL', [$id, $id, Auth::user()['id']])[0];

			$entidad->consumoAcumuladoAnimal = $entidad->animales == null ? 0 : round($entidad->consumoAcumulado / $entidad->animales, 2);
			$entidad->consumoDiarioAnimal = $entidad->animales == null ? 0 : round($entidad->consumoDiario / $entidad->animales, 2);
			$entidad->prevision = $entidad->consumoAcumulado == 0 ? 0 : intval($entidad->kilos / $entidad->consumoAcumulado);
			$entidad->propio = $entidad->autorizado == 0 ? true : false; unset ($entidad->autorizado);
			$entidad->latitud = floatval($entidad->latitud);
			$entidad->longitud = floatval($entidad->longitud);
			$entidad->animales = $entidad->animales == null ? 0 : $entidad->animales;

			// Ahora obtenemos la url del sensor asociado (en caso de haberlo)
			if (isset($entidad->id_sensor) && $entidad->id_sensor != null)
				$entidad->urlSensor = url("https://clientes.ventumidc.es/sensores/general/detalles/$entidad->id_tipo_sensor/$entidad->id_sensor");
			else
				$entidad->urlSensor = null;

			return $entidad;
		}
		catch (Exception $e)
		{
			return null;
		}
	}

	protected function obtenerDetalles($id)
	{
		try
		{
			return response()->json(['estado' => true, 'mensaje' => $this->obtenerDetallesEntidad($id)], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	protected function obtenerHistorico($idEntidad, $fechaInicio, $fechaFin, $factor = null)
	{
		try
		{
			if (esAdmin(Auth::user()['role']) || $this->puedeVer($idEntidad, Auth::user()['id']))
			{
				// $altura = Auth::user()['role'] == USUARIO_NORMAL ? '' : 'altura, ';

				if (Auth::user()['id'] == 36)
				{
					$historico = DB::select('SELECT DATE_FORMAT(fecha, "%Y-%c-%d %H:00:00") fecha, ROUND(avg(porcentaje), 0) porcentaje, ROUND(avg(altura), 0) altura, ROUND(avg(kilos), 0) kilos, ROUND(avg(temperatura), 0) temperatura, avg(acelerometroX) acelerometroX, avg(acelerometroY) acelerometroY, avg(acelerometroZ) acelerometroZ FROM lecturas_silos WHERE id_entidad = ? AND fecha BETWEEN ? AND ? GROUP BY DATE_FORMAT(fecha, "%Y-%c-%d %H:00:00") ORDER BY DATE_FORMAT(fecha, "%Y-%c-%d %H:00:00") DESC', [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
				}

				else
				{
					$historico = DB::select("SELECT fecha, porcentaje, altura, kilos, temperatura, acelerometroX, acelerometroY, acelerometroZ FROM lecturas_silos WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC", [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
				}



				// Si tenemos un factor de corrección, aplicamos el factor de corrección a la información obtenida. Comprobamos que no esté vacío para que funcione corectamente el bucle, ya que empieza a partir de la segunda posición.
				/*if ($factor && !empty($historico))
				{
					$distancia = $historico[0]->altura;
					$porcentaje = $historico[0]->porcentaje;
					$kilos = $historico[0]->kilos;
					$tam = count($historico);
					$factor = intval($factor);

					for($i = 1; $i < $tam; $i++)
					{
						if (($historico[$i]->altura + $factor) <= $distancia)
						{
							$distancia = $historico[$i]->altura;
							$porcentaje = $historico[$i]->porcentaje;
							$kilos = $historico[$i]->kilos;
						}
						else
						{
							// Se corrige la distancia y resto de parámetros
							$historico[$i]->altura = $distancia;
							$historico[$i]->porcentaje = $porcentaje;
							$historico[$i]->kilos = $kilos;
						}
					}
				}*/

				if ($factor)
					$historico = app('App\Http\Controllers\SensoresSilosController')->corregirConFactor($historico, $factor);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			// Ahora hacemos el tratamiento de al tapa
			foreach ($historico as $lectura)
			{
				$lectura->tapa = tapaCerrada($lectura->acelerometroX, $lectura->acelerometroY, $lectura->acelerometroZ);
				unset ($lectura->acelerometroX);
				unset ($lectura->acelerometroY);
				unset ($lectura->acelerometroZ);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function llenadoRetroActivo(Request $request)
	{
		if ($this->calcularLlenadoRetroActivo($request['id_entidad']))
			return json_encode(["estado" => "OK", "mensaje" => "Se ha calculado correctamente el llenado retroactivo"]);
		else
			return json_encode(["estado" => "ERROR", "mensaje" => "Ha ocurrido un error. Consulte el log de excepciones"]);
	}

	public function CalcularPorcentajeLlenado($id_entidad, $distancia)
	{
		try
		{
			// Comprobamos si está dado de alta como entidad recipiente y tiene un modelo asociado. Si no es así, entonces el porcentaje de llenado es 100%
			$modelo = DB::select('SELECT entidades_modelos_recipientes.id_tipo id_tipo, id_modelo, aconosup, acilindro, aconoinf, rconosup, rcilindro, rconoinf, altura, volumen FROM entidades_recipientes JOIN entidades_modelos_recipientes ON entidades_recipientes.id_modelo = entidades_modelos_recipientes.id WHERE id_entidad = ?', [$id_entidad]);

			if (!empty($modelo))
			{
				$modelo = $modelo[0];

				switch ($modelo->id_tipo)
				{
					case DEPOSITO_SILO:
						return $this->VolumenDepositoSilo($modelo->aconosup, $modelo->acilindro, $modelo->aconoinf, $modelo->rconosup, $modelo->rcilindro, $modelo->rconoinf, $distancia);
						break;
					case DEPOSITO_AGUA:
						return $this->VolumenDepositoAgua($modelo->aconosup, $modelo->acilindro, $modelo->aconoinf, $modelo->rcilindro, $distancia);
						break;

					// Por defecto, devolvemos lleno en caso de que no exista ningún tipo
					default:
						return 100;
						break;
				}
			}
			else
				return 100;
		}
		catch (Exception $e)
		{
			LogController::errores('[ENTIDADES RECIPIENTES CALCULAR PORCENTAJE LLENADO] ' . $e->getMessage());
			return 100;
		}
	}

	public function calcularLlenadoRetroActivo($idEntidad)
	{
		try
		{
			$lecturas = DB::select('SELECT id, id_entidad, altura,temperatura,fecha FROM lecturas_silos WHERE id_entidad = ? ORDER BY fecha ASC', [$idEntidad]);
			// $idTipo = DB::select('SELECT id_tipo FROM entidades WHERE id = ?', [$idEntidad])[0]->id_tipo;
			$info = DB::select('SELECT id_tipo, capacidad FROM entidades JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad WHERE id = ?', [$idEntidad])[0];
			$idTipo = $info->id_tipo;
			$capacidad = $info->capacidad;

			if(!empty($lecturas))
			{
				foreach ($lecturas as $lectura)
				{
					$porcentaje = $this->CalcularPorcentajeLlenado($lectura->id_entidad, $lectura->altura);
					$kilos = intval($porcentaje * $capacidad / 100);
					DB::update('UPDATE lecturas_silos SET porcentaje = ?, kilos = ? WHERE id = ?', [$porcentaje, $kilos, $lectura->id]);
				}

				$ultimoDato = $lecturas[count($lecturas) - 1];
				$ultimoDato->temperatura = $ultimoDato->temperatura ? $ultimoDato->temperatura : 0;
				//TODO: Cambiar el 0 por los kilos correctos
				$this->actualizar($idEntidad, $idTipo, $ultimoDato->fecha, $porcentaje, $kilos, $ultimoDato->temperatura);
			}
		}
		catch (Exception $e)
		{
			LogController::errores('[ENTIDADES RECIPIENTES LLENADO RETROACTIVO] ' . $e->getMessage());
			return false;
		}

		return true;		
	}

	private function VolumenDepositoSilo($aconosup, $acilindro, $aconoinf, $rconosup, $rcilindro, $rconoinf, $altura)
	{
		$volumenConoSup = (pi() * $aconosup * (pow($rcilindro, 2) + pow($rconosup, 2) + ($rcilindro * $rconosup))) / 3;
		$volumenCilindro = pi() * $rcilindro * $rcilindro * $acilindro;
		$volumenConoInf = (pi() * $aconoinf * (pow($rcilindro, 2) + pow($rconoinf, 2) + ($rcilindro * $rconoinf))) / 3;

		$volumenTotal = $volumenConoSup + $volumenCilindro + $volumenConoInf;
		// Ya tenemos el volumen total. Ahora vamos a por el actual. Primero, obtenemso los tramos
		$tramo1 = $aconosup;
		$tramo2 = $tramo1 + $acilindro;

		if ($altura >= $tramo2)
		{
			$baseNueva = $rcilindro - $rconoinf;
			$alturaNueva = $aconoinf - ($altura - $tramo2);
			$radioNuevo = ($alturaNueva * $baseNueva / $aconoinf) + $rconoinf;
			$volumen = (pi() * $alturaNueva * (pow($radioNuevo, 2) + pow($rconosup, 2) + ($radioNuevo * $rconosup))) / 3;
		}
		else
		{
			if ($altura >= $tramo1)
			{
				$volumen = (pi() * $rcilindro * $rcilindro * ($tramo2 - $altura)) + $volumenConoInf;
			}
			else
			{
				$baseNueva = $rcilindro - $rconosup;
				$alturaNueva = $tramo1 - $altura;
				$radioNuevo = ($altura * $baseNueva / $aconosup) + $rconosup;
				$volumen = (pi() * $alturaNueva * (pow($rcilindro, 2) + pow($radioNuevo, 2) + ($rcilindro * $radioNuevo))) / 3 + $volumenCilindro + $volumenConoInf;
			}
		}

		// Acotamos el porcentaje entre 0 y 100 para evitar fallos
		return max(0, min(Round(($volumen / $volumenTotal) * 100), 100));
	}

	private function VolumenDepositoAgua($alturaSuperior, $alturaCentral, $alturaInferior, $radio, $altura)
	{
		// Suponemos que la parte superior e inferior son iguales. Lo calculamos ahora y lo aprovechamos para hacer luego las restas
		$volumenElipsoide = (2/3) * pi() * $radio * $radio * $alturaSuperior;
		$volumenCentro = pi() * $radio * $radio * $alturaCentral;

		$volumenTotal = $volumenElipsoide * 2 + $volumenCentro;

		// Ya tenemos el volumen total. Ahora vamos a por el actual. Primero, obtenemso los tramos
		$tramo1 = $alturaSuperior;
		$tramo2 = $tramo1 + $alturaCentral;

		if ($altura >= $tramo2)
		{
			$alturaDesdeAbajo = $altura - $tramo2;
			$volumen = pi() * $radio * $radio * ((2 * $alturaSuperior / 3) - $alturaDesdeAbajo + (pow($alturaDesdeAbajo, 3)/(3 * $alturaSuperior * $alturaSuperior)));
		}
		else
		{
			if ($altura >= $tramo1)
			{
				$volumen = (pi() * $radio * $radio * ($tramo2 - $altura)) + $volumenElipsoide;
			}
			else
			{
				$desdeTramoHastaAltura = $alturaSuperior - $altura;
				$volumenConoSuperior = pi() * $radio * $radio * ((2 * $alturaSuperior / 3) - $desdeTramoHastaAltura + (pow($desdeTramoHastaAltura, 3)/(3 * $alturaSuperior * $alturaSuperior)));
				$volumen = $volumenElipsoide - $volumenConoSuperior + $volumenElipsoide + $volumenCentro;
			}
		}

		// Acotamos el porcentaje entre 0 y 100 para evitar fallos
		return max(0, min(Round(($volumen / $volumenTotal) * 100), 100));
	}

	private function calcularVolumenTotalSilo($aconosup, $acilindro, $aconoinf, $rconosup, $rcilindro, $rconoinf)
	{
		$volumenConoSup = (pi() * $aconosup * (pow($rcilindro, 2) + pow($rconosup, 2) + ($rcilindro * $rconosup))) / 3;
		$volumenCilindro = pi() * $rcilindro * $rcilindro * $acilindro;
		$volumenConoInf = (pi() * $aconoinf * (pow($rcilindro, 2) + pow($rconoinf, 2) + ($rcilindro * $rconoinf))) / 3;

		return $volumenConoSup + $volumenCilindro + $volumenConoInf;
	}

	public function obtenerEspecialidad($id_tipo)
	{
		return json_encode(['estado' => 'OK', 'modelos_recipientes' => $this->obtenerModelosPorEntidad($id_tipo)]);
	}

	private function obtenerModelosPorEntidad($id_tipo)
	{
		switch ($id_tipo)
		{
			case ENTIDAD_SILO: $tipo = 1; break;
			case ENTIDAD_AGUA_DEPOSITOS: $tipo = 2; break;
			default: $tipo = null;
		}
		
		return DB::select("SELECT id, nombre, capacidad FROM entidades_modelos_recipientes WHERE id_tipo = ?", [$tipo]);
	}

	public function obtenerTiposGanado()
	{
		return DB::select("SELECT id, nombre FROM animales_tipos");
	}

	public function reiniciarAcumulado($id)
	{
		try
		{
			$fecha = date('Y-m-d H:i:s');

			DB::update('UPDATE entidades_recipientes SET fecha_reinicio = ? WHERE id_entidad = ?', [$fecha, $id]);

			return response()->json(['estado' => true, 'mensaje' => 'Fecha actualizada correctamente', 'datos' => $fecha], 200);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return json_encode(['estado' => false, 'mensaje' => 'No se ha podido actualizar la fecha, por favor, contacte que nosotros']);
		}
	}
}
