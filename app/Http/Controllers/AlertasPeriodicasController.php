<?php

namespace App\Http\Controllers;

use Exception;
use App\Alerta;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

// define('CLAVE', 'dfqkrVB*GPy_4(aKQGSn');

class AlertasPeriodicasController extends Controller
{
	public function PuertasEstadosProlongadosURL(Request $request)
	{
		$request['clave'] == CLAVE ? $this->PuertasEstadosProlongados() : LogController::errores('Clave incorrecta para activar bot de llenado de silos');
	}

	// Esta función se implementa aquí debido a que la mayoría de alertas se realizan cuando el sensor envía información, mientras que esta es periódica
	private function PuertasEstadosProlongados()
	{
		// 12-9-2019 Así quedaría al usar las constantes
		// SELECT alertas_sensores.id as id, id_alerta, alertas_sensores.id_sensor AS id_sensor, email, parametros, alertas_sensores.nombre AS nombreEmail, sensores.nombre AS nombreSensor, fecha, ultima FROM alertas JOIN sensores_puertas ON alertas_sensores.id_sensor = sensores_puertas.id_sensor JOIN sensores ON alertas_sensores.id_sensor = sensores.id WHERE ((id_alerta = 20402 AND estado = 1) OR (id_alerta = 20404 AND estado = 0)) AND now() > ADDTIME(fecha, (parametros * 100)) AND now() > ADDTIME(ultima, parametros * 100) AND alertas_sensores.activo = 1

		// $alertas = DB::select("SELECT alertas_sensores.id as id, id_alerta, alertas_sensores.id_sensor, email, parametros, alertas_sensores.nombre AS nombreEmail, sensores.nombre AS nombreSensor, fecha, ultima FROM alertas JOIN sensores_puertas ON alertas_sensores.id_sensor = sensores_puertas.id_sensor JOIN sensores ON alertas_sensores.id_sensor = sensores.id WHERE ((id_alerta = ? AND estado = ?) OR (id_alerta = ? AND estado = ?)) AND now() > ADDTIME(fecha, (parametros * 100)) AND now() > ADDTIME(ultima, parametros * 100) AND alertas_sensores.activo = 1", [ALARMA_PUERTAS_APERTURA_PROLONGADA, PUERTA_ABIERTA, ALARMA_PUERTAS_CIERRE_PROLONGADO, PUERTA_CERRADA]);

		$fecha = date('Y/m/d H:i:s');

		foreach ($alertas as $alerta)
		{
			switch ($alerta->id_alerta)
			{
				case ALARMA_PUERTAS_APERTURA_PROLONGADA:
					try
					{
						$data = [
							"nombre" => $alerta->nombreEmail,
							"fecha" => $fecha,
							"idSensor" => $alerta->id_sensor,
							"nombreSensor" => $alerta->nombreSensor,
							"titulo" => "Alerta por apertura prolongada",
							"parametros" => $alerta->parametros,
							"idTipo" => SENSOR_PUERTA
						];
						LogController::enviarEmail($alerta->email, "alertas.puertas.apertura_prolongada", $data, "Puerta $alerta->nombreSensor abierta demasiado tiempo");
						$this->actualizarFechaUltimaAlerta($alerta->id);
					}
					catch (Exception $e)
					{
						LogController::errores($e->getMessage());
					}
					break;
				case ALARMA_PUERTAS_CIERRE_PROLONGADO:
					try
					{
						$data = [
							"nombre" => $alerta->nombreEmail,
							"fecha" => $fecha,
							"idSensor" => $alerta->id_sensor,
							"nombreSensor" => $alerta->nombreSensor,
							"titulo" => "Alerta por cierre prolongado",
							"parametros" => $alerta->parametros,
							"idTipo" => SENSOR_PUERTA
						];
						LogController::enviarEmail($alerta->email, "alertas.puertas.cierre_prolongado", $data, "Puerta $alerta->nombreSensor cerrada demasiado tiempo");
						$this->actualizarFechaUltimaAlerta($alerta->id);
					}
					catch (Exception $e)
					{
						LogController::errores($e->getMessage());
					}
					break;
			}
		}
	}

	// Función que actualiza la fecha de la última vez que se envió la alerta. idAlerta corresponde al id de la tabla de alertas, no al código de alerta
	private function actualizarFechaUltimaAlerta($idAlerta)
	{
		try
		{
			$alertaDB = Alerta::find($idAlerta);
			$alertaDB->ultima = date('Y-m-d H:i:s');
			$alertaDB->save();
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}

	// Actualiza los consumos de todos los recipientse
	private function actualizarConsumos()
	{
		$fecha = date("Y-m-d H:m:s");
		// 13/11/2019 Método antiguo
		/*try
		{
			// Actualiza los consumos con los datos de 1 mes
			DB::update("UPDATE entidades_recipientes SET consumo = (SELECT avg(consumo) consumo FROM (SELECT DATE(fecha) fecha, (max(porcentaje) - min(porcentaje)) consumo, id_sensor FROM lecturas_silos WHERE fecha BETWEEN (DATE_ADD(now(), INTERVAL -15 DAY)) AND now() GROUP BY DATE(fecha), id_sensor ORDER BY id_sensor, fecha ASC) datos WHERE datos.id_sensor = entidades_recipientes.id_sensor GROUP by id_sensor)");
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return json_encode(["estado" => "ERROR", "mensaje" => "No se ha podido actualizar, consulte con el administrador"]);
		}
		return json_encode(["estado" => "OK", "mensaje" => "Consumos actualizados correctamente"]);*/

		// Primero obtener todas las entidades activas
		$entidades = DB::select("SELECT id_entidad FROM entidades_recipientes JOIN entidades ON entidades_recipientes.id_entidad = entidades.id WHERE entidades.fechaBaja IS NULL");

		// Ahora se procede a su actualización
		foreach ($entidades as $entidad)
			app('App\Http\Controllers\RecipientesController')->actualizarConsumoAcumuladoRecipiente($entidad->id_entidad);

		//Actualizamos ahora el histórico de consumos
		app('App\Http\Controllers\RecipientesController')->insertarConsumosEnHistorico();
	}

	public function actualizarConsumosURL(Request $request)
	{
		return $request["clave"] == CLAVE ? $this->actualizarConsumos() : LogController::errores("Clave incorrecta al actualizar los consumos");
	}

	public function Silos_insertadoDefectuoso()
	{
		$datos = 
		[
			"2019-12-11 17:50:08#388C50#d111503ffcfcc0#403#20.25",
			"2019-12-11 18:11:18#388904#d1115c1117053c#403#30"
		];

		$errores = [];

		foreach ($datos as $dato)
		{
			$informacion = explode("#", $dato);
			$fecha = strtotime($informacion[0]);
			$id = $informacion[1];
			$data = $informacion[2];
			$snr = $informacion[4];

			$url = "http://" . $_SERVER["SERVER_NAME"] . "/silos/sigfox/$id/$fecha/0/$snr/$data/false";
			$client = new \GuzzleHttp\Client(['verify' => false, 'http_errors' => false]);
			$request = $client->request('POST', $url, ['headers' => [], 'body' => json_encode([])]);

			if ($request->getStatusCode() != 200)
			{
				$errores[] = "Error en $dato";
			}
		}

		if (count($errores) == 0)
		{
			echo "Todo OK";
		}
		else
		{
			foreach ($errores as $error)
			{
				echo $error . "<br>";
			}
		}
	}

	/*public function TemperaturaHumedad_insertadoDefectuoso()
	{
		$datos = 
		[
			"2019-12-11 17:48:01#79A63#Forbidden#02a5800004281f830c360000#403#15.03",
			"2019-12-11 18:08:22#79A77#Internal Server Error#03c8800008270fbf0ca60000#500#17.46",
			"2019-12-11 18:18:01#79A63#Forbidden#02a6800003e320d30c280000#403#16.91",
			"2019-12-11 17:46:58#2039D6#Forbidden#0c0d800003dc20620c840000#403#14.8",
			"2019-12-11 17:50:16#2039A2#Forbidden#0013800003c41dde0cd70000#403#30",
			"2019-12-11 17:53:58#203819#Forbidden#0013800003c81ed70ced0000#403#14.37",
			"2019-12-11 18:02:17#20398B#Internal Server Error#080b800003ae1e120c770000#500#16.9",
			"2019-12-11 18:09:46#2039DB#Forbidden#051880000146185d0ca80000#403#30",
			"2019-12-11 18:16:58#2039D6#Forbidden#0c0e800003a021160c800000#403#14.89",
			"2019-12-11 18:20:15#2039A2#Forbidden#0014800003901ea50cd80000#403#26.13",
			"2019-12-11 18:23:58#203819#Internal Server Error#00148000039e1f520ceb0000#500#14.32",
			"2019-12-11 18:32:17#20398B#Internal Server Error#080c800003791e9b0c690000#500#18.93"
		];

		$errores = [];

		foreach ($datos as $dato)
		{
			$informacion = explode("#", $dato);
			$fecha = strtotime($informacion[0]);
			$id = $informacion[1];
			$data = $informacion[3];
			$snr = $informacion[5];

			//if ($this->esNevera($id))
			//{
				$url = "http://" . $_SERVER["SERVER_NAME"] . "/temp_hum/sigfox/$id/$fecha/0/$snr/$data";
				$client = new \GuzzleHttp\Client(['verify' => false, 'http_errors' => false]);
				$request = $client->request('POST', $url, ['headers' => [], 'body' => json_encode([])]);

				if ($request->getStatusCode() != 200)
				{
					$errores[] = "Error en $dato";
				}
			//}
		}

		if (count($errores) == 0)
		{
			echo "Todo OK";
		}
		else
		{
			foreach ($errores as $error)
			{
				echo $error . "<br>";
			}
		}
	}

	private function esNevera($device)
	{
		$tipo = DB::select("SELECT id_tipo FROM entidades WHERE id = (SELECT id_entidad FROM entidades_sensores WHERE id_sensor = (SELECT id FROM sensores WHERE dispositivo = ?) AND fechaBaja IS NULL)", [$device]);

		if (!empty($tipo))
		{
			return $tipo[0]->id_tipo == ENTIDAD_NEVERAS ? true : false;
		}
		return false;
	}*/

	// Función que simula emisiones de los sensores de silos falsos. Estos luego se insertarán con el procedimiento habitual en sus entidades falsas relacionadas

	public function activarBotSensoresSilos(Request $request)
	{
		return $request["clave"] == CLAVE ? $this->llenadoSilosFalsos() : LogController::errores("Clave incorrecta en alertas periódicas de puertas");
	}

	private function llenadoSilosFalsos()
	{
	}

	// Ya no hace falta, lo hacemos por comando
	/*public function AlertaNavesIHTURL(Request $request)
	{
		$request['clave'] == CLAVE ? $this->AlertaNavesIHT() : LogController::errores('Clave incorrecta en Alertas NAVES IHT');
	}*/

	// Esta función se implementa aquí debido a que la mayoría de alertas se realizan cuando el sensor envía información, mientras que esta es periódica
	public function AlertaNavesIHT()
	{
		$alertas = app('App\Http\Controllers\EntidadesNavesController')->obtenerAlertasIHT();

		// dd($alertas);

		// Ya tenemos las alertas que deberían saltar. Ahora lo que hacemos es quedarnos con los IDs de las naves a buscar, para ver si cumplen el IHT. De esta forma evitarmos acceder a la DB por cada nave y ahorrar tiempo y costes. Lo hacemos todo en una única consulta
		$naves = [];

		foreach ($alertas as $alerta)
		{
			$idNave = intval($alerta->id_entidad);
			if (!in_array($idNave, $naves))
				$naves[] = $idNave;
		}

		$informacion = app('App\Http\Controllers\EntidadesNavesController')->obtenerIHTNaves($naves, date('Y-m-d'));
		$total = count($informacion); // Aquí para no calcularlo en cada vuelta de bucle y ser mas eficiente
		$fecha = date('Y-m-d H:i:s');

		// Obtenidas ya, ahora recorremos las alertas y si el IHT es elevado, entonces enviamos la notificación
		foreach ($alertas as $alerta)
		{
			$encontrado = false;
			// Buscamos la nave
			for ($i = 0; $i < $total && $encontrado == false; $i++)
			{
				if ($informacion[$i]->id_entidad == $alerta->id_entidad)
				{
					$encontrado = true;
					if ($informacion[$i]->IHTpeligro)
					{
						$parametros = $informacion[$i]->IHTmax . '#' . $informacion[$i]->rango;
						$mensajeNotificacion = 'Se ha detectado IHT elevado (IHTmax ' . $informacion[$i]->IHTmax . ' y rango ' . $informacion[$i]->rango . ") en su nave $alerta->nombreEntidad";

						app('App\Http\Controllers\AlertasController')->enviarAlertaNotificacion($alerta->id_entidad, ALERTA_IHT_PELIGROSO, $alerta->id_usuario, $parametros, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

						if ($alerta->email_alerta && $alerta->email_instantaneo)
							app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->IHTPeligroso($alerta->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $fecha, $parametros);
					}
				}
			}
		}
	}

	public function enviarEmailNoLeidasURL(Request $request)
	{
		$request['clave'] == CLAVE ? $this->enviarEmailNoLeidas() : LogController::errores('Clave incorrecta en Comprobación de alertas no leidas para enviar por mail');
	}

	private function enviarEmailNoLeidas()
	{
		try
		{
			$idsNotificaciones = [];
			$alertas = app('App\Http\Controllers\AlertasController')->obtenerAlertasNoLeidasParaEnviarPorMail();

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_tipo)
				{
					case ENTIDAD_SILO:
						app('App\Http\Controllers\AlertasEmail\AlertasEntidadRecipientesController')->enviarAlertaMail($alerta->id_alerta, $alerta->id_entidad, $alerta->id_tipo, $alerta->nombreEntidad, $alerta->codigo, $alerta->nombreUsuario, $alerta->email, $alerta->fecha, $alerta->parametrosNotificacion, $alerta->parametrosAlerta);
						break;
					case ENTIDAD_PUERTA:
						app('App\Http\Controllers\AlertasEmail\AlertasEntidadPuerta')->enviarAlertaMail($alerta->id_alerta, $alerta->id_entidad, $alerta->nombreEntidad, $alerta->codigo, $alerta->nombreUsuario, $alerta->email, $alerta->fecha, $alerta->parametrosNotificacion, $alerta->parametrosAlerta);
						break;
					case ENTIDAD_AGUA_DEPOSITOS:
						app('App\Http\Controllers\AlertasEmail\AlertasEntidadRecipientesController')->enviarAlertaMail($alerta->id_alerta, $alerta->id_entidad, $alerta->id_tipo, $alerta->nombreEntidad, $alerta->codigo, $alerta->nombreUsuario, $alerta->email, $alerta->fecha, $alerta->parametrosNotificacion, $alerta->parametrosAlerta);
						break;
					case ENTIDAD_NAVES:
						app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->enviarAlertaMail($alerta->id_alerta, $alerta->id_entidad, $alerta->nombreEntidad, $alerta->codigo, $alerta->nombreUsuario, $alerta->email, $alerta->fecha, $alerta->parametrosNotificacion, $alerta->parametrosAlerta);
						break;
					case ENTIDAD_NEVERAS:
						app('App\Http\Controllers\AlertasEmail\AlertasEntidadNeveraController')->enviarAlertaMail($alerta->id_alerta, $alerta->id_entidad, $alerta->nombreEntidad, $alerta->codigo, $alerta->nombreUsuario, $alerta->email, $alerta->fecha, $alerta->parametrosNotificacion, $alerta->parametrosAlerta);
						break;
					case ENTIDAD_NAVES:
						app('App\Http\Controllers\AlertasEmail\AlertasEntidadCeldaController')->enviarAlertaMail($alerta->id_alerta, $alerta->id_entidad, $alerta->nombreEntidad, $alerta->codigo, $alerta->nombreUsuario, $alerta->email, $alerta->fecha, $alerta->parametrosNotificacion, $alerta->parametrosAlerta);
						break;
					case ENTIDAD_ANIMAL:
						app('App\Http\Controllers\AlertasEmail\AlertasEntidadAnimalController')->enviarAlertaMail($alerta->id_alerta, $alerta->id_entidad, $alerta->nombreEntidad, $alerta->codigo, $alerta->nombreUsuario, $alerta->email, $alerta->fecha, $alerta->parametrosNotificacion, $alerta->parametrosAlerta);
						break;
					default:
						LogController::errores('[ALERTAS PERIODICAS ENTIDAD MAIL] TIPO NO ENCONTRADO : ' . json_encode($alerta));
						break;
				}

				// Guardamos el id de la notificación que se enviará por email. Luego con una consulta actualizaremos la tabla notificaciones
				$idsNotificaciones[] = $alerta->id;

				$this->marcarEmailEnviado($idsNotificaciones);
			}
		}
		catch (Exception $e)
		{
			LogController::errores('[ALERTAS PERIODICAS ENTIDAD MAIL] EXCEPCION : ' . $e->getMessage());
		}
	}

	private function marcarEmailEnviado($notificaciones)
	{
		$resultado = DB::update('UPDATE notificaciones SET fecha_email = ? WHERE id IN ' . obtenerInterrogacionesSQL($notificaciones), array_merge([date('Y-m-d H:i:s')], $notificaciones));
	}
}
