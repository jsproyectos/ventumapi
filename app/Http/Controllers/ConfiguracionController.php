<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Excel;
use Storage;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Process\Process;
use App\Http\Controllers\LogController;
set_time_limit(300);

define('CLAVE', 'kQC7o#P9V3sqj(w?mjlb:)]dOwgfY3MmAK_ZTE9K5vq}IeRvn2T_:W7-]JG;8Z/');
define('MIN_CONTRASENA', 10);
define('MAX_CONTRASENA', 15);

class ConfiguracionController extends Controller
{
	public function __construct(){}

	public function generarContrasenaAleatoria()
	{
		// Los que salen por defecto en el teclado de Android e Iphone
		// Tengo que usar arrays en vez de cadenas porque si no luego salen simbolos raros
		$simbolos = ['"', '@', '#', '€', '_', '&', '-', '+', '(', ')', '/', '*', '=', ':', ';', '.', ',', '!', '?'];
		$tamSimbolos = count($simbolos) - 1;
		$numeros = ['0','1','2','3','4','5','6','7','8','9'];
		$tamNumeros = count($numeros) - 1;
		$minusculas = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
		$tamMinusculas = count($minusculas) - 1;
		$mayusculas = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		$tamMayusculas = count($mayusculas) - 1;

		$longitudContrasena = rand(MIN_CONTRASENA, MAX_CONTRASENA);

		$longitudSimbolos = rand(1, 3);
		$longitudNumeros = rand(1, 4);
		$longitudMinusculas = rand(1, $longitudContrasena - $longitudSimbolos - $longitudNumeros);
		$longitudMayusculas = $longitudContrasena - $longitudSimbolos - $longitudNumeros - $longitudMinusculas;

		$simbolosCaracteres = [];
		$numerosCaracteres = [];
		$minusculasCaracteres = [];
		$mayusculasCaracteres = [];

		for ($i = 0; $i < $longitudSimbolos; $i++)
			$simbolosCaracteres[] = $simbolos[rand(0, $tamSimbolos)];

		for ($i = 0; $i < $longitudNumeros; $i++)
			$numerosCaracteres[] = $numeros[rand(0, $tamNumeros)];

		for ($i = 0; $i < $longitudMinusculas; $i++)
			$minusculasCaracteres[] = $minusculas[rand(0, $tamMinusculas)];

		for ($i = 0; $i < $longitudMayusculas; $i++)
			$mayusculasCaracteres[] = $mayusculas[rand(0, $tamMayusculas)];

		$contrasenaAntesDePermutar = array_merge($simbolosCaracteres, $numerosCaracteres, $minusculasCaracteres, $mayusculasCaracteres);
		$contrasena = "";

		// Iniciamos la permutación, para cambiar el orden de los caracteres
		while (!empty($contrasenaAntesDePermutar))
		{
			$indice = rand(0, count($contrasenaAntesDePermutar) - 1);
			$contrasena .= $contrasenaAntesDePermutar[$indice];
			// Eliminamos del array el índice utilizado
			array_splice($contrasenaAntesDePermutar, $indice, 1);
		}

		return $contrasena;
	}

	private function obtenerTodosLosUsuarios()
	{
		return DB::select("SELECT users.id AS id, name AS nombre, users.email as email, created_at, users.role AS role, tipo, empresas.nombre as empresa FROM users JOIN empresas ON (users.id_empresa = empresas.id) JOIN users_tipos ON (users.role = users_tipos.role)");
	}

	private function obtenerTodosLosUsuariosParaAutorizar()
	{
		return DB::select("SELECT users.id AS id, name AS nombre, users.email as email, created_at, users.role AS role, tipo, empresas.nombre as empresa FROM users JOIN empresas ON (users.id_empresa = empresas.id) JOIN users_tipos ON (users.role = users_tipos.role) WHERE users.role NOT IN (1, 2)");
	}

	private function vistaConfiguracionSinPermiso($tipoUsuario)
	{
		return view('configuracion.vacio', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => "Configuracion", "menuConf" => "vacio"]);
	}

	public function mostrarConfiguracion()
	{
		$tipoUsuario = Auth::user()["role"];		
		$nombrePagina = "configuracion";

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
			return $this->mostrarUsuarios();
		else
			return $this->mostrarPerfil();
	}

	public function mostrarUsuarios()
	{
		$tipoUsuario = Auth::user()["role"];		

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$usuarios = $this->obtenerTodosLosUsuarios();
			$tiposUsuario = obtenerTiposUsuario();

			return view('configuracion.usuarios', ["tipoUsuario" => $tipoUsuario, "menu" => "configuracion", "submenu" => "usuarios", "usuarios" => $usuarios, "tiposUsuario" => $tiposUsuario, "tituloPagina" => "Usuarios"]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	public function mostrarPerfil()
	{
		$tipoUsuario = Auth::user()["role"];
		$nombrePagina = "Perfil";

		return view('configuracion.perfil', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => $nombrePagina, "menu" => "configuracion", "submenu" => "perfil"]);
	}

	public function mostrarAlertas()
	{
		$tipos = app('App\Http\Controllers\SensoresController')->obtenerTiposSensores();
		$tipoUsuario = Auth::user()["role"];
		$nombrePagina = "Alertas";

		return view('configuracion.alertas', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => $nombrePagina, "menu" => "configuracion", "submenu" => "alertas", "tipos" => $tipos]);
	}

	public function mostrarEmpresas()
	{
		$tipoUsuario = Auth::user()["role"];		

		$nombrePagina = "Empresas";

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			return view('configuracion.empresas', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => $nombrePagina, "menu" => "configuracion", "submenu" => "empresas"]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	public function mostrarSensores()
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = app('App\Http\Controllers\SensoresController')->obtenerTodosLosSensoresConTipo();
			$tipos = app('App\Http\Controllers\SensoresController')->obtenerTiposSensores();

			return view('configuracion.sensores', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => "Sensores", "sensores" => $sensores, "tipos" => $tipos, "menu" => "configuracion", "submenu" => "sensores"]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	public function mostrarEntidades()
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$entidades = app('App\Http\Controllers\EntidadesController')->obtenerConTipos();
			$tipos = app('App\Http\Controllers\EntidadesController')->obtenerTipos();
			$explotaciones = app('App\Http\Controllers\EntidadesController')->obtenerExplotaciones();
			$tipoGanado = app('App\Http\Controllers\RecipientesController')->obtenerTiposGanado();
			$sensores = app('App\Http\Controllers\SensoresController')->obtenerSensoresParaAsignar();
			$tiposEmision = app('App\Http\Controllers\SensoresController')->obtenerTiposEmision();
			$tiposSensores = app('App\Http\Controllers\SensoresController')->obtenerTiposSensores();

			return view('configuracion.entidades', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => "Entidades", "entidades" => $entidades, "tipos" => $tipos, "explotaciones" => $explotaciones, "tipoGanado" => $tipoGanado, "menu" => "configuracion", "submenu" => "entidades", "sensores" => $sensores, "tiposEmision" => $tiposEmision, "tiposSensores" => $tiposSensores]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	public function mostrarAsignacion()
	{
		$tipoUsuario = Auth::user()["role"];

		$nombrePagina = "configuracion";

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = $this->obtenerSensoresConAsignacion("", "", "*", "");
			$usuarios = $this->obtenerTodosLosUsuarios();
			$tipos = app('App\Http\Controllers\SensoresController')->obtenerTiposSensores();

			return view('configuracion.asignacion', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => $nombrePagina, "sensores" => $sensores, "usuarios" => $usuarios, "tipos" => $tipos, "menu" => "configuracion", "submenu" => "sasignacion"]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	// Carga el menú de configuración que permite añadir, editar o eliminar animales
	public function mostrarAnimales()
	{
		$tipoUsuario = Auth::user()["role"];

		$nombrePagina = "Animales";

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$animales = $this->obtenerAnimales("", "", "", "*", "*", "*");
			$usuarios = $this->obtenerTodosLosUsuarios();
			$sensores = app('App\Http\Controllers\GPSController')->obtenerSensoresSinAsignar();

			dd($sensores);

			return view('configuracion.animales', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => $nombrePagina, "animales" => $animales, "usuarios" => $usuarios, "sensores" => $sensores, "menu" => "configuracion", "submenu" => "animales"]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	public function mostrarAutorizacion()
	{
		$tipoUsuario = Auth::user()["role"];
		$nombrePagina = "configuracion";

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$usuariosParaAutorizar = $this->obtenerTodosLosUsuariosParaAutorizar();
			$sensores = $this->obtenerSensoresConAutorizacion("", "", "*", "", $usuariosParaAutorizar[0]->id, "*");
			$tipos = app('App\Http\Controllers\SensoresController')->obtenerTiposSensores();

			return view('configuracion.autorizacion', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => $nombrePagina, "sensores" => $sensores, "usuarios" => $usuariosParaAutorizar, "tipos" => $tipos, "menu" => "configuracion", "submenu" => "sautorizacion"]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	public function asignarSensores(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = json_decode($request["sensores"]);

			foreach ($sensores as $sensor)
			{
				if ($request["usuario"] == "*")
				{
					$this->eliminarSensor($sensor);
				}
				else
				{
					$this->asignarSensor($request["usuario"], $sensor);
				}
			}

			return json_encode(["estado" => "OK", "mensaje" => "Asignados correctamente " . count($sensores) . " sensores"]);
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
		}
	}

	private function asignarSensor($usuario, $sensor)
	{
		$existe = DB::select("SELECT id_usuario FROM sensores_usuarios WHERE id_sensor = ?", [$sensor]);

		if (count($existe) > 0)
		{
			// Ya existe, por tanto se actualiza
			DB::update("UPDATE sensores_usuarios SET id_usuario = ? WHERE id_sensor = ?", [$usuario, $sensor]);
		}
		else
		{
			// No existe, se inserta
			DB::insert("INSERT INTO sensores_usuarios (id_sensor, id_usuario) VALUES (?, ?)", [$sensor, $usuario]);
		}
	}

	private function eliminarSensor($sensor)
	{
		DB::delete("DELETE FROM sensores_usuarios WHERE id_sensor = ?", [$sensor]);
	}

	public function autorizarSensores(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = json_decode($request["sensores"]);

			if (!empty($sensores))
			{
				$cuantos = 0;
				foreach ($sensores as $sensor)
				{
					$cuantos += $this->autorizarSensor($request["usuario"], $sensor);
				}

				return json_encode(["estado" => "OK", "mensaje" => "Se han autorizado correctamente " . $cuantos . " sensores"]);
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No hay sensores seleccionados para autorizar"]);
			}
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
		}
	}

	private function autorizarSensor($usuario, $sensor)
	{
		// Primero se compruebasi existe, y si no, se inserta
		$existe = DB::select("SELECT id_usuario, id_sensor FROM sensores_usuarios_autorizados WHERE id_sensor = ? AND id_usuario = ?", [$sensor, $usuario]);

		if (count($existe) == 0)
		{
			// No existe, se inserta
			DB::insert("INSERT INTO sensores_usuarios_autorizados (id_sensor, id_usuario) VALUES (?, ?)", [$sensor, $usuario]);
			return 1;
		}
		else
			return 0;
	}

	public function desautorizarSensores(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = json_decode($request["sensores"]);

			if (!empty($sensores))
			{
				$cuantos = $this->eliminarSensoresAutorizadosPorUsuario($sensores, $request["usuario"]);
				return json_encode(["estado" => "OK", "mensaje" => "Se han desautorizado correctamente " . $cuantos . " sensores"]);
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No hay sensores seleccionados para desautorizar"]);
			}
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
		}
	}

	private function eliminarSensoresAutorizadosPorUsuario($sensores, $usuario)
	{
		// Primero se generan tantas ? como elementos haya en el array. Empieza desde 1 porque si estamos aquí sabemos que como mínimo hay un elemento en el array.

		$elementosABorrar = "?";
		$totalElementos = count($sensores);

		for($i = 1; $i < $totalElementos; $i++)
		{
			$elementosABorrar .= ",?";
		}

		// Añadimos el id de usuario al array de sensores, que luego se pasará a la consulta como un solo array con todos los valores que tiene que tomar.
		$sensores[] = $usuario;

		return DB::delete("DELETE FROM sensores_usuarios_autorizados WHERE id_sensor IN (".$elementosABorrar.") AND id_usuario = ?", $sensores);
	}

	public function mostrarExcel()
	{
		$tipoUsuario = Auth::user()["role"];		

		$nombrePagina = "configuracion";

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			return view('configuracion.excel', ["tipoUsuario" => $tipoUsuario, "tituloPagina" => $nombrePagina, "menu" => "configuracion", "submenu" => "excel"]);
		}
		else
		{
			return $this->vistaConfiguracionSinPermiso($tipoUsuario);
		}
	}

	public function convertirMuestraAExcel(Request $request)
	{
		$punteroLecturaFichero = fopen($request->ficheroMonta,"r");
		$coordenadas[] = [];

		// Eliminamos la extensión en mayúsculaso en minúsculas
		$nombreFichero = str_replace(".TXT", "", $request->ficheroMonta->getClientOriginalName());
		$nombreFichero = str_replace(".txt", "", $nombreFichero);

		while(!feof($punteroLecturaFichero))
		{
			$linea = fgets($punteroLecturaFichero);
			$lineaFormateada = str_replace(" ","",$linea);

			list($x, $y, $z) = sscanf($lineaFormateada, "X:%dY:%dZ:%d\n");
			$coordenadas[] = ["x" => $x, "y" => $y, "z" => $z];
		}

		fclose($punteroLecturaFichero);
		unset($coordenadas[0]); // Eliminar el primer elemento, el cual es vacío

		Excel::create($nombreFichero, function($excel) use($coordenadas)
		{
			$excel->setTitle("Coordenadas del sensor");
			$excel->setCreator('Coordenadas')->setCompany('Ventum IDC');

			$excel->sheet('Coordenadas', function($sheet) use($coordenadas)
			{
				// Se establece el formato de las columnas. En este caso, la primera, para que los crotales salgan correctamente
				$sheet->setColumnFormat(array(
					'A' => '0',
					'B' => '0',
					'C' => '0'
				));

				// Se genera la primera fila, la que es la cabecera
				$cabecera = ["X", "Y", "Z",];

				$sheet->row(1, $cabecera);
				$fila = 2;

				foreach ($coordenadas as $coordeanda)
				{
					$sheet->row($fila, [$coordeanda["x"], $coordeanda["y"], $coordeanda["z"]]);
					$fila++;
				}
			});
		})->export('xlsx');

		return redirect('/configuracion');
	}

	public function nuevoUsuario(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
				'nombre' => ['required', 'string', 'max:255'],
				'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
				'password' => ['required', 'string', 'min:6'],
				'role' => ['required', 'string']
			]);

			if ($validator->fails())
			{
				return response()->json(erroresFormularioJSON($validator->errors()->all()), 500);
			}
			else
			{
				$usuario = User::create([
					'name' => $request['nombre'],
					'email' => $request['email'],
					'password' => Hash::make($request['password']),
					'role' => $request['role']
				]);

				// Ahora damos de alta el usuario en la tabla de categorías visuales, sin categorías visuales
				DB::insert("INSERT INTO categorias_visuales_usuario (id_usuario) VALUES (?)", [$usuario->id]);

				// Si se ha solicitado la activación, enviamos un correo con los datos de la cuenta. Se realiza de forma asíncrona
				if ($request["activacion"])
					dispatch(new \App\Jobs\nuevoUsuario($request["nombre"], $request["email"], $request["password"]));

				return response()->json(['mensaje' => "Se ha creado correctamente el usuario " . $request['email']], 200);
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['mensaje' => $e->getMessage()], 500);
		}
	}

	public function obtenerUsuarios()
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$usuarios = $this->obtenerTodosLosUsuarios();

			return json_encode(["estado" => "OK", "mensaje" => $usuarios]);
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para ver esta información"]);
		}
	}

	public function buscarUsuarios(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$criterioBusqueda = '%' . $request["nombre"] . '%';

			$usuarios = DB::select("SELECT users.id AS id, name AS nombre FROM users LEFT JOIN empresas ON users.id_empresa = empresas.id WHERE name LIKE ? OR users.email LIKE ? OR empresas.nombre LIKE ?", [$criterioBusqueda, $criterioBusqueda, $criterioBusqueda]);

			return json_encode(["estado" => "OK", "mensaje" => $usuarios]);
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para ver esta información"]);
		}
	}

	public function editarUsuario(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
				'nombre' => ['required', 'string', 'max:255'],
				'email' => ['required', 'string', 'email', 'max:255'],
				'password' => ['string', 'min:6'],
				'role' => ['required', 'string']
			]);

			if ($validator->fails())
			{
				return json_encode(erroresFormulario($validator->errors()->all()));
			}
			else
			{
				$usuario = User::find($request["id"]);

				$usuario->name = $request['nombre'];
				$usuario->email = $request['email'];
				$usuario->role = $request['role'];

				if($request['password'])
					$usuario->password = bcrypt($request["password"]);

				if($usuario->save())
					return response()->json(['mensaje' => 'Se ha modificado correctamente el usuario '. $request["email"]], 200);
				else
					return response()->json(['mensaje' => "No se ha podido modificar el usuario. Por favor, contacte con el administrador"], 500);
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['mensaje' => $e->getMessage()], 500);
		}
	}

	public function borrarUsuario(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$usuario = User::find($request["id"]);

			if($usuario->delete())
			{
				return json_encode(["estado" => "OK", "mensaje" => "Usuario " . $request["email"] . " borrado correctamente"]);
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No se ha podido eliminar el usuario. Por favor, contacte con el administrador"]);
			}
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
		}
	}

	public function obtenerSensores()
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = app('App\Http\Controllers\SensoresController')->obtenerTodosLosSensoresConTipo();

			return json_encode(["estado" => "OK", "mensaje" => $sensores]);
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para ver esta información"]);
		}
	}

	private function obtenerSensoresConAsignacion($nombre, $dispositivo, $tipo, $propietario)
	{
		$nombre = '%' . $nombre . '%';
		$dispositivo = '%' . $dispositivo . '%';

		// Si tipo es cualquiera
		if ($tipo == '*')
		{
			if ($propietario == "")
			{
				return DB::select("SELECT sensores.id AS id, dispositivo, sensores.nombre AS nombre, descripcion, latitud, longitud, bateria, snr, id_tipo, tipos_sensores.nombre as tipo, users.name as nombreUsuario FROM sensores JOIN tipos_sensores ON sensores.id_tipo = tipos_sensores.id LEFT JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN users ON sensores_usuarios.id_usuario = users.id WHERE sensores.nombre LIKE ? AND dispositivo LIKE ? ORDER BY id_tipo, nombre ASC", [$nombre, $dispositivo]);
			}
			else
			{
				$propietario = '%' . $propietario . '%';

				return DB::select("SELECT sensores.id AS id, dispositivo, sensores.nombre AS nombre, descripcion, latitud, longitud, bateria, snr, id_tipo, tipos_sensores.nombre as tipo, users.name as nombreUsuario FROM sensores JOIN tipos_sensores ON sensores.id_tipo = tipos_sensores.id LEFT JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN users ON sensores_usuarios.id_usuario = users.id WHERE sensores.nombre LIKE ? AND dispositivo LIKE ? AND users.name LIKE ? ORDER BY id_tipo, nombre ASC", [$nombre, $dispositivo, $propietario]);
			}

		}
		else
		{
			if ($propietario == "")
			{
				return DB::select("SELECT sensores.id AS id, dispositivo, sensores.nombre AS nombre, descripcion, latitud, longitud, bateria, snr, id_tipo, tipos_sensores.nombre as tipo, users.name as nombreUsuario FROM sensores JOIN tipos_sensores ON sensores.id_tipo = tipos_sensores.id LEFT JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN users ON sensores_usuarios.id_usuario = users.id WHERE sensores.nombre LIKE ? AND dispositivo LIKE ? AND id_tipo = ? ORDER BY id_tipo, nombre ASC", [$nombre, $dispositivo, $tipo]);
			}
			else
			{
				return DB::select("SELECT sensores.id AS id, dispositivo, sensores.nombre AS nombre, descripcion, latitud, longitud, bateria, snr, id_tipo, tipos_sensores.nombre as tipo, users.name as nombreUsuario FROM sensores JOIN tipos_sensores ON sensores.id_tipo = tipos_sensores.id LEFT JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN users ON sensores_usuarios.id_usuario = users.id WHERE sensores.nombre LIKE ? AND dispositivo LIKE ? AND users.name LIKE ? AND id_tipo = ? ORDER BY id_tipo, nombre ASC", [$nombre, $dispositivo, $propietario, $tipo]);
			}
		}
	}

	private function obtenerSensoresConAutorizacion($nombre, $dispositivo, $tipo, $propietario, $usuario, $autorizacion)
	{
		$nombre = '%' . $nombre . '%';
		$dispositivo = '%' . $dispositivo . '%';
		$tipo = $tipo == '*' ? '%%' : $tipo;
		$propietario = '%' . $propietario . '%';

		if ($autorizacion == 's')
		{
			$sensores = DB::select("SELECT DISTINCT * FROM ( SELECT sensores.id AS id, dispositivo, sensores.nombre AS nombre, descripcion, latitud, longitud, bateria, snr, id_tipo, tipos_sensores.nombre as tipo, IFNULL(users.name, '') as propietario FROM sensores JOIN tipos_sensores ON sensores.id_tipo = tipos_sensores.id LEFT JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN users ON sensores_usuarios.id_usuario = users.id LEFT JOIN sensores_usuarios_autorizados ON sensores.id = sensores_usuarios_autorizados.id_sensor WHERE sensores.nombre LIKE ? AND dispositivo LIKE ? AND id_tipo LIKE ? AND sensores_usuarios_autorizados.id_usuario = ?) AS consulta WHERE consulta.propietario LIKE ? ORDER BY id_tipo, nombre ASC", [$nombre, $dispositivo, $tipo, $usuario, $propietario]);

			foreach ($sensores as $sensor)
			{
				$sensor->autorizado = true;
			}
			return $sensores;
		}
		else
		{
			$sensoresProcesados = [];
			$sensores = DB::select("SELECT DISTINCT * FROM ( SELECT sensores.id AS id, dispositivo, sensores.nombre AS nombre, descripcion, latitud, longitud, bateria, snr, id_tipo, tipos_sensores.nombre as tipo, IFNULL(users.name, '') as propietario FROM sensores JOIN tipos_sensores ON sensores.id_tipo = tipos_sensores.id LEFT JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN users ON sensores_usuarios.id_usuario = users.id WHERE sensores.nombre LIKE ? AND dispositivo LIKE ? AND id_tipo LIKE ? AND sensores.id NOT IN (SELECT id_sensor FROM sensores_usuarios WHERE id_usuario = ?)) AS consulta WHERE consulta.propietario LIKE ? ORDER BY id_tipo, nombre ASC", [$nombre, $dispositivo, $tipo, $usuario, $propietario]);
			switch ($autorizacion)
			{
			// Recoge solo los sensores que no tienes autorizados
				case 'n':
					foreach ($sensores as $sensor)
					{
						if (!$this->estaAutorizado($sensor->id, $usuario))
						{
							$sensor->autorizado = false;
							$sensoresProcesados[] = $sensor;
						}
					}
				break;
			// Recoge todos los sensores. Se pone en default como medida de seguridad ante cualquier valor extraño de entrada.
			default:
					foreach ($sensores as $sensor)
					{
						$sensor->autorizado = $this->estaAutorizado($sensor->id, $usuario);
						$sensoresProcesados[] = $sensor;
					}
				break;
			}
			return $sensoresProcesados;
		}
	}

	private function estaAutorizado($sensor, $usuario)
	{
		$sensorAutorizado = DB::select("SELECT id_sensor, id_usuario FROM sensores_usuarios_autorizados WHERE id_sensor = ? AND id_usuario = ?", [$sensor, $usuario]);

		if (count($sensorAutorizado) == 0)
			return false;
		return true;
	}

	public function obtenerSensoresConAsignacionAJAX(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = $this->obtenerSensoresConAsignacion($request["nombre"], $request["dispositivo"], $request["tipo"], $request["propietario"]);

			return json_encode(["estado" => "OK", "mensaje" => $sensores]);
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para ver esta información"]);
		}
	}

	public function obtenerSensoresConAutorizacionAJAX(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensores = $this->obtenerSensoresConAutorizacion($request["nombre"], $request["dispositivo"], $request["tipo"], $request["propietario"], $request["usuario"], $request["autorizado"]);

			return json_encode(["estado" => "OK", "mensaje" => $sensores]);
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para ver esta información"]);
		}
	}

	// Obtienen los animales por AJAX con los filtros servidos por parámetro. En caso de ser un usuario no administrador utiliza su propio id para filtrarlos de todas las tablas. Un administrador puede filtrar el que quiera
	public function obtenerAnimalasAJAX(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			return json_encode(["estado" => "OK", "mensaje" => $this->obtenerAnimales($request["crotal"], $request["nombre"], $request["edad"], $request["sexo"], $request["tipo"], $request["propietario"])]);
		}
		else
		{
			return json_encode(["estado" => "OK", "mensaje" => $this->obtenerAnimales($request["crotal"], $request["nombre"], $request["edad"], $request["sexo"], $request["tipo"], Auth::user()["id"])]);
		}
	}

	// Obtiene la lista de animales aplicando los filtros correspondientes
	private function obtenerAnimales($crotal, $nombre, $edad, $sexo, $tipo, $propietario)
	{
		// Primero: Se inicializa el vector de búsqueda y las variables de búsqueda
		$vectorBusqueda = [];
		$busquedaEdad = "";
		$busquedaSexo = "";
		$busquedaTipo = "";
		$busquedaPropietario = "";

		// Segundo: Se formatean los datos de entrada para que se construya la consulta de forma sencilla y se insertan en el vector de búsqueda
		$vectorBusqueda[] = '%' . $crotal . '%';
		$vectorBusqueda[] = '%' . $nombre . '%';

		// Ahora se generan otros criterios de búsqueda
		if ($edad != "")
		{
			$busquedaEdad = " AND edad = ?";
			$vectorBusqueda[] = $edad;
		}

		if ($sexo == "H" || $sexo == "M")
		{
			$busquedaSexo = " AND sexo = ?";
			$vectorBusqueda[] = $sexo;
		}

		if ($tipo != "*")
		{
			$busquedaTipo = " AND id_tipo = ?";
			$vectorBusqueda[] = $tipo;
		}

		// Esta función también nos permitirá jugar a la hora de visualizar los animales si se trata de un cliente o un administrador. Un cliente solo podrá ver sus animales
		if ($propietario != "*")
		{
			$busquedaPropietario = " AND id_usuario = ?";
			$vectorBusqueda[] = $propietario;
		}

		return DB::select("SELECT animales.id AS id, crotal, animales.nombre AS nombre, descripcion, edad, sexo, id_tipo, id_usuario, users.email AS email, users.name AS nombrePropietario, empresas.nombre AS nombreEmpresa FROM animales LEFT JOIN users ON animales.id_usuario = users.id LEFT JOIN empresas ON users.id_empresa = empresas.id WHERE crotal LIKE ? AND animales.nombre LIKE ?" . $busquedaEdad . $busquedaSexo . $busquedaTipo . $busquedaPropietario, $vectorBusqueda);		
	}

	public function cambiarContrasena(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
				'contrasenaAntigua' => 'required',
				'contrasenaNueva' => 'required',
				'contrasenaNuevaRepetida' => 'required'
			]);

			if ($validator->fails())
				return response()->json(["estado" => false, "mensaje" => erroresFormulario($validator->errors()->all())], 200);
			else
			{
				if ($request["contrasenaNueva"] != $request["contrasenaNuevaRepetida"])
					return response()->json(["estado" => false, "mensaje" => "La contraseña nueva no coincide. Por favor, escriba las dos iguales"], 200);
				else
				{
					if (Hash::check($request["contrasenaAntigua"], Auth::user()->password))
					{
						$user = User::find(Auth::user()->id);
						if($user->where("email", "=", Auth::user()->email)->update(["password" => bcrypt($request["contrasenaNueva"])]))
						{
							// TODO: A lo mejor habría que hacer que el usuario se deslogeara del servidor
							return response()->json(["estado" => true, "mensaje" => "La contraseña se ha cambiado correctamente. Por favor, vuelva a iniciar sesión"], 200);
						}
						else
							return response()->json(["estado" => false, "mensaje" => "Ha ocurrido un error al cambiar la contraseña. Por favor, intentelo de nuevo mas tarde"], 200);
					}
					else
						return response()->json(["estado" => false, "mensaje" => "La contraseña actual no es la correcta. Por favor, escriba la contraseña correcta"], 200);
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "No se han podido obtener. Por favor, contacte con nostros"], 200);
		}
	}

	public function recuperarContrasena(Request $request)
	{
		try
		{
			$email = $request['email'];
			$contrasena = $this->generarContrasenaAleatoria();

			// $email = 'jlproyectos@ventumidc.es';

			$user = User::where('email', $email)->first();
			$user->where('email', '=', $email)->update(['password' => bcrypt($contrasena)]);

			$datos = ['nombre' => $user->name, 'password' => $contrasena];

			LogController::enviarEmail($email, 'alertas.recuperar_password', $datos, 'Nueva contraseña para Gavilán Control');

			return response()->json(['estado' => true, 'mensaje' => 'Se ha generado una nueva contraseña y se ha enviado al correo solicitado'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[RECUPERAR CONTARSEÑA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Se ha generado una nueva contraseña y se ha enviado al correo solicitado'], 200);	
		}
	}

	// Acciones del importador

	public function mostrarImportador()
	{
		return view("importar");	
	}

	public function importarElementos(Request $request)
	{
		/*$contador = 0;
		$pregunta = null;
		$correcta = null;
		$respuesta1 = null;
		$respuesta2 = null;
		$respuesta3 = null;

		foreach(file($request->excel) as $line)
		{
			$line = str_replace("\r", "", $line);
			$line = str_replace("\n", "", $line);
			$line = str_replace("\"", "'", $line);

			switch ($contador) {
				case 0:
					$pregunta = $line;
					$contador += 1;
					break;
				case 1:
					$correcta = $line;
					$contador += 1;
					break;
				case 2:
					$respuesta1 = $line;
					$contador += 1;
					break;
				case 3:
					$respuesta2 = $line;
					$contador += 1;
					break;
				case 4:
					$respuesta3 = $line;
					$contador = 0;
					echo "{p: \"$pregunta\", r: [{c: \"$correcta\"}, \"$respuesta1\", \"$respuesta2\", \"$respuesta3\"]},<br>";
					break;			
				default:
					# code...
					break;
			}
		}*/

		foreach(file($request->datos) as $linea)
		{
			//Eliminamos los caracteres de retorno de carro y fin de línea
			$linea = str_replace("\r", "", $linea);
			$linea = str_replace("\n", "", $linea);

			$elementos = explode("#", $linea);
			//$fecha = strtotime($elementos[0]);

			$idSensor = app('App\Http\Controllers\SensoresSigfoxController')->obtenerIDSensor($elementos[1]);

			// Insertar en la Base de datos
			$fecha = $elementos[0];
			$snr = $elementos[3];
			$data = $elementos[2];

			$trama = app('App\Http\Controllers\GPSController')->decodificarTramaGPS($data);

			echo "INSERT INTO lecturas_gps (id_sensor, fecha, snr, latitud, longitud, bateria, flag_configuracion, flag_movimiento, flag_alarma_digital, flag_entrada_digital, flag_cobertura, flag_satelites, velocidad) VALUES ($idSensor, '$fecha', $snr, " . $trama["LATITUDE"]. ", " . $trama["LONGITUDE"]. ", " . $trama["BATTERY"]. ", " . $trama["NSEQ_CONFIG"]. ", " . $trama["IRQ_ACC_FLAG"]. ", " . $trama["IRQ_DI_FLAG"]. ", " . $trama["DI_VALUE"]. ", " . $trama["LAST_GOOD_POSITION_FLAG"]. ", " . $trama["NUMBER_OF_SATELLITES"]. ", " . $trama["SPEED_IN_KMPH"] . ");<br>";
		}

		return "OK";
	}

	public function mostrarCola()
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
		{
			$colas = DB::select("SELECT * FROM jobs");

			return view('configuracion.colas', ["tipoUsuario" => Auth::user()["role"], "tituloPagina" => "Cola de tareas", "menu" => "configuracion", "submenu" => "colas", "colas" => $colas]);
		}
		else
			return $this->vistaConfiguracionSinPermiso(Auth::user()["role"]);
	}

	public function borrarColas($id)
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
		{
			$id == '*' ? DB::table("jobs")->truncate() : DB::delete("DELETE FROM jobs WHERE id = ?", [$id]);
		}

		return redirect(route("colas"));
	}

	public function mostrarRegistro()
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
		{
			if (File::exists("../storage/app/" . LogController::GET_LOG_MENSAJES()))
				$log = File::get("../storage/app/" . LogController::GET_LOG_MENSAJES());
			else
				$log = "";

			return view('configuracion.log', ["tipoUsuario" => Auth::user()["role"], "tituloPagina" => "Registro", "menu" => "configuracion", "submenu" => "registro", "log" => $log, "vaciar" => route("registro") . "/b"]);
		}
		else
			return $this->vistaConfiguracionSinPermiso(Auth::user()["role"]);
	}

	public function borrarRegistro()
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
			LogController::borrar_log_mensajes();

		return redirect(route("registro"));
	}

	public function mostrarErrores()
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
		{
			if (File::exists("../storage/app/" . LogController::GET_LOG_ERRORES()))
				$log = File::get("../storage/app/" . LogController::GET_LOG_ERRORES());
			else
				$log = "";

			return view('configuracion.log', ["tipoUsuario" => Auth::user()["role"], "tituloPagina" => "Errores", "menu" => "configuracion", "submenu" => "errores", "log" => $log, "vaciar" => route("errores") . "/b"]);
		}
		else
			return $this->vistaConfiguracionSinPermiso(Auth::user()["role"]);
	}

	public function borrarErrores()
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
			LogController::borrar_log_errores();

		return redirect(route("errores"));
	}

	public function mostrarErroresFiware()
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
		{
			if (File::exists("../storage/app/" . LogController::GET_LOG_ERRORES_FIWARE()))
				$log = File::get("../storage/app/" . LogController::GET_LOG_ERRORES_FIWARE());
			else
				$log = "";

			return view('configuracion.log', ["tipoUsuario" => Auth::user()["role"], "tituloPagina" => "Errores Fiware", "menu" => "configuracion", "submenu" => "erroresf", "log" => $log, "vaciar" => route("erroresf") . "/b"]);
		}
		else
			return $this->vistaConfiguracionSinPermiso(Auth::user()["role"]);
	}

	public function borrarErroresFiware()
	{
		if (Auth::user()["role"] == USUARIO_SUPERADMINISTRADOR || Auth::user()["role"] == USUARIO_ADMINISTRADOR)
			LogController::borrar_log_errores_fiware();

		return redirect(route("erroresf"));
	}

	// Comando para lanzar:
	// curl -d "authorization=kQC7o#P9V3sqj(w?mjlb:)]dOwgfY3MmAK_ZTE9K5vq}IeRvn2T_:W7-]JG;8Z/" -X POST https://api.ventumidc.es/api/exportar_db > backup.sql
	public function exportarDB(Request $request)
	{
		try
		{
			if ($request['authorization'] == CLAVE)
			{
				//$comando = 'mysqldump -h localhost -u homestead -psecret ventum_db > ../storage/app/backup_ventum_db.sql';
				$comando = '/opt/bitnami/mysql/bin/mysqldump -u root -h localhost -p7uO6g6pltmP7 ventum_db > ../storage/app/backup_ventum_db.sql';
				$process = new Process($comando);
				$process->run();

				return Storage::download('backup_ventum_db.sql');
			}
			else
			{
				LogController::errores("Clave incorrecta para descargar el backup de la db");
				return response()->json(['estado' => FALSE, 'mensaje' => 'Clave incorrecta'], 403);
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[DESCARGA BACKUP_DB] " . $e->getMessage());
			return response()->json(['estado' => FALSE, 'mensaje' => 'Ha ocurrido un error al descargar la DB'], 500);
		}
	}


	public function obtenerConfiguracion()
	{
		try
		{
			$config = DB::select('SELECT email_alerta, email_instantaneo, email_tiempo, notificaciones_movil FROM configuracion WHERE id_usuario = ?', [Auth::user()['id']])[0];

			$config->email_alerta = $config->email_alerta == '1' ? true : false;
			$config->email_instantaneo = $config->email_instantaneo == '1' ? true : false;
			$config->notificaciones_movil = $config->notificaciones_movil == '1' ? true : false;

			return response()->json(['estado' => true, 'data' => $config], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER CONFIGURACION] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 500);
		}
	}

	public function actualizarConfiguracion(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
				'email_tiempo' => ['nullable', 'numeric']
			]);

			if ($validator->fails())
				return response()->json(['estado' => false, 'mensaje' => 'Se han encontrado errores: Tiempo de alerta email debe ser un número'], 200);

			$email_alerta = $request['email_alerta'] == 'true' ? 1 : 0;
			$email_instantanoe = $request['email_instantaneo'] == 'true' ? 1 : 0;
			$email_tiempo = isset($request['email_tiempo']) ? $request['email_tiempo'] : 30;
			$notificaciones_movil = $request['notificaciones_movil'] == 'true' ? 1 : 0;

			DB::update('UPDATE configuracion SET email_alerta = ?, email_instantaneo = ?, email_tiempo = ?, notificaciones_movil = ? WHERE id_usuario = ?', [$email_alerta, $email_instantaneo, $email_tiempo, $notificaciones_movil, Auth::user()['id']]);

			return response()->json(['estado' => true, 'mensaje' => 'Configuración actualizada correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[ACUALIZAR CONFIGURACION] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 500);
		}
	}

	public function obtenerPerfil()
	{
		try
		{
			$config = DB::select('SELECT email_alerta, email_instantaneo, email_tiempo, notificaciones_movil FROM configuracion WHERE id_usuario = ?', [Auth::user()['id']])[0];

			$config->email_alerta = $config->email_alerta == '1' ? true : false;
			$config->email_instantaneo = $config->email_instantaneo == '1' ? true : false;
			$config->notificaciones_movil = $config->notificaciones_movil == '1' ? true : false;

			$alertas = app('App\Http\Controllers\AlertasController')->obtenerAlertasGenerales();

			return response()->json(['estado' => true, 'config' => $config, 'alertas' => $alertas], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER CONFIGURACION] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 500);
		}
	}

	public function actualizarPerfil(Request $request)
	{
		try
		{
			// Si hay configuración, la actualizamos
			if ($request['config'])
			{
				$config = json_decode($request['config']);

				$contenidoParaValidar = array('email_tiempo' => $config->email_tiempo);

				if ($config->email_tiempo && !is_numeric($config->email_tiempo))
					return response()->json(['estado' => false, 'mensaje' => 'Error: Tiempo de alerta email debe ser un número'], 200);

				// TODO: Corregir
				/*$validator = Validator::make($config, [
					'email_tiempo' => ['nullable', 'numeric']
				]);

				if ($validator->fails())
					return response()->json(['estado' => false, 'mensaje' => 'Se han encontrado errores: Tiempo de alerta email debe ser un número'], 200);*/

				$email_alerta = $config->email_alerta == 'true' ? 1 : 0;
				$email_instantaneo = $config->email_instantaneo == 'true' ? 1 : 0;
				$email_tiempo = isset($config->email_tiempo) ? $config->email_tiempo : 30;
				$notificaciones_movil = $config->notificaciones_movil == 'true' ? 1 : 0;

				DB::update('UPDATE configuracion SET email_alerta = ?, email_instantaneo = ?, email_tiempo = ?, notificaciones_movil = ? WHERE id_usuario = ?', [$email_alerta, $email_instantaneo, $email_tiempo, $notificaciones_movil, Auth::user()['id']]);
			}

			if ($request['alertas'])
			{
				// Primero obtenemos los datos del usuario logeado
				$usuario = Auth::user();
				$alertas = json_decode($request['alertas']);

				// En caso de que queramos colocar la alerta a otro usuario (si somos un administrador) lo indicamos en el idUsuario, que será el usuario real objetivo de esa alerta. En caso de no ser administrador o no indicar el usuario objetivo, será el usuario logeado.
				$idUsuario = $request['idUsuarioDestino'] && esAdmin($usuario['role']) ? $request['idUsuarioDestino'] : $usuario['id'];

				// Array donde iremos guardando los errores
				$alertasErroneas = [];

				foreach ($alertas as $alerta)
				{
					// Control de errores
					if ($alerta->activo == true && ($alerta->parametros == '' || $alerta->parametros == null ))
						$alertasErroneas[] = DB::select('SELECT nombre FROM alertas_tipos WHERE id  = ?', [$alerta->tipo_alerta])[0]->nombre;
					else
						app('App\Http\Controllers\AlertasController')->agregarAlertaGeneral($idUsuario, $alerta, ENTIDAD_ANIMAL);
				}

				if (!empty($alertasErroneas))
					return response()->json(['estado' => false, 'mensaje' => 'Faltan valores en las siguientes alertas: ' . concatenarErrores($alertasErroneas)], 200);
			}

			// Procedemos a cargar las alertas actualizadas para devolverlas en la response
			$alertas = app('App\Http\Controllers\AlertasController')->filtrarAlertasSinValor(app('App\Http\Controllers\AlertasController')->obtenerAlertasDelPropietarioEntidadGeneral(ENTIDAD_ANIMAL));

			return response()->json(['estado' => true, 'mensaje' => 'Perfil actualizado correctamente', 'alertas' => $alertas], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[ACUALIZAR PERFIL] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 500);
		}
	}

	private function obtenerTipo($tipo)
	{
		switch ($tipo)
		{
			case 'entidad': return ENTIDAD; break;
			case 'sensor': return SENSOR; break;
			default: return null;
		}
	}

	private function anotaciones($tipo, $id)
	{
		return DB::select('SELECT anotaciones.id id, fecha, tipo, id_usuario, name, nota FROM anotaciones JOIN users ON anotaciones.id_usuario = users.id WHERE tipo_ref = ? AND id_referencia = ? AND anotaciones.fechaBaja IS NULL', [$tipo, $id]);
	}

	public function anotacionesObtener($tipo, $id)
	{
		try
		{
			if (esAdmin(Auth::user()['role']))
			{
				$tipo = $this->obtenerTipo($tipo);

				if ($tipo === null)
					return response()->json(['estado' => false, 'mensaje' => 'No se ha indicado el tipo. Debe ser sensor o entidad'], 400);		

				return response()->json(['estado' => true, 'datos' => $this->anotaciones($tipo, $id)], 200);
			}
			else
			{
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esa acción'], 403);	
			}

		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Error desconocido, consulte con el administrador'], 500);
		}
	}

	public function anotacionesInsertar($tipo, $id, Request $request)
	{
		try
		{
			if (esAdmin(Auth::user()['role']))
			{
				$tipo = $this->obtenerTipo($tipo);

				if ($tipo === null)
					return response()->json(['estado' => false, 'mensaje' => 'No se ha indicado el tipo. Debe ser sensor o entidad'], 400);	

				// Insertamos en la base de datos
				DB::insert('INSERT INTO anotaciones (tipo_ref, id_referencia, tipo, id_usuario, nota) VALUES (?, ?, ?, ?, ?)', [$tipo, $id, $request['tipoAnotacion'], Auth::user()['id'], $request['nota']]);

				// Si todo ha ido bien, devolvemos la totalidad de las anotaciones para mostrarlas
				return response()->json(['estado' => true, 'datos' => $this->anotaciones($tipo, $id)], 200);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 403);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Error desconocido, consulte con el administrador'], 500);
		}
	}

	/*public function AnotacionesEditar(Request $request)
	{
		try
		{
			if (esAdmin(Auth::user()["role"]))
			{
				// Insertamos en la base de datos
				DB::update("UPDATE anotaciones SET tipo = ?, nota = ?, id_usuario = ? WHERE id = ?", [$request["tipo"], $request["nota"], Auth::user()["id"], $request["id_nota"]]);

				// Si todo ha ido bien, devolvemos la totalidad de las anotaciones para mostrarlas
				return response()->json(['mensaje' => $this->obtenerAnotacionesPorIdEntidad($request["id_entidad"])], 200);
			}
			else
				return response()->json(['mensaje' => 'No tienes permiso para realizar esta acción'], 403);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['mensaje' => 'Error desconocido, consulte con el administrador'], 500);
		}
	}*/

	/*public function AnotacionesMostrar(Request $request)
	{
		try
		{
			if (esAdmin(Auth::user()["role"]))
				return response()->json(['mensaje' => $this->obtenerAnotacionesPorIdEntidad($request["id_entidad"])], 200);
			else
				return response()->json(['mensaje' => 'No tienes permiso para realizar esta acción'], 403);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['mensaje' => 'Error desconocido, consulte con el administrador'], 500);
		}
	}*/

	/*public function AnotacionesBorrar(Request $request)
	{
		try
		{
			if (esAdmin(Auth::user()["role"]))
			{
				// Marcamos su fecha de eliminación cmo la actual
				DB::update("UPDATE anotaciones SET fechaBaja = now(), id_usuario = ? WHERE id = ?", [Auth::user()["id"], $request["id_nota"]]);

				// Si todo ha ido bien, devolvemos la totalidad de las anotaciones para mostrarlas
				return response()->json(['mensaje' => $this->obtenerAnotacionesPorIdEntidad($request["id_entidad"])], 200);
			}
			else
				return response()->json(['mensaje' => 'No tienes permiso para realizar esta acción'], 403);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['mensaje' => 'Error desconocido, consulte con el administrador'], 500);
		}
	}*/
}
