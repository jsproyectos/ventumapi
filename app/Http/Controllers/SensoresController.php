<?php

namespace App\Http\Controllers;

use Auth;
use Excel;
use Storage;
use Validator;
Use Exception;
use App\Sensor;
use App\SensorSilo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class SensoresController extends Controller
{
	// Indica si el usuario logeado puede ver el sensor. Los datos del usuario logeado con jwt se guardan en la variabale de sesión del usuario de forma transparente, como si fuera autenticación por sesión
	protected function puedeVer($idSensor)
	{
		try
		{
			// Si es administrador, puede verlo
			if (esAdmin(Auth::user()['role']))
				return true;

			// Si no es administrador, vemos si tiene permisos para verlo
			if (!empty(DB::select("SELECT * FROM sensores JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_sensor = ? AND id_usuario = ? AND sensores.fechaBaja IS NULL AND sensores_usuarios.fechaBaja IS NULL", [$idSensor, Auth::user()['id']])))
				return true;
			return false;
		}
		catch (Exception $e)
		{
			return false;		
		}
	}

	// Calcula el porcentaje de una batería de 3V
	public function calcularBateriaPorcentaje3($voltajeBateria)
	{
		// Hago esta resta puesto que una batería por debajo de VALOR_MINIMO_BATERÍA es inservible. Luego lo hago para que valores por encima de 3.8 V sea 3.8V
		$valorBateriaUsable = min(max($voltajeBateria - VALOR_MINIMO_BATERIA, 0), INTERVALO_VALOR_MAXIMO_BATERIA);

		return intval(($valorBateriaUsable * 100) / INTERVALO_VALOR_MAXIMO_BATERIA); //Porcentaje real de batería.
	}

	// Obtiene el porcentaje de carga de las baterías en función del voltaje de carga. En este caso se tienen en cuenta las baterías de litio de 3.6 V
	private function calcularBateriaPorcentaje($bateria)
	{
		// Las baterías de litio funcionan con una tensión mínima de 2.5V y un máximo de 3.6 o 3.7 (en las recargables incluso 4)
		// En este caso, nuestros sensores de temperatura sigfox de TST utilizan dos baterías, cuyo voltaje mínimo sería 5 y voltaje máximo 7.2 (3.6 * 2)
		return min(Round(($bateria * 100) / 7.2), 100);
	}

	public function calcularBateriaIOTA($valor)
	{
		$valor = intval(str_replace('c', '', $valor));
		$voltaje = INTERVALO_VALOR_MAXIMO_BATERIA * ($valor / 10) + VALOR_MINIMO_BATERIA;
		$porcentaje = $this->calcularBateriaPorcentaje3($voltaje);
		return ['voltaje' => $voltaje, 'bp' => $porcentaje];
	}

	// Calcula el porcentaje de batería en base al voltaje de 3 pilas de 1.5 V
	public function calcularBateriaPilas45($bateria)
	{
		$maximoDiferencia = VALOR_MAXIMO_3_PILAS - VALOR_MINIMO_3_PILAS;
		$bateriaDiferencia = $bateria - VALOR_MINIMO_3_PILAS;

		// Se calcula el porcentaje de batería
		$porcentaje = intval($bateriaDiferencia * 100 / $maximoDiferencia);

		// Se acota para que siempre esté entre 0 y 100
		return max(min(100, $porcentaje), 0);
	}

	protected function completarConsultaYEjecutar($consulta, $idUsuario, $nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		// Metemos en un array los criterios de búsqueda para pasarselos a la consulta y evitar inyección SQL
		$criterios = [$idUsuario];

		if ($nombre != null)
		{
			$consulta .= ' AND sensores.nombre LIKE ?';
			$criterios[] = likear($nombre);
		}

		if ($dispositivo != null)
		{
			$consulta .= ' AND sensores.dispositivo LIKE ?';
			$criterios[] = likear($dispositivo);
		}

		// Los tipos de emisión
		$consulta .= ' AND id_emision IN ' . obtenerInterrogacionesSQL($comunicacion);
		$criterios = array_merge($criterios, $comunicacion);

		if ($fake == false)
			$consulta .= ' AND sensores.fake = 0';

		if ($bajas == false)
			$consulta .= ' AND sensores.fechaBaja IS NULL';

		$consulta .= ' AND sensores_usuarios.fechaBaja IS NULL ORDER BY nombre';

		//dd(DB::select($consulta, $criterios));

		return DB::select($consulta, $criterios);
	}

	public function obtenerResumen(Request $request)
	{
		try
		{
			// Medida de seguridad y estandarizado de información
			$nombre = $request['nombre'] ? $request['nombre'] : null;
			$dispositivo = $request['dispositivo'] ? $request['dispositivo'] : null;
			$comunicacion = json_decode($request['comunicacion']);
			$fake = $request['fake'] ? true : false;
			$bajas = $request['bajas'] ? true : false;
			$orden = $request['orden'] ? $request['orden'] : null;
			$tipos = json_decode($request['tipos']);

			$sensores = [];

			foreach ($tipos as $tipo)
			{
				switch ($tipo)
				{
					case SENSOR_SILO:
						$sensoresTipo = app('App\Http\Controllers\SensoresNivelLlenadoController')->obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_TEMPERATURA:
						$sensoresTipo = app('App\Http\Controllers\SensoresTempHumController')->obtenerTH($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_TEMP_HUM_LUZ:
						$sensoresTipo = app('App\Http\Controllers\SensoresTempHumController')->obtenerTHL($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_PUERTA:
						$sensoresTipo = app('App\Http\Controllers\SensoresPuertasController')->obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_MACHOS:
						$sensoresTipo = app('App\Http\Controllers\SensoresMachosController')->obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_ESTACION:
						$sensoresTipo = app('App\Http\Controllers\SensoresEstacionMeteorologicaController')->obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_CO2:
						$sensoresTipo = app('App\Http\Controllers\SensoresCO2Controller')->obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_CAUDALIMETRO:
						$sensoresTipo = app('App\Http\Controllers\SensoresCaudalimetroController')->obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;

					case SENSOR_GASES:
						$sensoresTipo = app('App\Http\Controllers\SensoresGasesController')->obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas);
						$sensores = array_merge($sensores, $sensoresTipo);
						break;
				}
			}

			return response()->json(['estado' => true, 'datos' => $sensores], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[ERROR RESUMEN SENSORES] ' . $e->getMessage());
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function esPropietario($usuario, $sensor)
	{
		if (count(DB::select('SELECT id_usuario, id_sensor FROM sensores_usuarios WHERE id_usuario = ? AND id_sensor = ?', [$usuario, $sensor])) == 0)
			return false;
		return true;
	}

	public function obtenerAlertasActivas($idSensor, $idAlerta = null)
	{
		$consulta = 'SELECT alertas_sensores.id, id_alerta, parametros, users.id id_usuario, users.name nombreUsuario, users.email email, user_id_ionic, sensores.nombre nombreSensor, dispositivo, id_tipo, email_instantaneo, email_alerta, notificaciones_movil, ultima FROM alertas_sensores JOIN sensores ON alertas_sensores.id_sensor = sensores.id JOIN users ON alertas_sensores.id_usuario = users.id JOIN configuracion on users.id = configuracion.id_usuario WHERE id_sensor = ? AND sensores.fechaBaja IS NULL AND alertas_sensores.activo = 1';

		$parametros = [$idSensor];

		// Esto es por si queremos localizar un tipo de alerta en concreto.
		if ($idAlerta != null)
		{
			$consulta .= ' AND id_alerta = ?';
			$parametros[] = $idAlerta;
		}

		$alertas = DB::select($consulta, $parametros);

		// Convertimos las banderas de la BD en booleanos que son mas fáciles de trabajar
		foreach ($alertas as $alerta)
		{
			$alerta->email_instantaneo = $alerta->email_instantaneo == ACTIVO ? true : false;
			$alerta->email_alerta = $alerta->email_alerta == ACTIVO ? true : false;
			$alerta->notificaciones_movil = $alerta->notificaciones_movil == ACTIVO ? true : false;
		}

		return $alertas;
	}

	protected function obtenerDetalles($id)
	{
		try
		{
			if (($sensor = $this->obtenerDetallesSensor($id)) != false)
				return response()->json(['estado' => true, 'datos' => $sensor], 200);
			return response()->json(['estado' => false, 'datos' => 'El sensor no existe o no tienes permiso para verlo'], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	// Modifica los valores de un sensor desde AJAX, y que puede venir desde cualquier formulario
	public function editar(Request $request)
	{
		try
		{
			$idSensor = $request["id"];

			if (esAdmin(Auth::user()["role"]))
			{
				$validator = Validator::make($request->all(), [
					'id' => ['required', 'numeric'],
					'nombre' => ['required', 'string', 'max:255'],
					'dispositivo' => ['required', 'string', 'max:255'],
					'descripcion' => ['string', 'max:255', 'nullable'],
					'latitud' => ['required', 'numeric', 'nullable'],
					'longitud' => ['required', 'numeric', 'nullable'],
					'id_tipo' => ['required', 'numeric'],
					'id_emision' => ['required', 'numeric'],
					'downlink' => ['string', 'max:255', 'nullable'],
					'tiempo' => ['numeric', 'nullable'],
					'seguimiento' => ['required', 'boolean'],
				]);

				if ($validator->fails())
					return response()->json(["estado" => false, "mensaje" => erroresFormulario($validator->errors()->all())], 200);
				else
				{
					$sensor = Sensor::find($idSensor);


					$sensor->nombre = $request['nombre'];
					$sensor->dispositivo = $request['dispositivo'];
					isset($request['descripcion']) ? $sensor->descripcion = $request['descripcion'] : null;
					$sensor->latitud = $request['latitud'];
					$sensor->longitud = $request['longitud'];
					$sensor->id_tipo = $request['id_tipo'];
					$sensor->id_emision = $request['id_emision'];
					isset($request['downlink']) ? $sensor->downlink = $request['downlink'] : null;
					$sensor->seguimiento = $request['seguimiento'];

					// Si hemos recibido tiempo, lo actualizamos y marcamos como callback
					if (isset($request['tiempo']))
					{
						$sensor->tiempo = $request['tiempo'];
						$sensor->callback = true;
					}

					// Cuando se indica que se va a dar de baja, si el sensor ya tiene una fecha de baja, no se cambia.
					if ($request['baja'] && $request['baja'] == 'true' && $sensor->fechaBaja == null)
						$sensor->fechaBaja = date('Y-m-d H:i:s');
					else if ($request['baja'] && $request['baja'] == 'false' && $sensor->fechaBaja != null)
						$sensor->fechaBaja = NULL;
					// Cuando se indica que se va a quitar de baja, solo se puede quitar si el sensor ya tiene una fecha

					if ($request['fake'] && $request['fake'] == 'true')
						$sensor->fake = true;
					else if ($request['fake'] && $request['fake'] == 'false')
						$sensor->fake = false;

					$sensor->save();

					// Datos que se van a devolver tras actualizar y que serán usados en la actualización de la interfaz
					$sensor = null;
					$mensaje = 'Se ha modificado correctamente ' . $request['nombre'];

					//Ahora, según el tipo que sea, se actualizan sus parámetros
					switch ($request['id_tipo'])
					{
						case SENSOR_SILO:
							$validator = Validator::make($request->all(), [
								'altura' => ['required', 'numeric'],
							]);

							if ($validator->fails())
								return response()->json(['estado' => false, 'mensaje' => erroresFormulario($validator->errors()->all())], 200);
							else
							{
								$sensorEspecifico = SensorSilo::find($idSensor);
								$sensorEspecifico->altura = $request['altura'];

								// Guardamos el cambio
								$sensorEspecifico->save();
								$sensor = app('App\Http\Controllers\SensoresNivelLlenadoController')->obtenerDetallesSensor($idSensor);
							}
							break;
						case SENSOR_TEMPERATURA:
								$sensor = app('App\Http\Controllers\SensoresTempHumController')->obtenerDetallesSensor($idSensor); break;
						case SENSOR_GPS:
								$sensor = app('App\Http\Controllers\SensoresGpsController')->obtenerDetallesSensor($idSensor); break;
						case SENSOR_MACHOS:
								$sensor = app('App\Http\Controllers\SensoresMachosController')->obtenerDetallesSensor($idSensor); break;
						case SENSOR_TEMP_HUM_LUZ:
								$sensor = app('App\Http\Controllers\SensoresTempHumController')->obtenerDetallesSensor($idSensor); break;

					}

					return response()->json(['estado' => true, 'mensaje' => $mensaje, 'datos' => $sensor], 200);
				}
			}
			else
			{
				return response()->json(['estado'=> false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 200);
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'No se ha podido modificar. Por favor, contacte con el administrador', 'e' => $e->getMessage()], 200);
		}
	}

	/*private function inicializarSensor($idSensor, $idTipo)
	{
		switch ($idTipo)
		{
			case SENSOR_SILO:
				return app('App\Http\Controllers\SensorNivelLlenadoController')->inicializarResumen($idSensor);
				break;
			case SENSOR_TEMPERATURA:
				return app('App\Http\Controllers\TemperaturaHumedadController')->inicializarResumen($idSensor);
				break;
			case SENSOR_PUERTA:
				return app('App\Http\Controllers\SensoresPuertasController')->inicializarResumen($idSensor);
				break;
			case SENSOR_MACHOS: //REVISAR
				return app('App\Http\Controllers\SensoresMachosController')->inicializarResumen($idSensor);
				break;
			case SENSOR_TEMP_HUM_LUZ:
				return app('App\Http\Controllers\TemperaturaHumedadController')->inicializarResumen($idSensor);
				//return app('App\Http\Controllers\TempHumLuzController')->inicializarResumen($idSensor);
				break;
			// En caso de que no se requieran nuevas tablas, se pone a verdadero para continuar la ejecución
			default: return true; break;
		}
	}

	public function nuevoSensor(Request $request)
	{
		//$tipoUsuario = Auth::user()["role"];
		$tipoUsuario = USUARIO_SUPERADMINISTRADOR;
		$fecha = date("Y-m-d H:m:s");

		try
		{
			if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
			{
				$validator = Validator::make($request->all(), [
					'nombre' => ['required', 'string', 'max:255'],
					'dispositivo' => ['required', 'string', 'max:255'],
					'descripcion' => ['string', 'max:255'],
					'latitud' => ['required', 'numeric'],
					'longitud' => ['required', 'numeric'],
					'tipo' => ['numeric']
				]);

				if ($validator->fails())
				{
					return json_encode(erroresFormulario($validator->errors()->all()));
				}
				else
				{
					$nuevoSensor = Sensor::create([
					'nombre' => $request['nombre'],
					'dispositivo' => $request['dispositivo'],
					'descripcion' => $request['descripcion'],
					'latitud' => $request['latitud'],
					'longitud' => $request['longitud'],
					'id_tipo' => $request['tipo'],
					'fechaAlta' => $fecha,
					]);

					if ($nuevoSensor)
					{
						$tipoNuevoSensor = (int)$request['tipo'];

						// Se crean las tablas adicionales según el tipo de sensor
						$nuevasTablas = $this->inicializarSensor($nuevoSensor->id, $tipoNuevoSensor);
						if (!$nuevasTablas)
							return json_encode(["estado" => "ERROR", "mensaje" => "No se han podido crear tablas auxiliares. Contacte con el administrador"]);
						return json_encode(["estado" => "OK", "mensaje" => "Se ha creado correctamente el sensor " . $request['nombre']]);
					}
					else
					{
						return json_encode(["estado" => "ERROR", "mensaje" => "No se ha podido crear el sensor. Por favor, contacte con el administrador"]);
					}
				}
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
			}
		}
		catch (Exception $e)
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "Mensaje para el administrador: " . $e->getMessage()]);
		}
	}

	public function borrarSensor(Request $request)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensor = Sensor::find($request["id"]);

			if($sensor->delete())
			{
				return json_encode(["estado" => "OK", "mensaje" => "Sensor " . $request["nombre"] . " borrado correctamente"]);
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No se ha podido eliminar el sensor. Por favor, contacte con el administrador"]);
			}
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
		}
	}*/
}
