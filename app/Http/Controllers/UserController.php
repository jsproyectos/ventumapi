<?php

namespace App\Http\Controllers;

use Storage;
use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

class UserController extends Controller
{
	private function escribirLog($mensaje)
	{
		Storage::append("Autenticacion.log", date("d-m-Y H:i:s") . $mensaje);
	}

	private function obtenerDatosUsuarioPorEmail($email)
	{
		$usuario = DB::select('SELECT id, name, email, user_id_ionic, role, notificaciones, cat_silos, cat_tolvas, cat_naves, cat_neveras, cat_depositos, cat_leche, cat_seguimiento, cat_machos, cat_gesterrit FROM users WHERE email = ?', [$email])[0];

		$usuario->cat_silos = $usuario->cat_silos == 1 ? true : false;
		$usuario->cat_tolvas = $usuario->cat_tolvas == 1 ? true : false;
		$usuario->cat_naves = $usuario->cat_naves == 1 ? true : false;
		$usuario->cat_neveras = $usuario->cat_neveras == 1 ? true : false;
		$usuario->cat_depositos = $usuario->cat_depositos == 1 ? true : false;
		$usuario->cat_leche = $usuario->cat_leche == 1 ? true : false;
		$usuario->cat_seguimiento = $usuario->cat_seguimiento == 1 ? true : false;
		$usuario->cat_machos = $usuario->cat_machos == 1 ? true : false;
		$usuario->cat_gesterrit = $usuario->cat_gesterrit == 1 ? true : false;

		return $usuario;
	}

	// joseluis_f1@hotmail.com / 123456
	public function authenticate(Request $request)
	{
		try
		{
			$credentials = $request->only('email', 'password');

			if (! $token = JWTAuth::attempt($credentials))
			{
				return response()->json(['estado' => false, 'mensaje' => 'El Usuario o contraseña no son correctos'], 400);
			}

			// Si estamos desde un movil, guardamos el user id de ionic para enviar notificaciones
			if (isset($request['userId']))
				DB::update('UPDATE users SET user_id_ionic = ? WHERE email = ?', [$request['userId'], $request['email']]);


			// Cogemos los datos del usuario
			return response()->json(['estado' => true, 'token' => $token, 'user' => User::where('email', $credentials['email'])->get()->first()], 200);
			// return response()->json(['estado' => true, 'token' => $token, 'user' => $this->obtenerDatosUsuarioPorEmail($credentials['email'])], 200);
		}
		catch (JWTException $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'No se ha podido autenticar al usuario'], 500);
		}
	}

	public function getAuthenticatedUser()
	{
		try
		{
			if (!$user = JWTAuth::parseToken()->authenticate())
			{
				return response()->json(['estado' => false, 'mensaje' => 'user_not_found'], 404);
			}

			return response()->json(['estado' => true, 'user' => $user], 200);
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
            // Si ha expirado, procedemos a renovarlo
            /*try
            {
                $token = JWTAuth::getToken();
                $token = JWTAuth::refresh($token);

                //return response()->json(["estado" => true, "token" => $token], 200);    
                return response()->json(["estado" => true, "user" => $user, "refresh" => $token], 200);
            }
            catch (Exception $e)
            {
                return     response()->json(["estado" => false, "mensaje" => "No tienes permiso para hacer esta acción"], 403);
            }*/

			return response()->json(['estado' => false, 'mensaje' => 'token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(["estado" => false, "mensaje" => "token_invalid"], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(["estado" => false, "mensaje" => "token_absent"], $e->getStatusCode());
		}
	}

	public function register(Request $request)
	{
			$validator = Validator::make($request->all(), [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
		]);

		if($validator->fails()){
				return response()->json($validator->errors(), 400);
		}

		$user = User::create([
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'password' => Hash::make($request->get('password')),
		]);

		$token = JWTAuth::fromUser($user);

		return response()->json(compact('user','token'),201);
	}

	public function refreshToken()
	{
		$token = JWTAuth::getToken();
		try
		{
			$token = JWTAuth::refresh($token);
			return response()->json(["estado" => true, "token" => $token], 200);
		}
		catch (TokenBlackListedException $ex)
		{
			return response()->json(["estado" => false, "mensaje" => "No se ha podido refrescar"], 422);
		}
		catch (TokenExpiredException $ex)
		{
			return response()->json(["estado" => false, "mensaje" => "El token ha expirado"], 422);
		}
	}

	public function logout()
	{
		$token = JWTAuth::getToken();
		try
		{
			$token = JWTAuth::invalidate($token);
			DB::update('UPDATE users SET user_id_ionic = null WHERE email = ?', [JWTAuth::user()["email"]]);

			return response()->json(["estado" => true, "mensaje" => "Se ha cerrado la sesión"], 200);
		}
		catch (JWTException $e)
		{
			return response()->json(["estado" => false, "mensaje" => "No ha sido posible cerrar sesión. Por favor, contacte con nosotros", "e" => $e->getMessage()], 422);
		}
	}

	public function pruebasNotificaciones(Request $request)
	{
		$userId = $request["user_id"];
		$mensaje = $request["mensaje"];

		Storage::append("Notificaciones.log", date("d-m-Y H:i:s") . " [$userId] $mensaje");

		return response()->json(['estado' => true, "mensaje" => "Mensaje recibido"], 200); 
	}

	public function obtenerNumeroNotificaciones()
	{
		try
		{
			return response()->json(['estado' => true, 'data' => JWTAuth::user()['notificaciones']], 200); 
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 0], 200); 	
		}
	}
}
