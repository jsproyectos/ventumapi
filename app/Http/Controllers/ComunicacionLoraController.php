<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class ComunicacionLoraController extends ComunicacionController
{
	private function todoBien($idSensor, $device, $ack)
	{
		if ($ack == 'true')
		{
			$downlinkData = DB::select('SELECT downlink FROM sensores WHERE id = ?', [$idSensor])[0]->downlink;

			return $downlinkData == null ? json_encode(array("" => array("noData" => "true"))) : json_encode(array($device => array('downlinkData' => $downlinkData)));
		}
		else
			return json_encode(array("" => array('noData' => 'true')));
	}

	public function registrarInformacion(Request $request)
	{
		Storage::append('log_lora.txt', $request);
		/*$fecha = date('Y/m/d H:i:s', $time);
		$idSensor = SensoresSigfoxController::obtenerIDSensor($device);
		$lineaRegistro = $time . " - $fecha - device: $device, snr: $snr, data: $data, lat: $lat, lng: $lng";
		Storage::append('log_temperatura.txt', $lineaRegistro);

		if ($idSensor == -1)
		{
			SensoresSigfoxController::registrarSensorSigfox($device);
			$idSensor = SensoresSigfoxController::obtenerIDSensor($device);
		}

		// Procedemos a separar el payload
		$contador = hexdec(substr($data, 0, 4));
		$banderas = decbin(hexdec(substr($data, 4, 2)));
		$configuracion = hexdec(substr($data, 6, 2));
		$temperatura = hexdec(substr($data, 8, 4)) / 100;
		$humedad = hexdec(substr($data, 12, 4)) / 100;
		$bateria = hexdec(substr($data, 16, 4));

		DB::insert("INSERT INTO lecturastemperaturahumedad (id_sensor, payload, fecha, temperatura, humedad, bateria, snr) values (?, ?, ?, ?, ?, ?, ?)", [$idSensor, $data, $fecha, $temperatura, $humedad, $bateria, $snr]);

		$bateria = $bateria / 1000;
		SensoresSigfoxController::actualizarInformacionSensor($idSensor, $lat, $lng, $bateria, $snr);
		// SensoresSigfoxController::insertarAlarmas($banderas, $temperatura, $humedad, $bateria);

		$retorno[""]["noData"] = true;
		echo json_encode($retorno);*/
		return "OK";
	}


	public function registrarPruebas(Request $request)
	{
		LogController::mensajeLora($request);

		return response()->json(['estado' => 'ok', 'mensaje' => 'Información recibida correctamente'], 200);
	}

	public function registrarTracker(Request $request)
	{

	}

	public function registrarGasesLibelium(Request $request)
	{
		try
		{
			// Lo primero de todo es obtener los datos que nos interesan de la Request, que es el id del dispositivo
			$device = $request['end_device_ids']['dev_eui'];
			$data = $request['uplink_message']['frm_payload'];
			$snr = $request['uplink_message']['rx_metadata'][0]['snr'];
			$rssi = $request['uplink_message']['rx_metadata'][0]['rssi'];

			// Se obtiene la información importante del sensor
			$sensor = $this->obtenerInfoSensor($device);

			// $fecha = date('Y/m/d H:i:s', $time);
			$fecha = date('Y-m-d H:i:s');

			// Se decodifica la trama
			$trama = app('App\Http\Controllers\SensoresGasesController')->decodificarTramaLibelum($data);

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresGasesController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->co2, $trama->nh3, $trama->ch4, $trama->temperatura, $trama->humedad, $trama->etermico, $trama->presion, $trama->luz);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[LORA GASES LIBELIUM] ' . $e->getMessage());
		}
	}

	public function registrarEstacionVentum(Request $request)
	{
		try
		{
			// Lo primero de todo es obtener los datos que nos interesan de la Request, que es el id del dispositivo
			$device = $request['end_device_ids']['device_id'];
			$data = $request['uplink_message']['decoded_payload']['bytes'];
			$snr = $request['uplink_message']['rx_metadata'][0]['snr'];
			$rssi = $request['uplink_message']['rx_metadata'][0]['rssi'];

			// Se obtiene la información importante del sensor
			$sensor = $this->obtenerInfoSensor($device);

			// $fecha = date('Y/m/d H:i:s', $time);
			$fecha = date('Y-m-d H:i:s');

			// Se decodifica la trama
			$trama = app('App\Http\Controllers\SensoresEstacionMeteorologicaController')->decodificarTramaVentum($data);

			// Guardamos la trama en una cadena para insertarla en la base de datos
			$data = $data[0] . ' ' . $data[1] . ' ' . $data[2] . ' ' . $data[3] . ' ' . $data[4] . ' ' . $data[5] . ' ' . $data[6] . ' ' . $data[7] . ' ' . $data[8] . ' ' . $data[9];

			app('App\Http\Controllers\SensoresEstacionMeteorologicaController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, $trama->presion, $trama->velViento, $trama->dirViento, $trama->lluvia1h);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);	
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[LORA ESTACION VENTUM] ' . $e->getMessage());
		}
	}

	public function registrarSilosVentum(Request $request)
	{
		try
		{
			// Lo primero de todo es obtener los datos que nos interesan de la Request, que es el id del dispositivo
			$device = $request['end_device_ids']['device_id'];
			$data = $request['uplink_message']['decoded_payload']['bytes'];
			$snr = $request['uplink_message']['rx_metadata'][0]['snr'];
			$rssi = $request['uplink_message']['rx_metadata'][0]['rssi'];

			// Se obtiene la información importante del sensor
			$sensor = $this->obtenerInfoSensorSilo($device);

			// $fecha = date('Y/m/d H:i:s', $time);
			$fecha = date('Y-m-d H:i:s');

			// Se decodifica la trama
			$trama = app('App\Http\Controllers\SensoresNivelLlenadoController')->decodificarTramaLoraVentum($data);

			// Convertimos la data (que son bytes) a una cadena, para poder insertarla en la base de datos
			$data = $data[0] . ' ' . $data[1] . ' ' . $data[2] . ' ' . $data[3];

			// Se calcula el porcentaje de llenado del silo
			$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $trama->distancia);

			// Se calcula el peso en kilos aproximado en función del porcentaje y la capacidad máxima del silo
			$kilos = $porcentaje * $sensor->capacidad / 100;

			// Ahora se procede a su inserción y a insertar las alertas sobre las entidades
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $fecha, $trama->bateria, $trama->bp, $rssi, $snr, 1, $trama->distancia, $porcentaje, $kilos, $trama->temperatura, $trama->acelerometroX, $trama->acelerometroY, $trama->acelerometroZ);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $trama->temperatura, $trama->acelerometroX, $trama->acelerometroY, $trama->acelerometroZ);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[LORA SILOS VENTUM] ' . $e->getMessage());
		}
	} 
}
