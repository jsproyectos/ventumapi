<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

define("DATA_KEY", "db1735853ce2b38e22016ac86a15ae8f30a7fb0e");

class PruebasController extends Controller
{
	private function obtenerIDSensor($device)
	{
		$id = DB::select("SELECT id from sensores where dispositivo = ?", [$device]);

		if (count($id) > 0)	// si hay más de un valor, es que existe id y por tanto obtenemos el primero de ellos que será el correcto
		{
			return $id[0]->id;
		}
		else
		{
			// Si no hay, se registra y se obtiene el id
			DB::insert("INSERT INTO sensores (dispositivo, id_emision) values (?, '4')", [$device]);
			$idSensor = $this->obtenerIDSensor($device);
		}
	}

	private function calcularEstressTermico($temperatura, $humedad)
	{
		return (0.8 * $temperatura + (($humedad / 100) * $temperatura - 14.3) + 46.4);
	}

	public function recibirInformacion($device, $data, Request $request)
	{
		try
		{
			$fecha = date("Y-m-d H:i:s");
			if ($request["api_key"] == DATA_KEY)
			{
				/*Storage::append('log_pedro.log', "-- $fecha. Nueva petición");
				Storage::append('log_pedro.log', "$fecha. Dispositivo $device");
				Storage::append('log_pedro.log', "$request");
				Storage::append('log_pedro.log', "\n");*/

				$idSensor = $this->obtenerIDSensor($device);
				$bateria = 4;

				$datos = json_decode($request->getContent());

				$temperatura = $datos->Temp;
				$humedad = $datos->Humidity;
				$snr = $datos->Señal_Wifi;

				$etermico = $this->calcularEstressTermico($temperatura, $humedad);
				// Insertar en la tabla de historicos
				app('App\Http\Controllers\TemperaturaHumedadController')->insertarLectura($idSensor, "", $bateria, $snr, $fecha, $temperatura, $humedad, $etermico);
				$this->actualizarInformacionSensor($idSensor, $bateria, $snr);

				return json_encode(["estado" => "OK", "mensaje" => "Insertado correctamente"]);
			}
			else
			{
				$claveIncorrecta = $request["api_key"];
				/*Storage::append('log_pedro.log', "-- $fecha. Nueva petición");
				Storage::append('log_pedro.log', "$fecha. Dispositivo $device. Clave errónea: $claveIncorrecta");
				Storage::append('log_pedro.log', "\n");*/
				return json_encode(["estado" => "ERROR", "mensaje" => "Clave incorrecta"]);
			}	
		}
		catch (Exception $e)
		{
			return json_encode(["estado" => "ERROR", "mensaje" => $e->getMessage()]);
		}
	}

	public function borrarInformacion()
	{
		Storage::delete('log_pedro.log');
		Storage::append('log_pedro.log', "");
		return redirect('home');
	}

	private function actualizarInformacionSensor($idSensor, $bateria, $snr)
	{
		if (DB::update("UPDATE sensores SET bateria = ?, snr = ? WHERE id = ?", [$bateria, $snr, $idSensor]) > 0)
			return true;
		else
			return false;
	}

	public function secuencia()
	{
		$tramas = [
			"2020-02-04 15:07:19#01808000081117ae0d090000",
			"2020-02-04 14:37:19#017f800007e517630d040000",
			"2020-02-04 14:07:19#017e800007b217f00d000000",
			"2020-02-04 13:37:19#017d80000764185b0d0c0000",
			"2020-02-03 19:14:06#017c800006fc1c850cfb0000",
			"2020-02-03 18:44:06#017b800007a01a8e0d060000",
			"2020-02-03 18:14:06#017a8000080819d50d070000",
			"2020-02-03 17:44:05#01798000082b193c0cfc0000",
			"2020-02-03 17:14:05#01788000084c16dc0d0e0000",
			"2020-02-03 16:44:04#017780000888156b0d120000",
			"2020-02-03 16:14:05#0176800008ee13910d150000",
			"2020-02-03 15:44:04#0175800008ff183e0d070000",
			"2020-02-03 15:14:05#0174800008bb19680cfe0000",
			"2020-02-03 14:44:05#01738000085419a70cf50000",
			"2020-02-03 14:14:05#0172800007e519700cff0000",
			"2020-02-02 18:04:29#0171800007a01aa50cdc0000",
			"2020-02-02 17:34:29#01708000081119660ce30000",
			"2020-02-02 16:47:06#016f8000087f19dc0c8b0000"
		];

		foreach ($tramas as $trama)
		{
			$tramaSeparada = explode("#", $trama);
			$tramaDecodificada = app('App\Http\Controllers\TemperaturaHumedadController')->decodificarTramaTempHum($tramaSeparada[1]);

			echo "$tramaSeparada[0]#$tramaSeparada[1]#$tramaDecodificada->contador<br>";
		}

		/*$tramas = DB::select("SELECT lecturas_temperatura_humedad.id id, nombre, id_sensor, id_tipo, payload, fecha, temperatura, humedad, luminosidad, etermico FROM lecturas_temperatura_humedad JOIN sensores ON lecturas_temperatura_humedad.id_sensor = sensores.id WHERE temperatura > 100");

		foreach ($tramas as $trama)
		{
			switch ($trama->id_tipo)
			{
				case SENSOR_TEMPERATURA:
				//	dump("Tipo: SENSOR TEMPERATURA HUMEDAD");
					$tramaDecodificada = app('App\Http\Controllers\TemperaturaHumedadController')->decodificarTramaTempHum($trama->payload);
					break;

				case SENSOR_TEMP_HUM_LUZ:
				//	dump("Tipo: SENSOR TEMPERATURA HUMEDAD LUMINOSIDAD");
					$tramaDecodificada = app('App\Http\Controllers\TemperaturaHumedadController')->decodificarTramaTempHumLuz($trama->payload);
				break;
				
				default:
					$tramaDecodificada = null;
					break;
			}

			echo "UPDATE lecturas_temperatura_humedad SET temperatura = $tramaDecodificada->temperatura, etermico = $tramaDecodificada->etermico WHERE id = $trama->id;<br>";
		}*/ 
	}

	public function registrarInformacion($distancia, Request $request)
	{
		$fecha = date("d-m-Y H:i:s");

		Storage::append('log_mxm.log', "$fecha. Mensaje: $distancia");

		return json_encode(["estado" => "OK", "mensaje" => "Se ha recibido correctamente"]);
	}

	public function obtenerResumenUltrasonidos()
	{
		try
		{
			$tipoUsuario = Auth::user()["role"];
			if ($tipoUsuario == USUARIO_ADMINISTRADOR || $tipoUsuario == USUARIO_SUPERADMINISTRADOR)
			{
				$informacion = DB::select("SELECT id_sensor, nombre, dispositivo, distancia, fecha FROM sensores JOIN sensores_desarrollo ON sensores.id = sensores_desarrollo.id_sensor");
				return json_encode(["estado" => "OK", "mensaje" => $informacion]);
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
			}
		}
		catch (Exception $e)
		{
			return json_encode(["estado" => "ERROR", "mensaje" => $e->getMessage()]);
		}
	}

	public function mostrarUltrasonidos()
	{
		$informacion = DB::select("SELECT id_sensor, nombre, dispositivo, distancia, fecha FROM sensores JOIN sensores_desarrollo ON sensores.id = sensores_desarrollo.id_sensor");

		return view("desarrollo.ultrasonidos", ["tipoUsuario" => Auth::user()["role"], "sensores" => $informacion, "tituloPagina" => "Pruebas ultrasonidos"]);
	}

	public function pruebas()
	{
		$informacion = [
		"2020-04-19 19:16:43#79A63#Moved Permanently#116c8000064d203b0d420000#301#16",
		"2020-04-19 20:16:41#79A63#Moved Permanently#116e8000064d203b0d420000#301#15.71",
		"2020-04-19 21:46:40#79A63#Moved Permanently#11718000065620300d3f0000#301#16.68",
		"2020-04-19 22:16:40#79A63#Moved Permanently#11728000065620360d410000#301#15.84"
		];

		foreach ($informacion as $registro)
		{
			$elementos = explode("#", $registro);

			$device = $elementos[1];
			$time = strtotime($elementos[0]);
			$snr = $elementos[5];
			$rssi = 0;
			$data = $elementos[3];

			echo "https://clientes.ventumidc.es/sigfox/temperatura/tst/$device/$time/$rssi/$snr/$data</br>";

			
			//app('App\Http\Controllers\ComunicacionSigfoxController')->registrarTemperaturaIOTA($device, $time, $snr, $rssi, $data);
		}

		return "OK";
	}

	public function guardarTagsAntenaDipole(Request $request)
	{
		LogController::mensajeFichero($request, "antena.log");

		return response()->json(['estado' => 'ok', 'mensaje' => 'Información recibida correctamente'], 200);
	}
}
