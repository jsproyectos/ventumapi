<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use Validator;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class SensoresTempHumController extends SensoresController
{
	// Crea una nueva entrada en la tabla sensores_temperatura_humedad_luz
	public function inicializarResumen($idSensor)
	{
		if (DB::insert("INSERT INTO sensores_temperatura_humedad_luz (id_sensor) VALUES (?)", [$idSensor]) == 1)
			return true;
		return false;
	}

	public function calcularEstressTermico($temperatura, $humedad)
	{
		return (0.8 * $temperatura + (($humedad / 100) * $temperatura - 14.3) + 46.4);
	}

	// Calcula el voltaje de carga de las baterías
	private function calcularBateria($bateria)
	{
		return Round(($bateria * 8.25) / 4096, 2);
	}

	// Obtiene el porcentaje de carga de las baterías en función del voltaje de carga.
	private function calcularBateriaPorcentaje($bateria)
	{
		// Las baterías de litio funcionan con una tensión mínima de 2.5V y un máximo de 3.6 o 3.7 (en las recargables incluso 4)
		// En este caso, nuestros sensores de temperatura sigfox de TST utilizan dos baterías, cuyo voltaje mínimo sería 5 y voltaje máximo 7.2 (3.6 * 2)
		return min(Round(($bateria * 100) / 7.2), 100);
	}

	public function decodificarTramaTempHum($data)
	{
		// Procedemos a separar el payload
		$trama = new \stdClass();

		$trama->contador = hexdec(substr($data, 0, 4));
		$banderas = decbin(hexdec(substr($data, 4, 2)));
		$configuracion = hexdec(substr($data, 6, 2));
		//$trama->temperatura = hexdec(substr($data, 8, 4)) / 100;
		$trama->temperatura = bindecComplemento(decbin(hexdec(substr($data, 8, 4))), 16) / 100;
		$trama->humedad = hexdec(substr($data, 12, 4)) / 100;
		$trama->bateria = $this->calcularBateria(hexdec(substr($data, 16, 4)));
		$trama->bp = $this->calcularBateriaPorcentaje($trama->bateria);
		$trama->luminosidad = 0;
		$trama->etermico = $this->calcularEstressTermico($trama->temperatura, $trama->humedad);

		return $trama;
	}

	public function decodificarTramaTempHumLuz($data)
	{
		$trama = new \stdClass();

		// Procedemos a separar el payload
		$identificador = hexdec(substr($data, 0, 4));
		$alarmas = base_convert(substr($data, 4, 2), 16, 2);
		$config = hexdec(substr($data, 6, 2));
		//$trama->temperatura = hexdec(substr($data, 8, 4)) / 100;		// Temperatura en centesimas de grado, por eso se divide entre 100
		$trama->temperatura = bindecComplemento(decbin(hexdec(substr($data, 8, 4))), 16) / 100;
		$trama->humedad = hexdec(substr($data, 12, 4)) / 100;						// Humedad en centesimas, por eso se divide entre 100
		$trama->bateria = $this->calcularBateria(hexdec(substr($data, 16, 4)));		// Nivel de batería en V
		$trama->bp = $this->calcularBateriaPorcentaje($trama->bateria);
		$trama->luminosidad = hexdec(substr($data, 20, 4)) / 10;					// Luminosidad en Lux
		$trama->etermico = $this->calcularEstressTermico($trama->temperatura, $trama->humedad);

		return $trama;
	}

	// Hay que tener en cuenta que la trama que le llega ya está convertida a binario, NO es la trama original
	public function decodificarTramaIOTA($data)
	{
		$datos = [];
		for ($i = 0; $i < 96; $i+=24)
		{
			$trama = new \stdClass();
			$trama->payload = substr($data, $i, 24);
			$trama->temperatura = bindec8(substr($trama->payload, 0, 8)) / 2 + 25;
			$trama->temperaturaMedia = bindecComplemento(substr($trama->payload, 8, 6), 6) / 2;
			$trama->humedad = bindecComplemento(substr($trama->payload, 14, 6), 6) * 2 + 50;
			$trama->humedadMedia = bindecComplemento(substr($trama->payload, 20, 4), 4) * 2;
			$trama->etermico = $this->calcularEstressTermico($trama->temperatura, $trama->humedad);

			$datos[] = $trama;
		}

		return $datos;
	}

	public function decodificarTramaTemperaturaVentum($data)
	{

		$trama = new \stdClass();

		$trama->temperatura = bindecComplemento(decbin(hexdec($data)), 32) / 100; // En RAW es un número entero, se divide por 100 para decimales
		$trama->humedad = 0; // No tiene
		$trama->luminosidad = 0; // No tiene
		$trama->etermico = $this->calcularEstressTermico($trama->temperatura, $trama->humedad);
		$trama->bateria = 3.700;
		$trama->bp = 100;

		return $trama;
	}

	public function decodificarTramaTemperaturaYHumedadVentum($data)
	{
		$trama = new \stdClass();

		$trama->temperatura =  (bindec(substr($data, 0, 32)) - 4000) / 100;
		$trama->humedad =  bindec(substr($data, 32, 32)) / 100;
		$trama->luminosidad = 0; // No tiene
		$trama->etermico = $this->calcularEstressTermico($trama->temperatura, $trama->humedad);
		$trama->bateria = 3.700;
		$trama->bp = 100;

		return $trama;
	}

	public function decodificarTramaTHBVentum($data)
	{
		$trama = new \stdClass();

		$trama->temperatura =  (bindec(substr($data, 0, 8)) - 40) + (bindec(substr($data, 8, 8)) / 100);
		$trama->humedad =  bindec(substr($data, 16, 8));
		$trama->luminosidad = 0; // No tiene
		$trama->etermico = $this->calcularEstressTermico($trama->temperatura, $trama->humedad);
		$trama->bateria = bindec(substr($data, 24, 8)) / 10;
		$trama->bp = 100;

		return $trama;
	}

	public function insertarLectura($idSensor, $idEntidad, $payload, $bateria, $bp, $rssi, $snr, $fecha, $temp, $hum, $luz, $etermico, $alerta = true)
	{
		try
		{
			// Insertar en histórico
			DB::insert("INSERT INTO lecturas_temperatura_humedad (id_sensor, id_entidad, payload, rssi, snr, bateria, bp, fecha, temperatura, humedad, luminosidad, etermico) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [$idSensor, $idEntidad, $payload, $rssi, $snr, $bateria, $bp, $fecha, $temp, $hum, $luz, $etermico]);

			DB::update("UPDATE sensores_temperatura_humedad_luz SET temperatura = ?, humedad = ?, luminosidad = ?, etermico = ?, fecha = ? WHERE id_sensor = ?", [$temp, $hum, $luz, $etermico, $fecha, $idSensor]);

			if ($alerta)
			{
				// Si se indica alerta por parámetro, la creamos en la cola para alertas del sensor. Si no está activada, es por inserciones en masa.
				$job = new \App\Jobs\alertasSensoresTemperatura($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $temp, $hum, $luz, $etermico);
				dispatch($job);
			}

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}

	// Obtien la información ya formateada lista para usar en gráficos
	/*public function obtenerInformacionParaGraficos($idSensor, $fechaInicio, $fechaFin, $orden)
	{
		$historico = DB::select("SELECT fecha, temperatura, humedad, luminosidad, etermico FROM lecturas_temperatura_humedad WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha $orden", [$idSensor, $fechaInicio, incrementarUnDia($fechaFin)]);

		return $this->procesarTablaParaGraficos($historico);
	}*/

	// Función que convierte la información ya obtenida para tablas y las convierte en información preparada para los gráficos de Chart.js
	/*protected function procesarTablaParaGraficos($informacionParaTablas)
	{
		$temperatura = []; $humedad = []; $luminosidad = []; $etermico = [];

		foreach ($informacionParaTablas as $registro)
		{
			$fecha = intval(strtotime($registro->fecha)) * 1000;

			$temperatura[] = ["x" => $fecha, "y" => $registro->temperatura];
			$humedad[] = ["x" => $fecha, "y" => $registro->humedad];
			$luminosidad[] = ["x" => $fecha, "y" => $registro->luminosidad];
			$etermico[] = ["x" => $fecha, "y" => $registro->etermico];
		}

		return ["temperatura" => $temperatura, "humedad" => $humedad, "luminosidad" => $luminosidad, "etermico" => $etermico];
	}*/

	public function obtenerTH($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		return $this->obtener(SENSOR_TEMPERATURA, $nombre, $dispositivo, $comunicacion, $fake, $bajas);
	}

	public function obtenerTHL($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		return $this->obtener(SENSOR_TEMP_HUM_LUZ, $nombre, $dispositivo, $comunicacion, $fake, $bajas);
	}

	// Obtenemos los sensores y la entidad a la que está asociada
	private function obtener($tipo, $nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		$consulta = "SELECT sensores.id, dispositivo, fake, seguimiento, id_tipo, sensores.nombre, e.nombre nombreEntidad, bateria, bp, snr, temperatura, humedad, luminosidad, id_emision, sensores_tipos_emision.nombre comunicacion, sensores.fechaAlta, sensores.fechaBaja, fecha, tiempo, autorizado, notificaciones FROM sensores JOIN sensores_temperatura_humedad_luz ON sensores.id = sensores_temperatura_humedad_luz.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_temperatura_humedad_luz.id_sensor = e.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_tipo = $tipo AND id_usuario = ?";


		$sensores = $this->completarConsultaYEjecutar($consulta, Auth::user()['id'], $nombre, $dispositivo, $comunicacion, $fake, $bajas);

		foreach ($sensores as $sensor)
		{
			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			unset ($sensor->autorizado);
			$sensor->fake = $sensor->fake == 1 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			$sensor->temperatura = floatval($sensor->temperatura);
			$sensor->humedad = floatval($sensor->humedad);
			$sensor->luminosidad = floatval($sensor->luminosidad);
			$sensor->bateria = floatval($sensor->bateria);
			$sensor->snr = floatval($sensor->snr);
		}

		return $sensores;
	}

	public function obtenerDetallesSensor($id)
	{
		$sensor = DB::select("SELECT id, sensores.nombre nombre, sensores_temperatura_humedad_luz.fecha, tiempo, sensores.fechaAlta, sensores.fechaBaja, id_tipo, dispositivo, descripcion, latitud, longitud, bateria, bp, snr, rssi, id_emision, seguimiento, temperatura, humedad, luminosidad, autorizado, e.nombre nombreEntidad FROM sensores JOIN sensores_temperatura_humedad_luz ON sensores.id = sensores_temperatura_humedad_luz.id_sensor JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_temperatura_humedad_luz.id_sensor = e.id_sensor WHERE sensores.id = ? AND id_usuario = ?", [$id, Auth::user()["id"]])[0];

		$sensor->propio = $sensor->autorizado == 0 ? true : false;
		unset ($sensor->autorizado);
		$sensor->latitud = floatval($sensor->latitud);
		$sensor->longitud = floatval($sensor->longitud);
		$sensor->bateria = floatval($sensor->bateria);
		$sensor->snr = floatval($sensor->snr);
		$sensor->rssi = floatval($sensor->rssi);
		$sensor->temperatura = floatval($sensor->temperatura);
		$sensor->humedad = floatval($sensor->humedad);
		$sensor->luminosidad = floatval($sensor->luminosidad);
		$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;

		return $sensor;
	}

	// Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico($idSensor, $fechaInicio, $fechaFin)
	{
		try
		{
			if ($this->puedeVer($idSensor))
			{
				$historico = DB::select("SELECT fecha, payload, temperatura, humedad, luminosidad, etermico, bateria, bp, snr, rssi FROM lecturas_temperatura_humedad WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC", [$idSensor, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			// Ahora hacemos el tratamiento de al tapa
			foreach ($historico as $lectura)
			{
				$lectura->temperatura = floatval($lectura->temperatura);
				$lectura->humedad = floatval($lectura->humedad);
				$lectura->luminosidad = floatval($lectura->luminosidad);
				$lectura->etermico = floatval($lectura->etermico);
				$lectura->bateria = floatval($lectura->bateria);
				$lectura->rssi = floatval($lectura->rssi);
				$lectura->snr = floatval($lectura->snr);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function insertarValorFalso($id, Request $request)
	{
		try
		{
			if (!esAdmin(Auth::user()['role']))
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 403);

			$fecha = date('Y-m-d H:i:s');

			// Obtenemos datos del sensor, SIMILAR  a la función obtenerInfoSensorSilo de ComunicacionController
			$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, ES.id id_entidad, ES.id_tipo id_tipo_entidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.id_sensor = ? AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE sensores.id = ? AND fechaBaja IS NULL', [$id, $id])[0];

			$luminosidad = isset($request['luminosidad']) ? $request['luminosidad'] : 0;
			$etermico = $this->calcularEstressTermico($request['temperatura'], $request['humedad']);
			$bp = $this->calcularBateriaPorcentaje($request['bateria'] * 2);


			// Insertar en la tabla de historicos
			$this->insertarLectura($id, $sensor->id_entidad, 'FAKE', $request['bateria'], $bp, $request['rssi'], $request['snr'], $fecha, $request['temperatura'], $request['humedad'], $luminosidad, $etermico);
			app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $request['temperatura'], $request['humedad'], $luminosidad, $etermico);

			// Actualizar la batería y la señal
			app('App\Http\Controllers\ComunicacionController')->actualizarInformacionSensor($id, $request['bateria'], $bp, $request['snr'], $request['rssi']);

			return response()->json(['estado' => true, 'mensaje' => 'Se ha insertado correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA FALSA TEMPERATURA] ' . $e->getMessage());
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}