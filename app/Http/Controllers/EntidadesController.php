<?php

namespace App\Http\Controllers;

use Auth;
//use Excel;
use Exception;
use Validator;
use App\Entidad;
use App\EntidadNevera;
use App\EntidadRecipiente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

define("TIEMPO_CALCULO_CONSUMO", 30); // 30 días

class EntidadesController extends Controller
{
	private function obtenerNombreEntidad($id_entidad)
	{
		return DB::select("SELECT nombre FROM entidades WHERE id = ?", [$id_entidad])[0]->nombre;
	}

	public function obtenerTipos()
	{
		//02/01/2020 Hago un cambio para que solo muestre las entidades funcionales
		//return DB::select("SELECT id, nombre FROM entidades_tipos");
		return DB::select("SELECT id, nombre FROM entidades_tipos WHERE id NOT IN (?, ?, ?, ?)", [ENTIDAD_LOCALIZACION, ENTIDAD_PUERTA, ENTIDAD_ESTACION, ENTIDAD_LECHE]);
	}

	public function obtenerConTipos()
	{
		return DB::select("SELECT entidades.id id, entidades.nombre nombre, entidades.codigo codigo, fake, entidades.descripcion descripcion, entidades.latitud latitud, entidades.longitud longitud, id_tipo, entidades_tipos.nombre tipo, id_explotacion, explotaciones.nombre explotacion, fechaAlta, fechaBaja FROM entidades LEFT JOIN entidades_tipos ON entidades.id_tipo = entidades_tipos.id LEFT JOIN explotaciones ON entidades.id_explotacion = explotaciones.id ORDER BY id_explotacion ASC, nombre ASC");
	}

	public function obtenerExplotaciones()
	{
		return DB::select("SELECT id, nombre FROM explotaciones ORDER BY nombre ASC");
	}

	public function obtenerTiposContratados()
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			return $this->obtenerTipos();
		}
		else
		{
			return DB::select("SELECT DISTINCT entidades_tipos.id id, entidades_tipos.nombre nombre FROM entidades JOIN entidades_tipos ON entidades.id_tipo = entidades_tipos.id WHERE entidades.id IN (SELECT id_entidad FROM entidades_usuarios WHERE id_usuario = ? AND entidades_usuarios.fechaBaja IS NULL) ORDER BY nombre ASC", [Auth::user()["id"]]);
		}
	}

	public function actualizarTemperatura($idEntidad, $idTipo, $fecha, $temp, $hum, $luz, $etermico)
	{
		try
		{
			switch ($idTipo)
			{
				case ENTIDAD_NEVERAS: app('App\Http\Controllers\EntidadesNeverasController')->actualizar($idEntidad, $fecha, $temp, $hum, PUERTA_CERRADA);break;
				case ENTIDAD_CELDA_NAVE: app('App\Http\Controllers\EntidadesNavesController')->actualizarCelda($idEntidad, $fecha, $temp, $hum, $luz, $etermico); break;
				case ENTIDAD_LECHE: app('App\Http\Controllers\EntidadesTanquesLecheController')->actualizar($idEntidad, null, $fecha, null, null, $temp, null, null, null); break;
				default: return false; break;
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}

	public function actualizarTemperaturaIOTA($idEntidad, $idTipo, $fecha, $temp, $hum, $luz, $etermico)
	{
		try
		{
			switch ($idTipo)
			{
				case ENTIDAD_NEVERAS: app('App\Http\Controllers\EntidadesNeverasController')->actualizar($idEntidad, $fecha, $temp, $hum, PUERTA_CERRADA);break;
				case ENTIDAD_CELDA_NAVE: app('App\Http\Controllers\EntidadesNavesController')->actualizarCeldaIOTA($idEntidad, $fecha, $temp, $hum, $luz, $etermico); break;
				default: return false; break;
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}

	public function esPropietario($id_usuario, $id_entidad)
	{
		if (empty(DB::select("SELECT id_usuario, id_entidad FROM entidades_usuarios WHERE id_usuario = ? AND id_entidad = ? AND autorizado = 0", [$id_usuario, $id_entidad])))
			return false;
		return true;
	}

	public function obtenerEspecialidadFormulario(Request $request)
	{
		switch ($request["id_tipo"])
		{
			case ENTIDAD_SILO:
			case ENTIDAD_AGUA_DEPOSITOS:
				return app('App\Http\Controllers\RecipientesController')->obtenerEspecialidad($request["id_tipo"]);
				break;
			case ENTIDAD_LOCALIZACION:
			case ENTIDAD_PUERTA:
			case ENTIDAD_ESTACION:
			case ENTIDAD_MACHOS:
			case ENTIDAD_LECHE:
			case ENTIDAD_NAVES:
			case ENTIDAD_NEVERAS:
			case ENTIDAD_CELDA_NAVE:
				return json_encode(["estado" => "OK", "mensaje" => null]);
				break;
			
			default:
				return json_encode(["estado" => "ERROR", "mensaje" => "No se pueden cargar los datos específicos. Contacte con el administrador"]);
				break;
		}
	}

	private function crearEntidad($nombre, $codigo, $fake, $descripcion, $latitud, $longitud, $id_tipo, $id_explotacion, $fechaAlta)
	{
		$nuevaEntidad = Entidad::create([
			'nombre' => $nombre,
			'codigo' => $codigo,
			'fake' => $fake,
			'descripcion' => $descripcion,
			'latitud' => $latitud,
			'longitud' => $longitud,
			'id_tipo' => $id_tipo,
			'fechaAlta' => $fechaAlta,
			'id_explotacion' => $id_explotacion,
		]);

		return $nuevaEntidad ? $nuevaEntidad : false;
	}

	// Da de alta una nueva entidad en la base de datos desde AJAX
	public function nuevaEntidad(Request $request)
	{
		try
		{
			$tipoUsuario = Auth::user()["role"];

			if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
			{
				$validator = Validator::make($request->all(), [
					'nombre' => ['required', 'string', 'max:255'],
					'codigo' => ['string', 'max:255'],
					'fake' => ['numeric'],
					'descripcion' => ['string', 'max:255'],
					'latitud' => ['required', 'numeric'],
					'longitud' => ['required', 'numeric'],
					'id_tipo' => ['numeric'],
					'id_explotacion' => ['numeric'],
				]);

				if ($validator->fails())
				{
					return json_encode(erroresFormulario($validator->errors()->all()));
				}
				else
				{
					$fake = $request['fake'] == -1 ? 1 : null;
					$id_explotacion = $request['id_explotacion'] == -1 ? null : $request['id_explotacion'];
					$fecha = date("Y-m-d H:m:s");

					// Ahora comprobamos los datos específicos de la entidad seleccionada

					switch ($request['id_tipo'])
					{
						case ENTIDAD_SILO:
						case ENTIDAD_AGUA_DEPOSITOS:
							$validatorE = Validator::make($request->all(), [
								'id_modelo' => ['required', 'numeric'],
								'id_ganado' => ['numeric'],
								'animales' => ['numeric'],
								'capacidad' => ['required', 'numeric'],
							]);

							if ($validatorE->fails())
							{
								return json_encode(erroresFormulario($validatorE->errors()->all()));
							}
							else
							{
								// Creamos la entidad propiamente dicha
								$nuevaEntidad = $this->crearEntidad($request['nombre'], $request['codigo'], $fake, $request['descripcion'], $request['latitud'], $request['longitud'], $request['id_tipo'], $id_explotacion, $fecha);
								// Creamos la tabla específica por entidad si fuera necesario
								app('App\Http\Controllers\RecipientesController')->inicializar($nuevaEntidad->id, $request['id_modelo'], $request['id_ganado'], $request['animales'], $request['capacidad'], $fecha);
								// Todo bien
							}
							break;
						case ENTIDAD_MACHOS:
							$validatorE = Validator::make($request->all(), [
								'id_animal' => ['required', 'numeric']
							]);

							if ($validatorE->fails())
								return json_encode(erroresFormulario($validatorE->errors()->all()));

							// Creamos la entidad propiamente dicha
							$nuevaEntidad = $this->crearEntidad($request['nombre'], $request['codigo'], $fake, $request['descripcion'], $request['latitud'], $request['longitud'], $request['id_tipo'], $id_explotacion, $fecha);
							// Creamos la tabla específica por entidad si fuera necesario
							app('App\Http\Controllers\MachosController')->inicializar($nuevaEntidad->id, $fecha, $request['id_animal']);
							// Todo bien
							break;
						case ENTIDAD_NEVERAS:
							$validatorE = Validator::make($request->all(), [
								'min' => ['required', 'numeric'],
								'max' => ['required', 'numeric']
							]);

							if ($validatorE->fails())
								return json_encode(erroresFormulario($validatorE->errors()->all()));

							// Creamos la entidad propiamente dicha
							$nuevaEntidad = $this->crearEntidad($request['nombre'], $request['codigo'], $fake, $request['descripcion'], $request['latitud'], $request['longitud'], $request['id_tipo'], $id_explotacion, $fecha);
							// Creamos la tabla específica por entidad si fuera necesario
							app('App\Http\Controllers\EntidadesNeverasController')->inicializar($nuevaEntidad->id, $fecha, $request['min'], $request['max']);
							// Todo bien
							break;
						
						default:
							return json_encode(["estado" => "ERROR", "mensaje" => "No se han podido crear tablas auxiliares. Contacte con el administrador"]);
					}
				}
				return json_encode(["estado" => "OK", "mensaje" => "Entidad creada correctamente"]);
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
			}	
		}
		catch (Exception $e)
		{
			return json_encode(["estado" => "ERROR", "mensaje" => $e->getMessage()]);
		}
	}

	// Modifica los valores de dicha entidad desde AJAX, y que puede venir desde cualquier formulario
	public function editar(Request $request)
	{
		try
		{
			$id_entidad = $request['id_entidad'];

			if (esAdmin(Auth::user()['role']) || $this->esPropietario(Auth::user()['id'], $id_entidad))
			{
				$validator = Validator::make($request->all(), [
					'id_entidad' => ['required', 'numeric'],
					'nombre' => ['required', 'string', 'max:255'],
					'codigo' => ['string', 'max:255', 'nullable'],
					'descripcion' => ['string', 'max:255', 'nullable'],
					'latitud' => ['numeric', 'nullable'],
					'longitud' => ['numeric', 'nullable'],
					'id_tipo' => ['required', 'numeric'],
					'tiempo' => ['numeric', 'nullable'],
				]);

				if ($validator->fails())
					return response()->json(['estado' => false, 'mensaje' => erroresFormulario($validator->errors()->all())], 200);
				else
				{
					$entidad = Entidad::find($id_entidad);

					$entidad->nombre = $request['nombre'];
					isset($request['codigo']) ? $entidad->codigo = $request['codigo'] : null;
					isset($request['descripcion']) ? $entidad->descripcion = $request['descripcion'] : null;
					isset($request['latitud']) ? $entidad->latitud = $request['latitud'] : null;
					isset($request['longitud']) ? $entidad->longitud = $request['longitud'] : null;

					// Si hemos indicado un tiempo, entonces se lo decimos al sensor y le indicamos callback
					if (isset($request['tiempo']))
					{
						$entidad->tiempo = $request['tiempo'];

						// Ahora, le indicamos callback al sensor asociado a la entidad
						DB::update('UPDATE sensores SET tiempo = ?, callback = 1 WHERE id IN (SELECT id_sensor FROM entidades_sensores WHERE id_entidad = ? AND fechaBaja IS NULL)', [$request['tiempo'], $id_entidad]);
					}

					$entidad->save();

					// Datos que se van a devolver tras actualizar y que serán usados en la actualización de la interfaz
					$entidad = null;
					$mensaje = 'Se ha modificado correctamente ' . $request['nombre'];

					//Ahora, según el tipo que sea, se actualizan sus parámetros
					switch ($request['id_tipo'])
					{
						case ENTIDAD_SILO:
							$validator = Validator::make($request->all(), [
								'animales' => ['numeric'],
								'tipo_ganado' => ['required', 'numeric'],
								'capacidad' => ['required', 'numeric'],
							]);

							if ($validator->fails())
								return response()->json(['estado' => false, 'mensaje' => erroresFormulario($validator->errors()->all())], 200);
							else
							{
								$entidadEspecifica = EntidadRecipiente::find($id_entidad);

								if ($request['id_modelo'] && (esAdmin(Auth::user()["role"])))
									$entidadEspecifica->id_modelo = $request['id_modelo'];
								if ($request['id_sensor'] && (esAdmin(Auth::user()["role"])))
									$entidadEspecifica->id_sensor = $request['id_sensor'];

								$request['animales'] ? $entidadEspecifica->animales = $request['animales'] : null;
								$request['tipo_ganado'] ? $entidadEspecifica->id_ganado = $request['tipo_ganado'] : null;
								$request['capacidad'] ? $entidadEspecifica->capacidad = $request['capacidad'] : null;

								// Guardamos el cambio en el histórico
								app('App\Http\Controllers\EntidadesRecipientesController')->insertarCambioEnHistorico($id_entidad);
								$entidadEspecifica->save();
								$entidad = app('App\Http\Controllers\EntidadesRecipientesController')->obtenerDetallesEntidad($id_entidad);
							}
							break;
						case ENTIDAD_AGUA_DEPOSITOS:
							$validator = Validator::make($request->all(), [
								'animales' => ['numeric'],
								'tipo_ganado' => ['required', 'numeric'],
								'capacidad' => ['numeric', 'required'],
							]);

							if ($validator->fails())
								return response()->json(['estado' => false, 'mensaje' => erroresFormulario($validator->errors()->all())], 200);
							else
							{
								$entidadEspecifica = EntidadRecipiente::find($id_entidad);

								if ($request['id_modelo'] && (esAdmin(Auth::user()["role"])))
									$entidadEspecifica->id_modelo = $request['id_modelo'];
								if ($request['id_sensor'] && (esAdmin(Auth::user()["role"])))
									$entidadEspecifica->id_sensor = $request['id_sensor'];

								isset($request['animales']) ? $entidadEspecifica->animales = intval($request['animales']) : null;
								isset($request['tipo_ganado']) ? $entidadEspecifica->id_ganado = intval($request['tipo_ganado']) : null;
								isset($request['capacidad']) ? $entidadEspecifica->capacidad = intval($request['capacidad']) : null;

								// Guardamos el cambio en el histórico
								app('App\Http\Controllers\EntidadesRecipientesController')->insertarCambioEnHistorico($id_entidad);

								$entidadEspecifica->save();
								$entidad = app('App\Http\Controllers\EntidadesRecipientesController')->obtenerDetallesEntidad($id_entidad);
							}
							break;
						case ENTIDAD_PUERTA:
								$entidad = app('App\Http\Controllers\EntidadesPuertasController')->obtenerDetallesEntidad($id_entidad); break;
						case ENTIDAD_ANIMAL:
								$entidad = app('App\Http\Controllers\EntidadesMachosController')->obtenerDetallesEntidad($id_entidad); break;
						case ENTIDAD_NAVES:
								$entidad = app('App\Http\Controllers\EntidadesNavesController')->obtenerDetallesNave($id_entidad); break;
						case ENTIDAD_NEVERAS:
							$validator = Validator::make($request->all(), [
								'min' => ['required', 'numeric'],
								'max' => ['required', 'numeric'],
							]);

							if ($validator->fails())
								return response()->json(["estado" => false, "mensaje" => erroresFormulario($validator->errors()->all())], 200);
							else
							{
								$entidadEspecifica = EntidadNevera::find($id_entidad);

								$entidadEspecifica->min = floatval($request['min']);
								$entidadEspecifica->max = floatval($request['max']);

								$entidadEspecifica->save();

								$entidad = app('App\Http\Controllers\EntidadesNeverasController')->obtenerDetallesEntidad($id_entidad);
							}
							break;							
					}

					return response()->json(['estado' => true, 'mensaje' => $mensaje, 'entidad' => $entidad], 200);
				}
			}
			else
			{
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 200);
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "No se ha podido modificar. Por favor, contacte con el administrador"], 200);
		}
	}

	public function borrarEntidad(Request $request)
	{
		/*$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensor = Sensor::find($request["id"]);

			if($sensor->delete())
			{
				return json_encode(["estado" => "OK", "mensaje" => "Sensor " . $request["nombre"] . " borrado correctamente"]);
			}
			else
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "No se ha podido eliminar el sensor. Por favor, contacte con el administrador"]);
			}
		}
		else
		{
			return json_encode(["estado" => "ERROR", "mensaje" => "No tienes permiso para realizar esta acción"]);
		}*/
	}

	public function obtenerAlertasActivas($idEntidad, $idAlerta = null)
	{
		$consulta = 'SELECT alertas_entidades.id, id_alerta, parametros, users.id id_usuario, users.name nombreUsuario, users.email email, user_id_ionic, entidades.nombre nombreEntidad, codigo, id_tipo, email_instantaneo, email_alerta, notificaciones_movil, ultima FROM alertas_entidades JOIN entidades ON alertas_entidades.id_entidad = entidades.id JOIN users ON alertas_entidades.id_usuario = users.id JOIN configuracion on users.id = configuracion.id_usuario WHERE id_entidad = ? AND entidades.fechaBaja IS NULL AND alertas_entidades.activo = 1';

		$parametros = [$idEntidad];

		// Esto es por si queremos localizar un tipo de alerta en concreto.
		if ($idAlerta != null)
		{
			$consulta .= ' AND id_alerta = ?';
			$parametros[] = $idAlerta;
		}

		$alertas = DB::select($consulta, $parametros);

		// Convertimos las banderas de la BD en booleanos que son mas fáciles de trabajar
		foreach ($alertas as $alerta)
		{
			$alerta->email_instantaneo = $alerta->email_instantaneo == ACTIVO ? true : false;
			$alerta->email_alerta = $alerta->email_alerta == ACTIVO ? true : false;
			$alerta->notificaciones_movil = $alerta->notificaciones_movil == ACTIVO ? true : false;
		}

		return $alertas;
	}

	public function obtenerDueno($id_entidad)
	{
		$dueno = DB::select("SELECT id_usuario FROM entidades_usuarios WHERE id_entidad = ? AND autorizado = 0 AND fechaBaja IS NULL", [$id_entidad]);

		return (count($dueno) == 1) ? $dueno[0]->id_usuario : false;
	}

	public function obtenerPorFiltroAsignacion(Request $request)
	{
		$consulta = "SELECT entidades.id id, entidades.nombre nombre, entidades.codigo codigo, entidades.descripcion descripcion, id_tipo, entidades_tipos.nombre tipo, id_explotacion, explotaciones.nombre explotacion, fechaAlta FROM entidades LEFT JOIN entidades_tipos ON entidades.id_tipo = entidades_tipos.id LEFT JOIN explotaciones ON entidades.id_explotacion = explotaciones.id WHERE fechaBaja IS NULL";
		$parametros = [];

		if ($request['id'])
		{
			$consulta .= " AND id = ?";
			$parametros[] = $request["id"];
		}

		if ($request['codigo'])
		{
			$consulta .= " AND codigo LIKE ?";
			$parametros[] = likear($request["codigo"]);
		}

		if ($request['nombre'])
		{
			$consulta .= " AND nombre LIKE ?";
			$parametros[] = likear($request["nombre"]);
		}

		if ($request['id_tipo'])
		{
			$consulta .= " AND id_tipo = ?";
			$parametros[] = $request["id_tipo"];
		}

		if ($request['explotacion'])
		{
			$consulta .= " AND id_explotacion IN (SELECT id FROM explotaciones WHERE nombre LIKE ?)";
			$parametros[] = $request["id_explotacion"];
		}

		return json_encode(['estado' => true, 'entidades' => DB::select($consulta, $parametros)]);
	}

	public function asignarSensor(Request $request)
	{
		/*
		Códigos de tipos:
		1: El sensor seleccionado ya tiene una entidad asignada
		2: La entidad seleccionada ya tiene un sensor asignado
		*/
		$id_entidad = $request["id_entidad"];

		// Primero buscamos si el sensor ya tiene una entidad asignada
		try
		{
			$entidad = DB::select("SELECT * FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE id_sensor = ? AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL", [$request["id_sensor"]]);

			if (!empty($entidad)) // El sensor ya tiene una entidad asignada
			{
				return json_encode(["estado" => "ERROR", "mensaje" => "El sensor seleccionado ya está asignado a la entidad " . $entidad[0]->nombre . "[" . $entidad[0]->id_entidad . "]", "tipo" => 1]);
			}
			else
			{
				// Ahora comprobamos que la entidad seleccionada no tenga un sensor asignado
				$sensor = DB::select("SELECT * FROM sensores WHERE id = (SELECT id_sensor FROM entidades_sensores WHERE id_entidad = ? AND fechaBaja IS NULL) AND fechaBaja is NULL", [$request["id_entidad"]]);
				if (!empty($sensor)) // La entidad ya tiene un sensor asignado
				{
					return json_encode(["estado" => "ERROR", "mensaje" => "La entidad seleccionada ya tiene un sensor asignado: " . $sensor[0]->nombre. "[" . $sensor[0]->dispositivo . "]", "tipo" => 2]);
				}
				else
				{
					// Asignamos la entidad con la fecha actual de alta
					$sensor = DB::insert("INSERT INTO entidades_sensores (id_entidad, id_sensor) VALUES (?, ?)", [$request["id_entidad"], $request["id_sensor"]]);
					return json_encode(["estado" => "OK", "mensaje" => "Sensor asignado correctamente"]);
				}
			}
		}
		catch (Exception $e)
		{
			return json_encode(["estado" => "ERROR", "mensaje" => $e->getMessage()]);
		}
	}

	public function obtenerLink($id_tipo, $id_entidad)
	{
		switch ($id_tipo)
		{
			case ENTIDAD_SILO: $url = "/ver/silos/informacion/"; break;
			case ENTIDAD_PUERTA: $url =  "/ver/puertas/informacion/"; break;
			case ENTIDAD_AGUA_DEPOSITOS: $url = "/ver/depositos/informacion/"; break;
			case ENTIDAD_MACHOS: $url = "/ver/machos/informacion/"; break;
			case ENTIDAD_NAVES:	$url = "/ver/naves/informacion/"; break;
			case ENTIDAD_NEVERAS: $url = "/ver/neveras/informacion/"; break;
		}

		return url($url . $id_entidad);
	}

	protected function puedeVer($idEntidad, $idUsuario)
	{
		try
		{
			$consulta = DB::select("SELECT * FROM entidades JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE id_entidad = ? AND id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL", [$idEntidad, $idUsuario]);
			if (!empty($consulta))
				return true;
			return false;
		}
		catch (Exception $e)
		{
			return false;
		}
	}
}