<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use App\EntidadMacho;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntidadesMachosController extends EntidadesController
{
	public function __construct()
	{
	}
	/*public function inicializar($id_entidad, $fecha, $id_animal)
	{
		if ($entidad = EntidadMacho::create([
			'id_entidad' => $id_entidad,
			'fecha' => $fecha,
			'montas' => 0,
			'id_animal' => $id_animal,
			'activo' => 1,
		]))
		{
			return true;
		}
		return false;
	}*/

	/*private function obtenerNombreEntidad($id_entidad)
	{
		return DB::select("SELECT nombre FROM entidades WHERE id = ?", [$id_entidad])[0]->nombre;
	}*/

	// Obtiene la información en forma de tabla (array) entre dos fechas concretas para mostrarse, o procesarse a posteriori para mostrar gráficos
	private function obtenerInformacionParaTablas($id_entidad, $fechaInicio, $fechaFin, $orden)
	{
		return DB::select("SELECT DATE_FORMAT(fecha, '%d-%m-%Y %H:%i:%S') fecha FROM lecturas_machos WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY lecturas_machos.fecha $orden", [$id_entidad, $fechaInicio, $fechaFin]);
	}

	private function diaSemanaEnEspanol($diaSemana)
	{
		switch ($diaSemana)
		{
			case 'Monday': return "Lunes"; break;
			case 'Tuesday': return "Martes"; break;
			case 'Wednesday': return "Miércoles"; break;
			case 'Thursday': return "Jueves"; break;
			case 'Friday': return "Viernes"; break;
			case 'Saturday': return "Sábado"; break;
			default: return "Domingo"; break;
		}
	}

	public function actualizar($idEntidad, $fecha)
	{
		try
		{
			DB::update("UPDATE entidades_machos SET montas = (SELECT count(*) FROM lecturas_machos WHERE id_entidad = ?), hoy = (hoy + 1), fecha = ? WHERE id_entidad = ?", [$idEntidad, $fecha, $idEntidad]);

			// Ahora se lanza la cola para las alertas de las entidades animal macho
			$job = new \App\Jobs\alertasEntidadMacho($idEntidad, $fecha);
			dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}

	private function obtenerInformacionParaGraficos($id_entidad, $fechaInicio, $fechaFin, $orden)
	{
		$datos = DB::select("SELECT DATE(fecha) fecha, count(*) total FROM lecturas_machos WHERE id_entidad = ? AND fecha BETWEEN ? AND ? GROUP BY DATE(fecha) ORDER BY DATE(fecha) $orden", [$id_entidad, $fechaInicio, $fechaFin]);

		$fechas = [];
		$total = [];

		foreach ($datos as $dato)
		{
			//$fechas[] = strtotime($dato->fecha);
			$fechas[] = date("d-m-Y", strtotime($dato->fecha));
			$total[] = $dato->total;
		}

		return ["fechas" => $fechas, "total" => $total];
	}

	//AJAX Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico($idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			if (esAdmin(Auth::user()['role']) || $this->puedeVer($idEntidad, Auth::user()['id']))
			{
				$fechaFin = incrementarUnDia($fechaFin);

				return json_encode([
					'estado' => true,
					'datos' => $this->obtenerInformacionParaTablas($idEntidad, $fechaInicio, $fechaFin, 'DESC'),
					'grafico' => $this->obtenerInformacionParaGraficos($idEntidad, $fechaInicio, $fechaFin, 'ASC')
				]);
			}
			else
			{
				return response()->json(["estado" => false, 'mensaje' => 'No tienes permiso para ver este animal'], 200);
			}
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => $e->getMessage()], 200);
		}
	}

	public function obtenerActividadReciente(Request $request)
	{
		try
		{
			$montasSemanal = DB::select('SELECT sum(hoy) hoy, sum(ayer) ayer, sum(dias2) dias2, sum(dias3) dias3, sum(dias4) dias4, sum(dias5) dias5, sum(dias6) dias6 FROM entidades_machos JOIN entidades_usuarios ON entidades_machos.id_entidad = entidades_usuarios.id_entidad WHERE id_usuario = ?', [Auth::user()['id']])[0];

			$fecha = date('Y-m-d'); $semanal = [];

			for($i = 0; $i < 7; $i++)
			{
				$semanal[] = ['fecha' => $fecha, 'diaSemana' => $this->diaSemanaEnEspanol(date("l", strtotime($fecha)))];
				$fecha = date('Y-m-d', strtotime($fecha . '-1 days'));
			}

			// Lo hacemos a pelo, es mas fácil
			$semanal[0]['diaSemana'] = 'Hoy'; $semanal[0]['montas'] = intval($montasSemanal->hoy);
			$semanal[1]['diaSemana'] = 'Ayer'; $semanal[1]['montas'] = intval($montasSemanal->ayer);
			$semanal[2]['montas'] = intval($montasSemanal->dias2);
			$semanal[3]['montas'] = intval($montasSemanal->dias3);
			$semanal[4]['montas'] = intval($montasSemanal->dias4);
			$semanal[5]['montas'] = intval($montasSemanal->dias5);
			$semanal[6]['montas'] = intval($montasSemanal->dias6);

			$montasPorAnimal = DB::select('SELECT nombre, montas FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad JOIN entidades_usuarios ON entidades_machos.id_entidad = entidades_usuarios.id_entidad WHERE entidades.fechaBaja IS NULL and entidades_usuarios.fechaBaja IS NULL AND id_usuario = ? ORDER BY nombre', [Auth::user()['id']]);

			return response()->json(['estado' => true, 'datos' => ['semanal' => $semanal, 'total' => $montasPorAnimal]], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[Obtener Actividad Macho] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerResumen(Request $request)
	{
		try
		{
			$orden = $this->ordenMostrar($request["orden"]);

			$entidades = DB::select("SELECT entidades.id id, tiempo, entidades.nombre nombre, entidades.codigo crotal, entidades.descripcion, entidades_machos.fecha ultimaMonta, montas, autorizado, hoy, ayer, explotaciones.nombre nombreExplotacion, notificaciones FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE entidades_usuarios.id_usuario = ? AND entidades_usuarios.fechaBaja IS NULL AND entidades.fechaBaja IS NULL ORDER BY $orden", [Auth::user()["id"]]);

			foreach ($entidades as $entidad)
			{
				$entidad->propio = $entidad->autorizado == "0" ? true : false; unset($entidad->autorizado);
			}

			return response()->json(["estado" => true, "datos" => $entidades, "orden" => $request["orden"]], 200);
		}
		catch (Exception $e)
		{
			return response()->json(["estado" => false, "mensaje" => "Ha ocurrido un error. Por favor, contacte con nosotros", "e" => $e->getMessage()], 200);
		}
	}

	private function ordenMostrar($orden)
	{
		switch ($orden)
		{
			case 'nombre': return "nombre ASC"; break;
			case 'crotal': return "crotal ASC"; break;
			case 'montas': return "montas DESC, nombre ASC"; break;
			default: return "hoy DESC, nombre ASC"; break;
		}
	}

	// Función usada para obtener los detalles de la entidad, anto al cargarla como al actualizarla.
	public function obtenerDetallesEntidad($idEntidad)
	{
		$entidad = DB::select("SELECT entidades.id id, tiempo, entidades.nombre nombre, entidades.codigo crotal, entidades_animales.id_tipo id_tipo_animal, entidades.id_tipo id_tipo, SA.id_tipo id_tipo_sensor, id_sensor, entidades.descripcion, entidades.longitud, entidades.latitud, montas, hoy, ayer, autorizado, explotaciones.nombre nombreExplotacion, fichero, notificaciones FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad JOIN entidades_animales ON entidades.id = entidades_animales.id_entidad LEFT JOIN (SELECT id_entidad, id id_sensor, id_tipo FROM entidades_sensores JOIN sensores ON entidades_sensores.id_sensor = sensores.id WHERE entidades_sensores.fechaBaja IS NULL AND id_entidad = ?) SA ON entidades.id = SA.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades_machos.id_entidad = entidades_usuarios.id_entidad WHERE entidades.id = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL", [$idEntidad, $idEntidad])[0];

		$entidad->propio = $entidad->autorizado == 0 ? true : false; unset($entidad->autorizado);
		$entidad->latitud = floatval($entidad->latitud); $entidad->longitud = floatval($entidad->longitud);

		return $entidad;
	}

	public function obtenerDetalles($id)
	{
		try
		{
			return response()->json(["estado" => true, "datos" => $this->obtenerDetallesEntidad($id)], 200);
		}
		catch (Exception $e)
		{
			return response()->json(["estado" => false, "mensaje" => "Ha ocurrido un error. Por favor, contacte con nosotros", "e" => $e->getMessage()], 200);
		}
	}

	/*public function obtenerMontas($fechaInicio, $fechaFin)
	{
		return DB::select("SELECT id_sensor, DATE(fecha) AS fecha, count(*) AS total FROM lecturas_machos WHERE fecha BETWEEN ? AND ?  GROUP BY id_sensor, DATE(fecha) ORDER BY id_sensor, DATE(fecha) ASC", [$fechaInicio, $fechaFin]);
	}

	public function obtenerMontasAgrupadas($fechaInicio, $fechaFin)
	{
		return DB::select("SELECT id_sensor, count(*) AS total FROM lecturas_machos WHERE fecha BETWEEN ? AND ? GROUP BY id_sensor ORDER BY id_sensor", [$fechaInicio, $fechaFin]);
	}*/
}