<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SensoresMachosController extends SensoresController
{
// Crea una nueva entrada en la tabla sensores_llenado_leche
	// TODO
	public function inicializarResumen($idSensor)
	{
		if (DB::insert("INSERT INTO sensores_llenado_leche (id_sensor) VALUES (?)", [$idSensor]) == 1)
			return true;
		return false;
	}

	public function insertarLectura($idSensor, $idEntidad, $fecha, $bateria, $bp, $snr, $rssi)
	{
		try
		{
			// Insertar en histórico
			DB::insert("INSERT INTO lecturas_machos (id_sensor, id_entidad, fecha, bateria, bp, rssi, snr) values (?, ?, ?, ?, ?, ?, ?)", [$idSensor, $idEntidad, $fecha, $bateria, $bp, $rssi, $snr]);
			// Insertar en resumen
			DB::update("UPDATE sensores_machos SET montas = (SELECT count(*) FROM lecturas_machos WHERE id_sensor = ?), fecha = ? WHERE id_sensor = ?", [$idSensor, $fecha, $idSensor]);

			// Ahora se lanza la cola para las alertas del sensor de machos
			$job = new \App\Jobs\alertasSensoresMachos($idSensor, $fecha, $bateria, $bp, $snr, $rssi);
			dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}

	// Función que devuelve la información de las montas agrupadas por día
	public function obtenerInformacionParaGraficos($id_sensor, $fechaInicio, $fechaFin)
	{
		$historico = DB::select("SELECT id_sensor, DATE(fecha) fecha, count(*) total FROM lecturas_machos WHERE id_sensor = ? AND fecha BETWEEN ? AND ? GROUP BY id_sensor, DATE(fecha) ORDER BY DATE(fecha) ASC", [$id_sensor, $fechaInicio, $fechaFin]);

		$fechas = []; $montas = [];

		foreach ($historico as $registro)
		{
			$fechas[] = $registro->fecha;
			$montas[] = $registro->total;
		}

		return ["fechas" => $fechas, "montas" => $montas];
	}

	// Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	protected function obtenerHistorico($idSensor, $fechaInicio, $fechaFin)
	{
		return DB::select("SELECT fecha FROM lecturas_machos WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC", [$idSensor, $fechaInicio, $fechaFin]);
	}

	// Función de AJAX. Devuelve una tabla con los registros y los datos procesados para un gráfico
	public function obtenerInformacion(Request $request)
	{
		return response()->json([
			"datos" => $this->obtenerHistorico($request["id_sensor"], $request["fechaInicio"], incrementarUnDia($request["fechaFin"])),
			"grafico" => $this->obtenerInformacionParaGraficos($request["id_sensor"], $request["fechaInicio"], incrementarUnDia($request["fechaFin"]))
		], 200);
	}

	// Obtenemos los sensores y la entidad a la que está asociada
	public function obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		$consulta = 'SELECT sensores.id, dispositivo, id_tipo, seguimiento, sensores.nombre, e.nombre nombreEntidad, bateria, bp, enchufe, snr, id_emision, sensores_tipos_emision.nombre comunicacion, montas, sensores.fechaAlta, sensores.fechaBaja, fecha, tiempo, sensores.fechaAlta, sensores.fechaBaja, autorizado, notificaciones FROM sensores JOIN sensores_machos ON sensores.id = sensores_machos.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_machos.id_sensor = e.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_tipo = ' . SENSOR_MACHOS . ' AND id_usuario = ?';

		$sensores = $this->completarConsultaYEjecutar($consulta, Auth::user()["id"], $nombre, $dispositivo, $comunicacion, $fake, $bajas);

		foreach ($sensores as $sensor)
		{
			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			unset ($sensor->autorizado);
			$sensor->montas = intval($sensor->montas);
			$sensor->bateria = floatval($sensor->bateria);
			$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
		}

		return $sensores;
	}

	public function obtenerInformacionDetallada($id_sensor)
	{
		$tipoUsuario = Auth::user()["role"];

		if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
		{
			$sensor = DB::select("SELECT id, nombre, dispositivo, fake, id_tipo, descripcion, longitud, latitud, bateria, bp, enchufe, snr, id_emision, tiempo, downlink, montas, '0' AS autorizado FROM sensores JOIN sensores_machos ON sensores.id = sensores_machos.id_sensor WHERE sensores.id = ?", [$id_sensor])[0];
		}
		else
		{
			$id_usuario = Auth::user()["id"];
			$sensor = DB::select("SELECT sensores.id, sensores.id_tipo AS id_tipo, sensores.nombre, sensores.descripcion, longitud, latitud, bateria, enchufe, snr, id_animal, crotal, montas, '0' AS autorizado FROM sensores JOIN sensores_machos ON sensores.id = sensores_machos.id_sensor JOIN animales ON sensores_machos.id_animal = animales.id WHERE sensores.id IN(SELECT id_sensor FROM sensores_usuarios WHERE id_usuario = ?) AND sensores.id = ? UNION SELECT sensores.id, sensores.id_tipo AS id_tipo, sensores.nombre, sensores.descripcion, longitud, latitud, bateria, snr, id_animal, crotal, montas, IFNULL(users.name, '-1') AS autorizado FROM sensores JOIN sensores_machos ON sensores.id = sensores_machos.id_sensor JOIN animales ON sensores_machos.id_animal = animales.id LEFT JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN users ON sensores_usuarios.id_usuario = users.id WHERE sensores.id = (SELECT id_sensor FROM sensores_usuarios_autorizados WHERE id_usuario = ? AND id_sensor = ?)", [$id_usuario, $id_sensor, $id_usuario, $id_sensor])[0];
		}

		$sensor->autorizado = $sensor->autorizado == '-1' ? "Prototipo" : $sensor->autorizado;

		$bateria = calcularPorcentajeBateriaBarra($sensor->bateria);
		$signal = calcularPorcentajeSignalBarra($sensor->snr);

		$sensor->bateria = $bateria["porcentaje"];
		$sensor->colorBateria = $bateria["color"];

		$sensor->signalPorcentaje = $signal["porcentaje"];
		$sensor->signalColor = $signal["color"];
		$sensor->signalTexto = $signal["texto"];
		$sensor->enchufe = $sensor->enchufe == 1 ? true : false;

		return $sensor;
	}

	public function obtenerMontas($fechaInicio, $fechaFin)
	{
		return DB::select("SELECT id_sensor, DATE(fecha) AS fecha, count(*) AS total FROM lecturas_machos WHERE fecha BETWEEN ? AND ?  GROUP BY id_sensor, DATE(fecha) ORDER BY id_sensor, DATE(fecha) ASC", [$fechaInicio, $fechaFin]);
	}

	public function obtenerMontasAgrupadas($fechaInicio, $fechaFin)
	{
		return DB::select("SELECT id_sensor, count(*) AS total FROM lecturas_machos WHERE fecha BETWEEN ? AND ? GROUP BY id_sensor ORDER BY id_sensor", [$fechaInicio, $fechaFin]);
	}

	public function exportarHistoricoExcel($idSensor, $fechaInicio, $fechaFin)
	{
		$fechaFinOld = $fechaFin;
		$informacion = $this->obtenerHistorico($idSensor, $fechaInicio, incrementarUnDia($fechaFin));
		$criterios = ["fecha"];

		$this->exportarExcel($idSensor, $criterios, $informacion, $fechaInicio, $fechaFinOld);
	}
}
