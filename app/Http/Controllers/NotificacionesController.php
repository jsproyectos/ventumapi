<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use App\Notificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

define('ELEMENTOS_PAGINA', 15);

class NotificacionesController extends Controller
{
	// Ya no se usa porque ahora se obtienen paginadas
	/*public function obtenerNotificaciones(Request $request)
	{
		try
		{
			$idUsuario = Auth::user()["id"];
			$notificaciones = null;

			if ($request["id_tipo_entidad"])
				$notificaciones =  $this->obtenerNotificacionesUsuarioEntidad($idUsuario, $request["id_tipo_entidad"]);
			else
				$notificaciones =  $this->obtenerNotificacionesUsuario($idUsuario);

			//$this->marcarLeidas($idUsuario);

			return response()->json(["estado" => true, "mensaje" => $this->formatearNotificaciones($notificaciones)], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[Obtener Notificaciones] ' . $e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "Ha ocurrido un error. Por favor, contacte con nosotros"], 200);
		}
	}*/

	private function obtenerNotificacionesUsuario($id_usuario)
	{
		// SELECT NE.id, fecha, NE.tipo, entidades.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, entidades.nombre nombre, entidades.codigo codigo, explotaciones.nombre nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 0 AND id_usuario = 1) NE JOIN entidades ON NE.id_referencia = entidades.id JOIN explotaciones ON entidades.id_explotacion = explotaciones.id UNION SELECT NS.id, fecha, NS.tipo, sensores.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, sensores.nombre nombre, sensores.dispositivo codigo, null nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 1 AND id_usuario = 1) NS JOIN sensores ON NS.id_referencia = sensores.id ORDER BY fecha DESC LIMIT 5, 5
		// En el limit, primero son los que se saltan y luego los que saca

		return DB::select("SELECT NE.id, fecha, NE.tipo, entidades.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, entidades.nombre nombre, entidades.codigo codigo, explotaciones.nombre nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 0 AND id_usuario = ?) NE JOIN entidades ON NE.id_referencia = entidades.id JOIN explotaciones ON entidades.id_explotacion = explotaciones.id UNION SELECT NS.id, fecha, NS.tipo, sensores.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, sensores.nombre nombre, sensores.dispositivo codigo, null nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 1 AND id_usuario = ?) NS JOIN sensores ON NS.id_referencia = sensores.id ORDER BY fecha DESC", [$id_usuario, $id_usuario]);
	}

	public function obtenerNotificacionEntidadFormateadaPorId($id)
	{
		return $this->formatearNotificacion($this->obtenerNotificacionEntidadPorId($id));
	}

	private function obtenerNotificacionEntidadPorId($id)
	{
		return DB::select('SELECT NE.id, fecha, NE.tipo, entidades.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, entidades.nombre nombre, entidades.codigo codigo, explotaciones.nombre nombreExplotacion FROM notificaciones NE JOIN entidades ON NE.id_referencia = entidades.id JOIN explotaciones ON entidades.id_explotacion = explotaciones.id WHERE NE.id = ' . $id)[0];
	}

	private function obtenerNotificacionesUsuarioEntidad($idUsuario, $idTipo)
	{
		return DB::select("SELECT NE.id, fecha, NE.tipo, entidades.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, entidades.nombre nombre, entidades.codigo codigo, explotaciones.nombre nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 0 AND id_usuario = ?) NE JOIN entidades ON NE.id_referencia = entidades.id JOIN explotaciones ON entidades.id_explotacion = explotaciones.id WHERE entidades.id_tipo = ? ORDER BY fecha DESC", [$idUsuario, $idTipo]);
	}

	public function obtenerNotificacionesEntidadPagina($pagina)
	{
		try
		{
			$idUsuario = Auth::user()["id"];
			$saltos = intval($pagina * ELEMENTOS_PAGINA);

			$notificaciones =  DB::select("SELECT NE.id, fecha, NE.tipo, entidades.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, entidades.nombre nombre, entidades.codigo codigo, explotaciones.nombre nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 0 AND id_usuario = ?) NE JOIN entidades ON NE.id_referencia = entidades.id JOIN explotaciones ON entidades.id_explotacion = explotaciones.id UNION SELECT NS.id, fecha, NS.tipo, sensores.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, sensores.nombre nombre, sensores.dispositivo codigo, null nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 1 AND id_usuario = ?) NS JOIN sensores ON NS.id_referencia = sensores.id ORDER BY fecha DESC LIMIT $saltos, " . ELEMENTOS_PAGINA, [$idUsuario, $idUsuario]);

			// Formateamos las notificaciones
			foreach ($notificaciones as $notificacion)
				$notificacion = $this->formatearNotificacion($notificacion);

			return response()->json(["estado" => true, "data" => $notificaciones], 200);			
		}
		catch (Exception $e)
		{
			LogController::errores('[NOTIFICACIONES PAGINA ' . $pagina . '] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 200);
		}
	}

	public function obtenerNotificacionesPorEntidadPagina($entidad, $pagina)
	{
		try
		{
			$saltos = intval($pagina * ELEMENTOS_PAGINA);

			$notificaciones =  DB::select("SELECT NE.id, fecha, NE.tipo, entidades.id_tipo id_tipo, id_referencia, id_alerta, parametros, leida, entidades.nombre nombre, entidades.codigo codigo, explotaciones.nombre nombreExplotacion FROM (select id, fecha, tipo, id_referencia, id_alerta, parametros, leida FROM notificaciones WHERE tipo = 0 AND id_usuario = ? AND id_referencia = ?) NE JOIN entidades ON NE.id_referencia = entidades.id JOIN explotaciones ON entidades.id_explotacion = explotaciones.id ORDER BY fecha DESC LIMIT $saltos, " . ELEMENTOS_PAGINA, [Auth::user()['id'], $entidad]);
			
			// Formateamos las notificaciones
			foreach ($notificaciones as $notificacion)
				$notificacion = $this->formatearNotificacion($notificacion);

			return response()->json(['estado' => true, 'data' => $notificaciones], 200);			
		}
		catch (Exception $e)
		{
			LogController::errores('[NOTIFICACIONES PAGINA ' . $pagina . '] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros'], 200);
		}
	}

	public function marcarLeidas(Request $request)
	{
		try
		{
			$notificaciones = isset($request['notificaciones']) ? json_decode($request['notificaciones']) : [];

			// Marcamos como leídas las indicadas
			DB::update('UPDATE notificaciones SET leida = 1 WHERE id IN ' . obtenerInterrogacionesSQL($notificaciones), $notificaciones);

			// Actualizamos las notificaciones del usuario
			DB::statement('call actualizar_notificaciones_usuario(?)',[Auth::user()['id']]);

			// Obtenemos las notificaciones actuales para devolverlas
			$notificaciones = $this->obtenerTotalNotificacionesPorUsuario(Auth::user()['id']);

			// Las colocamos en middleware Auth
			Auth::user()['notificaciones'] = $notificaciones;

			return response()->json(['estado' => true, 'data' => $notificaciones], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MARCAR NOTIFICACIONES LEIDAS] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos'], 200);
		}
	}

	public function marcarLeidasYBorradas(Request $request)
	{
		try
		{
			$leidas = isset($request['leidas']) ? json_decode($request['leidas']) : [];
			$borradas = isset($request['borradas']) ? json_decode($request['borradas']) : [];
			$idUsuario = Auth::user()['id'];

			if (!empty($borradas))
				DB::update('DELETE FROM notificaciones WHERE id IN ' . obtenerInterrogacionesSQL($borradas) . ' AND id_usuario = ?', array_merge($borradas, [$idUsuario]));

			// Marcamos como leídas las indicadas
			if (!empty($leidas))
				DB::update('UPDATE notificaciones SET leida = 1 WHERE id IN ' . obtenerInterrogacionesSQL($leidas), $leidas);

			// Actualizamos las notificaciones del usuario
			DB::statement('call actualizar_notificaciones_usuario(?)',[$idUsuario]);

			// Obtenemos las notificaciones actuales para devolverlas
			$notificaciones = $this->obtenerTotalNotificacionesPorUsuario($idUsuario);

			// Las colocamos en middleware Auth
			Auth::user()['notificaciones'] = $notificaciones;

			return response()->json(['estado' => true, 'data' => $notificaciones], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MARCAR ALERTAS LEIDAS] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos', 'e' => $e->getMessage()], 200);
		}	
	}

	private function obtenerTotalNotificacionesPorUsuario($idUsuario)
	{
		return intval(DB::select('SELECT notificaciones FROM users WHERE id = ?', [$idUsuario])[0]->notificaciones);
	}

	// Email es un booleano que indica si esa notificación será susceptible de enviarse un mail si no se ha leído en un tiempo concreto. Le ponemos una fecha falsa para identificar esa notificación en concreto, así cuando el scheduler compruebe las notificaciones a las que hay que enviar el mail, si estuviera en nulo posiblemente pudiera coger notificaciones muy antiguas que no se enviaron un email en su momento.
	private function insertarNotificacion($tipo, $id_referencia, $id_alerta, $id_usuario, $parametros, $email_alerta, $email_instantaneo)
	{
		try
		{
			$fecha_email = null;

			// Si se va a enviar un email instantáneo, entonces ponemos la fecha actual como fecha del email. Si no es intantáneo, pero hay que enviar email establecemos a nulo. Si el usuario tiene puesto que en ese momento no quiere recibir emails de las alertas, pues establecemos una fecha predeterminada para distinguirlo y no dejarlo a nulo para que luego el sistema automático de enviar emails si no se ha leido la notifiación la coja.
			if ($email_alerta)
				$fecha_email = $email_instantaneo ? date('Y-m-d H:i:s') : null;
			else
				$fecha_email = NOTIFICACION_NO_ENVIAR_MAIL;

			// Insertar la notificación que no se enviará por email al pasar el tiempo
			$notificacion = Notificacion::create([
				'tipo' => $tipo,
				'id_referencia' => $id_referencia,
				'id_alerta' => $id_alerta,
				'id_usuario' => $id_usuario,
				'parametros' => $parametros,
				'leida' => 0,
				'fecha_email' => $fecha_email,
			]);

			if ($notificacion)
			{
				// Actualizar el contador de notificaciones
				DB::update('UPDATE users SET notificaciones = (SELECT count(*) FROM notificaciones WHERE id_usuario = ? AND leida = 0) WHERE id = ?', [$id_usuario, $id_usuario]);

				// Devuelve el ID de la notificación insertada
				return $notificacion;
			}
			
			return false;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}

	// Inserta una notificacion o una alerta en la tabla de notificaciones, referente a una entidad
	public function insertarNotificacionEntidad($idEntidad, $idTipoAlerta, $idUsuario, $parametros, $email, $email_instantaneo)
	{
		return $this->insertarNotificacion(ENTIDAD, $idEntidad, $idTipoAlerta, $idUsuario, $parametros, $email, $email_instantaneo);
	}

	// Inserta una notificacion o una alerta en la tabla de notificaciones, referente a un sensor
	public function insertarNotificacionSensor($idSensor, $idTipoAlerta, $idUsuario, $parametros, $email, $email_instantaneo)
	{
		return $this->insertarNotificacion(SENSOR, $idSensor, $idTipoAlerta, $idUsuario, $parametros, $email, $email_instantaneo);
	}

	private function formatearNotificacion($notificacion)
	{
		$notificacion->leida = $notificacion->leida == 1 ? true : false;

		switch ($notificacion->tipo)
		{
			// Entidad
			case ENTIDAD:
				switch ($notificacion->id_alerta)
				{
					case ALARMA_SILOS_CAPACIDAD_BAJA: $notificacion->texto = "Alerta por baja capacidad ($notificacion->parametros %)";	break;
					case ALARMA_SILOS_CAPACIDAD_ALTA: $notificacion->texto = "Alerta por alta capacidad ($notificacion->parametros %)";	break;
					case ALARMA_SILOS_DESNIVEL:	$notificacion->texto = 'Alerta por desnivel de sensor. Es posible que se haya movido o que la tapa no esté cerrada'; break;
					case ALARMA_SILOS_RECEPCION: $notificacion->texto = "Alerta por capacidad actualizada ($notificacion->parametros %)"; break;
					case ALARMA_TEMPERATURA_BAJA: $notificacion->texto = "Alerta por baja temperatura ($notificacion->parametros" . "º C)"; break;
					case ALARMA_TEMPERATURA_ALTA: $notificacion->texto = "Alerta por alta temperatura ($notificacion->parametros" . "º C)"; break;
					case ALARMA_HUMEDAD_BAJA: $notificacion->texto = "Alerta por baja humedad relativa ($notificacion->parametros %)"; break;
					case ALARMA_HUMEDAD_ALTA: $notificacion->texto = "Alerta por baja humedad relativa ($notificacion->parametros %)"; break;
					case ALARMA_LUMINOSIDAD_BAJA: $notificacion->texto = "Alerta por baja luminosidad ($notificacion->parametros lux)"; break;
					case ALARMA_LUMINOSIDAD_ALTA: $notificacion->texto = "Alerta por alta luminosidad ($notificacion->parametros lux)"; break;
					case ALERTA_ETERMICO_PROLONGADO: $notificacion->texto = "Los animales se podrían encontrar en peligro debido al estrés térmico prolongado en su recinto"; break;
					case ALARMA_GPS_SALIDA_ANIMAL: $notificacion->texto = "Alerta por salida: El animal se encuentra fuera de su recinto"; break;
					case ALARMA_GPS_ESTANCAMIENTO: $notificacion->texto = "El animal no se ha movido por un tiempo prolongado"; break;
					case ALARMA_PUERTAS_APERTURA: $notificacion->texto = "Se ha realizado una apertura de la puerta"; break;
					case ALARMA_PUERTAS_APERTURA_PROLONGADA: $notificacion->texto = "La puerta se encuentra abierta cuando debería estar cerrada"; break;
					case ALARMA_PUERTAS_CIERRE: $notificacion->texto = "Se ha realizado un cierre de la puerta"; break;
					case ALARMA_PUERTAS_CIERRE_PROLONGADO: $notificacion->texto = "La puerta se encuentra cerrada cuando debería estar abierta"; break;
					case ALARMA_MONTA_MACHO: $notificacion->texto = "Monta detectada en $notificacion->codigo ($notificacion->nombre)"; break;
					case ALERTA_IHT_PELIGROSO:
						$parametros = explode('#', $notificacion->parametros);
						$notificacion->texto = "IHT elevado detectado en $notificacion->nombre ($parametros[0] y rango $parametros[1])";
						break;
				}
				break;

			// Sensor
			case SENSOR:
				switch ($notificacion->id_alerta)
				{
					case ALARMA_BATERIA_BAJA: $notificacion->texto = "baja capacidad de la batería ($notificacion->parametros %)"; break;
					case ALARMA_SILOS_CAPACIDAD_BAJA: $notificacion->texto = "Alerta por baja capacidad ($notificacion->parametros %)"; break;
					case ALARMA_SILOS_CAPACIDAD_ALTA: $notificacion->texto = "Alerta por alta capacidad ($notificacion->parametros %)"; break;
					case ALARMA_SILOS_DESNIVEL: $notificacion->texto = "Alerta por desnivel de sensor. Es posible que se haya movido o que la tapa no esté cerrada"; break;
					case ALARMA_SILOS_RECEPCION: $notificacion->texto = "Alerta por lectura actualizada ($notificacion->parametros cm)"; break;
					case ALARMA_TEMPERATURA_BAJA: $notificacion->texto = "Alerta por baja temperatura ($notificacion->parametros" . "º C)"; break;
					case ALARMA_TEMPERATURA_ALTA: $notificacion->texto = "Alerta por alta temperatura ($notificacion->parametros" . "º C)"; break;
					case ALARMA_HUMEDAD_BAJA: $notificacion->texto = "Alerta por baja humedad relativa ($notificacion->parametros %)"; break;
					case ALARMA_HUMEDAD_ALTA: $notificacion->texto = "Alerta por baja humedad relativa ($notificacion->parametros %)"; break;
					case ALARMA_LUMINOSIDAD_BAJA: $notificacion->texto = "Alerta por baja luminosidad ($notificacion->parametros lux)"; break;
					case ALARMA_LUMINOSIDAD_ALTA: $notificacion->texto = "Alerta por alta luminosidad ($notificacion->parametros lux)"; break;
					case ALERTA_ETERMICO_PROLONGADO: $notificacion->texto = "Los animales se podrían encontrar en peligro debido al estrés térmico prolongado en su recinto"; break;
					case ALARMA_GPS_SALIDA_ANIMAL: $notificacion->texto = "Alerta por salida: El sensor se encuentra fuera del recinto indicado"; break;
					case ALARMA_GPS_ESTANCAMIENTO: $notificacion->texto = "El sensor no ha detectado cambios de movimiento por un tiempo prolongado"; break;
					case ALARMA_PUERTAS_APERTURA: $notificacion->texto = "Se ha detectad una apertura de puerta en el sensor"; break;
					case ALARMA_PUERTAS_APERTURA_PROLONGADA: $notificacion->texto = "Alerta: Se ha detectado que el sensor indica que la puerta lleva abierta mas tiempo del indicado, cuando debería estar cerrada"; break;
					case ALARMA_PUERTAS_CIERRE: $notificacion->texto = "Se ha detectad una cierre de puerta en el sensor"; break;
					case ALARMA_PUERTAS_CIERRE_PROLONGADO: $notificacion->texto = "Alerta: Se ha detectado que el sensor indica que la puerta lleva cerrada mas tiempo del indicado, cuando debería estar abierta"; break;
					case ALARMA_MONTA_MACHO: $notificacion->texto = "Alerta: Se ha detectado una monta del animal poseedor del sensor"; break;
				}
				break;
		}

		return $notificacion;
	}

	/*private function formatearNotificaciones($notificaciones)
	{
		// Ahora le añadimos un campo con el tiempo que ha pasado desde la notificación hasta ahora
		$actual = new \DateTime();

		foreach ($notificaciones as $notificacion)
		{
			$notificacion = $this->formatearNotificacion($notificacion, $actual);
		}

		return $notificaciones;
	}*/

	/*private function marcarLeidas($id_usuario)
	{
		try
		{
			// Las marcamos como leídas
			DB::update("UPDATE notificaciones SET leida = 1 WHERE id_usuario = ?", [$id_usuario]);

			// Actualizamos el resumen
			DB::update("UPDATE users SET notificaciones = 0 WHERE id = ?", [$id_usuario]);

			// Colocamos a 0 las del middleware Auth
			Auth::user()["notificaciones"] = 0;
			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}*/

	/*private function marcarNoLeidas()
	{
		$id_usuario = 1;
		try
		{
			// Las marcamos como leídas
			DB::update("UPDATE notificaciones SET leida = 0 WHERE id_usuario = ?", [$id_usuario]);

			// Actualizar el contador de notificaciones
			DB::update("UPDATE users SET notificaciones = (SELECT count(*) FROM notificaciones WHERE id_usuario = ? AND leida = 0) WHERE id = ?", [$id_usuario, $id_usuario]);
			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}	
	}*/

	// Permite filtrar las notificaciones y permite ver las de otros usuarios. Función solo válida para usuarios administradores
	public function obtenerNotificacionesConBuscador(Request $request)
	{
		try
		{
			if (esAdmin(Auth::user()["role"]))
			{
				$sql = "SELECT NE.id, entidades.id id_entidad, fecha, NE.tipo, entidades.id_tipo id_tipo, id_referencia, id_alerta, id_usuario, email, parametros, leida, entidades.nombre nombre, explotaciones.nombre nombreExplotacion, name nombreUsuario FROM (select id, fecha, tipo, id_referencia, id_alerta, id_usuario, parametros, leida FROM notificaciones WHERE tipo = 0 AND id_referencia = ?) NE JOIN entidades ON NE.id_referencia = entidades.id JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN users ON NE.id_usuario = users.id WHERE fecha BETWEEN ? AND ?";

				$criterios = [$request["id_entidad"], $request["fechaFiltroInicio"], incrementarUnDia($request["fechaFiltroFin"])];

				if($request["buscador"])
				{
					$sql .= " AND (email LIKE ? OR name LIKE ?)";
					$criterios[] = likear($request["buscador"]);
					$criterios[] = likear($request["buscador"]);
				}

				$sql .= " ORDER BY fecha DESC";

				// Ejecutamos la consulta
				$notificaciones = DB::select($sql, $criterios);

				// Formateamos las notificaciones
				foreach ($notificaciones as $notificacion)
					$notificacion = $this->formatearNotificacion($notificacion);

				return response()->view('notificaciones.notificaciones_entidad', ["notificaciones" => $notificaciones], 200);
			}
			else
			{
				return response()->json(['mensaje' => 'No tienes permiso para realizar esta acción'], 403);
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['mensaje' => 'Error desconocido, consulte con el administrador'], 500);
		}
	}

	// Función que envía una notificación al usuario indicado. Los parámetros son: id de entidad, service_id del usuario y mensaje
	// SOLO PARA LA APP DE MACHOS
	public function enviarPushNotificationMachos($idEntidad, $userIdOneSignal, $mensajeNotificacion)
	{
		// service_id_ionic Jose: a4182807-6583-4c80-9935-225972fdb879
		try
		{
			$url = "https://onesignal.com/api/v1/notifications";
			$client = new \GuzzleHttp\Client(['http_errors' => false]);

			$request = $client->request('POST', $url, [
				'headers' => [
				'Content-Type' => 'application/json',
				'authorization' => 'Basic YTUzMWUxYjctODY1Yy00NzAzLTgyMGYtZDNkNzcyZWRkYTdl'
			], 'body' => view("notificaciones.machos", ["mensaje" => $mensajeNotificacion, "id_entidad" => $idEntidad, "service_id_ionic" => $userIdOneSignal])]);

			// En caso de que haya un error en el envío de notificaciones, lo guardamos en el log
			if($request->getStatusCode() != 200)
				LogController::errores('[Notificaciones One Signal] ' . json_encode(json_decode($request->getBody())));

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores('[Notificaciones One Signal] ' . $e->getMessage());
		}
	}

	// Función que envía una notificación al usuario indicado. Los parámetros son: id de entidad, service_id del usuario y mensaje
	// En notificación está la notificación insertada, para que se coloque al principio del array de notificaciones
	public function enviarPushNotification($idEntidad, $userIdOneSignal, $mensajeNotificacion, $notificacion)
	{
		// service_id_ionic Jose: a4182807-6583-4c80-9935-225972fdb879
		try
		{
			// Formateamos la notificación antes de enviarla
			$notificacion = $this->obtenerNotificacionEntidadFormateadaPorId($notificacion->id);

			// Hacemos esto para que no envíe la notificación si el usuario no tiene token de one signal
			if ($userIdOneSignal == "")
				return false;

			$url = 'https://onesignal.com/api/v1/notifications';
			$client = new \GuzzleHttp\Client(['http_errors' => false]);

			$request = $client->request('POST', $url, [
				'headers' => [
				'Content-Type' => 'application/json',
				'authorization' => 'Basic ' . REST_API_KEY
			],'body' =>view('notificaciones.entidad', ['mensaje' => $mensajeNotificacion, 'id_entidad' => $idEntidad, 'service_id_ionic' => $userIdOneSignal, 'notificacion' => $notificacion])]);

			// En caso de que haya un error en el envío de notificaciones, lo guardamos en el log
			if($request->getStatusCode() != 200)
				LogController::errores('[Notificaciones One Signal] ' . json_encode(json_decode($request->getBody())));

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores('[Notificaciones One Signa ENTIDAD] ' . $e->getMessage());
		}
	}

	public function enviarPushNotificationSensor($idSensor, $userIdOneSignal, $mensajeNotificacion, $notificacion)
	{
		// service_id_ionic Jose: a4182807-6583-4c80-9935-225972fdb879
		try
		{
			// Formateamos la notificación antes de enviarla
			$notificacion = $this->obtenerNotificacionEntidadFormateadaPorId($notificacion->id);

			// Hacemos esto para que no envíe la notificación si el usuario no tiene token de one signal
			if ($userIdOneSignal == "")
				return false;

			$url = 'https://onesignal.com/api/v1/notifications';
			$client = new \GuzzleHttp\Client(['http_errors' => false]);

			$request = $client->request('POST', $url, [
				'headers' => [
				'Content-Type' => 'application/json',
				'authorization' => 'Basic ' . REST_API_KEY
			],'body' =>view('notificaciones.sensor', ['mensaje' => $mensajeNotificacion, 'id_sensor' => $idSensor, 'service_id_ionic' => $userIdOneSignal, 'notificacion' => $notificacion])]);

			// En caso de que haya un error en el envío de notificaciones, lo guardamos en el log
			if($request->getStatusCode() != 200)
				LogController::errores('[Notificaciones One Signal SENSOR] ' . json_encode(json_decode($request->getBody())));

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores('[Notificaciones One Signal SENSOR] ' . $e->getMessage());
		}
	}

	// Marca como leída la notificación seleccionada, siempre que sea del usuario logeado
	public function leerNotificacion($idNotificacion)
	{
		try
		{
			$idUsuario = Auth::user()['id'];

			// Comprobamos antes si está leída o no
			$leida = DB::select('SELECT id_referencia id_entidad, leida FROM notificaciones WHERE id = ? AND id_usuario = ? AND tipo = 0 AND leida = 0', [$idNotificacion, $idUsuario]);

			// Si ya estaba leída o no existe, no modificamos nada. Si por el contrario, no estaba leída, reducimos en 1 el número de notificaciones
			if (!empty($leida))
			{
				// Marcamos como leída la notificación
				DB::update('UPDATE notificaciones SET leida = 1 WHERE id = ? AND id_usuario = ?', [$idNotificacion, $idUsuario]);

				// Reducimos en 1 el contador de notificaciones de la entidad
				DB::update('UPDATE entidades_usuarios SET notificaciones = notificaciones - 1 WHERE id_usuario = ? AND id_entidad = ? AND notificaciones <> 0', [$idUsuario, $leida[0]->id_entidad]);

				// Reducimos en 1 el número de notificaciones del usuario
				DB::update('UPDATE users SET notificaciones = notificaciones - 1 WHERE id = ? AND notificaciones <> 0', [$idUsuario]);

				// Las colocamos en middleware Auth
				Auth::user()['notificaciones'] -= 1;

				return response()->json(['estado' => true, 'data' => true], 200);
			}

			return response()->json(['estado' => true, 'data' => false], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MARCAR NOTIFICACION ENTIDAD LEIDA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos', 'e' => $e->getMessage()], 200);
		}
	}

	// Marca como leída la notificación seleccionada, siempre que sea del usuario logeado
	public function marcarNotificacionNoLeida($idNotificacion)
	{
		try
		{
			$idUsuario = Auth::user()['id'];

			// Comprobamos antes si está leída o no
			$leida = DB::select('SELECT id_referencia id_entidad, leida FROM notificaciones WHERE id = ? AND id_usuario = ? AND tipo = 0 AND leida = 1', [$idNotificacion, $idUsuario]);

			// Si ya estaba leída o no existe, no modificamos nada. Si por el contrario, no estaba leída, reducimos en 1 el número de notificaciones
			if (!empty($leida))
			{
				// Marcamos como leída la notificación
				DB::update('UPDATE notificaciones SET leida = 0 WHERE id = ? AND id_usuario = ?', [$idNotificacion, $idUsuario]);

				// Reducimos en 1 el contador de notificaciones de la entidad
				DB::update('UPDATE entidades_usuarios SET notificaciones = notificaciones + 1 WHERE id_usuario = ? AND id_entidad = ? AND notificaciones <> 0', [$idUsuario, $leida[0]->id_entidad]);

				// Reducimos en 1 el número de notificaciones del usuario
				DB::update('UPDATE users SET notificaciones = notificaciones + 1 WHERE id = ? AND notificaciones <> 0', [$idUsuario]);

				// Las colocamos en middleware Auth
				Auth::user()['notificaciones'] += 1;

				return response()->json(['estado' => true, 'data' => true], 200);
			}

			return response()->json(['estado' => true, 'data' => false], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MARCAR NOTIFICACION ENTIDAD NO LEIDA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos', 'e' => $e->getMessage()], 200);
		}
	}

	// Borra la notificación seleccionada, siempre que sea del usuario logeado
	public function borrarNotificacion($idNotificacion)
	{
		try
		{
			$idUsuario = Auth::user()['id'];

			// Comprobamos antes si está leída o no
			$existe = DB::select('SELECT id_referencia id_entidad, leida FROM notificaciones WHERE id = ? AND id_usuario = ? AND tipo = 0', [$idNotificacion, $idUsuario]);

			// Indicamos si habrá reducción de notificaciones en la app
			$cambio = false;
			$borrado = false;

			// Si ya estaba leída o no existe, no modificamos nada. Si por el contrario, no estaba leída, reducimos en 1 el número de notificaciones
			if (!empty($existe))
			{
				// Borramos la notificación
				DB::delete('DELETE FROM notificaciones WHERE id = ? AND id_usuario = ?', [$idNotificacion, $idUsuario]);
				$borrado = true;

				// Ahora comprobamos si está leída. Si no está leida, reducimos en uno el número de notificaciones. Si está leída, el número de notificaciones se queda igual
				if ($existe[0]->leida != true)
				{
					// Reducimos en 1 el contador de notificaciones de la entidad
					DB::update('UPDATE entidades_usuarios SET notificaciones = notificaciones - 1 WHERE id_usuario = ? AND id_entidad = ?', [$idUsuario, $existe[0]->id_entidad]);

					// Reducimos en 1 el número de notificaciones del usuario
					DB::update('UPDATE users SET notificaciones = notificaciones - 1 WHERE id = ?', [$idUsuario]);

					// Las colocamos en middleware Auth
					Auth::user()['notificaciones'] -= 1;

					$cambio = true;
				}
			}

			return response()->json(['estado' => true, 'data' => $cambio, 'borrado' => $borrado], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[BORRAR ALERTA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos', 'e' => $e->getMessage()], 200);
		}
	}

	public function marcarSeleccionadasLeidas(Request $request)
	{
		try
		{
			$notificaciones = isset($request['notificaciones']) ? $request['notificaciones'] : [];
			$idUsuario = Auth::user()['id'];

			// Marcamos como leídas las indicadas
			DB::update('UPDATE notificaciones SET leida = 1 WHERE id IN ' . obtenerInterrogacionesSQL($notificaciones), $notificaciones);

			// Actualizamos las notificaciones del usuario
			DB::statement('call actualizar_notificaciones_usuario(?)',[$idUsuario]);

			// Obtenemos las notificaciones actuales para devolverlas
			$notificaciones = $this->obtenerTotalNotificacionesPorUsuario($idUsuario);

			// Las colocamos en middleware Auth
			Auth::user()['notificaciones'] = $notificaciones;

			return response()->json(['estado' => true, 'data' => $notificaciones], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MARCAR NOTIFICACIONES SELECCIONADAS LEIDAS] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos'], 200);
		}
	}

	public function marcarSeleccionadasNoLeidas(Request $request)
	{
		try
		{
			$notificaciones = isset($request['notificaciones']) ? $request['notificaciones'] : [];
			$idUsuario = Auth::user()['id'];

			// Marcamos como leídas las indicadas
			DB::update('UPDATE notificaciones SET leida = 0 WHERE id IN ' . obtenerInterrogacionesSQL($notificaciones), $notificaciones);

			// Actualizamos las notificaciones del usuario
			DB::statement('call actualizar_notificaciones_usuario(?)',[$idUsuario]);

			// Obtenemos las notificaciones actuales para devolverlas
			$notificaciones = $this->obtenerTotalNotificacionesPorUsuario($idUsuario);

			// Las colocamos en middleware Auth
			Auth::user()['notificaciones'] = $notificaciones;

			return response()->json(['estado' => true, 'data' => $notificaciones], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MARCAR NOTIFICACIONES SELECCIONADAS LEIDAS] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos'], 200);
		}
	}

	public function marcarSeleccionadasBorrar(Request $request)
	{
		try
		{
			$notificaciones = isset($request['notificaciones']) ? $request['notificaciones'] : [];
			$idUsuario = Auth::user()['id'];

			// Marcamos como leídas las indicadas
			DB::update('DELETE FROM notificaciones WHERE id IN ' . obtenerInterrogacionesSQL($notificaciones) . ' AND id_usuario = ?', array_merge($notificaciones, [$idUsuario]));

			// Actualizamos las notificaciones del usuario
			DB::statement('call actualizar_notificaciones_usuario(?)',[$idUsuario]);

			// Obtenemos las notificaciones actuales para devolverlas
			$notificaciones = $this->obtenerTotalNotificacionesPorUsuario($idUsuario);

			// Las colocamos en middleware Auth
			Auth::user()['notificaciones'] = $notificaciones;

			return response()->json(['estado' => true, 'data' => $notificaciones], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[MARCAR NOTIFICACIONES SELECCIONADAS LEIDAS] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotos'], 200);
		}
	}
}
