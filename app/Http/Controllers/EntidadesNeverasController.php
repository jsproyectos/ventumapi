<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use App\EntidadNevera;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class EntidadesNeverasController extends EntidadesController
{
	public function inicializar($id_entidad, $fecha, $min, $max)
	{
		if ($entidad = EntidadNevera::create([
			'id_entidad' => $id_entidad,
			'fecha' => $fecha,
			'temperatura' => 0,
			'humedad' => 0,
			'estado' => 0,
			'min' => $min,
			'max' => $max,
		]))
		{
			return true;
		}
		return false;
	}

	public function obtenerResumen(Request $request)
	{
		try
		{
			$orden = $this->ordenMostrar($request['orden']);

			$entidades = DB::select('SELECT entidades.id id, id_tipo, nombre, tiempo, codigo, descripcion, entidades_neveras.fecha, temperatura, humedad, min, max, autorizado, notificaciones FROM entidades JOIN entidades_neveras ON entidades.id = entidades_neveras.id_entidad JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE id_usuario = ? AND entidades_usuarios.fechaBaja IS NULL AND entidades.fechaBaja IS NULL ORDER BY ' . $orden, [Auth::user()['id']]);

			// Convertimos el autorizado en un booleano para indicar si la entidad es autorizada o propia y se convierten los valores string de humedad y temperatura en floats.
			foreach ($entidades as $entidad)
			{
				$entidad->propio = $entidad->autorizado == 0 ? true : false;
				unset ($entidad->autorizado);
				$entidad->temperatura = floatval($entidad->temperatura);
				$entidad->humedad = floatval($entidad->humedad);
				$entidad->min = floatval($entidad->min);
				$entidad->max = floatval($entidad->max);
			}

			return response()->json(['estado' => true, 'datos' => $entidades], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	private function ordenMostrar($orden)
	{
		switch ($orden)
		{
			case 'nombre': return "nombre ASC"; break;
			case 'temperatura': return "temperatura ASC"; break;
			case 'humedad': return "humedad ASC"; break;
			default: return "nombre ASC"; break;
		}
	}

	public function obtenerDetallesEntidad($idEntidad)
	{
		$entidad = DB::select("SELECT entidades.id id, id_tipo, tiempo, entidades_sensores.id_sensor, entidades.nombre nombre, entidades.codigo, entidades.latitud, entidades.longitud, entidades.descripcion, entidades_neveras.fecha, temperatura, humedad, min, max, explotaciones.nombre nombreExplotacion, fichero, autorizado, notificaciones FROM entidades JOIN entidades_neveras ON entidades.id = entidades_neveras.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad LEFT JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_usuarios.id_entidad = ? AND id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL", [$idEntidad, Auth::user()['id']])[0];

		$entidad->propio = $entidad->autorizado == 0 ? true : false;
		unset ($entidad->autorizado);
		$entidad->temperatura = floatval($entidad->temperatura);
		$entidad->humedad = floatval($entidad->humedad);
		$entidad->min = floatval($entidad->min);
		$entidad->max = floatval($entidad->max);
		$entidad->latitud = floatval($entidad->latitud);
		$entidad->longitud = floatval($entidad->longitud);

		return $entidad;
	}

	public function obtenerDetalles($idEntidad)
	{
		try
		{
			return response()->json(['estado' => true, 'mensaje' => $this->obtenerDetallesEntidad($idEntidad)], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerHistorico($idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			if (esAdmin(Auth::user()['role']) || $this->puedeVer($idEntidad, Auth::user()['id']))
			{
				$historico = DB::select("SELECT fecha, temperatura, humedad FROM lecturas_temperatura_humedad WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC", [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			foreach ($historico as $registro)
			{
				$registro->temperatura = floatval($registro->temperatura);
				$registro->humedad = floatval($registro->humedad);
			}

			return response()->json(['estado' => true, 'mensaje' => $historico], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER HISTORICO NEVERA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function actualizar($idEntidad, $fecha, $temperatura, $humedad, $estado)
	{
		try
		{
			DB::update("UPDATE entidades_neveras SET fecha = ?, temperatura = ?, humedad = ?, estado = ? WHERE id_entidad = ?", [$fecha, $temperatura, $humedad, $estado, $idEntidad]);

			// Una vez actualizados los datos de las neveras, se procede a lanzar las alertas
			$job = new \App\Jobs\alertasEntidadNevera($idEntidad, $fecha, $temperatura, $humedad);
			dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}
}
