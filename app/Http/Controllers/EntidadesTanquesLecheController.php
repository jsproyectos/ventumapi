<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntidadesTanquesLecheController extends EntidadesRecipientesController
{
	public function obtenerResumen(Request $request)
	{
		try
		{
			// Medida de seguridad
			$orden = $request['orden'] ? $request['orden'] : null;
			$nombre = $request['nombre'] ? $request['nombre'] : null;
			$explotacion = $request['explotacion'] ? $request['explotacion'] : null;
			$falsas = $request['falsas'] ? true : false;
			$bajas = $request['bajas'] ? true : false;

			$entidades = $this->obtenerResumenRecipientes(ENTIDAD_LECHE, $orden, $nombre, $explotacion, $falsas, $bajas);

			return response()->json(['estado' => true, 'datos' => $entidades], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerDetallesEntidad($idEntidad)
	{
		$entidad = DB::select('SELECT entidades.id id, id_tipo, tiempo, ES.id_sensor, entidades.nombre nombre, entidades.codigo, entidades.latitud, entidades.longitud, entidades.descripcion, id_ganado tipoGanado, animales, capacidad, porcentaje, kilos, temperatura, fecha_ordeno, celulas, ordenados, litros, autorizado, id_explotacion, fichero, explotaciones.nombre nombreExplotacion, notificaciones FROM entidades JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad JOIN entidades_tanques_leche ON entidades.id = entidades_tanques_leche.id_entidad JOIN explotaciones ON entidades.id_explotacion = explotaciones.id JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad LEFT JOIN (SELECT * FROM entidades_sensores WHERE entidades_sensores.fechaBaja IS NULL) ES ON entidades.id = ES.id_entidad WHERE entidades_usuarios.id_entidad = ? AND id_usuario = ? AND entidades.fechaBaja IS NULL AND entidades_usuarios.fechaBaja IS NULL', [$idEntidad, Auth::user()['id']])[0];

		$entidad->propio = $entidad->autorizado == 0 ? true : false;
		unset ($entidad->autorizado);
		$entidad->temperatura = floatval($entidad->temperatura);
		$entidad->latitud = floatval($entidad->latitud);
		$entidad->longitud = floatval($entidad->longitud);

		return $entidad;
	}

	public function obtenerDetalles($idEntidad)
	{
		try
		{
			return response()->json(['estado' => true, 'mensaje' => $this->obtenerDetallesEntidad($idEntidad)], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function obtenerHistorico($idEntidad, $fechaInicio, $fechaFin)
	{
		try
		{
			if (esAdmin(Auth::user()['role']) || $this->puedeVer($idEntidad, Auth::user()['id']))
			{
				$historico = DB::select('SELECT fecha, temperatura, "0" porcentaje, "0" kilos FROM lecturas_temperatura_humedad WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC', [$idEntidad, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			foreach ($historico as $registro)
			{
				$registro->temperatura = floatval($registro->temperatura);
				$registro->porcentaje = intval($registro->porcentaje);
				$registro->kilos = intval($registro->kilos);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER HISTORICO NEVERA] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function actualizar($idEntidad, $idTipo, $fecha, $porcentaje, $kilos, $temperatura, $aceX = 0, $aceY = 0, $aceZ = 0)
	{
		try
		{
			if($idEntidad != null)
			{
				// Utilizo consultas directas para que sea mas eficiente
				$consulta = 'UPDATE entidades_recipientes SET fecha = ?';
				$parametros = [$fecha];

				if ($porcentaje != null)
				{
					$consulta .= ', porcentaje = ?';
					$parametros[] = $porcentaje;
				}

				if ($temperatura != null)
				{
					$consulta .= ', temperatura = ?';
					$parametros[] = $temperatura;
				}

				if ($kilos != null)
				{
					$consulta .= ', kilos = ?';
					$parametros[] = $kilos;
				}

				$consulta .= ' WHERE id_entidad = ?';
				$parametros[] = $idEntidad;

				/*if ($aceX != null && $aceY != null && $aceZ != null)
				{
					$consulta .= ', kilos = ?'
					$parametros[] = $kilos;
				}*/

				DB::update($consulta, $parametros);
				
				$job = new \App\Jobs\alertasEntidadTanqueLeche($idEntidad, $porcentaje, $kilos, $fecha, $temperatura, $aceX, $aceY, $aceZ);
				dispatch($job);
			}
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
		return true;
	}
}
