<?php

namespace App\Http\Controllers;

define("LON_TEMP_DIST_SIGFOX_SILOS", 16);
define("LON_TEMP_DIST_SIGFOX_SILOS_VENTUM", 32);
define("ABRIR_PUERTA_IOTA", "aa"); // Información que envía IOTA cuando un sensor de puertas detecta apertura
define("CERRAR_PUERTA_IOTA", "bb"); // Información que envía IOTA cuando un sensor de puertas detecta cierre

use DateTime;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class ComunicacionSigfoxController extends ComunicacionController
{
	// Indica que toda la operación ha funcionado correctamente para que Sigfox lo interprete
	private function todoBien($idSensor, $device, $ack)
	{
		if ($ack == 'true')
		{
			$downlinkData = DB::select('SELECT downlink FROM sensores WHERE id = ?', [$idSensor])[0]->downlink;

			return $downlinkData == null ? json_encode(array("" => array("noData" => "true"))) : json_encode(array($device => array('downlinkData' => $downlinkData)));
		}
		else
			return json_encode(array("" => array('noData' => 'true')));
	}

	// Ejemplo
	// ventumapilocal.es/api/sigfox/temperatura/tst/2039A2/1594420062/000680000b1e10e40d460000
	// TH3 ventumapilocal.es/api/sigfox/temperatura/tst/20398B/1594420062/000680000b1e10e40d460000
	// ventumapilocal.es/api/sigfox/temperatura/tst/79A77/1594420062/000680000b1e10e40d460224
	// ventumapilocal.es/api/sigfox/temperatura/tst/79A63/1594420062/02728000069c18280ca00002
	// THL3 (26): ventumapilocal.es/api/sigfox/temperatura/tst/79B48/1594420062/02728000069c18280ca00002 0.2 lux
	// Falso: STF001, asignado a nevera falsa
	// ventumapilocal.es/api/sigfox/temperatura/tst/STF001/1594420062/02728000069c18280ca00002
	// Falso: STHLF001, asignado a la celda falsa Celda P0001
	// ventumapilocal.es/api/sigfox/temperatura/tst/STHLF001/1594420062/02728000069c18280ca00002
	// SELECT * FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE id_sensor = (SELECT id FROM sensores WHERE dispositivo = 'STF001')
	public function registrarTemperaturaTST($device, $time, $data, Request $request)
	{
		try
		{
			$fecha = date('Y/m/d H:i:s', $time);
			$sensor = $this->obtenerInfoSensor($device); $snr = null; $rssi = null;

			switch ($sensor->id_tipo_sensor)
			{
				case SENSOR_TEMPERATURA: $trama = app('App\Http\Controllers\SensoresTempHumController')->decodificarTramaTempHum($data); break;
				case SENSOR_TEMP_HUM_LUZ: $trama = app('App\Http\Controllers\SensoresTempHumController')->decodificarTramaTempHumLuz($data);	break;
			}

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);
			app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);

			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX TEMPERATURA TST] ' . $e->getMessage());
		}
	}

	// ventumapilocal.es/api/sigfox/temperatura/iota/37CCF0/1597742467/03fc20030020040010040010
	// https://api.ventumidc.es/api/sigfox/temperatura/iota/37CCF0/1597742467/fc03f0fb03fffb03e1fb03f0
	public function registrarTemperaturaIOTA($device, $time, $data)
	{
		try
		{
			//$sensor = $this->obtenerIdYEntidad($device);
			$sensor = $this->obtenerInfoSensor($device); $snr = null; $rssi = null;

			// Establecemos la primera fecha de la medición (-45 min). La útlima medición es atual, la anterior 15 minutos, la anterior 30 minutos y la primera 45 minutos
			$time = strtotime("-45 min", $time);

			// Comprobamos si el mensaje es acerca de la batería (c9, c8, etc..)
			if (strlen($data) == 2)
			{
				$bateria = app('App\Http\Controllers\SensoresController')->calcularBateriaIOTA($data);
				$sensor->bateria = $bateria['voltaje'];
				$sensor->bp = $bateria['bp'];
			}
			else
			{
				$tramas = app('App\Http\Controllers\SensoresTempHumController')->decodificarTramaIOTA($this->binarizarCadena($data));

				foreach ($tramas as $trama)
				{
					$fecha = date('Y/m/d H:i:s', $time);
					$time = strtotime("+15 min", $time);

					// Insertar en la tabla de historicos
					app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $sensor->bateria, $sensor->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, 0, $trama->etermico, false);
					app('App\Http\Controllers\EntidadesController')->actualizarTemperaturaIOTA($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $trama->temperatura, $trama->humedad, 0, $trama->etermico);
				}
			}

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $sensor->bateria, $sensor->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX TEMPERATURA IOTA] ' . $e->getMessage());
		}
	}

	// ventumapilocal.es/api/sigfox/temperatura/ventum/1B29CFB/1597998746/00000a92
	public function registrarTemperaturaVentum($device, $time, $data)
	{
		try
		{
			$fecha = date('Y/m/d H:i:s', $time);
			$sensor = $this->obtenerInfoSensor($device); $snr = null; $rssi = null;

			$trama = app('App\Http\Controllers\SensoresTempHumController')->decodificarTramaTemperaturaVentum($data);

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);
			app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);

			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX TEMPERATURA VENTUM] ' . $e->getMessage());
		}
	}

	public function registrarTemperaturaYHumedadVentum($device, $time, $data)
	{
		try
		{
			$fecha = date('Y/m/d H:i:s', $time);
			$sensor = $this->obtenerInfoSensor($device); $snr = null; $rssi = null;

			$trama = app('App\Http\Controllers\SensoresTempHumController')->decodificarTramaTemperaturaYHumedadVentum($this->binarizarCadena($data));

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);
			app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);

			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX TEMPERATURA VENTUM] ' . $e->getMessage());
		}
	}

	public function registrarTHBVentum($device, $time, $data)
	{
		try
		{
			$fecha = date('Y/m/d H:i:s', $time);
			$sensor = $this->obtenerInfoSensor($device); $snr = null; $rssi = null;

			$trama = app('App\Http\Controllers\SensoresTempHumController')->decodificarTramaTHBVentum($this->binarizarCadena($data));

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);
			app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);

			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX TEMPERATURA VENTUM] ' . $e->getMessage());
		}
	}

	// ventumapilocal.es/api/sigfox/co2/ventum/C1D3BF/1616603291/000017a900000fbf0000c60c
	public function registrarCO2Ventum($device, $time, $data)
	{
		try
		{
			$fecha = date('Y/m/d H:i:s', $time);
			$sensor = $this->obtenerInfoSensor($device); $snr = null; $rssi = null;

			$trama = app('App\Http\Controllers\SensoresCO2Controller')->decodificarTramaVentum($this->binarizarCadena($data));

			// Insertar en la tabla de historicos
			app('App\Http\Controllers\SensoresCO2Controller')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, $trama->etermico, $trama->co2);
			// app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $trama->temperatura, $trama->humedad, $trama->luminosidad, $trama->etermico);

			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX CO2 VENTUM] ' . $e->getMessage());
		}
	}

	// ventumapilocal.es/api/sigfox/gps/tst/2061C1/1598386055/04d84aa3ad5f952d77000000
	public function registrarGpsTST($device, $time, $data, Request $request)
	{
		try
		{
			$fecha = date('Y/m/d H:i:s', $time);
			$sensor = $this->obtenerInfoSensor($device);
			$snr = null; $rssi = null;
			
			// Insertar en la Base de datos
			$trama = app('App\Http\Controllers\SensoresGpsController')->decodificarTramaGPS($data);
			$bp = app('App\Http\Controllers\SensoresController')->calcularBateriaPorcentaje3($trama["BATTERY"]);

			app('App\Http\Controllers\SensoresGpsController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $trama["BATTERY"], $bp, $rssi, $snr, $fecha, $trama["LATITUDE"], $trama["LONGITUDE"], $trama["NSEQ_CONFIG"], $trama["IRQ_ACC_FLAG"], $trama["IRQ_DI_FLAG"], $trama["DI_VALUE"], $trama["LAST_GOOD_POSITION_FLAG"], $trama["NUMBER_OF_SATELLITES"], $trama["SPEED_IN_KMPH"]);

			// Si tiene entidad, la actualizamos
			if ($sensor->id_entidad)
				DB::update('UPDATE entidades SET latitud = ?, longitud = ? WHERE id = ?', [ $trama['LATITUDE'],  $trama['LONGITUDE'], $sensor->id_entidad]);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX GPS TST] ' . $e->getMessage());
		}
	}

	// Oficina
	// Bateria: ventumapilocal.es/api/sigfox/puertas/iota/C4201/1594420062/c9
	// Abrir: ventumapilocal.es/api/sigfox/puertas/iota/C4201/1594420062/aa
	// Cerrar: ventumapilocal.es/api/sigfox/puertas/iota/C4201/1594420062/bb
	public function registrarPuertasIOTA($device, $time, $data, Request $request)
	{
		try
		{
			$sensor = $this->obtenerInfoSensor($device);
			$fecha = date('Y-m-d H:i:s', $time);
			$snr = null; $rssi = null;

			switch ($data)
			{
				case ABRIR_PUERTA_IOTA: $tipo = PUERTA_ABIERTA;	break;
				case CERRAR_PUERTA_IOTA: $tipo = PUERTA_CERRADA; break;
				default:
					$tipo = PUERTA_BATERIA;
					$bateria = app('App\Http\Controllers\SensoresPuertasController')->calcularBateriaIOTA($data);
					$sensor->bateria = $bateria['voltaje']; $sensor->bp = $bateria['bp'];
					break;
			}

			app('App\Http\Controllers\SensoresPuertasController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $fecha, $tipo, $sensor->bateria, $sensor->bp, $snr, $rssi);
			app('App\Http\Controllers\EntidadesPuertasController')->actualizar($sensor->id_entidad, $fecha, $tipo);

			$this->actualizarInformacionSensor($sensor->id_sensor, $sensor->bateria, $sensor->bp, $snr, $rssi);
			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX PUERTAS IOTA] ' . $e->getMessage());
		}
	}

	// ventumapilocal.es/api/sigfox/llenado/tst/388C50/1594420062/d1116ae3fcfcc0/false
	// FAKE: Entidad 27 y sensor 46 (SiloPruebas001)
	// ventumapilocal.es/api/sigfox/llenado/tst/SF00001/1594420062/d1116ae3fcfcc0/false
	public function registrarLlenadoTST($device, $time, $data, $ack)
	{
		try
		{
			$sensor = $this->obtenerInfoSensorSilo($device);
			$fecha = date('Y/m/d H:i:s', $time);
			$snr = null; $rssi = null;

			// Procedemos a separar el payload
			$tipo = hexdec(substr($data, 0, 1));
			$config = hexdec(substr($data, 1, 1));
			$bateria = hexdec(substr($data, 2, 1)); // Nivel de batería: 0 (muy bueno) hasta 3 (muy malo)

			// Hacemos esto para que la batería siempre esté entre 0 y 4
			$bateria = min(max(0, $bateria), 4);

			$bp = app('App\Http\Controllers\SensoresNivelLlenadoController')->transformarBateria($bateria); // Batería porcentaje
			$frame = hexdec(substr($data, 3, 1));	// Número de frames enviados

			$acelerometroX = bindec8(decbin(hexdec(substr($data, 8, 2))));
			$acelerometroY = bindec8(decbin(hexdec(substr($data, 10, 2))));
			$acelerometroZ = bindec8(decbin(hexdec(substr($data, 12, 2))));

			// Ahora, lo más importante: la temperatura y la distancia. Primero se convierte a binario (ya que tempertura requiere 7 bits y distancia 9)
			$informacion = base_convert(substr($data, 4, 4), 16, 2);
			$numeroCeros = LON_TEMP_DIST_SIGFOX_SILOS - strlen($informacion); // Número de 0s que hay que poner a la izquierda de la trama binaria
			$cerosIzquierda = "";

			// Se crea la cadena con tantos 0s como sea necesario para concatenar
			for ($i = 0; $i < $numeroCeros; $i++)
				$cerosIzquierda .= "0";

			$informacion = $cerosIzquierda . $informacion;
			$temperatura = bindec(substr($informacion, 0, 7)) - 30;
			$altura = bindec(substr($informacion, 7, 9));

			// Si la altura es 1 (no tiene fin) indicamos 300 centimetros, en caso contrario, lo dejamos como está
			// CORREGIR, que 300 no sea nunca mayor que la altura máxima del silo
			$altura = $altura == 1 ? 300 : $altura;
			$porcentaje = 100;

			if ($sensor->id_tipo_entidad == ENTIDAD_TOLVA)
			{
				$porcentaje = app('App\Http\Controllers\EntidadesTolvasController')->CalcularPorcentajeTolvaValdesequera($altura);
			}
			else
			{
				$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $altura);
			}

			$kilos = $porcentaje * $sensor->capacidad / 100;

			// Ahora se procede a su inserción y a insertar las alertas sobre las entidades
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $fecha, $bateria, $bp, $rssi, $snr, 1, $altura, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, $ack);	
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX SILOS TST] ' . $e->getMessage());	
		}
	}


	// ventumapilocal.es/api/sigfox/llenado/ventum/C1D3C6/1620295795/46
	public function registrarLlenadoVentum($device, $time, $data, Request $request)
	{
		try
		{
			$sensor = $this->obtenerInfoSensorSilo($device);
			$fecha = date('Y/m/d H:i:s', $time);
			$snr = null; $rssi = null;

			$bateria = 0; // Nivel de batería: 0 (muy bueno) hasta 3 (muy malo)
			$bp = app('App\Http\Controllers\SensoresNivelLlenadoController')->transformarBateria($bateria); // Batería porcentaje
			$temperatura = 0;
			$acelerometroX = null; $acelerometroY = null; $acelerometroZ = null;

			// Obtenemos la distancia
			$informacion = base_convert($data, 16, 2);
			$distancia = str_pad($informacion, LON_TEMP_DIST_SIGFOX_SILOS_VENTUM, '0', STR_PAD_LEFT);
			$distancia = bindecComplemento($distancia, 32);

			// TODO Hacemos esto porque hubo un problema a la hora de calcular la distnacia con este sensor. Envía la distancia en mm en lugar de cm por tanto cuando se arregle en el sensor hay que quitar esta función
			//if ($device == 'C1D3C6')
				//$distancia = intval($distancia / 10);

			$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $distancia);
			$kilos = $porcentaje * $sensor->capacidad / 100;

			// Ahora se procede a su inserción y a insertar las alertas sobre las entidades
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $fecha, $bateria, $bp, $rssi, $snr, 1, $distancia, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ);

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX SILOS VENTUM] ' . $e->getMessage());	
		}
	}

	
	// NORMAL: ventumapilocal.es/api/sigfox/estacion/libelium/204644/1599742289/41AC0CDD19BD971039094909
	// LLUVIA: ventumapilocal.es/api/sigfox/estacion/libelium/204644/1599742289/E0D9F81A240010
	// https://api.ventumidc.es/api/sigfox/temperatura/iota/37CCF0/1597742467/fc03f0fb03fffb03e1fb03f0
	public function registrarEstacionLibelium($device, $time, $data)
	{
		try
		{
			//$sensor = $this->obtenerIdYEntidad($device);
			$sensor = $this->obtenerInfoSensor($device); $snr = null; $rssi = null;
			$fecha = date('Y/m/d H:i:s', $time);

			$trama = app('App\Http\Controllers\SensoresEstacionMeteorologicaController')->decodificarTrama($this->binarizarCadena($data));

			// Si configuración es 0, la trama es estándar
			if ($trama->configuracion == ESTACION_CONFIG_NORMAL)
			{
				// Insertar en la tabla de historicos
				app('App\Http\Controllers\SensoresEstacionMeteorologicaController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->temperatura, $trama->humedad, $trama->presion, $trama->viento, $trama->direccion);
				// app('App\Http\Controllers\EntidadesController')->actualizarTemperaturaIOTA($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $trama->temperatura, $trama->humedad, 0, $trama->etermico);				
			}
			else if ($trama->configuracion == ESTACION_CONFIG_EXCEPCIONAL) // Trama excepcional
			{
				switch ($trama->excepcion)
				{
					// Si es de lluvia, actualizamos la información
					case ESTACION_EXCEPCION_LUVIA:
						$fecha = date('Y/m/d H:i:s', $time - 3600);

						// Temporal, solo para registrar la trama de lluvia y ver cuando llega
						app('App\Http\Controllers\SensoresEstacionMeteorologicaController')->insertarLluvia($sensor->id_sensor, $sensor->id_entidad, $data, $trama->bateria, $trama->bp, $rssi, $snr, $fecha, $trama->lluvia);

						// Actualizamos de forma retroactiva las lecturas de la hora anterior
						app('App\Http\Controllers\SensoresEstacionMeteorologicaController')->actualizarLecturaConLluvia($sensor->id_sensor, $fecha, $trama->lluvia);
						break;
					
					default:
						# code...
						break;
				}
			}

			// Actualizar la batería y la señal
			$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			return $this->todoBien($sensor->id_sensor, $device, false);
		}
		catch (Exception $e)
		{
			return $this->devolverError($device, '[SIGFOX TEMPERATURA LIBELIUM] ' . $e->getMessage());
		}
	}

	/* Funciones de llenado automático de sensores, en caso de errores */
	/* Temperatura y humedad TST Sigfox */
	private function _registrarTemperaturaTST()
	{
		$temperaturas = [
			"2020-05-04 19:43:21;2039DB;124c8000029c1bf20ca60000",
		];

		// Una request vacía
		$request = new Request();

		foreach ($temperaturas as $temperatura)
		{
			$campos = explode(";", $temperatura);
			$device = $campos[1];
			$fecha = $campos[0];
			$data = $campos[2];

			$time = DateTime::createFromFormat('Y-m-d H:i:s', $fecha)->getTimestamp();
			$this->registrarTemperaturaTST($device, $time, $data, $request);
		}
	}

	/* Temperatura y humedad IOTA */
	private function _registrarTemperaturaIOTA()
	{
		$temperaturas = [
			'2020-07-22 16:29:01#37C9F5#0103c10103c00103c00003c0#8.5',
			'2020-07-22 16:35:54#C025DD#0e03b110ffc01003a11003a0#18.98',
			'2020-07-22 16:37:11#C025CD#1003c01003c01103c01103a1#16.88',
			'2020-07-22 16:46:14#37CCEE#11ff810e0b800fff80100371#10.01',
			'2020-07-22 16:47:11#37CCF0#1003af12ffa011038113038f#12.08',
			'2020-07-22 17:29:00#37C9F5#0003c0ff03c0ff03c0ff03c0#13.94',
			'2020-07-22 17:35:55#C025DD#11ffa01007a0100390100390#19.3',
			'2020-07-22 17:37:12#C025CD#12ffa01203b01203911303a0#13.36',
			'2020-07-22 17:46:14#37CCEE#0e07800f037110ff8f0f038f#12.72',
			'2020-07-22 17:47:10#37CCF0#1203af13038214037211079f#12.84'
		];

		foreach ($temperaturas as $temperatura)
		{
			$campos = explode("#", $temperatura);
			$device = $campos[1];
			$fecha = $campos[0];
			$data = $campos[2];

			$time = DateTime::createFromFormat('Y-m-d H:i:s', $fecha)->getTimestamp();

			//$time = DateTime::createFromFormat('d/m/Y H:i', $fecha)->getTimestamp();
			// $this->registrarTemperaturaIOTA($device, $time, $data);

			$url = "https://api.ventumidc.es/api/sigfox/temperatura/iota/$device/$time/$data";

			//echo $url . '</br>';

			$client = new \GuzzleHttp\Client(['verify' => false, 'http_errors' => false]);
			$request = $client->request('POST', $url, ['headers' => [], 'body' => json_encode([])]);

			if ($request->getStatusCode() != 200)
			{
				$errores[] = "Error en $dato";
			}
		}
	}

	private function _registrarLlenadoTST()
	{
		$silos = [
			"38B1C1;27/04/2020 15:36;d11174bafcfbc2"			
		];

		// Una request vacía
		$request = new Request();

		foreach ($silos as $silo)
		{
			$campos = explode(";", $silo);
			$device = $campos[0];
			$fecha = $campos[1];
			$data = $campos[2];

			$time = DateTime::createFromFormat('d/m/Y H:i', $fecha)->getTimestamp();
			$this->registrarLlenadoTST($device, $time, $data, false, $request);
		}
	}

	private function _registrarLlenadoVentum()
	{
		$lecturas = [
			'00#C1D195#29/05/2020 10:35', 
		];

		// Una request vacía
		$request = new Request();

		foreach ($lecturas as $lectura)
		{
			$campos = explode('#', $lectura);
			$data = $campos[0];
			$device = $campos[1];
			$fecha = date_create_from_format ('Y-m-d H:i:s', $campos[2]);

			$time = DateTime::createFromFormat('d/m/Y H:i', $fecha)->getTimestamp();
			$this->registrarLlenadoVentum($device, $time, $data, $request);
		}
	}

	public function pruebas()
	{
		//app('App\Http\Controllers\EntidadesNavesController')->calculoRetroActivo(378, '2020-07-31 12:00:00', '2020-09-01 00:00:00'); // Batería porcentaje
	}
}