<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Exception;
use App\Alerta;
use GuzzleHttp\Client;
use App\AlertaSensor;
use App\AlertaEntidad;
use Illuminate\Http\Request;
use App\AlertaEntidadGeneral;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

use Illuminate\Support\Facades\Crypt;

class AlertasController extends Controller
{
	private $alertasSinValor;

	public function __construct() { $this->alertasSinValor = [20103, 20302, 20401, 20403, 20104, 20801, 31202]; }

	// Indica si es una alerta sin valor
	private function esAlertaSinValor($alerta) { return in_array($alerta, $this->alertasSinValor); }

	// Algunas alertas son especiales y no necesitan parámtros como por ejempo salida de animal o sensor desnivelado. Se utiliza esto para traducir el parámetro
	public function filtrarAlertasSinValor($alertas)
	{
		foreach ($alertas as $alerta)
		{
			if ($this->esAlertaSinValor($alerta->tipo_alerta))
				$alerta->sinValor = true;
			else
				$alerta->sinValor = false;
		}

		return $alertas;
	}

	// Obtiene las alertas de la entidad indicada. Si se indica un usuario, se obtendrán las alertas para ese usuario (solo si eres administrador)
	public function alertasEntidadObtener($idEntidad, $idUsuario = false)
	{
		try
		{
			// En caso de que no se haya indicado un id de usuario, o que quien haya hecho la petición no sea un administrador, obtenemos las alertas del usuario que está logeado
			if (!$idUsuario || (!esAdmin(Auth::user()['role'])))
				$idUsuario = Auth::user()['id'];

			$alertas = DB::select('SELECT A.id tipo_alerta, A.nombre, descripcion, texto, recomendado, AU.id , parametros, activo FROM (SELECT * FROM alertas_tipos WHERE id IN (SELECT id_alerta FROM alertas_tipos_entidad WHERE id_tipo_entidad = ( SELECT id_tipo FROM entidades WHERE id =  ?))) A LEFT JOIN (SELECT * FROM alertas_entidades WHERE id_usuario = ? AND id_entidad = ?) AU ON A.id = AU.id_alerta ORDER BY tipo_alerta', [$idEntidad, $idUsuario, $idEntidad]);

			foreach ($alertas as $alerta)
				$alerta->activo = $alerta->activo == 1 ? true : false;

			return response()->json(['estado' => true, 'u' => $idUsuario, 'mensaje' => $this->filtrarAlertasSinValor($alertas)], 200);	
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(['estado' => false, 'u' => $idUsuario, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nostros'], 200);
		}
	}

	// Actualiza las alertas de la entidad indicada. Si se indica un usuario, se obtendrán las alertas para ese usuario (solo si eres administrador)
	public function alertasEntidadActualizar($idEntidad, $idUsuario = false, Request $request)
	{
		try
		{
			// En caso de que no se haya indicado un id de usuario, o que quien haya hecho la petición no sea un administrador, obtenemos las alertas del usuario que está logeado
			if (!$idUsuario || (!esAdmin(Auth::user()['role'])))
				$idUsuario = Auth::user()['id'];

			$idUsuarioAdd = Auth::user()['id']; // Usuario que añade la alerta. Esto es para saber si ha sido otra persona quien la ha puesto
			$alertasErroneas = [];
			$alertas = json_decode($request['alertas']);

			// Dos bucles. Explicación:
			// Primero recorremos las alertas para detectar los errores y enviarlos como respuesta. No se actualizan las alertas si hay algún error. Antes, se comprobaba todo en un solo bucle, y esto provocaba que las que estuviera mal no se actualizabany las que estuvieran bien si. Entonces, al corregir los errores desde la interfaz no se tenían en cuenta las actualizadas anteriormente (ya que no se devolvían las alertas insertadas en caso de haber error al actualizar, para optimizar los accesos a la base de datos).
			// El primer bucle además de corregir los errores formatea la información.
			foreach ($alertas as $alerta)
			{
				$alerta->sinValor = $this->esAlertaSinValor($alerta->tipo_alerta);
				$alerta->parametros = $alerta->sinValor == true ? VALOR_SIN_VALOR : $alerta->parametros;

				// El nombre se envía por parámetro desde la interfaz, así no tengo que buscarlo en la BD.
				if ($alerta->activo && $alerta->parametros == '')
					$alertasErroneas[] = $alerta->nombre;
			}

			// Si hay errores, respondemos la petición indicnado los errores
			if (!empty($alertasErroneas))
				return response()->json(['estado' => false, 'mensaje' => 'Faltan valores en las siguientes alertas: ' . concatenarErrores($alertasErroneas)], 200);

			// Recorremos en bucle las alertas enviadas
			foreach ($alertas as $alerta)
			{
				// Buscamos para ver si existe. En caso de existir, la actualizamos
				if ($alertaDB = AlertaEntidad::find($alerta->id))
				{
					// En caso de que ya exista (tenga un id en la BD) se actualiza
					$alertaDB->parametros = $alerta->parametros;
					$alertaDB->id_usuario = $idUsuario;
					$alertaDB->id_usuario_add = $idUsuarioAdd;
					$alertaDB->activo = $alerta->activo;
					$alertaDB->save();
				}
				else
				{
					// Una alerta sin valor no tiene sentido dejarla vacía. Pero si que podemos añadir una alerta no activa con un valor predefinido
					if (($alerta->sinValor && $alerta->activo) || $alerta->activo)
					{
						$nuevaAlerta = AlertaEntidad::create([
							'id_entidad' => $idEntidad,
							'id_alerta' => $alerta->tipo_alerta,
							'id_usuario' => $idUsuario,
							'id_usuario_add' => $idUsuarioAdd,
							'parametros' => $alerta->parametros,
							'activo' => $alerta->activo
						]);
					}
				}
			}

			return response()->json(['estado' => true, 'mensaje' => 'Todas las alertas han sido actualizadas correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[ACTUALIZAR ALERTAS ENTIDAD] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nostros'], 200);
		}
	}

	public function alertasEntidadObtenerGeneral(Request $request)
	{
		try
		{
			$alertas = $this->filtrarAlertasSinValor($this->obtenerAlertasDelPropietarioEntidadGeneral($request["id_tipo_entidad"]));
			return response()->json(["estado" => true, "mensaje" => $alertas], 200);	
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "No se han podido obtener. Por favor, contacte con nostros", "e" => $e->getMessage()], 200);
		}
	}

	public function obtenerAlertasDelPropietarioEntidadGeneral($id_tipo_entidad)
	{
		return DB::select("SELECT a.id as tipo_alerta, a.nombre AS nombre, descripcion, texto, recomendado, b.id AS id, parametros, activo FROM (SELECT * FROM alertas_tipos WHERE id IN (SELECT id_alerta FROM alertas_tipos_entidad WHERE id_tipo_entidad = ? AND general = 1 )) a LEFT JOIN (SELECT *	FROM alertas_generales WHERE id_usuario = ?) b ON a.id = b.id_alerta ORDER BY tipo_alerta", [$id_tipo_entidad, Auth::user()["id"]]);
	}

	// Funcion utilizada en el perfil para obtener las alertas generales, que usan las de arriba.
	public function obtenerAlertasGenerales()
	{
		return $this->filtrarAlertasSinValor($this->obtenerAlertasDelPropietarioEntidadGeneral(ENTIDAD_ANIMAL));
	}

	public function alertasEntidadActualizarGeneral(Request $request)
	{
		try
		{
			// Primero obtenemos los datos del usuario logeado
			$usuario = Auth::user();
			$alertas = json_decode($request["alertas"]);

			// En caso de que queramos colocar la alerta a otro usuario (si somos un administrador) lo indicamos en el idUsuario, que será el usuario real objetivo de esa alerta. En caso de no ser administrador o no indicar el usuario objetivo, será el usuario logeado.
			$idUsuario = $request["id_usuario_destino"] && esAdmin($usuario["role"]) ? $request["id_usuario_destino"] : $usuario["id"];

			// Array donde iremos guardando los errores
			$alertasErroneas = [];

			foreach ($alertas as $alerta)
			{
				// Control de errores
				if ($alerta->activo == true && ($alerta->parametros == "" || $alerta->parametros == null ))
					$alertasErroneas[] = DB::select("SELECT nombre FROM alertas_tipos WHERE id  = ?", [$alerta->tipo_alerta])[0]->nombre;
				else
					$this->agregarAlertaGeneral($idUsuario, $alerta, $request["id_tipo_entidad"]);
			}

			if (empty($alertasErroneas))
			{
				// Procedemos a cargar las alertas actualizadas para devolverlas en la response
				$alertas = $this->filtrarAlertasSinValor($this->obtenerAlertasDelPropietarioEntidadGeneral($request["id_tipo_entidad"]));
				return response()->json(["estado" => true, "mensaje" => "Todas las alertas han sido actualizadas", "alertas" => $alertas], 200);
			}
			return response()->json(["estado" => false, "mensaje" => "Faltan valores en las siguientes alertas: " . concatenarErrores($alertasErroneas)], 200);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "No se ha podido modificar. Por favor, contacte con nosotros", "e" => $e->getMessage()], 200);
		}
	}

	// Función identica a la anterior, pero ya se le pasa por parámetro las alertas decodificadas
	/*public function alertasEntidadActualizarGeneralJsonDecoded($alertas, $idUsuarioDestino)
	{
		try
		{
			// Primero obtenemos los datos del usuario logeado
			$usuario = Auth::user();

			// En caso de que queramos colocar la alerta a otro usuario (si somos un administrador) lo indicamos en el idUsuario, que será el usuario real objetivo de esa alerta. En caso de no ser administrador o no indicar el usuario objetivo, será el usuario logeado.
			$idUsuario = $idUsuarioDestino && esAdmin($usuario['role']) ? $idUsuarioDestino : $usuario['id'];

			// Array donde iremos guardando los errores
			$alertasErroneas = [];

			foreach ($alertas as $alerta)
			{
				// Control de errores
				if ($alerta->activo == true && ($alerta->parametros == '' || $alerta->parametros == null ))
					$alertasErroneas[] = DB::select('SELECT nombre FROM alertas_tipos WHERE id  = ?', [$alerta->tipo_alerta])[0]->nombre;
				else
					$this->agregarAlertaGeneral($idUsuario, $alerta, ENTIDAD_ANIMAL);
			}

			if (empty($alertasErroneas))
			{
				// Procedemos a cargar las alertas actualizadas para devolverlas en la response
				$alertas = $this->filtrarAlertasSinValor($this->obtenerAlertasDelPropietarioEntidadGeneral(ENTIDAD_ANIMAL));
				return response()->json(['estado' => true, 'mensaje' => 'Todas las alertas han sido actualizadas', 'alertas' => $alertas], 200);
			}
			return response()->json(['estado' => false, 'mensaje' => 'Faltan valores en las siguientes alertas: ' . concatenarErrores($alertasErroneas)], 200);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "No se ha podido modificar. Por favor, contacte con nosotros", "e" => $e->getMessage()], 200);
		}
	}*/

	public function agregarAlertaGeneral($idUsuario, $alerta, $idTipoEntidad)
	{
		// Por medidas de seguridad para no alterar la Base de datos y para saber si es una alerta sin valor para un posterior trato
		$alerta->parametros = $this->esAlertaSinValor($alerta->tipo_alerta) == true ? VALOR_SIN_VALOR : $alerta->parametros;

		if ($alertaDB = AlertaEntidadGeneral::find($alerta->id))
		{
			$alertaDB->parametros = $alerta->parametros;
			$alertaDB->id_usuario = $idUsuario;
			$alertaDB->activo = $alerta->activo;
			$alertaDB->save();
		}
		else
		{
			if (!($alerta->activo == false && $alerta->parametros == ""))
			{
				// Hacemos esto porque es posible que la alerta sea con valor, pero se le haya puesto -1, como por ejemplo temperatura
				if (!($this->esAlertaSinValor($alerta->tipo_alerta) && $alerta->activo == false))
				{
					$nuevaAlerta = AlertaEntidadGeneral::create([
						'id_alerta' => $alerta->tipo_alerta,
						'id_usuario' => $idUsuario,
						'parametros' => $alerta->parametros,
						'activo' => 1
					]);
				}
			}
		}
	}

	// Actualiza la fecha en la que se ha generado la alerta por si queremos establecer una periodicidad para evitar que salen varias muy seguidas
	public function actualizarFechaAlertaEntidad($idAlertaUsuarioEntidad, $fecha)
	{
		return DB::update('UPDATE alertas_entidades SET ultima = ? WHERE id = ?', [$fecha, $idAlertaUsuarioEntidad]);
	}

		// Obtiene las alertas del sensor y usuario indicados
	public function alertasSensorObtener($idSensor, $idUsuario = false)
	{
		try
		{
			// En caso de que no se haya indicado un id de usuario, o que quien haya hecho la petición no sea un administrador, obtenemos las alertas del usuario que está logeado
			if (!$idUsuario || (!esAdmin(Auth::user()['role'])))
				$idUsuario = Auth::user()['id'];

			$alertas = DB::select('SELECT A.id tipo_alerta, A.nombre, descripcion, texto, recomendado, AU.id , parametros, activo FROM (SELECT * FROM alertas_tipos WHERE id IN (SELECT id_alerta FROM alertas_tipos_sensor WHERE id_tipo_sensor = (SELECT id_tipo FROM sensores WHERE id =  ?))) A LEFT JOIN (SELECT * FROM alertas_sensores WHERE id_usuario = ? AND id_sensor = ?) AU ON A.id = AU.id_alerta ORDER BY tipo_alerta', [$idSensor, $idUsuario, $idSensor]);

			foreach ($alertas as $alerta)
				$alerta->activo = $alerta->activo == 1 ? true : false;

			return response()->json(['estado' => true, 'u' => $idUsuario, 'mensaje' => $this->filtrarAlertasSinValor($alertas)], 200);	
		}
		catch (Exception $e)
		{
			LogController::errores('[OBTENER ALERTAS SENSOR] ' . $e->getMessage());
			return response()->json(['estado' => false, 'u' => $idUsuario, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nostros'], 200);
		}
	}

	// TODO Obtiene las alerta generales de sensores de un usuario
	public function alertasSensorObtenerGeneral(Request $request)
	{
		/*try
		{
			$alertas = $this->filtrarAlertasSinValor($this->obtenerAlertasDelPropietarioEntidadGeneral($request["id_tipo_entidad"]));
			return response()->json(["estado" => true, "mensaje" => $alertas], 200);	
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "No se han podido obtener. Por favor, contacte con nostros", "e" => $e->getMessage()], 200);
		}*/
	}

	// Actualiza las alertas del sensor indicado. Si se indica un usuario, se obtendrán las alertas para ese usuario (solo si eres administrador)
	public function alertasSensorActualizar($idSensor, $idUsuario = false, Request $request)
	{
		try
		{
			// En caso de que no se haya indicado un id de usuario, o que quien haya hecho la petición no sea un administrador, obtenemos las alertas del usuario que está logeado
			if (!$idUsuario || (!esAdmin(Auth::user()['role'])))
				$idUsuario = Auth::user()['id'];

			$idUsuarioAdd = Auth::user()['id']; // Usuario que añade la alerta. Esto es para saber si ha sido otra persona quien la ha puesto
			$alertasErroneas = [];
			$alertas = json_decode($request['alertas']);

			// Dos bucles. Explicación:
			// Primero recorremos las alertas para detectar los errores y enviarlos como respuesta. No se actualizan las alertas si hay algún error. Antes, se comprobaba todo en un solo bucle, y esto provocaba que las que estuviera mal no se actualizabany las que estuvieran bien si. Entonces, al corregir los errores desde la interfaz no se tenían en cuenta las actualizadas anteriormente (ya que no se devolvían las alertas insertadas en caso de haber error al actualizar, para optimizar los accesos a la base de datos).
			// El primer bucle además de corregir los errores formatea la información.
			foreach ($alertas as $alerta)
			{
				$alerta->sinValor = $this->esAlertaSinValor($alerta->tipo_alerta);
				$alerta->parametros = $alerta->sinValor == true ? VALOR_SIN_VALOR : $alerta->parametros;

				// El nombre se envía por parámetro desde la interfaz, así no tengo que buscarlo en la BD.
				if ($alerta->activo && $alerta->parametros == '')
					$alertasErroneas[] = $alerta->nombre;
			}

			// Si hay errores, respondemos la petición indicnado los errores
			if (!empty($alertasErroneas))
				return response()->json(['estado' => false, 'mensaje' => 'Faltan valores en las siguientes alertas: ' . concatenarErrores($alertasErroneas)], 200);

			// Recorremos en bucle las alertas enviadas
			foreach ($alertas as $alerta)
			{
				// Buscamos para ver si existe. En caso de existir, la actualizamos
				if ($alertaDB = AlertaSensor::find($alerta->id))
				{
					// En caso de que ya exista (tenga un id en la BD) se actualiza
					$alertaDB->parametros = $alerta->parametros;
					$alertaDB->id_usuario = $idUsuario;
					$alertaDB->id_usuario_add = $idUsuarioAdd;
					$alertaDB->activo = $alerta->activo;
					$alertaDB->save();
				}
				else
				{
					// Una alerta sin valor no tiene sentido dejarla vacía. Pero si que podemos añadir una alerta no activa con un valor predefinido
					if (($alerta->sinValor && $alerta->activo) || $alerta->activo)
						$nuevaAlerta = AlertaSensor::create([
							'id_sensor' => $idSensor,
							'id_alerta' => $alerta->tipo_alerta,
							'id_usuario' => $idUsuario,
							'id_usuario_add' => $idUsuarioAdd,
							'parametros' => $alerta->parametros,
							'activo' => $alerta->activo
						]);
				}
			}

			return response()->json(['estado' => true, 'mensaje' => 'Todas las alertas han sido actualizadas correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[ACTUALIZAR ALERTAS SENSOR] ' . $e->getMessage());
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nostros'], 200);
		}
	}

	public function alertasSensorActualizarGeneral(Request $request)
	{
		/*try
		{
			// Primero obtenemos los datos del usuario logeado
			$usuario = Auth::user();
			$alertas = json_decode($request["alertas"]);

			// En caso de que queramos colocar la alerta a otro usuario (si somos un administrador) lo indicamos en el idUsuario, que será el usuario real objetivo de esa alerta. En caso de no ser administrador o no indicar el usuario objetivo, será el usuario logeado.
			$idUsuario = $request["id_usuario_destino"] && esAdmin($usuario["role"]) ? $request["id_usuario_destino"] : $usuario["id"];

			// Array donde iremos guardando los errores
			$alertasErroneas = [];

			foreach ($alertas as $alerta)
			{
				// Control de errores
				if ($alerta->activo == true && ($alerta->parametros == "" || $alerta->parametros == null ))
					$alertasErroneas[] = DB::select("SELECT nombre FROM alertas_tipos WHERE id  = ?", [$alerta->tipo_alerta])[0]->nombre;
				else
					$this->agregarAlertaGeneral($idUsuario, $alerta, $request["id_tipo_entidad"]);
			}

			if (empty($alertasErroneas))
			{
				// Procedemos a cargar las alertas actualizadas para devolverlas en la response
				$alertas = $this->filtrarAlertasSinValor($this->obtenerAlertasDelPropietarioEntidadGeneral($request["id_tipo_entidad"]));
				return response()->json(["estado" => true, "mensaje" => "Todas las alertas han sido actualizadas", "alertas" => $alertas], 200);
			}
			return response()->json(["estado" => false, "mensaje" => "Faltan valores en las siguientes alertas: " . concatenarErrores($alertasErroneas)], 200);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "No se ha podido modificar. Por favor, contacte con nosotros", "e" => $e->getMessage()], 200);
		}*/
	}

	// Envía una alerta con notificación, en vez de por email. Notificación es un booleando que si es true hay que enviar notificación al móvil. email es un booleando que si está a false, hay que marcar una fecha (1970-01-01) como fecha de email enviado, para diferenciarnos así de que a esa a lerta no hay que enviar email, ya que si lo dejamos a null el sistema nos lo marcará para enviar.
	public function enviarAlertaNotificacion($idEntidad, $idAlerta, $idUsuario, $parametros, $oneSignal, $mensaje, $id, $fecha, $notificaciones_movil, $email_alerta, $email_instantaneo)
	{
		// Insertar notificación en la tabla de notificaciones
		$notificacion = app('App\Http\Controllers\NotificacionesController')->insertarNotificacionEntidad($idEntidad, $idAlerta, $idUsuario, $parametros, $email_alerta, $email_instantaneo);

		// Actualizar la fecha en la que se ha enviado la alerta. $id es el identificador de la tupla de la tabla de alertas donde se indica la entidad, el usuario, el tipo de alerta y demás. De esta forma la identificamos y podemos saber cual es para marcar la fecha de la última vez que saltó.
		$this->actualizarFechaAlertaEntidad($id, $fecha);

		// Enviar notificación al móvil, siempre que el usuario así lo tenga especificado
		if ($notificaciones_movil)
			app('App\Http\Controllers\NotificacionesController')->enviarPushNotification($idEntidad, $oneSignal, $mensaje, $notificacion);
	}

	public function enviarAlertaNotificacionSensor($idSensor, $idAlerta, $idUsuario, $parametros, $oneSignal, $mensaje, $id, $fecha, $notificaciones_movil, $email_alerta, $email_instantaneo)
	{
		// Insertar notificación en la tabla de notificaciones
		$notificacion = app('App\Http\Controllers\NotificacionesController')->insertarNotificacionSensor($idSensor, $idAlerta, $idUsuario, $parametros, $email_alerta, $email_instantaneo);

		// Actualizar la fecha en la que se ha enviado la alerta. $id es el identificador de la tupla de la tabla de alertas donde se indica la entidad, el usuario, el tipo de alerta y demás. De esta forma la identificamos y podemos saber cual es para marcar la fecha de la última vez que saltó.
		$this->actualizarFechaAlertaEntidad($id, $fecha);

		// Enviar notificación al móvil, siempre que el usuario así lo tenga especificado
		if ($notificaciones_movil)
			app('App\Http\Controllers\NotificacionesController')->enviarPushNotificationSensor($idSensor, $oneSignal, $mensaje, $notificacion);
	}

	public function obtenerAlertasNoLeidasParaEnviarPorMail()
	{
		return DB::select('SELECT N.id, N.fecha, id_entidad, codigo, id_tipo, N.id_alerta, N.id_usuario, N.parametros parametrosNotificacion, A.parametros parametrosAlerta, name nombreUsuario, email, entidades.nombre nombreEntidad FROM (SELECT * FROM notificaciones WHERE leida = 0 AND tipo = 0 AND fecha_email IS NULL) N JOIN alertas_entidades A ON N.id_usuario = A.id_usuario AND N.id_referencia = A.id_entidad AND N.id_alerta = A.id_alerta JOIN users ON N.id_usuario = users.id JOIN configuracion ON N.id_usuario = configuracion.id_usuario JOIN entidades ON N.id_referencia = entidades.id WHERE email_alerta = 1 AND N.fecha < DATE_ADD(now(), INTERVAL - configuracion.email_tiempo MINUTE)');

		// Coger las alertas ordenadas por fecha mayor a menor
		// SELECT N.id, N.fecha, id_entidad, codigo, id_tipo, N.id_alerta, N.id_usuario, N.parametros parametrosNotificacion, A.parametros parametrosAlerta, name nombreUsuario, email, entidades.nombre nombreEntidad FROM (SELECT * FROM notificaciones WHERE leida = 0 AND tipo = 0 AND fecha_email IS NULL) N JOIN alertas_entidades A ON N.id_usuario = A.id_usuario AND N.id_referencia = A.id_entidad AND N.id_alerta = A.id_alerta JOIN users ON N.id_usuario = users.id JOIN configuracion ON N.id_usuario = configuracion.id_usuario JOIN entidades ON N.id_referencia = entidades.id WHERE email_alerta = 1 AND N.fecha < DATE_ADD(now(), INTERVAL - configuracion.email_tiempo MINUTE) ORDER BY fecha DESC

		// Sentencia utilizada para establecer una fecha a las alertas_email para que no salgan en la consulta anterior
		// UPDATE notificaciones SET fecha_email = DATE_ADD(now(), INTERVAL - 60 MINUTE) WHERE fecha_email IS NULL;
	}
}