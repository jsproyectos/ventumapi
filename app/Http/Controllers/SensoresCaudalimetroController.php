<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use Validator;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class SensoresCaudalimetroController extends SensoresController
{
	public function insertarLectura($idSensor, $idEntidad, $payload, $fecha, $bateria, $bp, $rssi, $snr, $mililitros, $litros, $caudal, $alerta = false)
	{
		try
		{
			// Insertar en histórico
			DB::insert('INSERT INTO lecturas_caudalimetro (id_sensor, id_entidad, payload, rssi, snr, bateria, bp, fecha, mililitros, litros, caudal) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$idSensor, $idEntidad, $payload, $rssi, $snr, $bateria, $bp, $fecha, $mililitros, $litros, $caudal]);

			DB::update('UPDATE sensores_caudalimetro SET mililitros = ?, litros = ?, caudal = ?, fecha = ? WHERE id_sensor = ?', [$mililitros, $litros, $caudal, $fecha, $idSensor]);

			/*if ($alerta)
			{
				// Si se indica alerta por parámetro, la creamos en la cola para alertas del sensor. Si no está activada, es por inserciones en masa.
				$job = new \App\Jobs\alertasSensoresTemperatura($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $temp, $hum, $luz, $etermico);
				dispatch($job);
			}*/

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}

	// Obtenemos los sensores y la entidad a la que está asociada
	public function obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		$consulta = 'SELECT sensores.id, dispositivo, fake, seguimiento, id_tipo, sensores.nombre, e.nombre nombreEntidad, bateria, bp, enchufe, snr, mililitros, litros, caudal, totalLitros, mediaCaudal, id_emision, sensores_tipos_emision.nombre comunicacion, sensores.fechaAlta, sensores.fechaBaja, fecha, tiempo, autorizado, notificaciones FROM sensores JOIN sensores_caudalimetro ON sensores.id = sensores_caudalimetro.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_caudalimetro.id_sensor = e.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_usuario = ?';


		$sensores = $this->completarConsultaYEjecutar($consulta, Auth::user()['id'], $nombre, $dispositivo, $comunicacion, $fake, $bajas);

		foreach ($sensores as $sensor)
		{
			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			unset ($sensor->autorizado);
			$sensor->fake = $sensor->fake == 1 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			$sensor->bateria = floatval($sensor->bateria);
			$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
			$sensor->snr = floatval($sensor->snr);
			$sensor->litros = floatval($sensor->litros);
			$sensor->caudal = floatval($sensor->caudal);
			$sensor->totalLitros = floatval($sensor->litros);
			$sensor->mediaCaudal = floatval($sensor->litros);
		}

		return $sensores;
	}

	public function obtenerDetallesSensor($id)
	{
		$sensor = DB::select('SELECT id, sensores.nombre nombre, sensores_caudalimetro.fecha, tiempo, autorizado, sensores.fechaAlta, sensores.fechaBaja, id_tipo, dispositivo, descripcion, latitud, longitud, bateria, bp, enchufe, snr, rssi, id_emision, mililitros, litros, caudal, fechaReinicio, totalLitros, mediaCaudal, e.nombre nombreEntidad FROM sensores JOIN sensores_caudalimetro ON sensores.id = sensores_caudalimetro.id_sensor JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_caudalimetro.id_sensor = e.id_sensor WHERE sensores.id = ? AND id_usuario = ?', [$id, Auth::user()['id']])[0];

		$sensor->propio = $sensor->autorizado == 0 ? true : false;
		unset ($sensor->autorizado);
		$sensor->latitud = floatval($sensor->latitud);
		$sensor->longitud = floatval($sensor->longitud);
		$sensor->bateria = floatval($sensor->bateria);
		$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
		$sensor->snr = floatval($sensor->snr);
		$sensor->rssi = floatval($sensor->rssi);
		$sensor->litros = floatval($sensor->litros);
		$sensor->caudal = floatval($sensor->caudal);
		$sensor->totalLitros = floatval($sensor->litros);
		$sensor->mediaCaudal = floatval($sensor->litros);

		return $sensor;
	}

	// Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico($idSensor, $fechaInicio, $fechaFin)
	{
		try
		{
			if ($this->puedeVer($idSensor))
			{
				$historico = DB::select('SELECT fecha, payload, mililitros, litros, caudal, bateria, bp, snr, rssi FROM lecturas_caudalimetro WHERE id_sensor = ? AND fecha BETWEEN ? AND ? ORDER BY fecha DESC', [$idSensor, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			// Ahora hacemos el tratamiento de al tapa
			foreach ($historico as $lectura)
			{
				$lectura->bateria = floatval($lectura->bateria);
				$lectura->rssi = floatval($lectura->rssi);
				$lectura->snr = floatval($lectura->snr);
				$lectura->litros = floatval($lectura->litros);
				$lectura->caudal = floatval($lectura->caudal);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	public function insertarValorFalso($id, Request $request)
	{
		try
		{
			if (!esAdmin(Auth::user()['role']))
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 403);

			$fecha = date('Y-m-d H:i:s');

			// Obtenemos datos del sensor, SIMILAR  a la función obtenerInfoSensorSilo de ComunicacionController
			$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, ES.id id_entidad, ES.id_tipo id_tipo_entidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.id_sensor = ? AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE sensores.id = ? AND fechaBaja IS NULL', [$id, $id])[0];

			$etermico = $this->calcularEstressTermico($request['temperatura'], $request['humedad']);
			$bp = $this->calcularBateriaPorcentaje($request['bateria']);

			// Insertar en la tabla de historicos
			$this->insertarLectura($id, $sensor->id_entidad, 'FAKE', $request['bateria'], $bp, $request['rssi'], $request['snr'], $fecha, $request['temperatura'], $request['humedad'], $etermico, $request['co2']);
			// app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $request['temperatura'], $request['humedad'], $luminosidad, $etermico);

			// Actualizar la batería y la señal
			app('App\Http\Controllers\ComunicacionController')->actualizarInformacionSensor($id, $request['bateria'], $bp, $request['snr'], $request['rssi']);

			return response()->json(['estado' => true, 'mensaje' => 'Se ha insertado correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA FALSA CAUDALIMETRO] ' . $e->getMessage());
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}
}
