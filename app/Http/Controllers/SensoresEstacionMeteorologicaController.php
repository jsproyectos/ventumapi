<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use Validator;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class SensoresEstacionMeteorologicaController extends SensoresController
{
	// Crea una nueva entrada en la tabla sensores_temperatura_humedad_luz
	/*public function inicializarResumen($idSensor)
	{
		if (DB::insert("INSERT INTO sensores_temperatura_humedad_luz (id_sensor) VALUES (?)", [$idSensor]) == 1)
			return true;
		return false;
	}*/

	public function decodificarTrama($data)
	{
		// Procedemos a separar el payload
		$trama = new \stdClass();

		$trama->configuracion = intval($data[0]);
		$trama->bp =  bindec(substr($data, 1, 7));
		$trama->bateria =  (bindec(substr($data, 8, 8)) + 200) / 100;

		if ($trama->configuracion == ESTACION_CONFIG_NORMAL)
		{
			$trama->temperatura =  (bindec(substr($data, 16, 16)) - 4000) / 100;
			$trama->humedad =  bindec(substr($data, 32, 16)) / 100;
			$trama->presion =  bindec(substr($data, 48, 24)) / 100;
			$trama->viento =  bindec(substr($data, 72, 16)) / 100;
			$trama->direccion =  bindec(substr($data, 88, 8));
		}
		else if ($trama->configuracion == ESTACION_CONFIG_EXCEPCIONAL)
		{
			$trama->excepcion = ESTACION_EXCEPCION_LUVIA;
			$trama->lluvia = bindec(substr($data, 24, 32)) / 10000;
		}

		return $trama;
	}

	public function decodificarTramaVentum($data)
	{
		$trama = new \stdClass();

		$trama->dirViento = $data[0] * 5;
		$trama->velViento = $data[1] * 3.6;
		$trama->maxVelViento = $data[2] * 3.6;
		$trama->lluvia1h = $data[3] + ($data[4] / 100);
		$trama->lluvia24h = $data[5];
		$trama->temperatura = $data[6] - 40 + ($data[7] / 100);
		$trama->humedad = max(min($data[8], 100), 0);
		$trama->presion = $data[9] + 870;
		$trama->bateria = 3.7;
		$trama->bp = 100;

		return $trama;
	}

	public function insertarLectura($idSensor, $idEntidad, $payload, $bateria, $bp, $rssi, $snr, $fecha, $temp, $hum, $pres, $viento_vel, $viento_dir, $lluvia = false)
	{
		try
		{
			if ($lluvia !== false)
			{
				DB::insert('INSERT INTO lecturas_estacion (id_sensor, id_entidad, fecha, payload, snr, rssi, bateria, bp, temperatura, humedad, presion, lluvia, viento_vel, viento_dir) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$idSensor, $idEntidad, $fecha, $payload, $snr, $rssi, $bateria, $bp, $temp, $hum, $pres, $lluvia, $viento_vel, $viento_dir]);

				DB::update("UPDATE sensores_estacion SET fecha = ?, temperatura = ?, humedad = ?, presion = ?, lluvia = ?, viento_vel = ?, viento_dir = ? WHERE id_sensor = ?", [$fecha, $temp, $hum, $pres, $lluvia, $viento_vel, $viento_dir, $idSensor]);	
			}
			else
			{
				// Insertar en histórico
				DB::insert('INSERT INTO lecturas_estacion (id_sensor, id_entidad, fecha, payload, snr, rssi, bateria, bp, temperatura, humedad, presion, viento_vel, viento_dir) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$idSensor, $idEntidad, $fecha, $payload, $snr, $rssi, $bateria, $bp, $temp, $hum, $pres, $viento_vel, $viento_dir]);

				DB::update("UPDATE sensores_estacion SET fecha = ?, temperatura = ?, humedad = ?, presion = ?, viento_vel = ?, viento_dir = ? WHERE id_sensor = ?", [$fecha, $temp, $hum, $pres, $viento_vel, $viento_dir, $idSensor]);			
			}


			// $job = new \App\Jobs\alertasSensoresTemperatura($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $temp, $hum, $luz, $etermico);
			// dispatch($job);

			return true;
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA ESTACION] ' . $e->getMessage());
			return false;
		}
	}

	public function insertarLluvia($idSensor, $idEntidad, $payload, $bateria, $bp, $rssi, $snr, $fecha, $lluvia)
	{
		try
		{
			DB::insert('INSERT INTO lecturas_estacion (id_sensor, id_entidad, fecha, payload, snr, rssi, bateria, bp, lluvia) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$idSensor, $idEntidad, $fecha, 'L' . $payload, $snr, $rssi, $bateria, $bp, $lluvia]);
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA ESTACION LLUVIA] ' . $e->getMessage());
			return false;	
		}
	}

	public function actualizarLecturaConLluvia($idSensor, $fecha, $lluvia)
	{
		try
		{
			// Actualizamos retroactivamente
			DB::update('UPDATE lecturas_estacion SET lluvia = ? WHERE id_sensor = ? AND fecha > ?', [$lluvia, $idSensor, $fecha]);

			// Actualizamos el resumen
			DB::update('UPDATE sensores_estacion SET lluvia = ? WHERE id_sensor = ?', [$lluvia, $idSensor]);
		}
		catch (Exception $e)
		{
			LogController::errores('[ACTUALIZAR LECTURA ESTACION LLUVIA] ' . $e->getMessage());
			return false;	
		}
	}

	// Obtenemos los sensores y la entidad a la que está asociada
	public function obtener($nombre, $dispositivo, $comunicacion, $fake, $bajas)
	{
		$consulta = 'SELECT sensores.id, id_tipo, fake, seguimiento, dispositivo, sensores.nombre, e.nombre nombreEntidad, bateria, bp, enchufe, snr, id_emision, sensores_tipos_emision.nombre comunicacion, tiempo, sensores.fechaAlta, sensores.fechaBaja, fecha, temperatura, humedad, presion, lluvia, viento_dir, viento_vel, autorizado, notificaciones FROM sensores JOIN sensores_estacion ON sensores.id = sensores_estacion.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja IS NULL) e ON sensores_estacion.id_sensor = e.id_sensor JOIN sensores_tipos_emision ON sensores.id_emision = sensores_tipos_emision.id JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor WHERE id_tipo = 7 AND id_usuario = ?';

		$sensores = $this->completarConsultaYEjecutar($consulta, Auth::user()['id'], $nombre, $dispositivo, $comunicacion, $fake, $bajas);

		foreach ($sensores as $sensor)
		{
			$sensor->propio = $sensor->autorizado == 0 ? true : false;
			unset ($sensor->autorizado);
			$sensor->fake = $sensor->fake == 1 ? true : false;
			$sensor->seguimiento = $sensor->seguimiento == 1 ? true : false;
			$sensor->temperatura = floatval($sensor->temperatura);
			$sensor->humedad = floatval($sensor->humedad);
			$sensor->presion = floatval($sensor->presion);
			$sensor->lluvia = floatval($sensor->lluvia);
			$sensor->viento_vel = floatval($sensor->viento_vel);
			$sensor->viento_dir = floatval($sensor->viento_dir);
			$sensor->snr = floatval($sensor->snr);
			$sensor->enchufe = $sensor->enchufe == 1 ? true : false;
		}

		return $sensores;
	}

	public function obtenerDetallesSensor($id)
	{
		$sensor = DB::select('SELECT id, sensores.nombre nombre, sensores_estacion.fecha, tiempo, sensores.fechaAlta, sensores.fechaBaja, id_tipo, dispositivo, descripcion, latitud, longitud, bateria, bp, enchufe, snr, rssi, id_emision, temperatura, humedad, presion, lluvia, viento_vel, viento_dir, autorizado, e.nombre nombreEntidad FROM sensores JOIN sensores_estacion ON sensores.id = sensores_estacion.id_sensor JOIN sensores_usuarios ON sensores.id = sensores_usuarios.id_sensor LEFT JOIN (SELECT id_entidad, id_sensor, nombre FROM entidades JOIN entidades_sensores ON entidades.id = entidades_sensores.id_entidad WHERE entidades_sensores.fechaBaja is NULL) e ON sensores_estacion.id_sensor = e.id_sensor WHERE sensores.id = ? AND id_usuario = ?', [$id, Auth::user()['id']])[0];

		$sensor->propio = $sensor->autorizado == 0 ? true : false;
		unset ($sensor->autorizado);
		$sensor->latitud = floatval($sensor->latitud);
		$sensor->longitud = floatval($sensor->longitud);
		$sensor->bateria = floatval($sensor->bateria);
		$sensor->snr = floatval($sensor->snr);
		$sensor->rssi = floatval($sensor->rssi);
		$sensor->temperatura = floatval($sensor->temperatura);
		$sensor->humedad = floatval($sensor->humedad);
		$sensor->presion = floatval($sensor->presion);
		$sensor->lluvia = floatval($sensor->lluvia);
		$sensor->viento_vel = floatval($sensor->viento_vel);
		$sensor->viento_dir = floatval($sensor->viento_dir);
		$sensor->enchufe = $sensor->enchufe == 1 ? true : false;

		return $sensor;
	}

	// Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico($idSensor, $fechaInicio, $fechaFin)
	{
		try
		{
			if ($this->puedeVer($idSensor))
			{
				$historico = DB::select('SELECT fecha, payload, temperatura, humedad, presion, lluvia, viento_vel, viento_dir, bateria, bp, snr, rssi FROM lecturas_estacion WHERE id_sensor = ? AND fecha BETWEEN ? AND ? AND payload NOT LIKE "L%" ORDER BY fecha DESC', [$idSensor, $fechaInicio, incrementarUnDia($fechaFin)]);
			}
			else
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para ver esta información'], 200);

			// Ahora hacemos el tratamiento de al tapa
			foreach ($historico as $lectura)
			{
				$lectura->temperatura = floatval($lectura->temperatura);
				$lectura->humedad = floatval($lectura->humedad);
				$lectura->presion = floatval($lectura->presion);
				$lectura->lluvia = floatval($lectura->lluvia);
				$lectura->viento_vel = floatval($lectura->viento_vel);
				$lectura->viento_dir = floatval($lectura->viento_dir);
				$lectura->bateria = floatval($lectura->bateria);
				$lectura->rssi = floatval($lectura->rssi);
				$lectura->snr = floatval($lectura->snr);
			}

			return response()->json(['estado' => true, 'datos' => $historico], 200);
		}
		catch (Exception $e)
		{
			return response()->json(['estado' => false, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}

	/*public function insertarValorFalso($id, Request $request)
	{
		try
		{
			if (!esAdmin(Auth::user()['role']))
				return response()->json(['estado' => false, 'mensaje' => 'No tienes permiso para realizar esta acción'], 403);

			$fecha = date('Y-m-d H:i:s');

			// Obtenemos datos del sensor, SIMILAR  a la función obtenerINfoSensorSilo de ComunicacionController
			$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, ES.id id_entidad, ES.id_tipo id_tipo_entidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.id_sensor = ? AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE sensores.id = ? AND fechaBaja IS NULL', [$id, $id])[0];

			$luminosidad = isset($request['luminosidad']) ? $request['luminosidad'] : 0;
			$etermico = $this->calcularEstressTermico($request['temperatura'], $request['humedad']);
			$bp = $this->calcularBateriaPorcentaje($request['bateria'] * 2);


			// Insertar en la tabla de historicos
			$this->insertarLectura($id, $sensor->id_entidad, 'FAKE', $request['bateria'], $bp, $request['rssi'], $request['snr'], $fecha, $request['temperatura'], $request['humedad'], $luminosidad, $etermico);
			app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $request['temperatura'], $request['humedad'], $luminosidad, $etermico);

			// Actualizar la batería y la señal
			app('App\Http\Controllers\ComunicacionController')->actualizarInformacionSensor($id, $request['bateria'], $bp, $request['snr'], $request['rssi']);

			return response()->json(['estado' => true, 'mensaje' => 'Se ha insertado correctamente'], 200);
		}
		catch (Exception $e)
		{
			LogController::errores('[INSERTAR LECTURA FALSA TEMPERATURA] ' . $e->getMessage());
			return response()->json(['estado' => true, 'mensaje' => 'Ha ocurrido un error. Por favor, contacte con nosotros', 'e' => $e->getMessage()], 200);
		}
	}*/
}
