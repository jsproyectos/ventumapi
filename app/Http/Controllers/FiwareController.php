<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\LogController;

DEFINE("INTENTOS_ENVIO", 3);

class FiwareController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	// Función que obtiene un token nuevo de la base de datos fiware y 
	private function obtenerToken($user, $pass, $host, $service, $subservice)
	{
		$url = "https://$host:15001/v3/auth/tokens";
		$client = new \GuzzleHttp\Client(['verify' => false, 'http_errors' => false]);

		$request = $client->request('POST', $url, [
			'headers' => [
			'Content-Type' => 'application/json',
		], 'body' => view('fiware.auth', ["user" => $user, 'pass' => $pass, 'host' => $host, 'service' => $service, 'subservice' => $subservice])]);

		if($request->getStatusCode() == 201)
			return $request->getHeaders()['X-Subject-Token'][0];
		else
			return false;
	}
					 
	public function enviarInformacion($info, $plantilla)
	{
		try
		{
			$url = "https://$info->host:$info->puerto/v2/entities/$info->id_device_fiware/attrs";
			$client = new \GuzzleHttp\Client(['verify' => false, 'http_errors' => false]);

			$codRespuesta = null;
			$intentos = 0;

			while($codRespuesta != 204 && $intentos < INTENTOS_ENVIO)
			{
				$request = $client->request('POST', $url, [
					'headers' => [
					'Fiware-Service' => $info->service,
					'Fiware-ServicePath' => $info->subservice,
					'X-Auth-Token' => $info->token,
					'Content-Type' => $info->ContentType,
				], 'body' => $plantilla]);

				$intentos++;
				$codigoRespuesta = $request->getStatusCode();
				$mensaje = json_decode($request->getBody()->getContents());

				// 401: Token inválido, hay que relogear
				if($codigoRespuesta == 401 || ($codigoRespuesta == 400 && isset($menasje->name) && $mensaje->name == 'MISSING_HEADERS'))
				{
					$user = Crypt::decrypt($info->user);
					$pass = Crypt::decrypt($info->pass);
					$info->token = $this->obtenerToken($user, $pass, $info->host, $info->service, $info->subservice);		
					$this->atualizarToken($info->id_fiware, $info->token);
					LogController::erroresFiware($this->obtenerMensajeError($info->id_entidad, $codigoRespuesta, $mensaje));
				}
				else
				{
					// En caso de que el código de respuesta sea diferente a login o respuesta correcta, escribimos en el log de errores fiware para visualizarlo
					if ($codigoRespuesta < 200 || $codigoRespuesta >= 300)
						LogController::erroresFiware($this->obtenerMensajeError($info->id_entidad, $codigoRespuesta, $mensaje));
				}
			}

			return $codigoRespuesta;
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}

	private function obtenerMensajeError($id_entidad, $codigo, $mensaje)
	{
		if (isset($mensaje->name))
		{
			return "[$id_entidad] [$codigo]:[$mensaje->name] - $mensaje->message";
		}
		else
		{
			if (isset($mensaje->error))
			{
				return "[$id_entidad] [$codigo]:[$mensaje->error] - $mensaje->description";
			}
			else
			{
				$respuesta = "[$id_entidad] [$codigo]:";
				foreach ($mensaje as $key => $value)
				{
					$respuesta .= "[$key] - $value";
				}
				return $respuesta;
			}
		}
	}

	private function atualizarToken($id, $token)
	{
		DB::update("UPDATE fiware SET token = ? WHERE id = ?", [$token, $id]);
	}
}
