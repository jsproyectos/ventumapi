<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntidadMacho extends Model
{
	public $timestamps = false;
	protected $table = 'entidades_machos';
	protected $primaryKey = 'id_entidad';

	protected $fillable = ["id_entidad", "fecha", "montas", "id_animal", "activo"];
}
