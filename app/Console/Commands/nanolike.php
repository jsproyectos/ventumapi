<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class nanolike extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nanolike';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restaura tramas perdidas de NanoLike';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		try
		{
			// Leemos el fichero de tramas
			$tramas = File::get('nanolike.txt');
			$tramas = explode("\r\n", $tramas);

			foreach ($tramas as $trama)
			{
				$trama = explode(',', $trama);

				$codigo = explode( '"', $trama[1])[3];
				$fecha = date('Y-m-d H:m:s', intval(explode( ':', $trama[2])[1]) / 1000);
				$porcentaje = intval(explode( ':', $trama[3])[1]);
				$kilos = intval(explode( ':', $trama[4])[1]);

				$this->info("[$fecha] Dispositivo: $codigo. Porcentaje: $porcentaje. Kilos: $kilos");

				$idEntidad = DB::select('SELECT id FROM entidades WHERE codigo = ?', [$codigo])[0]->id;

				// ($idSensor, $idEntidad, $data, $fecha, $bateria, $bp, $rssi, $snr, $altura, $porcentaje, $kilos, $temperatura, $aX, $aY, $aZ)
				app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura(null, $idEntidad, null, $fecha, 3.7, 100, 0, 0, 1, 0, $porcentaje, $kilos, 0, 0, 0, 0);
				app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($idEntidad, ENTIDAD_SILO, $fecha, $porcentaje, $kilos, 0, 0, 0, 0);
			}

			$this->info('Terminado');

			return response()->json(['status' => true, 'message' => 'OK'], 200);
		}
		catch (Exception $e)
		{
			$this->info('Error: ' . $e->message());
		}
    }
}
