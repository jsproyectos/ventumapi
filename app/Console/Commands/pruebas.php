<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class pruebas extends Command
{
	private function rellenarceros($numero)
	{
		if ($numero < 10)
		{
			return '00' . $numero;
		}
		else
		{
			if ($numero < 100)
			{
				return '0' . $numero;
			}
			return $numero;
		}
	}

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'pruebas';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Pruebas en la consola';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		try
		{
// Lo primero de todo es obtener los datos que nos interesan de la Request, que es el id del dispositivo
			$device = '70B3D57ED004B327';
			$data = [0,163,19,162];
			$snr = 0;
			$rssi = 0;

			// Se obtiene la información importante del sensor
			$sensor = app('App\Http\Controllers\ComunicacionController')->obtenerInfoSensorSilo($device);

			// $fecha = date('Y/m/d H:i:s', $time);
			$fecha = date('Y-m-d H:i:s');

			// Se decodifica la trama
			$trama = app('App\Http\Controllers\SensoresNivelLlenadoController')->decodificarTramaLoraVentum($data);

			// Convertimos la data (que son bytes) a una cadena, para poder insertarla en la base de datos
			$data = $data[0] . ' ' . $data[1] . ' ' . $data[2] . ' ' . $data[3];

			// Se calcula el porcentaje de llenado del silo
			$porcentaje = app('App\Http\Controllers\EntidadesRecipientesController')->CalcularPorcentajeLlenado($sensor->id_entidad, $trama->distancia);

			// Se calcula el peso en kilos aproximado en función del porcentaje y la capacidad máxima del silo
			$kilos = $porcentaje * $sensor->capacidad / 100;

			// Ahora se procede a su inserción y a insertar las alertas sobre las entidades
			app('App\Http\Controllers\SensoresNivelLlenadoController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $fecha, $trama->bateria, $trama->bp, $rssi, $snr, 1, $trama->distancia, $porcentaje, $kilos, $trama->temperatura, $trama->acelerometroX, $trama->acelerometroY, $trama->acelerometroZ);
			app('App\Http\Controllers\EntidadesRecipientesController')->actualizar($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $porcentaje, $kilos, $trama->temperatura, $trama->acelerometroX, $trama->acelerometroY, $trama->acelerometroZ);

			// Actualizar la batería y la señal
			//$this->actualizarInformacionSensor($sensor->id_sensor, $trama->bateria, $trama->bp, $snr, $rssi);

			//return $this->todoBien($sensor->id_sensor, $device, false);
			
		} catch (Exception $e)
		{
			$this->info('Error: ' . $e->getMessage());
		}
	}

		public function obtenerInfoSensorSilo($dispositivo)
	{
		$sensor = DB::select('SELECT sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, altura, ultimaLectura, offset, tiempo, debug, callback, fechaCallback, ES.id id_entidad, ES.id_tipo id_tipo_entidad, capacidad FROM sensores LEFT JOIN sensores_silos ON sensores.id = sensores_silos.id_sensor LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre, capacidad FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad WHERE entidades_sensores.id_sensor = (SELECT id FROM sensores WHERE dispositivo = ? AND fechaBaja IS NULL) AND entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE dispositivo = ? AND fechaBaja IS NULL', [$dispositivo, $dispositivo]);

		if (!empty($sensor))
		{
			$sensor[0]->callback = $sensor[0]->callback == 1 ? true : false;
			return $sensor[0];
		}
		return false;
	}
}
