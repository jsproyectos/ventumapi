<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class caducidadSigfox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
   protected $signature = 'sigfox:caducidad {fichero}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Establece la fecha de caducidad de los sensores sigfox';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	private function normalizarIdDevice($device)
	{
		// Buscamos el primer caracter no 0
		$longitud = strlen($device);

		for ($i = 0; $i < $longitud; $i++)
		{
			if ($device[$i] != '0')
				break;
		}

		return substr($device, $i);
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	// Permitir solo los que están activos
		/*$fichero = $this->argument('fichero');

		foreach(file($fichero) as $linea)
		{
			// Quitamos las comillas
			$linea = str_replace("\n", "", $linea);
			$linea = str_replace("\"", "", $linea);

			// Separamos por ; para tratar los datos
			$datos = explode(";", $linea);

			if (strlen($datos[2]) > 2)
			{
				$estado = $datos[0];
				$id = $this->normalizarIdDevice($datos[1]);
				$fecha = $datos[2];

				// Cogemos solo los que están en uso
				if ($estado == "not_seen")
				{
					$this->info("Estableciendo la fecha $fecha fin de contrato al dispositivo $id");
					DB::update('UPDATE sensores SET finContrato = ? WHERE dispositivo = ?', [$fecha, $id]);
				}
			}
		}*/

		// Ponerles fecha a todos, siempre que la tengan
		$fichero = $this->argument('fichero');

		foreach(file($fichero) as $linea)
		{
			// Quitamos las comillas
			$linea = str_replace("\n", "", $linea);
			$linea = str_replace("\"", "", $linea);

			// Separamos por ; para tratar los datos
			$datos = explode(";", $linea);

			if (strlen($datos[2]) > 2)
			{
				$estado = $datos[0];
				$id = $this->normalizarIdDevice($datos[1]);
				$fecha = $datos[2];

				$this->info("Estableciendo la fecha $fecha fin de contrato al dispositivo $id");
				DB::update('UPDATE sensores SET finContrato = ? WHERE dispositivo = ?', [$fecha, $id]);
			}
		}
    }
}
