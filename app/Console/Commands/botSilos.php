<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class botSilos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'bot:silos';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Bot de sensores de llenado de silos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	// SELECT dispositivo, altura, bateria, ultimaLectura, temperatura FROM sensores JOIN sensores_silos ON sensores.id = sensores_silos.id_sensor WHERE fake = 1 AND id_tipo = 1
		try
		{
			//$timestampInicial = 1596276000; // 1 de agosto a las 10 de la mañana
			//$timestampFinal = time();

			//for ($time = $timestampInicial; $time < $timestampFinal; $time += 14400)
			//{
			$sensores = DB::select('SELECT id, dispositivo, altura, bateria, ultimaLectura, temperatura FROM sensores JOIN sensores_silos ON sensores.id = sensores_silos.id_sensor WHERE fake = 1 AND id_tipo = ' . SENSOR_SILO);

			$time = time();

			$this->info('Fecha: ' . date($time));
			$this->info('Total: ' . count($sensores));
			foreach ($sensores as $sensor)
			{
				$rssi = -1 * rand(80, 120);
				$snr = round(rand(6, 40), 2);

				//Hay 2 probabilidades entre 5 de que no haya consumo:
				$probabilidad = rand(0, 4);

				// No hay consumo
				if ($probabilidad < 3)
				{
					$distancia = $sensor->ultimaLectura;
				}
				else // Hay consumo
				{
					$distancia = $sensor->ultimaLectura + rand (3, 8);
					$distancia = $distancia > ($sensor->altura - 40) ? rand(15, 40) : $distancia;
				}

				// Temperatura
				switch ((int)date('H'))
				{
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
						$temperatura = rand(0, 8);
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						$temperatura = rand(5, 12);
						break;
					case 9:
					case 10:
					case 11:
					case 12:
						$temperatura = rand(10, 20);
						break;
					case 13:
					case 14:
					case 15:
					case 16:
						$temperatura = rand(15, 30);
						break;
					case 17:
					case 18:
					case 19:
						$temperatura = rand(25, 15);
						break;
					case 20:
					case 22:
					case 23:
						$temperatura = rand(10, 5);
						# code...
						break;
					
					default:
						$temperatura = rand(0, 30);
						break;
				}

				//Data: Vamos a generar una trama simulando la nueva distancia y la temperatura, que será enviada a nuestro propio servidor como una petición para insertarlo de forma normal. Añadimos 0s a la izquierda para que los números coincidan correctamente con el número de bits necesarios
				$bateriaBinario = str_pad(decbin($sensor->bateria), 4, '0', STR_PAD_LEFT);
				$temperaturaBinario = str_pad(decbin($temperatura += 30), 7, '0', STR_PAD_LEFT); // Hay que sumarle 30 y luego convertirlo a binario
				$distanciaBinario = str_pad(decbin($distancia), 9, '0', STR_PAD_LEFT);

				$tramaBinariaImportante = $bateriaBinario . '0001' . $temperaturaBinario . $distanciaBinario;
				$trama = dechex(bindec($tramaBinariaImportante));
				$trama = str_pad($trama, 6, "0", STR_PAD_LEFT);
				$trama = 'd1' . $trama . '0507c3';

				$this->info("Sensor: $sensor->id. Dispositivo: $sensor->dispositivo. $sensor->id. Distancia: $distancia. Temperatura: $temperatura");
				app('App\Http\Controllers\ComunicacionSigfoxController')->registrarLlenadoTST($sensor->dispositivo, $time, $trama, false);
			}
			// }

			$this->info('Bot de silos terminado');
		}
		catch (Exception $e)
		{
			LogController::errores("Se han encontrado excepciones en el bot de sensores de silos");
			LogController::errores($e->getMessage());
		}
	}
}
