<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class sensorcambio extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sensores:cambiar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migra la tabla de un sensor a otro. Este comando se debe sobreescribir cuando se quiera usar';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		// Obtenemos la información que queremos migrar
    	$lecturas = DB::select('SELECT * FROM lecturas_temperatura_humedad WHERE id_sensor = 397 AND fecha BETWEEN "2021-03-02 10:00:00" AND "2021-03-03 10:00:00" ORDER BY fecha ASC');

    	// Obtenidas, insertamos
    	foreach ($lecturas as $lectura)
    	{
    		$idSensor = $lectura->id_sensor;
    		$payload = $lectura->payload;
    		$fecha = $lectura->fecha;
    		$bateria = 0;
    		$bp = 100;
    		$rssi = 0;
    		$snr = 0;

			// Obtenemos la distancia
			$informacion = base_convert($payload, 16, 2);
			$distancia = str_pad($informacion, 32, '0', STR_PAD_LEFT);
			$distancia = bindecComplemento($distancia, 32);

    		DB::insert('INSERT INTO lecturas_silos(id_sensor, payload, fecha, bateria, bp, rssi, snr, altura) values (?, ?, ?, ?, ?, ?, ?, ?)', [$idSensor, $payload, $fecha, $bateria, $bp, $rssi, $snr, $distancia]);
    	}
    }
}
