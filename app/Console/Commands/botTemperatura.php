<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class botTemperatura extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'bot:temperatura';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Bot de sensores de temperatura';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	try
    	{
			// Borrar Historico Falso
			// DELETE FROM entidades_naves_historico WHERE id_entidad IN (SELECT id FROM entidades WHERE fake = 1 AND id_tipo = 12)
			// -- Borrar Historico Sensores Falso
			// DELETE FROM lecturas_temperatura_humedad WHERE id_sensor IN (SELECT id FROM sensores WHERE fake = 1 AND id_tipo IN (2, 10))
			// $timestampInicial = 1598918400; // 1 de agosto a las 10 de la mañana
			// $timestampFinal = time();

			// for ($time = $timestampInicial; $time < $timestampFinal; $time += 1800)
			// {
			$time = time();
			$fecha = date("Y-m-d H:i:s", $time);
			$this->info($fecha . '------------------------------------------------------------');
			// Obtenemos datos del sensor, SIMILAR  a la función obtenerINfoSensorSilo de ComunicacionController
			$sensores = DB::select('SELECT dispositivo, sensores.id id_sensor, sensores.id_tipo id_tipo_sensor, ES.id id_entidad, ES.id_tipo id_tipo_entidad FROM sensores LEFT JOIN ( SELECT entidades.id, entidades.id_tipo, id_sensor, nombre FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades.fechaBaja IS NULL AND entidades_sensores.fechaBaja IS NULL) ES ON sensores.id = ES.id_sensor WHERE sensores.fake = 1 AND fechaBaja IS NULL AND sensores.id_tipo IN (2, 10)');			

			foreach ($sensores as $sensor)
			{
				if ($sensor->id_entidad)
				{
					$temperatura = 0;
					$humedad = 0;
					$luminosidad = 0;
					$etermico = 0;
					$tipo = "";

					switch ($sensor->id_tipo_entidad)
					{
						case ENTIDAD_LECHE: $temperatura = rand(200, 500); $tipo = 'Tanque leche'; break;

						case ENTIDAD_NEVERAS:
							$temperatura = rand(200, 800);
							$humedad = rand(4000, 7500);
							$tipo = 'Nevera';
							break;

						case ENTIDAD_CELDA_NAVE:
							$tipo = 'Celda Nave';
							switch ((int)date('H', $time))
							{
								case 0: $temperatura = rand(1900, 2100); $humedad = rand(3000, 5200); $luminosidad = rand(0, 5000); break;
								case 1: $temperatura = rand(1900, 2100); $humedad = rand(3000, 5400); $luminosidad = rand(0, 5000); break;
								case 2: $temperatura = rand(1900, 2100); $humedad = rand(2800, 5600); $luminosidad = rand(0, 5000); break;
								case 3: $temperatura = rand(1700, 2000); $humedad = rand(3200, 6200); $luminosidad = rand(0, 5000); break;
								case 4: $temperatura = rand(1700, 2000); $humedad = rand(3200, 6600); $luminosidad = rand(0, 5000); break;
								case 5: $temperatura = rand(1700, 2000); $humedad = rand(3000, 7000); $luminosidad = rand(0, 5000); break;
								case 6: $temperatura = rand(1900, 2200); $humedad = rand(3000, 7200); $luminosidad = rand(0, 10000); break;
								case 7: $temperatura = rand(1900, 2200); $humedad = rand(3600, 7400); $luminosidad = rand(5000, 20000); break;
								case 8: $temperatura = rand(2000, 2200); $humedad = rand(3600, 7400); $luminosidad = rand(10000, 25000); break;
								case 9: $temperatura = rand(2100, 2300); $humedad = rand(3400, 7400); $luminosidad = rand(20000, 30000); break;
								case 10: $temperatura = rand(2000, 2600); $humedad = rand(3200, 6800); $luminosidad = rand(30000, 40000); break;
								case 11: $temperatura = rand(2100, 2900); $humedad = rand(3200, 6000); $luminosidad = rand(40000, 50000); break;
								case 12: $temperatura = rand(2600, 3100); $humedad = rand(2600, 4600); $luminosidad = rand(45000, 60000); break;
								case 13: $temperatura = rand(2800, 3300); $humedad = rand(2400, 4400); $luminosidad = rand(50000, 60000); break;
								case 14: $temperatura = rand(2900, 3300); $humedad = rand(2000, 4000); $luminosidad = rand(50000, 60000); break;
								case 15: $temperatura = rand(3000, 3400); $humedad = rand(1600, 3200); $luminosidad = rand(50000, 60000); break;
								case 16: $temperatura = rand(3000, 3500); $humedad = rand(1600, 3400); $luminosidad = rand(50000, 60000); break;
								case 17: $temperatura = rand(3000, 3500); $humedad = rand(1600, 3200); $luminosidad = rand(50000, 60000); break;
								case 18: $temperatura = rand(3000, 3400); $humedad = rand(1600, 3000); $luminosidad = rand(50000, 60000); break;
								case 19: $temperatura = rand(2900, 3400); $humedad = rand(1800, 3400); $luminosidad = rand(50000, 55000); break;
								case 20: $temperatura = rand(2700, 3200); $humedad = rand(2000, 3600); $luminosidad = rand(40000, 50000); break;
								case 21: $temperatura = rand(2600, 2900); $humedad = rand(2400, 3600); $luminosidad = rand(30000, 35000); break;
								case 22: $temperatura = rand(2400, 2700); $humedad = rand(2600, 4400); $luminosidad = rand(10000, 20000); break;
								case 23: $temperatura = rand(2200, 2700); $humedad = rand(2600, 5000); $luminosidad = rand(0, 5000); break;
							}
							break;
					}

					$temperatura /= 100;
					$humedad /= 100;
					$luminosidad /= 100;
					// La fórmula debería de cogerse del controlador de temepratura y humedad
					$etermico = (0.8 * $temperatura + (($humedad / 100) * $temperatura - 14.3) + 46.4);

					$rssi = -1 * rand(80, 120);
					$snr = round(rand(6, 40), 2);
					$bateria = 3.7;
					$bp = 100;
					$data = 'FAKE';

					$this->info ("[$tipo] Insertando datos de sensor $sensor->dispositivo. Temperatura: $temperatura. Humedad: $humedad. Luz: $luminosidad. Etermico: $etermico");

					// Insertar en la tabla de historicos
					app('App\Http\Controllers\SensoresTempHumController')->insertarLectura($sensor->id_sensor, $sensor->id_entidad, $data, $bateria, $bp, $rssi, $snr, $fecha, $temperatura, $humedad, $luminosidad, $etermico);
					app('App\Http\Controllers\EntidadesController')->actualizarTemperatura($sensor->id_entidad, $sensor->id_tipo_entidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico);

					app('App\Http\Controllers\ComunicacionController')->actualizarInformacionSensor($sensor->id_sensor, $bateria, $bp, $snr, $rssi);
				}
			}
			// }
    	}
    	catch (Exception $e)
    	{
    		LogController::errores('[BOT TEMPERATURA] ' . $e->GetMessage());
    	}
    }
}
