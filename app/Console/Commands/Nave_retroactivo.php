<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class Nave_retroactivo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nave:retroactivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenera la información de una nave de forma retroactiva según sus celdas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		// Eliminamos primero todas losvalores historicos entre las dos fechas
		DB::delete("DELETE FROM entidades_naves_historico WHERE fecha BETWEEN ? AND ?", [$fechaInicio, $fechaFin]);

		// Primero obtenemos el estado de las celdas antes de la fecha de Inicio
		$entidadesYFechas = DB::select("SELECT max(fecha) fecha, id_entidad FROM lecturas_temperatura_humedad WHERE fecha < ? AND id_entidad IS NOT NULL AND id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE fechaBaja IS NULL) GROUP BY id_entidad", [$fechaInicio]);

		$medias = [];

		foreach ($entidadesYFechas as $entidadYFecha)
		{
			$ultimaLecturaAntes = DB::select("SELECT id_entidad, temperatura, humedad, luminosidad, etermico, id_nave FROM lecturas_temperatura_humedad LEFT JOIN entidades_naves_celdas ON lecturas_temperatura_humedad.id_entidad = entidades_naves_celdas.id_celda WHERE id_entidad = ? AND fecha = ?", [$entidadYFecha->id_entidad, $entidadYFecha->fecha]);

			$ultimaLectura = new \stdClass();
			$ultimaLectura->temperatura = $ultimaLecturaAntes[0]->temperatura;
			$ultimaLectura->humedad = $ultimaLecturaAntes[0]->humedad;
			$ultimaLectura->luminosidad = $ultimaLecturaAntes[0]->luminosidad;
			$ultimaLectura->etermico = $ultimaLecturaAntes[0]->etermico;
			$ultimaLectura->id_nave = $ultimaLecturaAntes[0]->id_nave;

			$medias[$ultimaLecturaAntes[0]->id_entidad] = $ultimaLectura;
		}

		// Ahora cogemos el histórico entre las dos fechas
		$lecturas = DB::select("SELECT id_entidad, fecha, temperatura, humedad, luminosidad, etermico, id_nave FROM lecturas_temperatura_humedad LEFT JOIN entidades_naves_celdas ON lecturas_temperatura_humedad.id_entidad = entidades_naves_celdas.id_celda WHERE id_entidad IN (SELECT id_celda FROM entidades_naves_celdas WHERE fechaBaja IS NULL) AND fecha BETWEEN ? AND ? ORDER BY fecha ASC", [$fechaInicio, $fechaFin]);

		// Ahora recorremos, calculamos la media entre las medidas e insertamos en el histórico
		foreach ($lecturas as $lectura)
		{
			$ultimaLectura = new \stdClass();
			$ultimaLectura->temperatura = $lectura->temperatura;
			$ultimaLectura->humedad = $lectura->humedad;
			$ultimaLectura->luminosidad = $lectura->luminosidad;
			$ultimaLectura->etermico = $lectura->etermico;
			$ultimaLectura->id_nave = $lectura->id_nave;

			$medias[$lectura->id_entidad] = $ultimaLectura;

			$datosNave = $this->calcularMediaNaves($lectura->id_nave, $medias);

			try
			{
				DB::insert("INSERT INTO entidades_naves_historico (id_entidad, fecha, temperatura, humedad, luminosidad, etermico) VALUES (?, ?, ?, ?, ?, ?)", [$lectura->id_nave, $lectura->fecha, $datosNave["temperatura"], $datosNave["humedad"], $datosNave["luminosidad"], $datosNave["etermico"]]);
			}
			catch (Exception $e)
			{
				dump($e->getMessage());
			}
		}
    }
}
