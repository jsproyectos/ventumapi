<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class restaurarSigfox extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	//protected $signature = 'sigfox:restaurar {tipo} {--csv=false}';
	// protected $signature = 'sigfox:restaurar {tipo}';
	protected $signature = 'sigfox:restaurar';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Restaura la información de sigfox que no ha llegado al servidor';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	    parent::__construct();
	}

	private function normalizarIdDevice($device)
	{
		// Buscamos el primer caracter no 0
		$longitud = strlen($device);

		for ($i = 0; $i < $longitud; $i++)
		{
			if ($device[$i] != '0')
				break;
		}

		return substr($device, $i);
	}

	private function obtenerTipoSensor($device)
	{
		$sensor = DB::select("SELECT id_tipo, id_fab FROM sensores WHERE dispositivo = ?", [$device]);

		if (empty($sensor))
			return false;
		return ["tipo" => (int)$sensor[0]->id_tipo, "fabricante" => (int)$sensor[0]->id_fab];
	}

	private function insertarLecturaSensorSilosTST($device, $time, $data)
	{
		app('App\Http\Controllers\ComunicacionSigfoxController')->registrarLlenadoTST($device, $time, $data, false, new Request());
	}

	private function insertarLecturaTemperaturaTST($device, $time, $data)
	{
		app('App\Http\Controllers\ComunicacionSigfoxController')->registrarTemperaturaTST($device, $time, $data, new Request());
	}

	private function insertarLecturaTemperaturaIOTA($device, $time, $data)
	{
		app('App\Http\Controllers\ComunicacionSigfoxController')->registrarTemperaturaIOTA($device, $time, $data);
	}

	private function insertarLecturaGpsTST($device, $time, $data)
	{
		app('App\Http\Controllers\ComunicacionSigfoxController')->registrarGpsTST($device, $time, $data, new Request());
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		/*$tipo = $this->argument('tipo');
		$csv = $this->option('csv');

		$this->info("CSV: $csv");

		$options = $this->options();


		$this->info('Ejecutando comando');

		$this->info("Tipo: $tipo");


		$this->info($options)		;

		$csv == true ? $this->info('Es CSV') : $this->info('NO es CSV');*/

		$ruta = 'sigfox/';
		$fichero = 'mensajes.csv';

		foreach(file($ruta . $fichero) as $linea)
		{
			$linea = str_replace("\"", "", $linea);
			$datos = explode(";", $linea);

			$fecha = date_create_from_format ('Y-m-d H:i:s', $datos[0]);
			if ($fecha != false)
			{
				$time = $fecha->getTimestamp();
				$device = $this->normalizarIdDevice($datos[1]);
				$data = $datos[2];
				$snr = $datos[3];

				$tipo = $this->obtenerTipoSensor($device);

				$this->info("Procediendo a insertar lectura de sensor $device");
				$this->info("Tipo: " . json_encode($tipo));

				/*
					Fabricantes
					1 TST
					2 IOTA
					3 Ventum
					4 Nemeus
					5 Pedro
				*/

				switch ($tipo['tipo'])
				{
					case SENSOR_SILO:
					{
						switch ($tipo['fabricante'])
						{
							// TST
							case 1: $this->info('Es un silo TST'); $this->insertarLecturaSensorSilosTST($device, $time, $data); break;
						}

					}
					break;

					case SENSOR_TEMPERATURA:
					case SENSOR_TEMP_HUM_LUZ:
					{
						switch ($tipo['fabricante'])
						{
							// TST
							case 1: $this->info('Es temperatura TST'); $this->insertarLecturaTemperaturaTST($device, $time, $data); break;
							// IOTA
							case 2: $this->info('Es temperatura IOTA'); $this->insertarLecturaTemperaturaIOTA($device, $time, $data); break;
						}
					}

					break;
					case SENSOR_GPS:
					{
						switch ($tipo['fabricante'])
						{
							// TST
							case 1: $this->info('Es un GPS TST'); $this->insertarLecturaGpsTST($device, $time, $data); break;
						}
					}
				}

				$this->info("Lectura insertada");

				//$this->info("Fecha: $time, Dispositivo: $device, Datos: $data, SNR: $snr. Tipo: $tipo");
			}
		}

		//$this->info()
	}
}

