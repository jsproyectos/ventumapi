<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\LogController;

class alertasiht extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertas:iht';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Evalúa las alertas por estrés térmico peligroso';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	try
    	{
    		$this->info('Realizando el cálcluo de alertas por IHT peligroso');
    		LogController::mensaje('Realizando el cálcluo de alertas por IHT peligroso');
    		app('App\Http\Controllers\AlertasPeriodicasController')->AlertaNavesIHT();
    	}
    	catch (Exception $e)
    	{
    		$this->info('Error: ' . $e->getMessage());
			LogController::errores('Se han encontrado errores en el gestor de alertas por IHT peligroso');
			LogController::errores($e->getMessage());
    	} 
    }
}
