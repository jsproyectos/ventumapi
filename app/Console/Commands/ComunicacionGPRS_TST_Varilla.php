<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ComunicacionGPRS_TST_Varilla extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'GPRS:TST:Varilla {time} {data}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Inserta una lectura de sensor de varilla de suelo de TST, recibido desde un broker MQTT de sensores GPRS';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */

	
	public function __construct()
	{
		parent::__construct();
	}

	private function calcularSNR($snr)
	{
		return -(256 - $snr);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	// php artisan GPRS:TST:Varilla 1598523314 b'|3|352353082518751|1598458829|4257|1598462429|4362|1598466029|4263|1598469629|3968|1598473229|3549|1598476829|3285|1598480429|3054|1598484029|2867|1598487629|2690|1598491229|2529|1598494829|2383|1598498429|2257|'
	// /opt/bitnami/php/bin/php /opt/bitnami/apache2/htdocs/ventumApi/artisan GPRS:TST:Varilla 1598523314 b'|3|352353082518751|1598458829|4257|1598462429|4362|1598466029|4263|1598469629|3968|1598473229|3549|1598476829|3285|1598480429|3054|1598484029|2867|1598487629|2690|1598491229|2529|1598494829|2383|1598498429|2257|'

	public function handle()
	{
		$time = $this->argument('time');
		$data = $this->argument('data');

		$this->info($time);
		$this->info($data);

		app('App\Http\Controllers\ComunicacionGPRSController')->registrarTemperaturaTSTVarilla($time, $data);
	}
}
