<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class SensoresSigfoxCaducidad extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	protected $signature = 'sensores:sigfoxcaducidad';
	// php artisan sensores:sigfoxcaducidad
	// /opt/bitnami/php/bin/php /opt/bitnami/apache2/htdocs/ventumApi/artisan sensores:sigfoxcaducidad

	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Envía un email a los administradores indicando si hay sensores de Sigfox que van a caducar en 10 días';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	public function __construct()
	{
		parent::__construct();
	}

	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle()
	{
		try
		{
			// Datos generales
			$emailsAdministradores = ['jlproyectos@ventumidc.es', 'administracion@ventumidc.es', 'cbproyectos@ventumidc.es', 'mmproyectos@ventumidc.es'];

			// 1. Obtenemos la información actualizada de los dispositivos Sigfox que estén dados de alta. De esta forma siempre tendremos actualizada la fecha de caducidad del token y no tendremos que hacerlo manualmente.

			$this->info('Obteniendo la información de los dispositivos');

			$client = new \GuzzleHttp\Client();
			$response = $client->get('https://api.sigfox.com/v2/devices', [
				'auth' => [
					'60d585cb113a482551bc816d', 
					'ba8d51eba7d2e29ac2f0669ccd2a2c4a'
				]
			]);

			$this->info('Información obtenida. Procediendo a actualizar la información de la base de datos');
			$sensores = json_decode($response->getBody());

			// 2. Ahora, por cada sensor, actualizamos la fecha de cacudidad del token
			foreach ($sensores->data as $sensor)
			{
				if (isset($sensor->token))
				{
					// dump($sensor);
					$fechaFin = date('Y-m-d H:i:s', $sensor->token->end / 1000);

					DB::update('UPDATE sensores SET finContrato = ? WHERE dispositivo = ?', [$fechaFin, $sensor->id]);

					$this->info("$sensor->id actualizada fecha de caducidad a $fechaFin");
				}
			}

			// 3. Buscamos los sensores que caducarán dentro de 10 días para así ir comprando tokens.
			$this->info('Fechas de caducidad actualizadas. Se procede a obtener los dispositivos cuyos tokens caducarán en 10 días');

			$fehchaFinContratoInicio = date('Y-m-d', strtotime(date('Y-m-d') . ' + 9 day'));
			$fehchaFinContratoFin = date('Y-m-d', strtotime(date('Y-m-d') . ' + 10 day'));

			$this->info("Buscando desde $fehchaFinContratoInicio hasta $fehchaFinContratoFin");

			$sensores = DB::select('SELECT sensores.nombre nombreSensor, sensores.dispositivo, E.nombre nombreEntidad, explotaciones.nombre nombreExplotacion, finContrato FROM sensores LEFT JOIN (SELECT id_sensor, id_entidad, nombre, id_explotacion FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.fechaBaja IS NULL AND entidades.fechaBaja IS NULL) E ON sensores.id = E.id_sensor LEFT JOIN explotaciones ON E.id_explotacion = explotaciones.id WHERE finContrato BETWEEN ? AND ? ORDER BY dispositivo ASC;', [$fehchaFinContratoInicio, $fehchaFinContratoFin]);

			// Ya tenemos los sensores que caducarán en 10 días. Procedemos a enviar un email.
			if (!empty($sensores))
			{
				$datosEmail = [
					'titulo' => 'Caducidad en 10 días Sigfox',
					'fecha' => date('H:i:s d-m-Y'),
					'sensores' => $sensores,
				];

				foreach ($emailsAdministradores as $email)
					LogController::enviarEmail($email, 'alertas.sensores_sigfox_caducidad_10_dias', $datosEmail, 'Caducidad próxima en sensores sigfox');
			}

			// 4. Buscamos los sensores cuyos tokens ya están caducados hoy
			$this->info('Se procede a obtener los dispositivos cuyos tokens ya han caducado');

			$fehchaFinContratoInicio = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
			$fehchaFinContratoFin = date('Y-m-d');

			$this->info("Buscando desde $fehchaFinContratoInicio hasta $fehchaFinContratoFin");

			$sensores = DB::select('SELECT sensores.nombre nombreSensor, sensores.dispositivo, E.nombre nombreEntidad, explotaciones.nombre nombreExplotacion, finContrato FROM sensores LEFT JOIN (SELECT id_sensor, id_entidad, nombre, id_explotacion FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.fechaBaja IS NULL AND entidades.fechaBaja IS NULL) E ON sensores.id = E.id_sensor LEFT JOIN explotaciones ON E.id_explotacion = explotaciones.id WHERE finContrato BETWEEN ? AND ? ORDER BY dispositivo ASC;', [$fehchaFinContratoInicio, $fehchaFinContratoFin]);

			if (!empty($sensores))
			{
				$datosEmail = [
					'titulo' => 'Tokens Sigfox caducados',
					'fecha' => date('H:i:s d-m-Y'),
					'sensores' => $sensores,
				];

				foreach ($emailsAdministradores as $email)
					LogController::enviarEmail($email, 'alertas.sensores_sigfox_caducidad', $datosEmail, 'Tokens Sigfox Caducados');			
			}
		}
		catch (Exception $e)
		{
			LogController::errores('[SENSORES NO COMUNICADOS] ' . $e->getMessage());
			$this->info('Se ha producido un error. Por favor, mira el log');
		}
	}
}
