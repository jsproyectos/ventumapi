<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

define('CICLOS', 1);
define('TOLERANCIA', 1.05); // 5% de margen

class SensoresNoComunicados extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	protected $signature = 'sensores:nocomunicados';
	// /opt/bitnami/php/bin/php /opt/bitnami/apache2/htdocs/ventumApi/artisan sensores:nocomunicados

	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Envía un email a los administradores indicando que sensores no han emitido cuando debieron haberlo hecho';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	public function __construct()
	{
		parent::__construct();
	}

	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle()
	{
		try
		{
			$fechaActual = date('Y-m-d H:i:s');
			$this->info("$fechaActual : Comenzando la búsqueda de sensores que no han comunicado");

			$sensores = DB::select('SELECT S.id id_sensor, S.nombre nombreSensor, S.dispositivo, S.tiempo, S.fecha fechaUltimaEmision, E.nombre nombreEntidad, explotaciones.nombre nombreExplotacion FROM (SELECT id, nombre, dispositivo, fecha, tiempo, fechaAviso FROM sensores JOIN sensores_silos ON sensores.id = sensores_silos.id_sensor WHERE tiempo > 0 AND fake IS NULL AND fechaBaja IS NULL UNION SELECT id, nombre, dispositivo, fecha, tiempo, fechaAviso FROM sensores JOIN sensores_temperatura_humedad_luz ON sensores.id = sensores_temperatura_humedad_luz.id_sensor WHERE tiempo > 0 AND fake IS NULL AND fechaBaja IS NULL) S LEFT JOIN (SELECT id_sensor, id_entidad, nombre, id_explotacion FROM entidades_sensores JOIN entidades ON entidades_sensores.id_entidad = entidades.id WHERE entidades_sensores.fechaBaja IS NULL AND entidades.fechaBaja IS NULL) E ON S.id = E.id_sensor LEFT JOIN explotaciones ON E.id_explotacion = explotaciones.id WHERE DATE_ADD(S.fecha, INTERVAL + (S.tiempo * ' . (CICLOS * TOLERANCIA) . ') MINUTE) < ? AND DATE_ADD(S.fechaAviso, INTERVAL + (S.tiempo * ' . (CICLOS * TOLERANCIA) . ') MINUTE) < ?', [$fechaActual, $fechaActual]);

			// Vamos guardando los IDs de los sensores a los cuales les vamos a poner una fecha de aviso actual
			$idsAActualizar = [];

			if (!empty($sensores))
			{
				// Normalizamos algunas cosas antes de pasarlas a la plantilla
				foreach ($sensores as $sensor)
				{
					$sensor->fechaNormal = date('H:i:s d-m-Y', strtotime($sensor->fechaUltimaEmision . " + $sensor->tiempo minute"));
					$sensor->fechaUltimaEmision = date('H:i:s d-m-Y', strtotime($sensor->fechaUltimaEmision));

					if ($sensor->tiempo < 61)
						$sensor->tiempoTexto = "$sensor->tiempo minutos";
					else
					{
						$horas = intdiv($sensor->tiempo, 60);
						$minutos = $sensor->tiempo % 60;
						$sensor->tiempoTexto = $minutos == 0 ? "$horas horas" : "$horas horas y $minutos minutos";
					}

					$idsAActualizar[] = $sensor->id_sensor;
				}

				// Ahora los enviamos por email
				$emailsAdministradores = ['jlproyectos@ventumidc.es', 'proyectos@ventumidc.es'];

				$datosEmail = [
					'titulo' => 'Sensores que no han emitido a su hora',
					'fecha' => $fechaActual,
					'sensores' => $sensores,
				];

				foreach ($emailsAdministradores as $email)
					LogController::enviarEmail($email, 'alertas.sensores_no_comunicados', $datosEmail, 'Fallo en comunicación de sensores');

				DB::update('UPDATE sensores SET fechaAviso = ? WHERE id IN ' . obtenerInterrogacionesSQL($idsAActualizar), array_merge([$fechaActual], $idsAActualizar));

				$this->info('Email con información enviado correctamente');
			}
			else
			{
				$this->info('No hay sensores. Todo está correcto');
			}
		}
		catch (Exception $e)
		{
			LogController::errores('[SENSORES NO COMUNICADOS] ' . $e->getMessage());
			$this->info('Se ha producido un error. Por favor, mira el log');
		}
	}
}
