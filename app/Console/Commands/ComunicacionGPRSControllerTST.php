<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ComunicacionGPRSControllerTST extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GPRS:TST:Silos {device} {time} {snr} {data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserta una lectura de Silos recibida por el broker para los sensores GPRS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	private function calcularSNR($snr)
	{
		return -(256 - $snr);
	}

    // Prueba
    // #2020-11-05 08:36:12# #169#
	// php artisan GPRS:TST:Silos 352353083077062 1594366703 snr=-69 f100014076985a31c6bb00d600070000025c0000089c01ffff200200c15ec11c4d
	// php artisan GPRS:TST:Silos 352353082513711 1604561772 169 f1000140769851992fa900d600070000025d0000139d01ffff0afb01c05fa3ab6c
	// /opt/bitnami/php/bin/php /opt/bitnami/apache2/htdocs/ventumApi/artisan ComunicacionGPRSController:TST 352353083077062 1594366703 snr=-69 f100014076985a31c6bb00d600070000025c0000089c01ffff200200c15ec11c4d

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$device = $this->argument('device');
		$time = $this->argument('time');
		$snr = $this->calcularSNR($this->argument('snr'));
		$data = $this->argument('data');

		app('App\Http\Controllers\ComunicacionGPRSController')->registrarLlenadoTST($device, $time, $snr, $data);
    }
}
