<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class corregirco2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corregir:co2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Corrige los 0s en las lecturas de sensores de CO2';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	// 1. Seleccionamos los sensores de CO2 activos
    	$this->info('Obteniendo sensores de CO2 activos');

    	$sensores = DB::select('SELECT id_sensor, nombre FROM sensores_co2 JOIN sensores ON sensores_co2.id_sensor = sensores.id');

    	$this->info('Empezando correcciones');

    	foreach ($sensores as $sensor)
    	{
    		$this->info('Corrigiendo ' . $sensor->nombre);

    		$lecturas = DB::select('SELECT temperatura, humedad, co2, fecha FROM lecturas_co2 WHERE temperatura = 0 OR humedad = 0 OR co2 = 0');

    		foreach ($lecturas as $lectura)
    		{
    			$actual = DB::select('SELECT avg(temperatura) temperatura, avg(humedad) humedad, avg(co2) co2 FROM ((SELECT temperatura, humedad, co2 FROM lecturas_co2 WHERE fecha > ? ORDER BY fecha ASC LIMIT 1) UNION (SELECT temperatura, humedad, co2 FROM lecturas_co2 WHERE fecha < ? ORDER BY fecha DESC LIMIT 1)) t', [$lectura->fecha, $lectura->fecha])[0];

				$this->info('Actualizando día ' . $lectura->fecha);

				$temperatura = $lectura->temperatura == 0 ? $actual->temperatura : $lectura->temperatura;
				$humedad = $lectura->humedad == 0 ? $actual->humedad : $lectura->humedad;
				$co2 = $lectura->co2 == 0 ? $actual->co2 : $lectura->co2;

				DB::update('UPDATE lecturas_co2 SET temperatura = ?, humedad = ?, co2 = ? WHERE fecha = ?', [$temperatura, $humedad, $co2, $lectura->fecha]);
    		}
    	}

        // 1. Seleccionamos las lecturas que contienen 0s
    }
}
