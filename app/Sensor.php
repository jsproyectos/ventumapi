<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
	public $timestamps = false;
	protected $table = 'sensores';

	protected $fillable = ['id', 'nombre', 'dispositivo', 'fake', 'proto', 'seguimiento', 'descripcion', 'latitud', 'longitud', 'bateria', 'bp', 'enchufe', 'rssi', 'snr', 'id_tipo', 'id_emision', 'downlink', 'tiempo', 'callback', 'fechaCallback', 'fechaAlta', 'fechaBaja'];
}