<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntidadNevera extends Model
{
	public $timestamps = false;
	protected $table = 'entidades_neveras';
	protected $primaryKey = 'id_entidad';

	protected $fillable = ["id_entidad", "min", "max"];
}
