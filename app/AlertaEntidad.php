<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertaEntidad extends Model
{
	public $timestamps = false;
	protected $table = 'alertas_entidades';

	protected $fillable = ['id', 'id_entidad', 'id_alerta', 'id_usuario_add', 'id_usuario', 'parametros', 'activo', 'ultima'];
}