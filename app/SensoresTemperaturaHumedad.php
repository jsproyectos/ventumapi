<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensoresTemperaturaHumedad extends Model
{
	public $timestamps = false;
	protected $table = 'sensores_temperatura_humedad';
	protected $primaryKey = 'id_sensor';

	protected $fillable = ['id_sensor', 'temperatura', 'humedad'];
}
