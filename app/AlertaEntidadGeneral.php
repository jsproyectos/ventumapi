<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertaEntidadGeneral extends Model
{
	public $timestamps = false;
	protected $table = 'alertas_generales';

	protected $fillable = ["id_alerta", "id_usuario", "parametros", "activo", "ultima"];
}
