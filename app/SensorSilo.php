<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorSilo extends Model
{
	public $timestamps = false;
	protected $table = 'sensores_silos';
	protected $primaryKey = 'id_sensor';

	protected $fillable = ['id_sensor', 'altura', 'ultimaLectura', 'temperatura', 'fecha'];
}
