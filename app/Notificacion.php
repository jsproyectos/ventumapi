<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
	public $timestamps = false;
	protected $table = 'notificaciones';

	protected $fillable = ['fecha', 'tipo', 'id_referencia', 'id_alerta', 'id_usuario', 'parametros', 'parametros_alerta', 'leida', 'fecha_email'];
}
