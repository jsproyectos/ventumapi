<?php

namespace App\Jobs;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadNave extends alertasEntidad
{
	protected $temperatura;
	protected $humedad;
	protected $luminosidad;
	protected $eTermico;

	public function __construct($id_entidad, $fecha, $temperatura, $humedad, $luminosidad, $eTermico)
	{
		$this->id_entidad = $id_entidad;
		$this->fecha = $fecha;
		$this->temperatura = $temperatura;
		$this->humedad = $humedad;
		$this->luminosidad = $luminosidad;
		$this->eTermico = $eTermico;
	}

	public function handle()
	{
		// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
		$alertas = app('App\Http\Controllers\EntidadesController')->obtenerAlertasActivas($this->id_entidad);

		try
		{
			// Ahora obtenemos el tipo de entidad con la que trabaja este sensor
			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado temperatura baja ($this->temperatura ºC) en su nave $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaBaja($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura > $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado temperatura alta ($this->temperatura ºC) en su nave $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaAlta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_HUMEDAD_BAJA:
						if ($this->humedad <= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado humedad relativa baja ($this->humedad%) en su nave $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_HUMEDAD_BAJA, $alerta->id_usuario, $this->humedad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailHumedadBaja($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->humedad, $alerta->parametros);
						}
						break;
					case ALARMA_HUMEDAD_ALTA:
						if ($this->humedad > $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado humedad relativa alta ($this->humedad%) en su nave $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_HUMEDAD_ALTA, $alerta->id_usuario, $this->humedad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailHumedadAlta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->humedad, $alerta->parametros);
						}
						break;
					case ALARMA_LUMINOSIDAD_BAJA:
						if ($this->luminosidad <= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado presencia de luz baja ($this->luminosidad lux) en su nave $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_LUMINOSIDAD_BAJA, $alerta->id_usuario, $this->luminosidad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailLuminosidadBaja($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->luminosidad, $alerta->parametros);
						}
						break;
					case ALARMA_LUMINOSIDAD_ALTA:
						if ($this->luminosidad > (int)$alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado presencia de luz alta ($this->luminosidad lux) en su nave $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_LUMINOSIDAD_ALTA, $alerta->id_usuario, $this->luminosidad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailLuminosidadAlta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->luminosidad, $alerta->parametros);
						}
						break;
					case ALERTA_ETERMICO_PROLONGADO:
						$parametroTroceados = explode("/", $alerta->parametros);
						if ($this->estresTermicoProlongado(intval($parametroTroceados[0]), intval($parametroTroceados[1])))
						{
							$mensajeNotificacion = "Se ha detectado estrés térmico prolongado peligroso en su nave $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALERTA_ETERMICO_PROLONGADO, $alerta->id_usuario, $this->parametros, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailETermicoProlongado($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->eTermico, $alerta->parametros);
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[ALERTAS ENTIDAD NAVE] " . $e->getMessage());	
		}
	}

	private function estresTermicoProlongado($prolongado, $reciente)
	{
		return app('App\Http\Controllers\EntidadesNavesController')->calculoEstresTermicoProlongado($this->id_entidad, $prolongado, $reciente);
	}

	private function emailTemperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros);
	}

	private function emailTemperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros);
	}

	private function emailHumedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros);
	}

	private function emailHumedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros);
	}

	private function emailLuminosidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->luminosidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros);
	}

	private function emailLuminosidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->luminosidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $luminosidad, $parametros);
	}

	private function emailETermicoProlongado($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $etermico, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->estresTermicoProlongado($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $etermico, $parametros);
	}
}