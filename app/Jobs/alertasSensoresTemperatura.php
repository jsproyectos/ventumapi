<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasSensoresTemperatura extends alertasSensores
{
protected $tipoApertura;

	protected $temperatura;
	protected $humedad;
	protected $luminosidad;
	protected $eTermico;

	public function __construct($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $temperatura, $humedad, $luminosidad, $eTermico)
	{
		$this->idSensor = $idSensor;
		$this->fecha = $fecha;
		$this->bateria = $bateria;
		$this->bp = $bp;
		$this->snr = $snr;
		$this->rssi = $rssi;
		$this->temperatura = $temperatura;
		$this->humedad = $humedad;
		$this->luminosidad = $luminosidad;
		$this->eTermico = $eTermico;
	}

	public function handle()
	{
		try
		{
			// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
			$alertas = $this->obtenerAlertasActivas($this->idSensor);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_BATERIA_BAJA:
						if ($this->bp <= $alerta->parametros)
						{
							$data = [
								'nombre' => $alerta->nombreUsuario,
								'fecha' => $this->fecha,
								'idSensor' => $this->idSensor,
								'nombreSensor' => $alerta->nombreSensor,
								'bp' => $this->bp,
								'titulo' => 'Alerta por batería baja',
							];
							$this->enviarAlerta($this->idSensor, ALARMA_BATERIA_BAJA, $alerta->id_usuario, $this->bp, $alerta->email, 'alertas.bateria_baja', $data, "Bateria baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$mensajeNotificacion = "Temperatura baja ($this->temperatura ºC) en sensor $alerta->nombreSensor";
							$this->enviarAlertaNotificacion($this->idSensor, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaBaja($this->idSensor, $alerta->nombreSensor, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura > $alerta->parametros)
						{
							LogController::mensaje("TEMPERATURA ALTA");
							$mensajeNotificacion = "Temperatura alta ($this->temperatura ºC) en sensor $alerta->nombreSensor";
							$this->enviarAlertaNotificacion($this->idSensor, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaAlta($this->idSensor, $alerta->nombreSensor, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_HUMEDAD_BAJA:
						if ($this->humedad <= $alerta->parametros)
						{
							$mensajeNotificacion = "Humedad relativa baja ($this->humedad%) en sensor $alerta->nombreSensor";
							$this->enviarAlertaNotificacion($this->idSensor, ALARMA_HUMEDAD_BAJA, $alerta->id_usuario, $this->humedad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailHumedadBaja($this->idSensor, $alerta->nombreSensor, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->humedad, $alerta->parametros);
						}
						break;
					case ALARMA_HUMEDAD_ALTA:
						if ($this->humedad > $alerta->parametros)
						{
							$mensajeNotificacion = "Humedad relativa alta ($this->humedad%) en sensor $alerta->nombreSensor";
							$this->enviarAlertaNotificacion($this->idSensor, ALARMA_HUMEDAD_ALTA, $alerta->id_usuario, $this->humedad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailHumedadAlta($this->idSensor, $alerta->nombreSensor, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->humedad, $alerta->parametros);
						}
						break;
					case ALARMA_LUMINOSIDAD_BAJA:
						if ($this->luminosidad <= $alerta->parametros)
						{
							$mensajeNotificacion = "Baja luminosidad ($this->luminosidad lux) en sensor $alerta->nombreSensor";
							$this->enviarAlertaNotificacion($this->idSensor, ALARMA_LUMINOSIDAD_BAJA, $alerta->id_usuario, $this->luminosidad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailLuminosidadBaja($this->idSensor, $alerta->nombreSensor, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->luminosidad, $alerta->parametros);
						}
						break;
					case ALARMA_LUMINOSIDAD_ALTA:
						if ($this->luminosidad > intval($alerta->parametros))
						{
							$mensajeNotificacion = "Alta luminosidad ($this->luminosidad lux) en sensor $alerta->nombreSensor";
							$this->enviarAlertaNotificacion($this->idSensor, ALARMA_LUMINOSIDAD_ALTA, $alerta->id_usuario, $this->luminosidad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailLuminosidadAlta($this->idSensor, $alerta->nombreSensor, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->luminosidad, $alerta->parametros);
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[ALERTAS SENSOR LUMINOSIDAD] $this->idSensor" . ': ' . $e->getMessage());
		}
	}

	private function emailTemperaturaBaja($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->temperaturaBaja($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $temperatura, $parametros);
	}

	private function emailTemperaturaAlta($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->temperaturaAlta($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $temperatura, $parametros);
	}

	private function emailHumedadBaja($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->humedadBaja($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $humedad, $parametros);
	}

	private function emailHumedadAlta($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->humedadAlta($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $humedad, $parametros);
	}

	private function emailLuminosidadBaja($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->luminosidadBaja($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $luminosidad, $parametros);
	}

	private function emailLuminosidadAlta($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $luminosidad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNaveController')->luminosidadAlta($idEntidad, $nombreSensor, $nombreUsuario, $email, $fecha, $luminosidad, $parametros);
	}
}