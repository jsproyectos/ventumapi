<?php

namespace App\Jobs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail
use Exception;

class FiwareRecipientes extends Fiware
{
	public function __construct($id_entidad, $fecha)
	{
		$this->id_entidad = $id_entidad;
		$this->fecha = $fecha;
	}

	public function handle()
	{
		try
		{
			$informacion = DB::select('SELECT entidades_recipientes.id_entidad id_entidad, id_device_fiware, id_fiware, entidades.nombre nombre, entidades.codigo codigo, entidades.descripcion descripcion, entidades.latitud latitud, entidades.longitud longitud, explotaciones.nombre nombreExplotacion, entidades.fechaAlta fechaAlta, entidades.fechaBaja fechaBaja, id_modelo, animales_tipos.fiware animal, IFNULL(animales, 0) animales, fecha, entidades_recipientes.capacidad capacidad, altura, entidades_modelos_recipientes.id_tipo tipo, porcentaje, temperatura, consumo, host, puerto, token, service, subservice, ContentType, user, pass FROM ((entidades_fiware JOIN entidades on entidades_fiware.id_entidad = entidades.id) JOIN fiware ON entidades_fiware.id_fiware = fiware.id JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad) LEFT JOIN explotaciones ON entidades.id_explotacion = explotaciones.id LEFT JOIN animales_tipos ON entidades_recipientes.id_ganado = animales_tipos.id JOIN entidades_modelos_recipientes ON entidades_recipientes.id_modelo = entidades_modelos_recipientes.id WHERE entidades_fiware.fechaBaja IS NULL and entidades.fechaBaja IS NULL AND entidades_recipientes.id_entidad = ?', [$this->id_entidad]);

			if (!empty($informacion))
			{
				$codRespuesta = 0;
				$intentos = 0;

				// Nos quedamos con el primer elemento así es mas facil trabajar y luego cargamos la vista con el contenido del json a enviar
				$informacion = $informacion[0];
				// Cambiamos el tipo de depósito (si es silo de pienso, depósito de agua o tanque de leche)
				switch ($informacion->tipo)
				{
					case DEPOSITO_SILO: $informacion->tipo = 'Silo de pienso'; break;
					case DEPOSITO_AGUA:	$informacion->tipo = 'Depósito de agua'; break;
					case DEPOSITO_LECHE: $informacion->tipo = 'Tanque de leche'; break;
					default: $informacion->tipo = 'Desconocido'; break;
				}

				$plantilla = view('fiware.recipientes', ['entidad' => $informacion]);
				app('App\Http\Controllers\FiwareController')->enviarInformacion($informacion, $plantilla);
			}	
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}
}
