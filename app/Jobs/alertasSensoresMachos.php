<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class alertasSensoresMachos extends alertasSensores
{
	public function __construct($idSensor, $fecha, $bateria, $bp, $snr, $rssi)
	{
		$this->idSensor = $idSensor;
		$this->fecha = $fecha;
		$this->bateria = $bateria;
		$this->bp = $bp;
		$this->snr = $snr;
		$this->rssi = $rssi;
	}

	public function handle()
	{
		try
		{
			// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
			$alertas = $this->obtenerAlertasActivas($this->idSensor);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_BATERIA_BAJA:
						if ($this->bp <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"bp" => $this->bp,
								"titulo" => "Alerta por batería baja",
								"idTipo" => SENSOR_MACHOS
							];
							$this->enviarAlerta($this->idSensor, ALARMA_BATERIA_BAJA, $alerta->id_usuario, $this->bp, $alerta->email, "alertas.bateria_baja", $data, "Bateria baja en sensor $alerta->nombreSensor");
						}
						break;
					/*case ALARMA_SNR_BAJA:
						if ($this->snr <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"titulo" => "Alerta por apertura de puerta",
								"idTipo" => SENSOR_MACHOS
							];
							$this->enviarAlerta($this->idSensor, ALARMA_SNR_BAJA, $alerta->id_usuario, null, $alerta->email, "alertas.sensoresPuertas.puertas", $data, "Apertura en sensor puerta: $alerta->nombreSensor");
						}
						break;*/
					case ALARMA_MONTA_MACHO:
						$data = [
							"nombre" => $alerta->nombreUsuario,
							"fecha" => $this->fecha,
							"idSensor" => $this->idSensor,
							"nombreSensor" => $alerta->nombreSensor,
							"titulo" => "Alerta por monta detectada",
							"idTipo" => SENSOR_MACHOS,
							"montas" => DB::select("SELECT montas FROM sensores_machos WHERE id_sensor = ?", [$this->idSensor])[0]->montas
						];
						$this->enviarAlerta($this->idSensor, ALARMA_MONTA_MACHO, $alerta->id_usuario, null, $alerta->email, "alertas.sensoresMachos.monta", $data, "Monta detectada en sensor $alerta->nombreSensor");
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[Alertas] $this->idSensor" . ": " . $e->getMessage());
		}
	}
}
