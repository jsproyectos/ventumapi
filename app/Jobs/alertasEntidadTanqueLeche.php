<?php

namespace App\Jobs;

use Exception;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadTanqueLeche extends alertasEntidadRecipiente
{
	public function __construct($id_entidad, $porcentaje, $kilos, $fecha, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ)
	{
		$this->id_entidad = $id_entidad;
		$this->porcentaje = $porcentaje;
		$this->fecha = $fecha;
		$this->temperatura = $temperatura;
		$this->kilos = $kilos;
		$this->acelerometroX = $acelerometroX;
		$this->acelerometroY = $acelerometroY;
		$this->acelerometroZ = $acelerometroZ;
	}

	public function handle()
	{
		try
		{
			$alertas = $this->obtenerAlertasActivas($this->id_entidad);

			// Ahora obtenemos el tipo de entidad con la que trabaja este sensor
			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura != null & $this->temperatura <= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado temperatura baja ($this->temperatura ºC) en su Tanque de leche $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaBaja($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura != null & $this->temperatura >= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado temperatura alta ($this->temperatura ºC) en su Tanque de leche $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaAlta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_SILOS_CAPACIDAD_BAJA:
						if ($this->porcentaje != null & $this->porcentaje < $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado capacidad baja ($this->porcentaje%) en su Tanque de leche $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_SILOS_CAPACIDAD_BAJA, $alerta->id_usuario, $this->porcentaje, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailCapacidadBaja($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->porcentaje, $alerta->parametros);
						}
						break;
					case ALARMA_SILOS_CAPACIDAD_ALTA:
						if ($this->porcentaje != null && $this->porcentaje >= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado capacidad alta ($this->porcentaje%) en su Tanque de leche $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_SILOS_CAPACIDAD_ALTA, $alerta->id_usuario, $this->porcentaje, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailCapacidadAlta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->porcentaje, $alerta->parametros);
						}
						break;
					case ALARMA_SILOS_RECEPCION:
						/*$mensajeNotificacion = "Capacidad actualizada ($this->porcentaje%) en su Silo $alerta->nombreEntidad";
						$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_SILOS_RECEPCION, $alerta->id_usuario, $this->porcentaje, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

						if ($alerta->email_alerta && $alerta->email_instantaneo)
							$this->emailRecepcion($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->porcentaje);
						break;*/
					case ALARMA_SILOS_DESNIVEL:
						if (!tapaCerrada($this->acelerometroX, $this->acelerometroY, $this->acelerometroZ)) // Si la tapa no se encuentra cerrada
						{
							$mensajeNotificacion = "Tapa no cerrada correctamente detectada en su Tanque de leche $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_SILOS_DESNIVEL, $alerta->id_usuario, $this->porcentaje, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailDesnivel($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha);
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores($this->id_entidad . ': ' . $e->getMessage());
		}
	}

	private function emailTemperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadTanqueLecheController')->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta);
	}

	private function emailTemperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadTanqueLecheController')->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta);
	}

	private function emailCapacidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadTanqueLecheController')->capacidadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta);
	}

	private function emailCapacidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadTanqueLecheController')->capacidadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion, $parametrosAlerta);
	}

	private function emailRecepcion($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadTanqueLecheController')->recepcion($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $parametrosNotificacion);
	}

	private function emailDesnivel($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadTanqueLecheController')->desnivel($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha);
	}
}

