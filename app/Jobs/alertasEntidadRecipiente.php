<?php

namespace App\Jobs;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadRecipiente extends alertasEntidad
{
	protected $porcentaje;
	protected $temperatura;
	protected $kilos;
	protected $acelerometroX;
	protected $acelerometroY;
	protected $acelerometroZ;

	public function __construct($id_entidad, $porcentaje, $kilos, $fecha, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ)
	{
		$this->id_entidad = $id_entidad;
		$this->porcentaje = $porcentaje;
		$this->fecha = $fecha;
		$this->temperatura = $temperatura;
		$this->kilos = $kilos;
		$this->acelerometroX = $acelerometroX;
		$this->acelerometroY = $acelerometroY;
		$this->acelerometroZ = $acelerometroZ;
	}

	public function handle()
	{
	}
}
