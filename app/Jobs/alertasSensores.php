<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class alertasSensores implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $idSensor;
	protected $fecha;
	protected $bateria;
	protected $bp;
	protected $snr;
	protected $rssi;

	public function __construct()
	{
	}

	public function handle()
	{
	}

	public function obtenerAlertasActivas($idSensor)
	{
		return app('App\Http\Controllers\SensoresController')->obtenerAlertasActivas($idSensor);
	}

	/*public function enviarAlerta($idSensor, $idAlerta, $idUsuario, $parametros, $email, $vista, $data, $asunto)
	{
		// Insertar notificación
		app('App\Http\Controllers\NotificacionesController')->insertarNotificacionSensor($idSensor, $idAlerta, $idUsuario, $parametros);
		// Enviar email con la alerta
		LogController::enviarEmail($email, $vista, $data, $asunto);
	}*/

	// Envía una alerta por email, en lugar de por notificaicón. Esto cambiará cuando esté lista la app
	protected function enviarAlertaEmail($idSensor, $idAlerta, $idUsuario, $parametros, $email, $vista, $dataEmail, $asunto)
	{
		// Insertar notificación
		app('App\Http\Controllers\NotificacionesController')->insertarNotificacionSensor($idSensor, $idAlerta, $idUsuario, $parametros);

		//Enviar email con la alerta
		LogController::enviarEmail($mail, $vista, $dataEmail, $asunto);
	}

	// Envía una alerta con notificación, en vez de por email
	protected function enviarAlertaNotificacion($idSensor, $idAlerta, $idUsuario, $parametros, $userIdOneSignal, $mensajeNotificacion, $id, $fecha, $notificacion, $email, $email_instantaneo)
	{
		app('App\Http\Controllers\AlertasController')->enviarAlertaNotificacionSensor($idSensor, $idAlerta, $idUsuario, $parametros, $userIdOneSignal, $mensajeNotificacion, $id, $fecha, $notificacion, $email, $email_instantaneo);
	}
}