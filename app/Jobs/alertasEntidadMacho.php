<?php

namespace App\Jobs;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadMacho extends alertasEntidad
{
	public function __construct($id_entidad, $fecha)
	{
		$this->id_entidad = $id_entidad;
		$this->fecha = $fecha;
	}

	public function handle()
	{
		// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
		$alertas = $this->obtenerAlertasActivas($this->id_entidad);

		foreach ($alertas as $alerta)
		{
			switch ($alerta->id_alerta)
			{
				case ALARMA_MONTA_MACHO:
					$mensaje = "Su carnero con bolo $alerta->codigo ($alerta->nombreEntidad) acaba de realizar una monta";
					$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_MONTA_MACHO, $alerta->id_usuario, '*', $alerta->user_id_ionic, $mensaje, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

					if ($alerta->email_alerta && $alerta->email_instantaneo)
						$this->emailMonta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $alerta->codigo);
					break;
			}
		}
	}

	private function emailMonta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $codigo)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadAnimalController')->montaMacho($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $codigo);
	}
}
