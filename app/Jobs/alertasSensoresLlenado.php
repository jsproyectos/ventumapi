<?php

namespace App\Jobs;
use Illuminate\Support\Facades\DB;

class alertasSensoresLlenado extends alertasSensores
{
	protected $altura;
	protected $temperatura;
	protected $aX;
	protected $aY;
	protected $aZ;

	public function __construct($idSensor, $fecha, $bateria, $rssi, $snr, $altura, $temperatura, $aX, $aY, $aZ)
	{
		$this->idSensor = $idSensor;
		$this->fecha = $fecha;
		$this->bateria = 100 - $bateria * 25; // Ya que las baterías van 0, 1, 2, 3 y 4, hacemos una escala de 0 a 100
		$this->bp = null;
		$this->rssi = $rssi;
		$this->snr = $snr;
		$this->altura = $altura;
		$this->temperatura = $temperatura;
		$this->aX = $aX;
		$this->aY = $aY;
		$this->aZ = $aZ;
	}


	public function handle()
	{
		try
		{
			$alertas = $this->obtenerAlertasActivas($this->idSensor);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_BATERIA_BAJA:
						if ($this->bp <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"bp" => $this->bateria,
								"titulo" => "Alerta por batería baja",
							];
							$this->enviarAlerta($this->idSensor, ALARMA_BATERIA_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, "alertas.bateria_baja", $data, "Bateria baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura baja"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $this->temperatura, $alerta->email, "alertas.temperatura_baja", $data, "Temperatura baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $this->temperatura, $alerta->email, "alertas.temperatura_alta", $data, "Temperatura alta en sensor $alerta->nombreSensor");
						}
						break;
					/*case ALARMA_SILOS_CAPACIDAD_BAJA:
						if ($this->porcentaje <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por baja ocupación",
								"parametros" => $alerta->parametros
							];
							$this->insertarNotificacion($this->id_entidad, ALARMA_SILOS_CAPACIDAD_BAJA, $alerta->id_usuario, $alerta->email, $this->porcentaje);
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".capacidad_baja", $data, "Capacidad baja en $tipoEntidad $alerta->nombreEntidad");
						}
						break;
					case ALARMA_SILOS_CAPACIDAD_ALTA:
						if ($this->porcentaje >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por alta ocupación",
								"parametros" => $alerta->parametros
							];
							$this->insertarNotificacion($this->id_entidad, ALARMA_SILOS_CAPACIDAD_ALTA, $alerta->id_usuario, $alerta->email, $this->porcentaje);
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".capacidad_alta", $data, "Capacidad alta en $tipoEntidad $alerta->nombreEntidad");
						}
						break;*/
					case ALARMA_SILOS_RECEPCION:
						$data = [
							"nombre" => $alerta->nombreUsuario,
							"fecha" => $this->fecha,
							"idSensor" => $this->idSensor,
							"nombreSensor" => $alerta->nombreSensor,
							"distancia" => $this->altura,
							"titulo" => "Lectura actualizada"
						];
						$this->enviarAlerta($this->idSensor, ALARMA_SILOS_RECEPCION, $alerta->id_usuario, $this->altura, $alerta->email, "alertas.sensoresNivelLlenado.recepcion", $data, "Nuevo estado en sensor de nivel de llenado $alerta->nombreSensor");
						break;
					/*case ALARMA_SILOS_DESNIVEL:
						if ($capacidad <= $alerta->parametros)
						{
							app('App\Http\Controllers\AlertasController')->SILOS_capacidadBaja($alerta->email, $alerta->nombreUsuario, $fecha, $idSensor, $alerta->nombreSensor, $capacidad);
						}
						break;*/
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores($this->idSensor . ": " . $e->getMessage());
		}
	}
}
