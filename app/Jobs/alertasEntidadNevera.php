<?php

namespace App\Jobs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadNevera extends alertasEntidad
{
	protected $temperatura;
	protected $humedad;

	public function __construct($id_entidad, $fecha, $temperatura, $humedad)
	{
		$this->id_entidad = $id_entidad;
		$this->fecha = $fecha;
		$this->temperatura = $temperatura;
		$this->humedad = $humedad;
	}

	public function handle()
	{
		try
		{
			$alertas = app('App\Http\Controllers\EntidadesController')->obtenerAlertasActivas($this->id_entidad);

			// Ahora obtenemos el tipo de entidad con la que trabaja este sensor
			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado temperatura baja ($this->temperatura ºC) en su nevera $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaBaja($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura > $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado temperatura alta ($this->temperatura ºC) en su nevera $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $this->temperatura, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailTemperaturaAlta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->temperatura, $alerta->parametros);
						}
						break;
					case ALARMA_HUMEDAD_BAJA:
						if ($this->humedad <= $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado humedad relativa baja ($this->humedad%) en su nevera $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_HUMEDAD_BAJA, $alerta->id_usuario, $this->humedad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailHumedadBaja($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->humedad, $alerta->parametros);
						}
						break;
					case ALARMA_HUMEDAD_ALTA:
						if ($this->humedad > $alerta->parametros)
						{
							$mensajeNotificacion = "Se ha detectado humedad relativa alta ($this->humedad%) en su nevera $alerta->nombreEntidad";
							$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_HUMEDAD_ALTA, $alerta->id_usuario, $this->humedad, $alerta->user_id_ionic, $mensajeNotificacion, $alerta->id, $this->fecha, $alerta->notificaciones_movil, $alerta->email_alerta, $alerta->email_instantaneo);

							if ($alerta->email_alerta && $alerta->email_instantaneo)
								$this->emailHumedadAlta($this->id_entidad, $alerta->nombreEntidad, $alerta->nombreUsuario, $alerta->email, $this->fecha, $this->humedad, $alerta->parametros);
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[ALERTAS ENTIDAD NEVERA] " . $e->getMessage());	
		}
	}

	private function emailTemperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNeveraController')->temperaturaBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros);
	}

	private function emailTemperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNeveraController')->temperaturaAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $temperatura, $parametros);
	}

	private function emailHumedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNeveraController')->humedadBaja($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros);
	}

	private function emailHumedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros)
	{
		app('App\Http\Controllers\AlertasEmail\AlertasEntidadNeveraController')->humedadAlta($idEntidad, $nombreEntidad, $nombreUsuario, $email, $fecha, $humedad, $parametros);
	}
}
