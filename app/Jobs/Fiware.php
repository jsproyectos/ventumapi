<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Fiware implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id_entidad;
    protected $fecha;

	public function __construct($id_entidad)
	{
		$this->id_entidad = $id_entidad;
	}

	public function handle()
	{
		// Código genérico de ejecución
	}
}
