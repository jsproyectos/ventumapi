<?php

namespace App\Jobs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadPuerta extends alertasEntidad
{
	protected $tipoApertura;

	public function __construct($idEntidad, $fecha, $tipoApertura)
	{
		$this->id_entidad = $idEntidad;
		$this->fecha = $fecha;
		$this->tipoApertura = $tipoApertura;
	}

	public function handle()
	{
		try
		{
			// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
			$alertas = $this->obtenerAlertasActivas($this->id_entidad);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_PUERTAS_APERTURA:
						if ($this->tipoApertura == PUERTA_ABIERTA)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"titulo" => "Alerta por apertura de puerta",
							];
							$vista = "alertas.entidad.puerta.apertura";
							$this->enviarAlertaEmail($this->id_entidad, ALARMA_PUERTAS_APERTURA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Apertura en puerta $alerta->nombreEntidad");
						}
						break;
					case ALARMA_PUERTAS_CIERRE:
						if ($this->tipoApertura == PUERTA_CERRADA)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"titulo" => "Alerta por cierre de puerta",
							];
							$vista = "alertas.entidad.puerta.cierre";
							$this->enviarAlertaEmail($this->id_entidad, ALARMA_PUERTAS_CIERRE, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Cierre en puerta $alerta->nombreEntidad");
						}
						break;
				}
			}	
		}
		catch (Exception $e)
		{
			LogController::errores("[ALERTAS ENTIDAD PUERTA] " . $e->getMessage());	
		}
	}
}
