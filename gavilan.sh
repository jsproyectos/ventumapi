cd htdocs/gavilan 							# Movemos al directorio
if [ -s www.zip ] 							# Preguntamos si existe el fichero www.zip
then										#
	mv www.zip /home/bitnami/backup/www.zip # Lo copiamos a la carpeta de backup
	rm -R *.* 								# Borramos el contenido
	rm -R assets 							#
	rm -R svg 								#
	mv /home/bitnami/backup/www.zip www.zip # Lo restauramos desde la carpeta de backup
	unzip www.zip 							# Extraemos el zip con la aplicación actualizada
	rm www.zip								# Eliminamos el fichero zip
else 										#
	rm -R *.* 								# En caso de que no, simplemente borramos todos los ficheros
	rm -R assets 							#
	rm -R svg 								#
fi 											#