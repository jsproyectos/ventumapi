<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SensoresTipoEmision extends Migration
{
	public function up()
	{
		Schema::create('sensores_tipos_emision', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string("nombre", 50);
		});
	}

	public function down()
	{
		Schema::dropIfExists('sensores_tipos_emision');
	}
}
