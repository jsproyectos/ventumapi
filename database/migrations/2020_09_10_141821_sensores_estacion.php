<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SensoresEstacion extends Migration
{
	public function up()
	{
		Schema::create('sensores_estacion', function (Blueprint $table)
		{
			$table->integer('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp('fecha')->nullable()->comment('Fecha de última actualización');
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('presion', 7, 2)->default(0)->comment('Presion atomesférica en Pa');
			$table->decimal('lluvia', 7, 4)->default(0)->comment('Precipitaciones en mm/h');
			$table->decimal('viento_vel', 5, 2)->default(0)->comment('Velocidad del viento en Km/h');
			$table->unsignedTinyInteger('viento_dir')->default(0);
		});
	}

	public function down()
	{
		Schema::dropIfExists('sensores_estacion');
	}
}
