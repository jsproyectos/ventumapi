<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaLecturasCaudalimetro extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lecturas_caudalimetro', function (Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedInteger('id_entidad')->unsigned()->nullable();
			$table->foreign('id_entidad')->references('id')->on('entidades')->onDelete('cascade')->onUpdate('cascade');
			$table->string('payload')->default(null)->nullable();
			$table->decimal('snr', 5, 2)->default(0)->nullable();
			$table->smallInteger('rssi')->default(0)->nullable();
			$table->decimal('bateria', 3, 2)->default(0);
			$table->tinyInteger('bp')->unsigned()->default(0);
			$table->timeStamp('fecha')->useCurrent()->nullable();
			$table->unsignedInteger('mililitros')->default(0);
			$table->decimal('litros', 7, 2)->default(0);
			$table->decimal('caudal', 7, 2)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('lecturas_caudalimetro');
	}
}
