<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresSilos extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sensores_silos', function (Blueprint $table)
		{
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("altura")->default(300)->unsigned();
			$table->decimal("porcentaje", 5, 2)->default(0);
			$table->integer("capacidad")->default(0)->unsigned();
			$table->integer("ultimaLectura")->default(0)->unsigned();
			$table->integer("temperatura")->default(0)->unsigned();
			$table->integer("consumo")->default(0)->unsigned();
			$table->integer("tipo_ganado")->default(0)->unsigned();
			$table->dateTime('fecha')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sensores_silos');
	}
}
