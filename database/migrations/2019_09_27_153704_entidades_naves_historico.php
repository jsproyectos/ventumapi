<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntidadesNavesHistorico extends Migration
{
	public function up()
	{
		Schema::create('entidades_naves_historico', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp("fecha")->useCurrent()->comment("Fecha de última actualización");
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('luminosidad', 7, 2)->default(0);
			$table->decimal('etermico', 5, 2)->default(0)->comment("Estress térmico");
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_naves_historico');
	}
}