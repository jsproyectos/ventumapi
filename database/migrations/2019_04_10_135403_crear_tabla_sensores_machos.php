<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresMachos extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sensores_machos', function (Blueprint $table)
		{
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_animal")->unsigned();
			$table->foreign("id_animal")->references("id")->on("animales")->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp("fecha")->useCurrent(); //Fecha de colocación del sensor. Establece que el valor por defecto es la fecha actual
			$table->integer("montas")->default(0)->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sensores_machos');
	}
}
