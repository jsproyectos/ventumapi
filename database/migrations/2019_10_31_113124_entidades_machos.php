<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntidadesMachos extends Migration
{
	public function up()
	{
		Schema::create('entidades_machos', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime("fecha")->useCurrent()->comment("Fecha de última actualización");
			$table->integer('montas')->default(0)->unsigned();
			// Ya no es necesario estos datos, puesto que ahora los animales son entidades y no hace falta ninguna tabla de animales
			/*$table->integer("id_animal")->unsigned();
			$table->foreign("id_animal")->references("id")->on("animales")->onDelete('cascade')->onUpdate('cascade');
			$table->boolean("activo");*/
			$table->primary('id_entidad');
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_machos');
	}
}
