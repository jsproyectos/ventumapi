<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaUsersFincas extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_fincas', function (Blueprint $table)
		{
			$table->integer("id_usuario")->unsigned();
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_finca")->unsigned();
			$table->foreign("id_finca")->references("id")->on("fincas")->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users_fincas');
	}
}
