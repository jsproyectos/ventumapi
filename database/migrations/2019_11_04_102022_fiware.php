<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fiware extends Migration
{
	public function up()
	{
		Schema::create('fiware', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string("host", 50);
			$table->string("puerto", 5);
			$table->string("token");
			$table->string("service", 50);
			$table->string("subservice", 50);
			$table->string("ContentType", 25);
		});
	}

	public function down()
	{
		Schema::dropIfExists('fiware');
	}
}
