<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAnotaciones extends Migration
{
	public function up()
	{
		Schema::create('anotaciones', function (Blueprint $table)
		{
			$table->increments('id');
			$table->timeStamp("fecha")->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timeStamp("fechaBaja")->nullable();
			$table->unsignedTinyInteger("tipo_ref")->comment("Indica si se refiere a un sensor, entidad, u otro")->nullable();
			$table->unsignedInteger("id_referencia")->comment("Indica el id del sensor o entidad a la que referencia")->nullable();
			$table->unsignedInteger("tipo")->comment("Indica el tipo de anotación determinado")->nullable();
			$table->unsignedInteger("id_usuario")->comment("Indica el id de usuario que ha puesto la anotación");
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			$table->string("nota")->comment("Texto de la notificación")->nullable();
		});
	}

	public function down()
	{
		Schema::dropIfExists('anotaciones');
	}
}