<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LecturasEstacion extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lecturas_estacion', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('id_entidad')->unsigned();
			$table->foreign('id_entidad')->references('id')->on('entidades')->onDelete('cascade')->onUpdate('cascade')->nullable();
			$table->timeStamp('fecha')->nullable();
			$table->string('payload')->default('');
			$table->decimal('snr', 5, 2)->default(0)->nullable();
			$table->smallInteger('rssi')->default(0)->nullable();
			$table->decimal('bateria', 5, 2)->default(0);
			$table->unsignedTinyInteger('bp')->default(0);
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('presion', 7, 2)->default(0)->comment('Presion atomesférica en Pa');
			$table->decimal('lluvia', 7, 4)->default(0)->comment('Precipitaciones en mm/h');
			$table->decimal('viento_vel', 5, 2)->default(0)->comment('Velocidad del viento en Km/h');
			$table->unsignedTinyInteger('viento_dir')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('lecturas_estacion');
	}
}
