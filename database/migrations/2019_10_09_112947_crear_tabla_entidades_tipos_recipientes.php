<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesTiposRecipientes extends Migration
{
	public function up()
	{
		Schema::create('entidades_tipos_recipientes', function (Blueprint $table)
		{
			$table->increments("id");
			$table->string("nombre", 30);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_tipos_recipientes');
	}
}
