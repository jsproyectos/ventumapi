<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesRecipientesConsumoHistorico extends Migration
{
	public function up()
	{
		Schema::create('entidades_recipientes_consumo_historico', function (Blueprint $table)
		{
			$table->increments("id");
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp("fecha")->useCurrent()->comment("Fecha de inserción");
			$table->integer("acumulado")->unsigned()->comment("Consumo en kilos del acumulado")->default(0);
			$table->integer("pacumulado")->unsigned()->comment("Porcentaje de bajada del acumulado")->default(0);
			$table->integer("diario")->unsigned()->comment("Consumo en kilos diario")->default(0);
			$table->integer("pdiario")->unsigned()->comment("Porcentaje de bajada diario")->default(0);
			$table->integer("animales")->unsigned()->nullable()->default(null);
			$table->integer("id_ganado")->unsigned()->nullable();
			$table->foreign("id_ganado")->references("id")->on("animales_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_modelo")->unsigned()->nullable();
			$table->foreign("id_modelo")->references("id")->on("entidades_modelos_recipientes")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("capacidad")->unsigned()->comment("Capacidad máxima del recipiente, en kilos o en litros")->default(0);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_recipientes_consumo_historico');
	}
}
