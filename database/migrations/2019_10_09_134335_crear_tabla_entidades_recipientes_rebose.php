<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesRecipientesRebose extends Migration
{
	public function up()
	{
		Schema::create('entidades_recipientes_rebose', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp("fecha")->nullable()->comment("Fecha de adición");
			$table->timeStamp("ultimo")->nullable()->comment("Fecha de último rebose");
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_recipientes_rebose');
	}
}
