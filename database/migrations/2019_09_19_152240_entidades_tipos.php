<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntidadesTipos extends Migration
{
	public function up()
	{
		Schema::create('entidades_tipos', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string("nombre")->default("");
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_tipos');
	}
}