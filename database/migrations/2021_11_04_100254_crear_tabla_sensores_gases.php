<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresGases extends Migration
{
    public function up()
    {
		Schema::create('sensores_gases', function (Blueprint $table)
		{
			$table->integer('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime('fecha')->useCurrent();
			$table->decimal('co2', 7, 2)->default(0);
			$table->decimal('ch4', 5, 2)->default(0);
			$table->decimal('nh3', 5, 2)->default(0);
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('etermico', 7, 2)->default(0);
			$table->unsignedInteger('presion')->default(0)->comment('Presion atomesférica en Pa');
			$table->unsignedInteger('luz')->default(0)->comment('Luminosidad en lux');
		});

		Schema::create('lecturas_gases', function (Blueprint $table)
		{
			$table->integer('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('id_entidad')->unsigned();
			$table->foreign('id_entidad')->references('id')->on('entidades')->onDelete('cascade')->onUpdate('cascade')->nullable();
			$table->dateTime('fecha')->useCurrent();
			$table->decimal('snr', 5, 2)->default(0)->nullable();
			$table->smallInteger('rssi')->default(0)->nullable();
			$table->decimal('bateria', 5, 2)->default(0);
			$table->unsignedTinyInteger('bp')->default(0);
			$table->decimal('co2', 7, 2)->default(0);
			$table->decimal('ch4', 5, 2)->default(0);
			$table->decimal('nh3', 5, 2)->default(0);
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('etermico', 7, 2)->default(0);
			$table->unsignedInteger('presion')->default(0)->comment('Presion atomesférica en Pa');
			$table->unsignedInteger('luz')->default(0)->comment('Luminosidad en lux');
		});
	}

	public function down()
	{
		Schema::dropIfExists('sensores_gases');
		Schema::dropIfExists('lecturas_gases');
	}
}
