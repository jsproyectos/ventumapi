<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntidadesUsuarios extends Migration
{
	public function up()
	{
		Schema::create('entidades_usuarios', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDeletphpe('cascade')->onUpdate('cascade');
			$table->integer("id_usuario")->unsigned();
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime("fecha")->useCurrent()->comment("Fecha de asignación");
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_usuarios');
	}
}
