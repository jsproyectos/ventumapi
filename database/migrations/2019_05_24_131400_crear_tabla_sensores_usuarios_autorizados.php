<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresUsuariosAutorizados extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sensores_usuarios_autorizados', function (Blueprint $table)
		{
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDeletphpe('cascade')->onUpdate('cascade');
			$table->integer("id_usuario")->unsigned();
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			/*$table->integer("id_autorizado")->unsigned();
			$table->foreign("id_autorizado")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');*/
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sensores_usuarios_autorizados');
	}
}
