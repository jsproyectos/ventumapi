<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAlertasTipoSensor extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alertas_tipos_sensor', function (Blueprint $table)
		{
			$table->integer("id_tipo_sensor")->unsigned();
			$table->foreign("id_tipo_sensor")->references("id")->on("tipos_sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_alerta")->unsigned();
			$table->foreign("id_alerta")->references("id")->on("alertas_tipos")->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('alertas_tipos_sensor');
	}
}
