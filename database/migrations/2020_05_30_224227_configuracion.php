<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Configuracion extends Migration
{
	public function up()
	{
		Schema::create('configuracion', function (Blueprint $table)
		{
			$table->integer('id_usuario')->unsigned();
			$table->foreign('id_usuario')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->boolean('email_instantaneo')->comment('1 indica que se debe enviar un email cuando se envía la notificación');
			$table->integer('email_tiempo')->unsigned()->comment('Tiempo en minutos para enviar la alerta por email en caso de no ver la notificación');
		});
	}

	public function down()
	{
		Schema::dropIfExists('configuracion');
	}
}
