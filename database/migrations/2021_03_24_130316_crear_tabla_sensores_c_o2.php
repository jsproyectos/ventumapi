<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresCO2 extends Migration
{
	public function up()
	{
		Schema::create('sensores_co2', function (Blueprint $table)
		{
			$table->integer('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('etermico', 7, 2)->default(0);
			$table->decimal('co2', 7, 2)->default(0);
			$table->dateTime('fecha')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sensores_co2');
	}
}
