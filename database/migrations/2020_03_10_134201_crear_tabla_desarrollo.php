<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDesarrollo extends Migration
{
	public function up()
	{
		Schema::create('lecturas_desarrollo', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->string("payload")->default("");
			$table->timeStamp("fecha")->nullable();
			$table->integer("distancia")->unsigned()->default(0);
			$table->decimal('temperatura', 10, 5)->default(0);
			$table->decimal('humedad', 10, 5)->default(0);
			$table->decimal('bateria', 5, 2)->default(0)->comment("Voltaje de la batería");
		});
	}

	public function down()
	{
		Schema::dropIfExists('lecturas_desarrollo');
	}
}
