<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAnimales extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('animales', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string("crotal", 15); //El crotal del animal
			$table->string("nombre", 20)->nullable(); // Nombre a un animal
			$table->longText("descripcion")->nullable(); // Anotaciones sobre el animal
			$table->integer("edad")->unsigned()->nullable(); // Edad del animal
			$table->char('sexo', 1); // M o H (Macho o Hembra)
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('animales');
	}
}