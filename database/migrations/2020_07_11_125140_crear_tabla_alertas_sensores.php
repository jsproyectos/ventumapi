<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAlertasSensores extends Migration
{
	public function up()
	{
		Schema::create('alertas_sensores', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('id_alerta')->unsigned();
			$table->foreign('id_alerta')->references('id')->on('alertas_tipos')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('id_usuario_add')->unsigned()->comment('id_usuario de quien inserta o actualiza la alerta');
			$table->foreign('id_usuario_add')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('id_usuario')->unsigned()->comment('id_usuario de la alerta');
			$table->foreign('id_usuario')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->string('parametros');
			$table->boolean('activo')->default(0);
			$table->dateTime('ultima')->useCurrent();
		});
	}

	public function down()
	{
		Schema::dropIfExists('alertas_sensores');
	}
}
