<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaLecturasSilos extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lecturas_silos', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->string("payload", 24)->default("000000000000000000000000");
			$table->timeStamp("fecha")->useCurrent();
			$table->tinyInteger("bateria")->unsigned()->default(0);
			$table->integer("altura")->unsigned()->default(0);
			$table->decimal("porcentaje", 5, 2)->default(0);
			$table->integer("temperatura")->unsigned()->default(0);
			$table->integer("acelerometroX")->unsigned()->default(0);
			$table->integer("acelerometroY")->unsigned()->default(0);
			$table->integer("acelerometroZ")->unsigned()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('lecturas_silos');
	}
}
