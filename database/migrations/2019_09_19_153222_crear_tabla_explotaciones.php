<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaExplotaciones extends Migration
{
	public function up()
	{
		Schema::create('explotaciones', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string("nombre")->nullable();
			$table->string("codigo")->nullable();
			$table->longText("descripcion")->nullable();
			$table->decimal('longitud', 10, 7)->default(0);
			$table->decimal('latitud', 10, 7)->default(0);
		});
	}

	public function down()
	{
		Schema::dropIfExists('explotaciones');
	}
}
