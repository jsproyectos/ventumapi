<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Entidades extends Migration
{
	public function up()
	{
		Schema::create('entidades', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string("nombre")->nullable();
			$table->string("codigo")->nullable();
			$table->boolean("fake")->nullable()->default(null)->comment('Indica si la entidad es falsa o no');
			$table->longText("descripcion")->nullable();
			$table->decimal('longitud', 10, 7)->default(0);
			$table->decimal('latitud', 10, 7)->default(0);
			$table->integer("id_tipo")->unsigned();
			$table->foreign("id_tipo")->references("id")->on("entidades_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_explotacion")->unsigned();
			$table->foreign("id_explotacion")->references("id")->on("explotaciones")->onDelete('cascade')->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades');
	}
}
