<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesFiware extends Migration
{
	public function up()
	{
		Schema::create('entidades_fiware', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_fiware")->unsigned();
			$table->foreign("id_fiware")->references("id")->on("fiware")->onDelete('cascade')->onUpdate('cascade');
			$table->string("id_device_fiware")->comment("Identificación del dispositivo en la db FiWare del cliente");
			$table->dateTime("fechaAlta")->default(DB::raw('CURRENT_TIMESTAMP'))->comment("Fecha de alta");
			$table->dateTime("fechaBaja")->nullable()->default(null);
			$table->primary(['id_entidad', 'id_fiware', 'fechaAlta']);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_fiware');
	}
}
