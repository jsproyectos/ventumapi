<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaLecturasDesarrollo extends Migration
{
	public function up()
	{
		Schema::create('sensores_desarrollo', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp("fecha")->useCurrent();
			$table->integer("distancia")->unsigned()->default(0);
			$table->decimal('bateria', 5, 2)->default(0)->comment("Voltaje de la batería");
		});
	}

	public function down()
	{
		Schema::dropIfExists('sensores_desarrollo');
	}
}
