<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaLecturasSensoresCO2 extends Migration
{
	public function up()
	{
		Schema::create('lecturas_co2', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('id_entidad')->unsigned();
			$table->foreign('id_entidad')->references('id')->on('entidades')->onDelete('cascade')->onUpdate('cascade');
			$table->string('payload')->default('');
			$table->decimal('snr', 5, 2)->default(0);
			$table->smallInteger('rssi')->default(0);
			$table->decimal('bateria', 3, 2)->default(0);
			$table->tinyInteger('bp')->unsigned()->default(0);
			$table->timeStamp('fecha')->useCurrent()->nullable();
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('etermico', 7, 2)->default(0);
			$table->decimal('co2', 7, 2)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('lecturas_co2');
	}
}
