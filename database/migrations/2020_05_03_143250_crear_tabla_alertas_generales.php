<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAlertasGenerales extends Migration
{
	public function up()
	{
		Schema::create('alertas_generales', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer("id_alerta")->unsigned();
			$table->foreign("id_alerta")->references("id")->on("alertas_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_usuario")->unsigned();
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			$table->string("parametros");
			$table->boolean("activo");
			$table->dateTime('ultima')->useCurrent();
		});
	}

	public function down()
	{
		Schema::dropIfExists('alertas_generales');
	}
}
