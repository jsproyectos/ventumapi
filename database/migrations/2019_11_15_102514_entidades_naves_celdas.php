<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntidadesNavesCeldas extends Migration
{
	public function up()
	{
		Schema::create('entidades_naves_celdas', function (Blueprint $table)
		{
			$table->integer("id_nave")->unsigned();
			$table->foreign("id_nave")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_celda")->unsigned();
			$table->foreign("id_celda")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime("fechaAlta")->default(DB::raw('CURRENT_TIMESTAMP'))->comment("Fecha de alta");
			$table->dateTime("fechaBaja")->nullable()->default(null);
			$table->primary(['id_nave', 'id_celda', 'fechaAlta']);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_naves_celdas');
	}
}
