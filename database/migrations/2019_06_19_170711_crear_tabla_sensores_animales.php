<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresAnimales extends Migration
{
	public function up()
	{
		Schema::create('sensores_animales', function (Blueprint $table)
		{
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_animal")->unsigned();
			$table->foreign("id_animal")->references("id")->on("animales")->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp("fecha")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sensores_animales');
	}
}
