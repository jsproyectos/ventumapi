<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTiposAlertas extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alertas_tipos', function (Blueprint $table)
		{
			$table->integer("id")->unsigned();
			$table->primary('id');
			$table->string("codigo");
			$table->string("nombre");
			$table->string("descripcion");
			$table->string("texto");
			$table->string("recomendado", 20);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('alertas_tipos');
	}
}