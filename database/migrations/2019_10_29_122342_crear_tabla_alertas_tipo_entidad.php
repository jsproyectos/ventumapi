<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAlertasTipoEntidad extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alertas_tipos_entidad', function (Blueprint $table)
		{
			$table->integer("id_tipo_entidad")->unsigned();
			$table->foreign("id_tipo_entidad")->references("id")->on("entidades_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_alerta")->unsigned();
			$table->foreign("id_alerta")->references("id")->on("alertas_tipos")->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('alertas_tipos_entidad');
	}
}
