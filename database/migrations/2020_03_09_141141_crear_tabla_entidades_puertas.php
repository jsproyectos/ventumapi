<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesPuertas extends Migration
{
	public function up()
	{
		Schema::create('entidades_puertas', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime("fecha")->useCurrent()->comment("Fecha de última actualización");
			$table->tinyInteger('estado')->comment("1 Abierta y 0 Cerrada")->default(0);
			$table->primary(['id_entidad']);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_puertas');
	}
}
