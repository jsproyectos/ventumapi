<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesSensores extends Migration
{
	public function up()
	{
		Schema::create('entidades_sensores', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime("fechaAlta")->default(DB::raw('CURRENT_TIMESTAMP'))->comment("Fecha de alta");
			$table->dateTime("fechaBaja")->nullable()->default(null);
			$table->primary(['id_entidad', 'id_sensor', 'fechaAlta']);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_sensores');
	}
}
