<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresHTemperaturaYHumedad extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lecturasTemperaturaHumedad', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->string("payload")->default("");
			$table->integer("bateria")->default(0);
			$table->timeStamp("fecha")->nullable();
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('lecturasTemperaturaHumedad');
	}
}
