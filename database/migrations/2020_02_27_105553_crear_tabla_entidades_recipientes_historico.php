<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesRecipientesHistorico extends Migration
{
	public function up()
	{
		Schema::create('entidades_recipientes_historico', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_ganado")->unsigned()->nullable();
			$table->foreign("id_ganado")->references("id")->on("animales_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_modelo")->unsigned();
			$table->foreign("id_modelo")->references("id")->on("entidades_modelos_recipientes")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("animales")->unsigned()->nullable();
			$table->integer("capacidad")->unsigned()->comment("Capacidad máxima del recipiente, en kilos o en litros");
			$table->timeStamp("fecha")->useCurrent()->comment("Fecha de inserción");
			$table->integer("id_usuario")->unsigned()->comment("Usuario que realizó el cambio");
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_recipientes_historico');
	}
}
