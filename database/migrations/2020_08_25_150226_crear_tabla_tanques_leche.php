<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTanquesLeche extends Migration
{
	public function up()
	{
		Schema::create('entidades_tanques_leche', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp("fecha_ordeno")->useCurrent()->comment("Fecha del último ordeño");
			$table->integer("celulas")->unsigned()->comment("Células somáticas")->default(0);
			$table->integer("ordenados")->unsigned()->comment("Total de animales del último ordeño")->default(0);
			$table->integer("litros")->unsigned()->comment("Litros ordeñados en el último ordeño")->default(0);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_tanques_leche');
	}
}
