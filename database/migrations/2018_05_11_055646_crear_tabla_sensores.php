<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensores extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sensores', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string("nombre")->nullable();
			$table->string("dispositivo"); //EUI en lora y lo otro en Sigfox
			$table->boolean("fake")->nullable()->default(null)->comment('Indica si el sensor es falso o no');
			$table->boolean('seguimiento')->default(0)->comment('1 está en seguimiento, 0 no');
			$table->longText("descripcion")->nullable();
			$table->decimal('latitud', 10, 7)->default(0);
			$table->decimal('longitud', 10, 7)->default(0);
			$table->decimal('bateria', 5, 4)->default(3.7);
			$table->unsignedTinyInteger('bp')->default(100)
			$table->decimal('rssi', 10, 5)->default(0);
			$table->decimal('snr', 5, 2)->default(0);
			$table->integer("id_tipo")->unsigned()->nullable();
			$table->foreign("id_tipo")->references("id")->on("tipos_sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_fab")->unsigned()->nullable();
			$table->foreign("id_fab")->references("id")->on("sensores_fabricantes")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_emision")->unsigned()->nullable();
			$table->foreign("id_emision")->references("id")->on("sensores_tipos_emision")->onDelete('cascade')->onUpdate('cascade');
			$table->string("downlink", 16); //Cadena de downlink a enviar
			$table->integer("tiempo")->unsigned()->nullable()->comment("Tiempo en minutos por cada emisión");
			$table->dateTime("fechaAlta")->default(DB::raw('CURRENT_TIMESTAMP'))->comment("Fecha de alta");
			$table->dateTime("fechaBaja")->nullable()->default(null)->comment("Fecha de baja");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sensores');
	}
}
