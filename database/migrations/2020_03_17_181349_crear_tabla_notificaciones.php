<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaNotificaciones extends Migration
{
	public function up()
	{
		Schema::create('notificaciones', function (Blueprint $table)
		{
			$table->increments('id');
			$table->timeStamp("fecha")->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->unsignedTinyInteger("tipo")->comment("Indica si se refiere a un sensor, entidad, u otro")->nullable();
			$table->unsignedInteger("id_referencia")->comment("Indica el id del sensor o entidad a la que referencia")->nullable();
			$table->unsignedInteger("id_alerta")->comment("Indica el tipo de alerta de sensor o entidad a la que referencia")->nullable();
			$table->unsignedInteger("id_usuario")->comment("Indica el id de usuario al que referencia");
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			$table->string("email")->comment("Email al que va dirigido la notificación, en caso de no ser usuario")->nullable();
			$table->string("parametros")->comment("Valor de la notificación")->nullable();
			$table->unsignedTinyInteger("leida")->comment("1 Leida, 0 no leída en la aplicación")->default(0);
		});
	}

	public function down()
	{
		Schema::dropIfExists('notificaciones');
	}
}
