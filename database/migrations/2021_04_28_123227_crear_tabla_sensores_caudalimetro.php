<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSensoresCaudalimetro extends Migration
{	
	public function up()
	{
		Schema::create('sensores_caudalimetro', function (Blueprint $table)
		{
			$table->unsignedInteger('id_sensor')->unsigned();
			$table->foreign('id_sensor')->references('id')->on('sensores')->onDelete('cascade')->onUpdate('cascade');
			$table->timeStamp('fecha')->useCurrent()->nullable();
			$table->unsignedInteger('mililitros')->default(0);
			$table->decimal('litros', 7, 2)->default(0);
			$table->decimal('caudal', 7, 2)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sensores_caudalimetro');
	}
}
