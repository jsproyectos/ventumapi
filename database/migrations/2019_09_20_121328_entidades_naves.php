<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntidadesNaves extends Migration
{
	public function up()
	{
		Schema::create('entidades_naves', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime("fecha")->useCurrent()->comment("Fecha de última actualización");
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->decimal('luminosidad', 7, 2)->default(0);
			$table->decimal('etermico', 5, 2)->default(0)->comment("Estress térmico");
			$table->string("modelo")->comment("Ruta donde se encuentra el modelo de la nave");;
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_naves');
	}
}
