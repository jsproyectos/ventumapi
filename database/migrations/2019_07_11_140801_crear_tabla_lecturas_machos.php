<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaLecturasMachos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturas_machos', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer("id_sensor")->unsigned();
            $table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
            $table->timeStamp("fecha")->useCurrent(); //Establece que el valor por defecto es la fecha actual
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturas_machos');
    }
}
