
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoriasVisuales extends Migration
{
	public function up()
	{
		Schema::create('categorias_visuales_usuario', function (Blueprint $table)
		{
			$table->integer("id_usuario")->unsigned();
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedTinyInteger("silos")->default(0);
			$table->unsignedTinyInteger("naves")->default(0);
			$table->unsignedTinyInteger("neveras")->default(0);
			$table->unsignedTinyInteger("localizacion")->default(0);
			$table->unsignedTinyInteger("puertas")->default(0);
			$table->unsignedTinyInteger("machos")->default(0);
			$table->unsignedTinyInteger("depositos")->default(0);
			$table->unsignedTinyInteger("estacion")->default(0);
			$table->unsignedTinyInteger("leche")->default(0);
			$table->unsignedTinyInteger("estadisticas")->default(0);
			$table->primary('id_usuario');
		});
	}

	public function down()
	{
		Schema::dropIfExists('categorias_visuales_usuario');
	}
}
