<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAlertas extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alertas', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer("id_sensor")->unsigned();
			$table->foreign("id_sensor")->references("id")->on("sensores")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_alerta")->unsigned();
			$table->foreign("id_alerta")->references("id")->on("alertas_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->string("email");
			$table->integer("id_usuario")->unsigned();
			$table->foreign("id_usuario")->references("id")->on("users")->onDelete('cascade')->onUpdate('cascade');
			$table->string("nombre");
			$table->string("parametros");
			$table->boolean("activo");
			$table->dateTime('ultima')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('alertas');
	}
}
