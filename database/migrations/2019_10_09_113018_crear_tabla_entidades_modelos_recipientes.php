<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesModelosRecipientes extends Migration
{
	public function up()
	{
		Schema::create('entidades_modelos_recipientes', function (Blueprint $table)
		{
			$table->increments("id");
			$table->integer("id_tipo")->unsigned();
			$table->foreign("id_tipo")->references("id")->on("entidades_tipos_recipientes")->onDelete('cascade')->onUpdate('cascade');
			$table->string("nombre", 30)->default("")->comment('Nombre del recipiente');
			$table->string("fabricante", 30)->default("")->comment('Nombre del fabricante del tipo de recipiente');
			$table->integer("aconosup")->unsigned()->default(0)->comment('Altura cono superior');
			$table->integer("acilindro")->unsigned()->default(0)->comment('Altura cilindro central o Altura del tanque de leche');
			$table->integer("aconoinf")->unsigned()->default(0)->comment('Altura del cono inferior');
			$table->integer("rconosup")->unsigned()->default(0)->comment('Radio del cono superior');
			$table->integer("rcilindro")->unsigned()->default(0)->comment('Radio del cilindro central o radio del tanque de leche');
			$table->integer("rconoinf")->unsigned()->default(0)->comment('Radio del cinlindro inferior');
			$table->integer("volumen")->unsigned()->default(0)->comment('Volumen total del recipiente');
			$table->integer("capacidad")->unsigned()->default(0)->comment('Volumen total del recipiente normalizado');
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_modelos_recipientes');
	}
}
