<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesRecipientes extends Migration
{
	public function up()
	{
		Schema::create('entidades_recipientes', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_modelo")->unsigned();
			$table->foreign("id_modelo")->references("id")->on("entidades_modelos_recipientes")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("id_ganado")->unsigned()->nullable();
			$table->foreign("id_ganado")->references("id")->on("animales_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("animales")->unsigned()->nullable();
			$table->timeStamp("fecha")->useCurrent()->comment("Fecha de última actualización");
			$table->timeStamp("fecha_reinicio")->useCurrent()->comment("Fecha en la que se reinicia el contador de consumo acumulado");
			$table->integer("capacidad")->unsigned()->comment("Capacidad máxima del recipiente, en kilos o en litros");
			$table->decimal('porcentaje', 5, 2)->default(0)->comment("Porcentaje actual de llenado");
			$table->decimal('temperatura', 5, 2)->default(0)->comment("Temperatura actual del recipiente");
			$table->decimal('consumo', 5, 2)->default(0)->comment("Consumo acumulado diario del recipiente");
			$table->decimal('consumoD', 5, 2)->default(0)->comment("Consumo diario del recipiente");
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_recipientes');
	}
}
