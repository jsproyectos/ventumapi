<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesAnimales extends Migration
{
	public function up()
	{
		Schema::create('entidades_animales', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->integer("edad")->unsigned()->nullable(); // Edad del animal
			$table->char('sexo', 1); // M o H (Macho o Hembra)
			$table->integer("id_tipo")->unsigned();
			$table->foreign("id_tipo")->references("id")->on("animales_tipos")->onDelete('cascade')->onUpdate('cascade');
			$table->primary('id_entidad');
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_animales');
	}
}