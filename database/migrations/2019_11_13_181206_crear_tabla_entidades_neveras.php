<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEntidadesNeveras extends Migration
{
	public function up()
	{
		Schema::create('entidades_neveras', function (Blueprint $table)
		{
			$table->integer("id_entidad")->unsigned();
			$table->foreign("id_entidad")->references("id")->on("entidades")->onDelete('cascade')->onUpdate('cascade');
			$table->dateTime("fecha")->useCurrent()->comment("Fecha de última actualización");
			$table->decimal('temperatura', 5, 2)->default(0);
			$table->decimal('humedad', 5, 2)->default(0);
			$table->tinyInteger('estado')->comment("1 Abierta y 0 Cerrada")->default(0);
			$table->decimal('min', 5, 2)->comment("Umbral mínimo de temperatura")->default(2);
			$table->decimal('max', 5, 2)->comment("Umbral máximo de temperatura")->default(8);
		});
	}

	public function down()
	{
		Schema::dropIfExists('entidades_naves');
	}
}
