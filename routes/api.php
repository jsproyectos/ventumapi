<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the 'api' middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['middleware' => ['cors']], function ()
{
	// Logear en la aplicación
	Route::post('usuario/login', 'UserController@authenticate');
	// Generar una nueva contraseña
	Route::post('configuracion/recuperarContrasena', 'ConfiguracionController@recuperarContrasena');
});

Route::group(['middleware' => ['cors', 'jwt.verify']], function ()
{
	// Obtiene el usuario autenticado
	Route::post('usuario/valido', 'UserController@getAuthenticatedUser');
	// Refresca el token, para cuando inicias sesión con un token ya almacenado
	Route::post('usuario/refrescar', 'UserController@refreshToken');
	// Cierra la sesión del usuario que está logeado
	Route::post('usuario/logout', 'UserController@logout');
	// Obtiene el número de notificaciones del usuario
	Route::post('usuario/notificaciones', 'UserController@obtenerNumeroNotificaciones');

	// Entidades
	// Route::post('entidad/nueva', 'EntidadesController@nuevaEntidad');
	// Route::post('entidad/form', 'EntidadesController@obtenerEspecialidadFormulario');
	// Route::post('entidad/editar', 'EntidadesController@editar');

	// Alertas
	/*Route::post('configuracion/alertas/obtener/tipos', 'AlertasController@obtenerTiposAlarmaPorSensorAJAX');
	Route::post('configuracion/alertas/obtener', 'AlertasController@obtenerTodasLasAlertasAJAX');*/

	// Obtiene las alertas de la entidad y usuario indicados
	Route::post('entidad/alertas/obtener/{idEntidad}/{idUsuario?}', 'AlertasController@alertasEntidadObtener');
	// Obtiene las alertas generales de un usuario
	Route::post('entidad/alertas/g/obtener', 'AlertasController@alertasEntidadObtenerGeneral'); 
	// Agrega o modifica una alerta de entidad y usuario indicados
	Route::post('entidad/alertas/actualizar/{idEntidad}/{idUsuario?}', 'AlertasController@alertasEntidadActualizar');
	// Agrega una alerta de entidad y usuario indicados
	Route::post('entidad/alertas/g/actualizar', 'AlertasController@alertasEntidadActualizarGeneral');

	// -------------------------	Anotaciones
	// Obtener las anotaciones de una entidad
	Route::post('anotaciones/{tipo}/{id}', "ConfiguracionController@anotacionesObtener");
	// Inserta una anotación de una entidad
	Route::post('anotaciones/insertar/{tipo}/{id}', "ConfiguracionController@anotacionesInsertar");

	// Obtiene las alertas del sensor y usuario indicados
	Route::post('sensores/alertas/obtener/{idSensor}/{idUsuario?}', 'AlertasController@alertasSensorObtener');
	// Obtiene las alerta generales de un usuario
	// Route::post('sensores/alertas/obtener/g', 'AlertasController@alertasSensorObtenerGeneral');
	// Agrega o modifica una alerta de un sensor y usuario indicados
	Route::post('sensores/alertas/actualizar/{idSensor}/{idUsuario?}', 'AlertasController@alertasSensorActualizar');
	// Agrega o modifica las alertas generales de un usuario
	// Route::post('sensores/alertas/actualizar/g', 'AlertasController@alertasSensorActualizarGeneral');

	/*Route::post('configuracion/alertas/agregar', 'AlertasController@agregarAlerta');
	Route::post('configuracion/alertas/editar', 'AlertasController@editarAlerta');
	Route::post('configuracion/alertas/activar', 'AlertasController@activarAlerta');
	Route::post('configuracion/alertas/borrar', 'AlertasController@borrarAlerta');*/

	// Configuración
	// Cambia la contraseña de usuario
	Route::post('configuracion/perfil/cambiarContrasena', 'ConfiguracionController@cambiarContrasena');
	// Obtiene la configuración del usuario
	Route::post('configuracion/configuracion', 'ConfiguracionController@obtenerConfiguracion');
	Route::post('configuracion/configuracion/actualizar', 'ConfiguracionController@actualizarConfiguracion');
	Route::post('configuracion/perfil', 'ConfiguracionController@obtenerPerfil');
	Route::post('configuracion/perfil/actualizar', 'ConfiguracionController@actualizarPerfil');

	// Recipientes: Silos, depósitos de agua y tanques de leche ------------------------
	// Obtiene el estado actual de los silos
	Route::post('silos', 'EntidadesSilosController@obtenerResumen')->name('silos');
	// Obtiene el estado actual de los depósitos de agua
	Route::post('depositos', 'EntidadesDepositosAguaController@obtenerResumen')->name('depositos');
	// Obtiene el estado actual de las tolvas
	Route::post('tolvas', 'EntidadesTolvasController@obtenerResumen')->name('tolvas');
	// Obtiene la información específica de un recipiente (silo o depósito de agua)
	Route::post('recipientes/{id}', 'EntidadesRecipientesController@obtenerDetalles');
	// Obtiene el histórico de un recipiente (silo o depósito de agua)
	Route::post('recipientes/{id}/{fechaInicio}/{fechaFin}/{factor?}', 'EntidadesRecipientesController@obtenerHistorico');
	// Reinicia el contador de consumo de un recipiente
	Route::post('recipientes/reiniciar/{id}', 'EntidadesRecipientesController@reiniciarAcumulado');

	// Naves ------------------------------------------------------------------------
	// Obtiene el estado actual de las naves
	Route::post('naves', 'EntidadesNavesController@obtenerResumen')->name('naves');
	// Obtiene la información específica de una nave y sus celdas asociadas
	Route::post('naves/{id}', 'EntidadesNavesController@obtenerDetalles');
	// Obtiene el histórico de una nave
	Route::post('naves/{id}/{fechaInicio}/{fechaFin}', 'EntidadesNavesController@obtenerHistorico');
	// Obtiene el histórico de una celda
	Route::post('naves/celda/{id}/{fechaInicio}/{fechaFin}', 'EntidadesNavesController@obtenerHistoricoCelda');
	// Obtiene la información de la nave y sus celdas para el parámetro indicado (temperatura, humedad o luminosidad)
	Route::post('naves/global/{parametro}/{id}/{fechaInicio}/{fechaFin}', 'EntidadesNavesController@obtenerHistoricoGlobal');
	// Obtiene los mínimos, máximos y media de una nave entre dos vechas
	Route::post('naves/resumen/{id}/{fechaInicio}/{fechaFin}', 'EntidadesNavesController@obtenerMinMaxMed');

	//Neveras -----------------------------------------------------------------------
	// Obtiene el estado actual de las neveras
	Route::post('neveras', 'EntidadesNeverasController@obtenerResumen')->name('neveras');
	// Obtiene la información específica de una nevera
	Route::post('neveras/{id}', 'EntidadesNeverasController@obtenerDetalles');
	// Obtiene el histórico de una nevera
	Route::post('neveras/{id}/{fechaInicio}/{fechaFin}', 'EntidadesNeverasController@obtenerHistorico');

	// Machos -----------------------------------------------------------------------
	// Obtiene la actividad reciente de todos los machos
	Route::post('machos/actividad', 'EntidadesMachosController@obtenerActividadReciente');
	// Obtener el resumen y actividad reciente por machos
	Route::post('machos', 'EntidadesMachosController@obtenerResumen')->name('machos');;
	// Obtener la información detallada por cada macho
	Route::post('machos/{id}', 'EntidadesMachosController@obtenerDetalles');
	// Obtiene el histórico de montas por cada macho
	Route::post('machos/{id}/{fechaInicio}/{fechaFin}', 'EntidadesMachosController@obtenerHistorico');

	// Puertas -----------------------------------------------------------------------
	// Obtiene el estado actual de las puertas
	Route::post('puertas', 'EntidadesPuertasController@obtenerResumen')->name('puertas');
	// Obtiene la información específica de una puerta
	Route::post('puertas/{id}', 'EntidadesPuertasController@obtenerDetalles');
	// Obtiene el histórico de una puerta
	Route::post('puertas/{id}/{fechaInicio}/{fechaFin}', 'EntidadesPuertasController@obtenerHistorico');

	// Animales -----------------------------------------------------------------------
	// Obtiene las explotaciones así como el número de animales con gps
	Route::post('animales', 'EntidadesAnimalesController@obtenerResumen')->name('animales');
	// Obtiene los animales y la posición
	Route::post('animales/{idExplotacion}', 'EntidadesAnimalesController@obtenerAnimalesPorExplotacion');
	// Obtiene el histórico de una puerta
	Route::post('animal/{id}/{fechaInicio}/{fechaFin}', 'EntidadesAnimalesController@obtenerHistorico');

	// Obtiene el estado actual de los tanques de leche
	Route::post('tanques_leche', 'EntidadesTanquesLecheController@obtenerResumen')->name('tanques_leche');
	// Obtiene la información específica de un tanque de leche
	Route::post('tanques_leche/{id}', 'EntidadesTanquesLecheController@obtenerDetalles');
	// Obtiene el histórico de un tanque de leche
	Route::post('tanques_leche/{id}/{fechaInicio}/{fechaFin}', 'EntidadesTanquesLecheController@obtenerHistorico');

	// ##### Sensores ###############################################################

	// Obtener todos los sensores
	Route::post('sensores', 'SensoresController@obtenerResumen');
	Route::post('sensores/editar', 'SensoresController@editar');

	// Sensores de llenado de silos
	// Obtiene la información actualizada referente a un sensor de llenado de silos
	Route::post('sensores/llenado_silos/{id}', 'SensoresNivelLlenadoController@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/llenado_silos/{id}/{fechaInicio}/{fechaFin}/{factor?}', 'SensoresNivelLlenadoController@obtenerHistorico');
	// Inserta un valor ficticio en el sensor de llenado de silos. Usado para pruebas o correcciones
	Route::post('sensores/llenado_silos/fake/{id}', 'SensoresNivelLlenadoController@insertarValorFalso');

	// Sensores de temperatura
	// Obtiene la información actualizada referente a un sensor de temperatura, con o sin humedad y luminosidad
	Route::post('sensores/temperatura/{id}', 'SensoresTempHumController@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/temperatura/{id}/{fechaInicio}/{fechaFin}', 'SensoresTempHumController@obtenerHistorico');
	// Inserta un valor ficticio simulando ser un sensor de temperatura y humedad. Usado para pruebas o correcciones
	Route::post('sensores/temperatura/fake/{id}', 'SensoresTempHumController@insertarValorFalso');

	// Sensores de CO2
	// Obtiene la información actualizada referente a un sensor de CO2
	Route::post('sensores/co2/{id}', 'SensoresCO2Controller@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/co2/{id}/{fechaInicio}/{fechaFin}', 'SensoresCO2Controller@obtenerHistorico');
	// Inserta un valor ficticio simulando ser un sensor de temperatura y humedad. Usado para pruebas o correcciones
	Route::post('sensores/co2/fake/{id}', 'SensoresCO2Controller@insertarValorFalso');

	// Sensores de Gases
	// Obtiene la información actualizada referente a un sensor de CO2
	Route::post('sensores/gases/{id}', 'SensoresGasesController@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/gases/{id}/{fechaInicio}/{fechaFin}', 'SensoresGasesController@obtenerHistorico');
	// Inserta un valor ficticio simulando ser un sensor de temperatura y humedad. Usado para pruebas o correcciones
	Route::post('sensores/gases/fake/{id}', 'SensoresGasesController@insertarValorFalso');
	
	// Obtener sensores de puertas
	// Obtiene la información actualizada referente a un sensor de puertas
	Route::post('sensores/puertas/{id}', 'SensoresPuertasController@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/puertas/{id}/{fechaInicio}/{fechaFin}', 'SensoresPuertasController@obtenerHistorico');
	// Inserta un valor ficticio simulando ser un sensor de puertas. Usado para pruebas o correcciones
	Route::post('sensores/puertas/fake/{id}', 'SensoresPuertasController@insertarValorFalso');

	// Obtener sensores de machos
	// Obtiene la información actualizada referente a un sensor de machos
	Route::post('sensores/machos/{id}', 'SensoresMachosController@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/machos/{id}/{fechaInicio}/{fechaFin}', 'SensoresMachosController@obtenerHistorico');

	// Sensor de estación meteorológica de Libelium
	// Obtiene la información actualizada referente a un sensor 'Estación meteorológica'
	Route::post('sensores/estacion/{id}', 'SensoresEstacionMeteorologicaController@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/estacion/{id}/{fechaInicio}/{fechaFin}', 'SensoresEstacionMeteorologicaController@obtenerHistorico');

	// Caudalímetro
	// Obtiene la información actualizada referente a un sensor de CO2
	Route::post('sensores/caudalimetro/{id}', 'SensoresCaudalimetroController@obtenerDetalles');
	// Obtiene el histórico del sensor
	Route::post('sensores/caudalimetro/{id}/{fechaInicio}/{fechaFin}', 'SensoresCaudalimetroController@obtenerHistorico');
	// Inserta un valor ficticio simulando ser un sensor de temperatura y humedad. Usado para pruebas o correcciones
	Route::post('sensores/caudalimetro/fake/{id}', 'SensoresCaudalimetroController@insertarValorFalso');

	// Mapa -------------------------------------------------------------------------
	// Obtiene las fincas y las entidades de cada usuario
	Route::post('mapa/obtener/menu', 'MapaController@obtenerMenu');
	Route::post('mapa/obtener/entidades', 'MapaController@obtenerEntidades');

	// -------------------------	Notificaciones
	// Obtiene las notificaciones del usuario logeado en el token. Si lleva parámetro id_tipo_entidad se mostrarán solo las de ese tipo de entidad.
	Route::post('notificaciones', 'NotificacionesController@obtenerNotificaciones');
	Route::post('notificaciones/{pagina}', 'NotificacionesController@obtenerNotificacionesEntidadPagina')->where('pagina', '[0-9]+');
	Route::post('notificaciones/e/{entidad}/{pagina}', 'NotificacionesController@obtenerNotificacionesPorEntidadPagina');
	Route::post('notificaciones/leidas', 'NotificacionesController@marcarLeidas');
	Route::post('notificaciones/noLeidas', 'NotificacionesController@marcarLeidas');
	Route::post('notificaciones/leidas/borradas', 'NotificacionesController@marcarLeidasYBorradas');
	Route::post('notificaciones/entidad/leer/{id}', 'NotificacionesController@leerNotificacion');
	Route::post('notificaciones/entidad/noleer/{id}', 'NotificacionesController@marcarNotificacionNoLeida');
	Route::post('notificaciones/entidad/borrar/{id}', 'NotificacionesController@borrarNotificacion');
	Route::post('notificaciones/s/leer', 'NotificacionesController@marcarSeleccionadasLeidas');
	Route::post('notificaciones/s/noleer', 'NotificacionesController@marcarSeleccionadasNoLeidas');
	Route::post('notificaciones/s/borrar', 'NotificacionesController@marcarSeleccionadasBorrar');
});

// Rutas para insertar información

// -------------------------	GPRS
// Silos TST
Route::post('gprs/llenado/tst/{device}/{time}/{snr}/{data}', 'ComunicacionGPRSController@registrarLlenadoTST');
// Varilla TST
Route::post('gprs/temperatura/tst/{time}/{data}', 'ComunicacionGPRSController@registrarTemperaturaTSTVarilla');
// Tracker ventum
Route::post('gprs/tracker/ventum/{device}/{lat}/{lon}/{sat?}/{bateria?}/{bp?}', 'ComunicacionGPRSController@registrarGpsVentum');

// -------------------------	LoRa

Route::post('lora/pruebas', 'ComunicacionLoraController@registrarPruebas');
Route::post('lora/tracker', 'ComunicacionLoraController@registrarTracker');

Route::post('lora/gases/libelium', 'ComunicacionLoraController@registrarGasesLibelium');
Route::post('lora/estacion/ventum', 'ComunicacionLoraController@registrarEstacionVentum');
Route::post('lora/silos/ventum', 'ComunicacionLoraController@registrarSilosVentum');

// -------------------------	Sigfox
// Silos TST
Route::post('sigfox/llenado/tst/{device}/{time}/{data}/{ack}', 'ComunicacionSigfoxController@registrarLlenadoTST');
// Silos Ventum
Route::post('sigfox/llenado/ventum/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarLlenadoVentum');
// GPS TST
Route::post('sigfox/gps/tst/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarGpsTST');
// Temperatura y humedad TST
Route::post('sigfox/temperatura/tst/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarTemperaturaTST');
// Temperatura Vetum
Route::post('sigfox/temperatura/ventum/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarTemperaturaVentum');
// Temperatura y Humedad Ventum
Route::post('sigfox/temperatura_humedad/ventum/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarTemperaturaYHumedadVentum');
// Temperatura y Humedad con Batería Ventum
Route::post('sigfox/temperatura_humedad_bateria/ventum/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarTHBVentum');
// Temperatura, humedad y CO2 Ventum
Route::post('sigfox/co2/ventum/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarCO2Ventum');
// Puertas IOTA
Route::post('sigfox/puertas/iota/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarPuertasIOTA');
// Temperatura IOTA
Route::post('sigfox/temperatura/iota/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarTemperaturaIOTA');
// Estación Meteorológica Libelium
Route::post('sigfox/estacion/libelium/{device}/{time}/{data}', 'ComunicacionSigfoxController@registrarEstacionLibelium');

// -------------------------	Wifi

// Ultrasonidos
Route::post('wifi/silos/{device}/{data}/{bateria?}/{rssi?}/{intentos?}', 'ComunicacionWifiController@registrarUltrasonidos');

// Ultrasonidos con temperatura
Route::post('wifi/silos_temp/{device}/{distancia}/{temperatura}/{bateria?}/{rssi?}/{intentos?}', 'ComunicacionWifiController@registrarUltrasonidosTemperatura');

// Temperatura y humedad Ventum
Route::post('wifi/temperatura/{device}/{temperatura}/{humedad}/{rssi?}/{intentos?}', 'ComunicacionWifiController@registrarTemperaturaVentum');

// Temperatura, humedad y CO2 Ventum
Route::post('wifi/co2/ventum/{device}/{temperatura}/{humedad}/{co2}/{rssi?}/{intentos?}/{calibrar?}', 'ComunicacionWifiController@registrarCO2Ventum');

// Caudalímetro
Route::post('wifi/caudalimetro/ventum/{device}/{ml}/{caudal}/{rssi?}/{intentos?}', 'ComunicacionWifiController@registrarCaudalimetroVentum');

// Ultrasonidos
// Route::post('d/ultrasonidos', 'ComunicacionWifiController@registrarUltrasonidos');

// Machos
Route::post('d/machos/tst/{device}/{snr}/{rssi}/{bateria}', 'ComunicacionWifiController@registrarMachosTST');

// -------------------------	Nanolike

Route::post('nanolike/silos', 'ComunicacionWifiController@registrarNanolike');

// -------------------------	Desarrollo
Route::get('d/ultrasonidos/resumen', 'PruebasController@mostrarUltrasonidos');
Route::post('d/obtener/ultrasonidos', 'PruebasController@obtenerResumenUltrasonidos');
Route::post('d/antena', 'PruebasController@guardarTagsAntenaDipole');

// -------------------------	Cron
// Alertas de apertura de puerta. Uso este controlador porque no requiere autenticación
Route::post('alertas/periodicas', 'AlertasPeriodicasController@PuertasEstadosProlongadosURL');
// Actualiza los consumos de todos los recipientes
Route::post('alertas/consumos', 'AlertasPeriodicasController@actualizarConsumosURL');
// Activa el bot del llenado automático de los sensores de silos
Route::post('alertas/silos', 'AlertasPeriodicasController@activarBotSensoresSilos');
// Alertas IHT
// Route::post('alertas/periodicas/naves/iht', 'AlertasPeriodicasController@AlertaNavesIHTURL');
// Alertas eMail
Route::post('alertas/email', 'AlertasPeriodicasController@enviarEmailNoLeidasURL');

// Exporta la BD para importar en local
Route::post('exportar_db', 'ConfiguracionController@exportarDB');
