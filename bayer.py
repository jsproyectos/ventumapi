#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys; print(sys.path)
import paho.mqtt.client as mqtt #import the client1
import time
from datetime import datetime
import binascii
import requests 

application 		= 	'VENTUM_BAYER'
broker_host			=	'ventum-silos.mqtt.tst-sistemas.es'
broker_port 		= 	1947
broker_user			= 	'ventum-silos'
broker_pass 		= 	'ventum-silos1'
broker_id			= 	'bayer_silos_ventum'
broker_clean_sess 	= 	False
broker_keepalive 	= 	60
qos 				= 	1
topic_datos			=	'bayer/temperatura/pub'

def obtenerFecha():
	"Función que devuelve la fecha actual"
	return datetime.now().strftime("%d-%m-%Y %H-%M-%S")

def obtenerTimeStamp():
	"Función que devuelve el timestamp actual"
	return int(datetime.timestamp(datetime.now()))

def escribirFichero(texto):
	"Función que escribe el texto en un fichero"
	f = open("bayer_registro.log", "a")
	f.write(texto)
	f.close()

def on_message(client, userdata, message):
	"Función de prueba para escribir en disco y mostrar por pantalla las tramas"
	try:
		escribirFichero(obtenerFecha() + " -- Nueva informacion\n")
		data = str(message.payload.decode("utf-8"))
		escribirFichero(data + "\n")

		# URL para hacer la petición al servidor
		# URL = "https://api.ventumidc.es/api/gprs/temperatura/tst/" + str(obtenerTimeStamp()) + "/" + data
		# Petición al servidor de Ventum para almacenar los datos
		# r = requests.post(url = URL) 
		# respuesta = r.text
		# escribirFichero("[respuesta] " + respuesta + "\n")

		time = str(obtenerTimeStamp())
		os.system('/opt/bitnami/php/bin/php /opt/bitnami/apache2/htdocs/ventumApi/artisan GPRS:TST:Varilla ' + time + ' ' + data)

		#Comprobamos que sea del canal que nos interesa
	except Exception as ex:
		print(ex)
		f = open("bayer_registro.log", "a")
		f.write(str(ex) + "\n")
		f.close()

def on_connect(mqttc, userdata, flags, rc):
	"Función que se ejecuta al conectar"
	#f = open("registro.log", "a")
	print("Conectado con estado " + str(rc))
	#f.write("Conectado con estado " + str(rc))
	# Suscribinedo al topic
	print("Suscribiendo a \"%s\"" % (topic_datos))
	#f.write("Suscribiendo a \"%s\"" % (topic_datos))
	client.subscribe(topic_datos, qos)
	print("Suscrito")
	#f.write("Suscrito")
	#f.close()


escribirFichero(obtenerFecha() + " Iniciando...\n")

print("Iniciando con nueva instancia")
client = mqtt.Client(client_id = broker_id, clean_session = broker_clean_sess, protocol = mqtt.MQTTv31)
client.username_pw_set(username = broker_user, password	= broker_pass)
client.on_message = on_message # Asignarla al callback

print("Conectando al broker")
client.connect(broker_host, port = broker_port, keepalive = broker_keepalive, bind_address = "")
client.on_connect = on_connect # Asignarla al callback

client.loop_forever()