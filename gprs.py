#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys; print(sys.path)
import paho.mqtt.client as mqtt #import the client1
import time
from datetime import datetime
import binascii
import requests 
import os

application 		= 	"VENTUM_GPRS"
broker_host			=	"ventum-silos.mqtt.tst-sistemas.es"
broker_port 		= 	1947
broker_user			= 	"ventum-silos"
broker_pass 		= 	"ventum-silos1"
broker_id			= 	"gprs_silos_ventum"
broker_clean_sess 	= 	False
broker_keepalive 	= 	60
qos 				= 	1
topic_datos			=	"data"

def on_message(client, userdata, message):
	"Función de prueba para escribir en disco y mostrar por pantalla las tramas"
	try:
		#Comprobamos que sea del canal que nos interesa
		if message.topic == "data":
			f = open("registro.log", "a")
			f.write("-- Nueva informacion\n")
			imei = str(int(binascii.hexlify(message.payload[1:9]), 16))
			time = int(binascii.hexlify(message.payload[29:33]), 16)
			fecha = str(datetime.fromtimestamp(time))
			time = str(time)
			snr = str(int(binascii.hexlify(message.payload[9:10]), 16))
			data = str(message.payload.hex())

			f.write(time + "#" + fecha + "#" + imei + "#" + snr + "#" + data + "\n")
			f.close()

			# Las tramas especiales las ignoramos
			if data[0:2] != '91':
				#URL = "https://api.ventumidc.es/api/gprs/llenado/tst/" + imei + "/" + time + "/" + snr + "/" + data
 
  				# Petición al servidor de Ventum para almacenar los datos
				#r = requests.post(url = URL) 
				#respuesta = r.text
				os.system('/opt/bitnami/php/bin/php /opt/bitnami/apache2/htdocs/ventumApi/artisan GPRS:TST:Silos ' + imei + ' ' + time + ' ' + snr + ' ' + data)
				
				##print("respuesta: " + respuesta)

				#f = open("registro.log", "a")
				#f.write("respuesta: " + respuesta + "\n")
				#f.close()

		#print("Menaje recibido: ", message.payload)
		'''trama = message.payload.hex()
		frameTipe = binascii.hexlify(message.payload[0:1])
		imei = int(binascii.hexlify(message.payload[1:9]), 16)
		GSMlevel = int(binascii.hexlify(message.payload[9:10]), 16)
		mobileCountryCode = message.payload[10:12].hex()
		mobileNetworkCode = binascii.hexlify(message.payload[12:14])
		locationAreaCode = binascii.hexlify(message.payload[14:18])
		cellID = binascii.hexlify(message.payload[18:22])
		bateria = int(binascii.hexlify(message.payload[22:23]), 16)
		distancia = int(binascii.hexlify(message.payload[23:25]), 16)
		temperatura = int(binascii.hexlify(message.payload[25:26]), 16)
		acelerometroX = int(binascii.hexlify(message.payload[26:27]), 16)
		acelerometroY = int(binascii.hexlify(message.payload[27:28]), 16)
		acelerometroZ = int(binascii.hexlify(message.payload[28:29]), 16)
		timestamp = int(binascii.hexlify(message.payload[29:33]), 16)'''

		'''print("device: " + str(imei))
		print("time: " + str(timestamp))
		print("snr: " + str(GSMlevel))
		print("data: " + str(trama))'''

		'''print("trama: " + trama);
		print("frameTipe: " + str(frameTipe))
		print("imei: " + str(imei))
		print("GSMlevel: " + str(GSMlevel))
		print("mobileCountryCode: " + str(mobileCountryCode))
		print("mobileNetworkCode: " + str(mobileNetworkCode))
		print("locationAreaCode: " + str(locationAreaCode))
		print("cellID: " + str(cellID))
		print("bateria: " + str(bateria))
		print("distancia: " + str(distancia))
		print("temperatura: " + str(temperatura))
		print("acelerometroX: " + str(acelerometroX))
		print("acelerometroY: " + str(acelerometroY))
		print("acelerometroZ: " + str(acelerometroZ))'''


		'''imei = message.payload[1:9]
		imei = binascii.hexlify(imei)
		imei = int(imei, 16)					#Get the hex string in integer format
		imei = str(imei)'''
		'''print("Topic: ", message.topic)
		print("QoS Mensaje: ", message.qos)
		print("Retain Flags: ", message.retain)'''
	except Exception as ex:
		print(ex)
		f = open("registro.log", "a")
		f.write(str(ex) + "\n")
		f.close()

def on_connect(mqttc, userdata, flags, rc):
	"Función que se ejecuta al conectar"
	#f = open("registro.log", "a")
	print("Conectado con estado " + str(rc))
	#f.write("Conectado con estado " + str(rc))
	# Suscribinedo al topic
	print("Suscribiendo a \"%s\"" % (topic_datos))
	#f.write("Suscribiendo a \"%s\"" % (topic_datos))
	client.subscribe(topic_datos, qos)
	print("Suscrito")
	#f.write("Suscrito")
	#f.close()

f = open("registro.log", "a")
f.write("Iniciando...\n")
f.close()

print("Iniciando con nueva instancia")
client = mqtt.Client(client_id = broker_id, clean_session = broker_clean_sess, protocol = mqtt.MQTTv31)
client.username_pw_set(username = broker_user, password	= broker_pass)
client.on_message = on_message # Asignarla al callback

print("Conectando al broker")
client.connect(broker_host, port = broker_port, keepalive = broker_keepalive, bind_address = "")
client.on_connect = on_connect # Asignarla al callback

client.loop_forever()

#client.loop_start() #start the loop
#print("Finalizando script")
#client.loop_stop() #stop the loop
