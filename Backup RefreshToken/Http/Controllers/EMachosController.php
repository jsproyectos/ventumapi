<?php

namespace App\Http\Controllers;

use Auth;
use Exception;
use App\EntidadMacho;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EMachosController extends Controller
{
	public function __construct()
	{
	}
	/*public function inicializar($id_entidad, $fecha, $id_animal)
	{
		if ($entidad = EntidadMacho::create([
			'id_entidad' => $id_entidad,
			'fecha' => $fecha,
			'montas' => 0,
			'id_animal' => $id_animal,
			'activo' => 1,
		]))
		{
			return true;
		}
		return false;
	}*/

	/*private function obtenerNombreEntidad($id_entidad)
	{
		return DB::select("SELECT nombre FROM entidades WHERE id = ?", [$id_entidad])[0]->nombre;
	}*/

	// Obtiene la información en forma de tabla (array) entre dos fechas concretas para mostrarse, o procesarse a posteriori para mostrar gráficos
	private function obtenerInformacionParaTablas($id_entidad, $fechaInicio, $fechaFin, $orden)
	{
		return DB::select("SELECT DATE_FORMAT(fecha, '%d-%m-%Y %H:%i:%S') fecha FROM lecturas_machos WHERE id_entidad = ? AND fecha BETWEEN ? AND ? ORDER BY lecturas_machos.fecha $orden", [$id_entidad, $fechaInicio, $fechaFin]);
	}

	private function diaSemanaEnEspanol($diaSemana)
	{
		switch ($diaSemana)
		{
			case 'Monday': return "Lunes"; break;
			case 'Tuesday': return "Martes"; break;
			case 'Wednesday': return "Miércoles"; break;
			case 'Thursday': return "Jueves"; break;
			case 'Friday': return "Viernes"; break;
			case 'Saturday': return "Sábado"; break;
			default: return "Domingo"; break;
		}
	}

	/*public function actualizar($idEntidad, $fecha)
	{
		try
		{
			return DB::update("UPDATE entidades_machos SET montas = (SELECT count(*) FROM lecturas_machos WHERE id_entidad = ?), fecha = ? WHERE id_entidad = ?", [$idEntidad, $fecha, $idEntidad]);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
			return false;
		}
	}*/

	private function obtenerInformacionParaGraficos($id_entidad, $fechaInicio, $fechaFin, $orden)
	{
		$datos = DB::select("SELECT DATE(fecha) fecha, count(*) total FROM lecturas_machos WHERE id_entidad = ? AND fecha BETWEEN ? AND ? GROUP BY DATE(fecha) ORDER BY DATE(fecha) $orden", [$id_entidad, $fechaInicio, $fechaFin]);

		$fechas = [];
		$total = [];

		foreach ($datos as $dato)
		{
			//$fechas[] = strtotime($dato->fecha);
			$fechas[] = date("d-m-Y", strtotime($dato->fecha));
			$total[] = $dato->total;
		}

		return ["fechas" => $fechas, "total" => $total];
	}

	//AJAX Obtiene la información en forma de tabla (array) para mostrarse, o procesarse a posteriori para mostrar gráficos
	public function obtenerHistorico(Request $request)
	{
		$fechaFin = incrementarUnDia($request["fechaFin"]);

		return json_encode([
			"estado" => "OK",
			"datos" => $this->obtenerInformacionParaTablas($request["id_entidad"], $request["fechaInicio"], $fechaFin, "DESC"),
			"grafico" => $this->obtenerInformacionParaGraficos($request["id_entidad"], $request["fechaInicio"], $fechaFin, "ASC")
		]);
	}

	public function obtenerActividadReciente(Request $request)
	{
		try
		{
			$hoy = date("Y-m-d");
			$fechaDesde = date('Y-m-d 00:00:00', strtotime($hoy . "-6 days"));

			$montasSemanales = DB::select("SELECT DATE(fecha) fecha, count(*) total FROM lecturas_machos WHERE fecha > ? GROUP BY DATE(fecha) ORDER BY fecha DESC", [$fechaDesde]);

			// Hacemos esto para asegurarnos de que hay almenos 0 montas y que ningún día de la semana no aparezca
			//$hoy = date("Y-m-d");
			//$ayer = date('Y-m-d',strtotime($hoy . "-1 days"));

			$semanal = [];

			for ($i = 0; $i < 7; $i ++)
			{
				$dia = date("Y-m-d", strtotime($hoy . "-$i days"));
				$semanal[] = ["fecha" => $dia, "diaSemana" => $this->diaSemanaEnEspanol(date("l", strtotime($dia))), "montas" => 0];
			}

			foreach ($semanal as $dia => $diaSemana)
			{
				foreach ($montasSemanales as $montasSemana)
				{
					if ($montasSemana->fecha == $semanal[$dia]["fecha"])
						$semanal[$dia]["montas"] = $montasSemana->total;
				}
			}

			// Le damos un formato mas fácil de usar
			$semanal[0]["diaSemana"] = "Hoy"; $semanal[1]["diaSemana"] = "Ayer";

			// Ahora obtenemos las montas por por cada Animal
			$montasPorAnimal = DB::select("SELECT nombre, montas FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad ORDER BY nombre");

			return response()->json(["estado" => true, "datos" => ["semanal" => $semanal, "total" => $montasPorAnimal]], 200);
		}
		catch (Exception $e)
		{
			return response()->json(["estado" => false, "mensaje" => $e->getMessage], 200);	
		}
	}

	public function obtenerResumen(Request $request)
	{
		try
		{
			$orden = $this->ordenMostrar($request["orden"]);
			$tipoUsuario = Auth::user()["role"];
			$hoy = date("Y-m-d 00:00:00");
			$hoyFin = incrementarUnDia($hoy);
			$ayer = reducirUnDia($hoy);

			if ($tipoUsuario == USUARIO_SUPERADMINISTRADOR || $tipoUsuario == USUARIO_ADMINISTRADOR)
			{
				// 24-04-2020 La cambio por una que además me muestra las montas de hoy y de ayer
				//$entidades = DB::select("SELECT entidades.id id, nombre, codigo crotal, descripcion, fecha, montas, '0' AS autorizado FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad ORDER BY nombre");

				//dd("SELECT entidades.id id, nombre, codigo crotal, descripcion, fecha, montas, '0' AS autorizado, IFNULL(HOY.hoy, 0) hoy, IFNULL(AYER.ayer, 0) ayer FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad LEFT JOIN (SELECT id_entidad, count(*) hoy FROM lecturas_machos WHERE fecha BETWEEN '$hoy' AND '$hoyFin' GROUP BY id_entidad) HOY ON entidades.id = HOY.id_entidad LEFT JOIN (SELECT id_entidad, count(*) ayer FROM lecturas_machos WHERE fecha BETWEEN '$ayer' AND '$hoy' GROUP BY id_entidad) AYER ON entidades.id = AYER.id_entidad ORDER BY ?");

				$entidades = DB::select("SELECT entidades.id id, nombre, codigo crotal, descripcion, fecha, montas, '0' AS autorizado, IFNULL(HOY.hoy, 0) hoy, IFNULL(AYER.ayer, 0) ayer FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad LEFT JOIN (SELECT id_entidad, count(*) hoy FROM lecturas_machos WHERE fecha BETWEEN '$hoy' AND '$hoyFin' GROUP BY id_entidad) HOY ON entidades.id = HOY.id_entidad LEFT JOIN (SELECT id_entidad, count(*) ayer FROM lecturas_machos WHERE fecha BETWEEN '$ayer' AND '$hoy' GROUP BY id_entidad) AYER ON entidades.id = AYER.id_entidad ORDER BY $orden");
			}
			else
			{
				$entidades = DB::select("SELECT entidades.id id, nombre, codigo crotal, descripcion, entidades_machos.fecha fecha, montas, autorizado FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad LEFT JOIN entidades_usuarios ON entidades.id = entidades_usuarios.id_entidad WHERE entidades_usuarios.id_usuario = ? AND entidades_usuarios.fechaBaja IS NULL ORDER BY nombre", [Auth::user()["id"]]);
			}

			foreach ($entidades as $entidad)
			{
				$entidad->autorizado = $entidad->autorizado == "0" ? "sensorPropio" : "sensorAutorizado";
			}

			return response()->json(["estado" => true, "datos" => $entidades, "orden" => $request["orden"]], 200);
		}
		catch (Exception $e)
		{
			return response()->json(["estado" => false, "mensaje" => $e->getMessage], 200);	
		}
	}

	private function ordenMostrar($orden)
	{
		switch ($orden)
		{
			case 'nombre': return "nombre ASC"; break;
			case 'crotal': return "crotal ASC"; break;
			case 'montas': return "montas DESC, nombre ASC"; break;
			default: return "hoy DESC, nombre ASC"; break;
		}
	}

	/*public function mostrarResumen()
	{
		$entidades = $this->obtenerResumen();
		return view("visualizacion.machos", ["tipoUsuario" => Auth::user()["role"], "nombrePagina" => "machos", "entidades" => $entidades, "tituloPagina" => "Control de machos"]);
	}*/

	public function mostrarDetalles($id)
	{
		try
		{
			if (esAdmin(Auth::user()["role"]))
			{
				$entidad = DB::select("SELECT entidades.id id, nombre, codigo crotal, entidades_animales.id_tipo id_tipo_animal, entidades.id_tipo id_tipo, SA.id_tipo id_tipo_sensor, id_sensor, descripcion, longitud, latitud, montas, 0 autorizado FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad JOIN entidades_animales ON entidades.id = entidades_animales.id_entidad LEFT JOIN (SELECT id_entidad, id id_sensor, id_tipo FROM entidades_sensores JOIN sensores ON entidades_sensores.id_sensor = sensores.id WHERE entidades_sensores.fechaBaja IS NULL AND id_entidad = ?) SA ON entidades.id = SA.id_entidad WHERE entidades.id = ?", [$id, $id])[0];
			}
			else
			{
				$entidad = DB::select("SELECT entidades.id id, nombre, codigo crotal, entidades_animales.id_tipo id_tipo, descripcion, longitud, latitud, montas, autorizado FROM entidades JOIN entidades_machos ON entidades.id = entidades_machos.id_entidad JOIN entidades_animales ON entidades.id = entidades_animales.id_entidad LEFT JOIN entidades_usuarios on entidades.id = entidades_usuarios.id_entidad WHERE entidades.id = ? AND entidades_usuarios.id_usuario = ?", [$id, Auth::user()["id"]])[0];
			}

			$entidad->propio = $entidad->autorizado == 0 ? true : false;

			return response()->json(["estado" => true, "datos" => $entidad], 200);
		}
		catch (Exception $e)
		{
			return response()->json(["estado" => false, "mensaje" => "Ha ocurrido un error. Por favor, contacte con nosotros", "e" => $e->getMessage()], 200);
		}
	}

	/*public function ExportarHistoricoExcel($id_entidad, $fechaInicio, $fechaFin)
	{
		$criterios = ["fecha"];
		$datos = $this->obtenerInformacionParaTablas($id_entidad, $fechaInicio, incrementarUnDia($fechaFin), "ASC");

		$this->exportarExcel($id_entidad, $criterios, $datos, $fechaInicio, $fechaFin);
	}*/

	/*public function obtenerMontas($fechaInicio, $fechaFin)
	{
		return DB::select("SELECT id_sensor, DATE(fecha) AS fecha, count(*) AS total FROM lecturas_machos WHERE fecha BETWEEN ? AND ?  GROUP BY id_sensor, DATE(fecha) ORDER BY id_sensor, DATE(fecha) ASC", [$fechaInicio, $fechaFin]);
	}

	public function obtenerMontasAgrupadas($fechaInicio, $fechaFin)
	{
		return DB::select("SELECT id_sensor, count(*) AS total FROM lecturas_machos WHERE fecha BETWEEN ? AND ? GROUP BY id_sensor ORDER BY id_sensor", [$fechaInicio, $fechaFin]);
	}*/

	public function pruebas()
	{
		$hoy = date("Y-m-d");
		$fechaDesde = date('Y-m-d 00:00:00', strtotime($hoy . "-6 days"));

		return $fechaDesde;
	}
}