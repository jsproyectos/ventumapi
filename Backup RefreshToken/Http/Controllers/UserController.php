<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Storage;

class UserController extends Controller
{
	private function escribirLog($mensaje)
	{
		Storage::append("Autenticacion.log", date("d-m-Y H:i:s") . $mensaje);
	}

	// joseluis_f1@hotmail.com / 123456
	public function authenticate(Request $request)
	{
		$credentials = $request->only('email', 'password');

		try
		{
			if (! $token = JWTAuth::attempt($credentials))
			{
				return response()->json(["estado" => false, "mensaje" => "El Usuario o contraseña no son correctos"], 400);
			}
		}
		catch (JWTException $e)
		{
			return response()->json(["estado" => false, "mensaje" => "No se ha podido autenticar al usuario"], 500);
		}

		return response()->json(["estado" => true, "token" => $token, "user" => User::where("email", $credentials['email'])->get()->first()], 200);
	}

	public function getAuthenticatedUser()
	{
		try
		{
			if (!$user = JWTAuth::parseToken()->authenticate())
			{
				$this->escribirLog("1");
				$this->escribirLog("Usuario no encontrado");
				return response()->json(["estado" => false, "mensaje" => "user_not_found"], 404);
			}

			$token = JWTAuth::getToken();
			dd($token);

			/*$token = JWTAuth::getToken();
			$apy = JWTAuth::getPayload($token)->toArray();*/

			$this->escribirLog("2");
			$this->escribirLog("Usuario encontrado");
			return response()->json(["estado" => true, "user" => $user, "token" => JWTAuth::getToken()], 200);

			/*$token = JWTAuth::getToken();
			$token = JWTAuth::refresh($token);
			return response()->json(["estado" => true, "refresh" => $token], 200);*/
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			$this->escribirLog("3");
			$this->escribirLog("Usuario con token expirado");
			// Si ha expirado, procedemos a renovarlo
			try
			{
				$token = JWTAuth::getToken();
				$token = JWTAuth::refresh($token);

				$this->escribirLog("4");
				$this->escribirLog("Token renovado. Nuevo token: " . $token);

				//return response()->json(["estado" => true, "token" => $token], 200);	
				return response()->json(["estado" => true, "user" => $user, "refresh" => $token], 200);
			}
			catch (Exception $e)
			{
				$this->escribirLog("5");
				$this->escribirLog("Error mientras se renueva el token: " . $e->getMessage());
				return 	response()->json(["estado" => false, "mensaje" => "No tienes permiso para hacer esta acción"], 403);
			}
			return response()->json(["estado" => false, "mensaje" => "token_expired"], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			$this->escribirLog("6");
			$this->escribirLog("Error, Token Inválido: " . $e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "token_invalid"], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			$this->escribirLog("7");
			$this->escribirLog("Excepción: " . $e->getMessage());
			return response()->json(["estado" => false, "mensaje" => "token_absent"], $e->getStatusCode());
		}
	}

	public function register(Request $request)
	{
			$validator = Validator::make($request->all(), [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
		]);

		if($validator->fails()){
				return response()->json($validator->errors(), 400);
		}

		$user = User::create([
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'password' => Hash::make($request->get('password')),
		]);

		$token = JWTAuth::fromUser($user);

		return response()->json(compact('user','token'),201);
	}

	public function refreshToken()
	{
		$token = JWTAuth::getToken();
		try
		{
			$token = JWTAuth::refresh($token);
			return response()->json(["estado" => true, "token" => $token], 200);
		}
		catch (TokenBlackListedException $ex)
		{
			return response()->json(["estado" => false, "mensaje" => "No se ha podido refrescar"], 422);
		}
		catch (TokenExpiredException $ex)
		{
			return response()->json(["estado" => false, "mensaje" => "El token ha expirado"], 422);
		}
	}

	public function logout()
	{
		$token = JWTAuth::getToken();
		try
		{
			$token = JWTAuth::invalidate($token);
			return response()->json(["estasdo" => false, "mensaje" => "Se ha cerrado la sesión"], 200);
		}
		catch (JWTException $e)
		{
			return response()->json(["estasdo" => false, "mensaje" => $e->getMessage()], 422);
		}
	}
}
