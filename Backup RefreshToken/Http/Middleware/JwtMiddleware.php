<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Storage;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware
{


	private function escribirLog($mensaje)
	{
		Storage::append("Autenticacion.log", date("d-m-Y H:i:s") . $mensaje);
	}
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// Siguiente acción
        $response = $next($request);

		try
		{
			$user = JWTAuth::parseToken()->authenticate();
		}
		catch (Exception $e)
		{
			$this->escribirLog("Middleware: " . $e->getMessage());
			if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
			{
				return response()->json(['status' => 'Token is Invalid']);
			}
			else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
			{
				try
				{
					$this->escribirLog("Middleware. Renovando token: " . $e->getMessage());
					$refreshed = JWTAuth::refresh(JWTAuth::getToken());
					$user = JWTAuth::setToken($refreshed)->toUser();
					//$response->header('Authorization', 'Bearer ' . $refreshed);
					header('Authorization: Bearer ' . $refreshed);
					//header('Authorization: Bearer ' . $refreshed);
					$this->escribirLog("Se ha renovado el token a " . $refreshed);
				}
				catch (JWTException $e)
				{
					/*return response()->json([
					'code'   => 103 // means not refreshable 
					'response' => null // nothing to show 
					]);*/
					$this->escribirLog("Middleware: Otra Excepción en Token Expirado: " . $e->getMessage());
					return response()->json(['status' => 'Token is Expired']);
				}
			}
			else
			{
				$this->escribirLog("Middleware: Autorización no encontrada " . $e->getMessage());
				return response()->json(['status' => 'Authorization Token not found']);
			}
		}

		$this->escribirLog("Middleware. Terminando...");
		//return $next($request);
		return $response;
	}
}
