<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class alertasSensores implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $idSensor;
	protected $fecha;
	protected $bateria;
	protected $bp;
	protected $snr;
	protected $rssi;

	public function __construct()
	{
	}

	public function handle()
	{
	}

	public function obtenerAlertasActivas($idSensor)
	{
		return app('App\Http\Controllers\SensoresController')->obtenerAlertasActivas($idSensor);
	}

	public function enviarAlerta($idSensor, $idAlerta, $idUsuario, $parametros, $email, $vista, $data, $asunto)
	{
		// Insertar notificación
		app('App\Http\Controllers\NotificacionesController')->insertarNotificacionSensor($idSensor, $idAlerta, $idUsuario, $parametros);
		// Enviar email con la alerta
		LogController::enviarEmail($email, $vista, $data, $asunto);
	}
}