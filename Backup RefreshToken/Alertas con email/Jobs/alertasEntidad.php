<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidad implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $id_entidad;
	protected $fecha;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		LogController::mensaje("Creando una alerta de entidad estándar");
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		LogController::mensaje("Handle standard");
	}

	public function obtenerAlertasActivas($id_entidad)
	{
		return app('App\Http\Controllers\EntidadesController')->obtenerAlertasActivas($id_entidad);
	}

	// Envía una alerta por email, en lugar de por notificaicón. Esto cambiará cuando esté lista la app
	protected function enviarAlertaEmail($idEntidad, $idAlerta, $idUsuario, $parametros, $email, $vista, $dataEmail, $asunto)
	{
		// Insertar notificación
		app('App\Http\Controllers\NotificacionesController')->insertarNotificacionEntidad($idEntidad, $idAlerta, $idUsuario, $parametros);

		//Enviar email con la alerta
		LogController::enviarEmail($email, $vista, $dataEmail, $asunto);
	}

	// Envía una alerta con notificación, en vez de por emailº
	protected function enviarAlertaNotificacion($idEntidad, $idAlerta, $idUsuario, $parametros, $userIdOneSignal, $mensajeNotificacion)
	{
		// Insertar notificación
		app('App\Http\Controllers\NotificacionesController')->insertarNotificacionEntidad($idEntidad, $idAlerta, $idUsuario, $parametros);

		// Enviar notificación al móvil
		app('App\Http\Controllers\NotificacionesController')->enviarPushNotification($idEntidad, $userIdOneSignal, $mensajeNotificacion);

		LogController::mensaje("Push enviada");
	}

	// Envía una alerta con notificación, en vez de por email
	// SOLO PARA LA APLICACIÓN MACHOS
	protected function enviarAlertaMachos($idEntidad, $idAlerta, $idUsuario, $parametros, $userIdOneSignal, $mensajeNotificacion)
	{
		// Insertar notificación
		app('App\Http\Controllers\NotificacionesController')->insertarNotificacionEntidad($idEntidad, $idAlerta, $idUsuario, $parametros);

		// Enviar notificación al móvil
		app('App\Http\Controllers\NotificacionesController')->enviarPushNotificationMachos($idEntidad, $userIdOneSignal, $mensajeNotificacion);
	}
}
