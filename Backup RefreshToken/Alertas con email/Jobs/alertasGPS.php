<?php

namespace App\Jobs;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

// Pruebas
// ventumlocal.es/GPS/Sigfox/2061C1/1567599070/39.76/04d84aa3ad5f952d77000000/39.0/-6.0

class alertasGPS extends alertas
{
	protected $latitud;
	protected $longitud;

	public function __construct($idSensor, $fecha, $bateria, $snr, $latitud, $longitud)
	{
		$this->idSensor = $idSensor;
		$this->fecha = $fecha;
		$this->bateria = calcularPorcentajeBateria($bateria); // Porcentaje de batería
		$this->snr = $snr;
		$this->latitud = $latitud;
		$this->longitud = $longitud;
	}

	public function handle()
	{
		// Una vez obtenidas las alarmas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alarmas
		$alarmas = app('App\Http\Controllers\SensoresController')->obtenerAlertasActivas($this->idSensor);

		foreach ($alarmas as $alarma)
		{
			switch ($alarma->tipo)
			{
				case ALARMA_BATERIA_BAJA:
					if ($this->bateria <= $alerta->parametros)
					{
						$data = [
							"nombre" => $alerta->nombreEmail,
							"fecha" => $this->fecha,
							"idSensor" => $this->idSensor,
							"nombreSensor" => $alerta->nombreSensor,
							"bateria" => $this->bateria,
							"titulo" => "Alerta por batería baja",
							"idTipo" => SENSOR_SILO
						];
						LogController::enviarEmail($alerta->email, "alertas.bateria_baja", $data, "Bateria baja en sensor $alerta->nombreSensor");
					}
					break;
				case ALARMA_SNR_BAJA:
					if ($this->snr <= 10)
					{
						$data = [
							"nombre" => $alerta->nombreEmail,
							"fecha" => $this->fecha,
							"idSensor" => $this->idSensor,
							"nombreSensor" => $alerta->nombreSensor,
							"titulo" => "Alerta por baja cobertura",
							"idTipo" => SENSOR_GPS
						];
						LogController::enviarEmail($alerta->email, "alertas.snr_baja", $data, "Mala cobertura en Silo $alerta->nombreSensor");
					}
					break;
			}
		}
	}
}