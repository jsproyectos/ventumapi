<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class alertasSensoresTemperatura extends alertasSensores
{
protected $tipoApertura;

	protected $temperatura;
	protected $humedad;
	protected $luminosidad;
	protected $eTermico;

	public function __construct($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $temperatura, $humedad, $luminosidad, $eTermico)
	{
		$this->idSensor = $idSensor;
		$this->fecha = $fecha;
		$this->bateria = $bateria;
		$this->bp = $bp;
		$this->snr = $snr;
		$this->rssi = $rssi;
		$this->temperatura = $temperatura;
		$this->humedad = $humedad;
		$this->luminosidad = $luminosidad;
		$this->eTermico = $eTermico;
	}

	public function handle()
	{
		try
		{
			// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
			$alertas = $this->obtenerAlertasActivas($this->idSensor);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_BATERIA_BAJA:
						if ($this->bp <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"bp" => $this->bp,
								"titulo" => "Alerta por batería baja",
							];
							$this->enviarAlerta($this->idSensor, ALARMA_BATERIA_BAJA, $alerta->id_usuario_a, $this->bp, $alerta->email, "alertas.bateria_baja", $data, "Bateria baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura baja"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario_a, $this->temperatura, $alerta->email, "alertas.temperatura_baja", $data, "Temperatura baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario_a, $this->temperatura, $alerta->email, "alertas.temperatura_alta", $data, "Temperatura alta en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_HUMEDAD_BAJA:
						if ($this->humedad <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"humedad" => $this->humedad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por humedad baja"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_HUMEDAD_BAJA, $alerta->id_usuario_a, $this->humedad, $alerta->email, "alertas.sensoresTemperatura.humedad_baja", $data, "Humedad baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_HUMEDAD_ALTA:
						if ($this->humedad >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"humedad" => $this->humedad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por humedad alta"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_HUMEDAD_ALTA, $alerta->id_usuario_a, $this->humedad, $alerta->email, "alertas.sensoresTemperatura.humedad_alta", $data, "Humedad alta en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_LUMINOSIDAD_BAJA:
						if ($this->luminosidad <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"luminosidad" => $this->luminosidad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por luminosidad baja"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_LUMINOSIDAD_BAJA, $alerta->id_usuario_a, $this->luminosidad, $alerta->email, "alertas.sensoresTemperatura.luminosidad_baja", $data, "Luminosidad baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_LUMINOSIDAD_ALTA:
						if ($this->luminosidad >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"luminosidad" => $this->luminosidad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por luminosidad alta"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_LUMINOSIDAD_ALTA, $alerta->id_usuario_a, $this->luminosidad, $alerta->email, "alertas.sensoresTemperatura.luminosidad_alta", $data, "Luminosidad alta en sensor $alerta->nombreSensor");
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[Alertas] $this->idSensor" . ": " . $e->getMessage());
		}
	}
}