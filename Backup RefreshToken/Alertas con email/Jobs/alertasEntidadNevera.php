<?php

namespace App\Jobs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadNevera extends alertasEntidad
{
	protected $temperatura;
	protected $humedad;

	public function __construct($id_entidad, $fecha, $temperatura, $humedad)
	{
		$this->id_entidad = $id_entidad;
		$this->fecha = $fecha;
		$this->temperatura = $temperatura;
		$this->humedad = $humedad;
	}

	public function handle()
	{
		try
		{
			$alertas = app('App\Http\Controllers\EntidadesController')->obtenerAlertasActivas($this->id_entidad);

			// Ahora obtenemos el tipo de entidad con la que trabaja este sensor
			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura baja"
							];
						$vista = "alertas.entidad.nevera.temperatura_baja";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Temperatura baja en nevera $alerta->nombreEntidad");
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura > $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta"
							];
						$vista = "alertas.entidad.nevera.temperatura_alta";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Temperatura alta en nevera $alerta->nombreEntidad");
						}
						break;
					case ALARMA_HUMEDAD_BAJA:
						if ($this->humedad <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"humedad" => $this->humedad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por humedad baja"
							];
						$vista = "alertas.entidad.nevera.humedad_baja";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_HUMEDAD_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Humedad baja en nevera $alerta->nombreEntidad");
						}
						break;
					case ALARMA_HUMEDAD_ALTA:
						if ($this->humedad > $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"humedad" => $this->humedad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por humedad alta"
							];
						$vista = "alertas.entidad.nevera.humedad_alta";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_HUMEDAD_ALTA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Humedad alta en nevera $alerta->nombreEntidad");
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[ALERTAS ENTIDAD NEVERA] " . $e->getMessage());	
		}
	}
}
