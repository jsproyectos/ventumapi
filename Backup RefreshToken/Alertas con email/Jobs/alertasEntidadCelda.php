<?php

namespace App\Jobs;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadCelda extends alertasEntidad
{
	protected $temperatura;
	protected $humedad;
	protected $luminosidad;
	protected $etermico;

	public function __construct($id_entidad, $fecha, $temperatura, $humedad, $luminosidad, $etermico)
	{
		$this->id_entidad = $id_entidad;
		$this->fecha = $fecha;
		$this->temperatura = $temperatura;
		$this->humedad = $humedad;
		$this->luminosidad = $luminosidad;
		$this->etermico = $etermico;
	}

	public function handle()
	{
		try
		{
			// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
			$alertas = $this->obtenerAlertasActivas($this->id_entidad);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura baja"
							];
						$vista = "alertas.entidad.celda.temperatura_baja";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Temperatura baja en celda $alerta->nombreEntidad");
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura > $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta"
							];
						$vista = "alertas.entidad.celda.temperatura_alta";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Temperatura alta en celda $alerta->nombreEntidad");
						}
						break;
					case ALARMA_HUMEDAD_BAJA:
						if ($this->humedad <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"humedad" => $this->humedad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por humedad baja"
							];
						$vista = "alertas.entidad.celda.humedad_baja";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_HUMEDAD_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Humedad baja en celda $alerta->nombreEntidad");
						}
						break;
					case ALARMA_HUMEDAD_ALTA:
						if ($this->humedad > $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"humedad" => $this->humedad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por humedad alta"
							];
						$vista = "alertas.entidad.celda.humedad_alta";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_HUMEDAD_ALTA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Humedad alta en celda $alerta->nombreEntidad");
						}
						break;
					case ALARMA_LUMINOSIDAD_BAJA:
						if ($this->luminosidad <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"luminosidad" => $this->luminosidad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por baja luminosidad"
							];
						$vista = "alertas.entidad.celda.luminosidad_baja";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_LUMINOSIDAD_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Baja luminosidad en celda $alerta->nombreEntidad");
						}
						break;
					case ALARMA_LUMINOSIDAD_ALTA:
						if ($this->luminosidad > (int)$alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"luminosidad" => $this->luminosidad,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por alta luminosidad"
							];
						$vista = "alertas.entidad.celda.luminosidad_alta";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_LUMINOSIDAD_ALTA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Alta luminosidad en celda $alerta->nombreEntidad");
						}
						break;
					case ALERTA_ETERMICO_PROLONGADO:
						$parametroTroceados = explode("/", $alerta->parametros);
						if ($this->estresTermicoProlongado(intval($parametroTroceados[0]), intval($parametroTroceados[1])))
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"id_entidad" => $this->id_entidad,
								"nombreEntidad" => $alerta->nombreEntidad,
								"titulo" => "Alerta por estrés térmico prolongado",
							];
							$vista = "alertas.entidad.celda.eTermicoProlongado";
							$this->enviarAlertaEmail($this->id_entidad, ALERTA_ETERMICO_PROLONGADO, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Estrés térmico prolongado en $alerta->nombreEntidad");
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[ALERTAS ENTIDAD CELDA] " . $e->getMessage());	
		}
	}

	private function estresTermicoProlongado($prolongado, $reciente)
	{
		return app('App\Http\Controllers\EntidadesNavesController')->calculoEstresTermicoProlongadoCelda($this->id_entidad, $prolongado, $reciente);
	}
}
