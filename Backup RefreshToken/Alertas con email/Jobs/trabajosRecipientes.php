<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail
use Exception;

/* Esta clase raliza tareas que deben hacerse después de la inserción, para que no se demore mucho tiempo la respuesta al sensor

Tareas a realizar:
	- Correción de valores para que sea siempre una escala igual o descendente (salvo llenados)
	- Cálculo de consumo diario cuando se actualiza las lecturas del sensor
*/

class trabajosRecipientes implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $id_entidad

	public function __construct($id_entidad)
	{
		$this->id_entidad = $id_entidad;
	}

	public function handle()
	{
		try
		{
			$consumoDiario = obtenerConsumoDia($this->id_entidad);
			// Utilizo una consulta directa para que sea mas eficiente
			DB::update("UPDATE entidades_recipientes SET consumoD = ?", $consumoDiario);
		}
		catch (Exception $e)
		{
			LogController::errores($e->getMessage());
		}
	}
}
