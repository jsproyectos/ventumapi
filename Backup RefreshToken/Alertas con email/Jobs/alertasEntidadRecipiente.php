<?php

namespace App\Jobs;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadRecipiente extends alertasEntidad
{
	protected $porcentaje;
	protected $temperatura;
	protected $kilos;
	protected $acelerometroX;
	protected $acelerometroY;
	protected $acelerometroZ;

	public function __construct($id_entidad, $porcentaje, $kilos, $fecha, $temperatura, $acelerometroX, $acelerometroY, $acelerometroZ)
	{
		$this->id_entidad = $id_entidad;
		$this->porcentaje = $porcentaje;
		$this->fecha = $fecha;
		$this->temperatura = $temperatura;
		$this->kilos = $kilos;
		$this->acelerometroX = $acelerometroX;
		$this->acelerometroY = $acelerometroY;
		$this->acelerometroZ = $acelerometroZ;
	}

	private function tapaCerrada($acelerometroX, $acelerometroY, $acelerometroZ)
	{
		return app('App\Http\Controllers\EntidadesRecipientesController')->tapaCerrada($acelerometroX, $acelerometroY, $acelerometroZ);
	}

	public function handle()
	{
		try
		{
			$alertas = $this->obtenerAlertasActivas($this->id_entidad);
			$idTipoEntidad = empty($alertas) ? null : $alertas[0]->id_tipo;
			$ruta = null;
			$tipoEntidad = null;

			switch ($idTipoEntidad)
			{
				case ENTIDAD_SILO:
					$ruta = 'silo';
					$tipoEntidad = 'silo';
					$rutaEntidad = '/ver/silos/informacion/';
					break;
				case ENTIDAD_AGUA_DEPOSITOS:
					$ruta = 'deposito_agua';
					$tipoEntidad = 'depósito de agua';
					$rutaEntidad = '/ver/depositos/informacion/';
					break;
				
				default:
					$ruta = 'silo';
					$tipoEntidad = 'silo';
					$rutaEntidad = '';
					break;
			}

			// Ahora obtenemos el tipo de entidad con la que trabaja este sensor
			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$data = [
								'nombre' => $alerta->nombreUsuario,
								'fecha' => $this->fecha,
								'nombreEntidad' => $alerta->nombreEntidad,
								'temperatura' => $this->temperatura,
								'parametros' => $alerta->parametros,
								'titulo' => 'Alerta por temperatura baja'
							];
							$vista = "alertas.entidad.$ruta.temperatura_baja";
							$this->enviarAlertaEmail($this->id_entidad, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Temperatura baja en $tipoEntidad $alerta->nombreEntidad");
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta"
							];
							$vista = "alertas.entidad.$ruta.temperatura_alta";
							$this->enviarAlertaEmail($this->id_entidad, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Temperatura alta en $tipoEntidad $alerta->nombreEntidad");
						}
						break;
					case ALARMA_SILOS_CAPACIDAD_BAJA:
						/*if ($this->porcentaje <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por baja capacidad",
								"parametros" => $alerta->parametros
							];
							$vista = "alertas.entidad.$ruta.capacidad_baja";
							$this->enviarAlertaEmail($this->id_entidad, ALARMA_SILOS_CAPACIDAD_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Capacidad baja en $tipoEntidad $alerta->nombreEntidad");
						}
						break;*/

						$mensajeNotificacion = "Se ha detectado capacidad baja ($this->porcentaje%) en su $tipoEntidad $alerta->nombreEntidad";
						$this->enviarAlertaNotificacion($this->id_entidad, ALARMA_SILOS_CAPACIDAD_BAJA, $alerta->id_usuario, $this->porcentaje, $alerta->user_id_ionic, $mensajeNotificacion);

					//$this->enviarAlerta($this->id_entidad, ALARMA_MONTA_MACHO, $alerta->id_usuario, null, $alerta->email, "alertas.sensoresMachos.monta", $data, "Monta detectada en sensor $alerta->nombreSensor");
					//LogController::enviarEmail($alerta->email, "alertas.puertas.puertas", $data, "Apertura de puerta: $alerta->nombreSensor");

					break;



					case ALARMA_SILOS_CAPACIDAD_ALTA:
						if ($this->porcentaje >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por alta capacidad",
								"parametros" => $alerta->parametros
							];
							$vista = "alertas.entidad.$ruta.capacidad_alta";
							$this->enviarAlertaEmail($this->id_entidad, ALARMA_SILOS_CAPACIDAD_BAJA, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Capacidad alta en $tipoEntidad $alerta->nombreEntidad");
						}
						break;
					case ALARMA_SILOS_RECEPCION:
						$data = [
							"nombre" => $alerta->nombreUsuario,
							"fecha" => $this->fecha,
							"nombreEntidad" => $alerta->nombreEntidad,
							"capacidad" => $this->porcentaje,
							"titulo" => "Capacidad actualizada"
						];
						$vista = "alertas.entidad.$ruta.recepcion";
						$this->enviarAlertaEmail($this->id_entidad, ALARMA_SILOS_RECEPCION, $alerta->id_usuario, $alerta->parametros, $alerta->email, $vista, $data, "Nuevo estado en $tipoEntidad $alerta->nombreEntidad");
						break;
					case ALARMA_SILOS_DESNIVEL:
						if (!$this->tapaCerrada($this->acelerometroX, $this->acelerometroY, $this->acelerometroZ)) // Si la tapa no se encuentra cerrada
						{
							$data = [
								"nombre" => $alerta->nombreUsuario,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"titulo" => "Alerta por alta capacidad",
							];
							$vista = "alertas.entidad.$ruta.desnivel";
							$this->enviarAlertaEmail($this->id_entidad, ALARMA_SILOS_DESNIVEL, $alerta->id_usuario, null, $alerta->email, $vista, $data, "Sensor desnivelado en $tipoEntidad $alerta->nombreEntidad");
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores($this->id_entidad . ": " . $e->getMessage());
		}
	}
}
