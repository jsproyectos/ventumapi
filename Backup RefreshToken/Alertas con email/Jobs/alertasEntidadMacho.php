<?php

namespace App\Jobs;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasEntidadMacho extends alertasEntidad
{
	public function __construct($id_entidad, $fecha)
	{
		$this->id_entidad = $id_entidad;
		$this->fecha = $fecha;
	}

	public function handle()
	{
		// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
		$alertas = $this->obtenerAlertasActivas($this->id_entidad);

		foreach ($alertas as $alerta)
		{
			switch ($alerta->id_alerta)
			{
				case ALARMA_MONTA_MACHO:
					/*$data = [
						"nombre" => $alerta->nombreEmail,
						"fecha" => $this->fecha,
						"nombreEntidad" => $alerta->nombreEntidad,
						"crotal" => $alerta->codigo,
						"titulo" => "Alerta por monta detectada",
						"idTipo" => ENTIDAD_ANIMAL
					];*/

					$mensajeNotificacion = "Su carnero con bolo $alerta->codigo ($alerta->nombreEntidad) acaba de realizar una monta";
					$this->enviarAlertaMachos($this->id_entidad, ALARMA_MONTA_MACHO, $alerta->id_usuario, null, $alerta->user_id_ionic, $mensajeNotificacion);

					//$this->enviarAlerta($this->id_entidad, ALARMA_MONTA_MACHO, $alerta->id_usuario, null, $alerta->email, "alertas.sensoresMachos.monta", $data, "Monta detectada en sensor $alerta->nombreSensor");
					//LogController::enviarEmail($alerta->email, "alertas.puertas.puertas", $data, "Apertura de puerta: $alerta->nombreSensor");

					break;
			}
		}
	}
}
