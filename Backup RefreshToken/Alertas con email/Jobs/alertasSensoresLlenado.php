<?php

namespace App\Jobs;
use Illuminate\Support\Facades\DB;

class alertasSensoresLlenado extends alertasSensores
{
	protected $altura;
	protected $temperatura;
	protected $aX;
	protected $aY;
	protected $aZ;

	public function __construct($idSensor, $fecha, $bateria, $rssi, $snr, $altura, $temperatura, $aX, $aY, $aZ)
	{
		$this->idSensor = $idSensor;
		$this->fecha = $fecha;
		$this->bateria = 100 - $bateria * 25; // Ya que las baterías van 0, 1, 2, 3 y 4, hacemos una escala de 0 a 100
		$this->bp = null;
		$this->rssi = $rssi;
		$this->snr = $snr;
		$this->altura = $altura;
		$this->temperatura = $temperatura;
		$this->aX = $aX;
		$this->aY = $aY;
		$this->aZ = $aZ;
	}


	public function handle()
	{
		try
		{
			$alertas = $this->obtenerAlertasActivas($this->idSensor);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_BATERIA_BAJA:
						if ($this->bp <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"bp" => $this->bateria,
								"titulo" => "Alerta por batería baja",
							];
							$this->enviarAlerta($this->idSensor, ALARMA_BATERIA_BAJA, $alerta->id_usuario_a, $alerta->parametros, $alerta->email, "alertas.bateria_baja", $data, "Bateria baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_BAJA:
						if ($this->temperatura <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura baja"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_TEMPERATURA_BAJA, $alerta->id_usuario_a, $this->temperatura, $alerta->email, "alertas.temperatura_baja", $data, "Temperatura baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
						if ($this->temperatura >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta"
							];
							$this->enviarAlerta($this->idSensor, ALARMA_TEMPERATURA_ALTA, $alerta->id_usuario_a, $this->temperatura, $alerta->email, "alertas.temperatura_alta", $data, "Temperatura alta en sensor $alerta->nombreSensor");
						}
						break;
					/*case ALARMA_SILOS_CAPACIDAD_BAJA:
						if ($this->porcentaje <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por baja ocupación",
								"parametros" => $alerta->parametros
							];
							$this->insertarNotificacion($this->id_entidad, ALARMA_SILOS_CAPACIDAD_BAJA, $alerta->id_usuario_a, $alerta->email, $this->porcentaje);
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".capacidad_baja", $data, "Capacidad baja en $tipoEntidad $alerta->nombreEntidad");
						}
						break;
					case ALARMA_SILOS_CAPACIDAD_ALTA:
						if ($this->porcentaje >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"nombreEntidad" => $alerta->nombreEntidad,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por alta ocupación",
								"parametros" => $alerta->parametros
							];
							$this->insertarNotificacion($this->id_entidad, ALARMA_SILOS_CAPACIDAD_ALTA, $alerta->id_usuario_a, $alerta->email, $this->porcentaje);
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".capacidad_alta", $data, "Capacidad alta en $tipoEntidad $alerta->nombreEntidad");
						}
						break;*/
					case ALARMA_SILOS_RECEPCION:
						$data = [
							"nombre" => $alerta->nombreEmail,
							"fecha" => $this->fecha,
							"idSensor" => $this->idSensor,
							"nombreSensor" => $alerta->nombreSensor,
							"distancia" => $this->altura,
							"titulo" => "Lectura actualizada"
						];
						$this->enviarAlerta($this->idSensor, ALARMA_SILOS_RECEPCION, $alerta->id_usuario_a, $this->altura, $alerta->email, "alertas.sensoresNivelLlenado.recepcion", $data, "Nuevo estado en sensor de nivel de llenado $alerta->nombreSensor");
						break;
					/*case ALARMA_SILOS_DESNIVEL:
						if ($capacidad <= $alerta->parametros)
						{
							app('App\Http\Controllers\AlertasController')->SILOS_capacidadBaja($alerta->email, $alerta->nombreEmail, $fecha, $idSensor, $alerta->nombreSensor, $capacidad);
						}
						break;*/
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores($this->idSensor . ": " . $e->getMessage());
		}
	}



	/*public function handle()
	{
		// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
		// $alertas = app('App\Http\Controllers\SensoresController')->obtenerAlertasActivas($this->idSensor);
		$entidad = DB::select("SELECT id, nombre, latitud, longitud, id_tipo, id_sensor FROM entidades JOIN entidades_recipientes ON entidades.id = entidades_recipientes.id_entidad WHERE id_sensor = ?", [$this->idSensor]);

		try
		{
			$entidad = $entidad[0];
			$ruta = null;
			$tipoEntidad = null;

			switch ($entidad->id_tipo)
			{
				case ENTIDAD_SILO:
					$ruta = "silos";
					$tipoEntidad = "silo";
					$rutaEntidad = "/ver/silos/informacion/";
					break;
				case ENTIDAD_AGUA_DEPOSITOS:
					$ruta = "agua_depositos";
					$tipoEntidad = "depósito de agua";
					$rutaEntidad = "/ver/depositos/informacion/";
					break;
				
				default:
					$ruta = "silos";
					$tipoEntidad = "silo";
					$rutaEntidad = "";
					break;
			}

			// Ahora obtenemos el tipo de entidad con la que trabaja este sensor
			foreach ($alertas as $alerta)
			{
				LogController::mensaje("Empezamos a buscar alertas");
				switch ($alerta->id_alerta)
				{
					case ALARMA_BATERIA_BAJA:
					LogController::mensaje("Alerta batería baja");
						if ($this->bateria <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"bateria" => $this->bateria,
								"titulo" => "Alerta por batería baja",
								"idTipo" => SENSOR_SILO
							];
							LogController::enviarEmail($alerta->email, "alertas.bateria_baja", $data, "Bateria baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_TEMPERATURA_BAJA:
					LogController::mensaje("Alerta temperatura baja");
						if ($this->temperatura <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"nombreEntidad" => $entidad->nombre,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta",
								"idTipo" => SENSOR_SILO
							];
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".temperatura_baja", $data, "Temperatura baja en $tipoEntidad $entidad->nombre");
						}
						break;
					case ALARMA_TEMPERATURA_ALTA:
					LogController::mensaje("Alerta temperatura alta");
						if ($this->temperatura >= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"nombreEntidad" => $entidad->nombre,
								"temperatura" => $this->temperatura,
								"parametros" => $alerta->parametros,
								"titulo" => "Alerta por temperatura alta",
								"idTipo" => SENSOR_SILO
							];
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".temperatura_alta", $data, "Temperatura alta en $tipoEntidad $entidad->nombre");
						}
						break;
					case ALARMA_SNR_BAJA:
					LogController::mensaje("Alerta SNR_BAJA");
						if ($this->snr <= 10)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"nombreEntidad" => $entidad->nombre,
								"titulo" => "Alerta por baja cobertura",
								"idTipo" => SENSOR_SILO
							];
							LogController::enviarEmail($alerta->email, "alertas.snr_baja", $data, "Mala cobertura en Silo $alerta->nombreSensor");
						}
						break;
					case ALARMA_SILOS_CAPACIDAD_BAJA:
					LogController::mensaje("Alerta capacidad baja");
						if ($this->porcentaje <= $alerta->parametros)
						{
							LogController::mensaje("Entramos en capacidad baja");
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"nombreEntidad" => $entidad->nombre,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por baja ocupación",
								"parametros" => $alerta->parametros,
								"idTipo" => SENSOR_SILO
							];
							LogController::mensaje("Capacidad baja formada");
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".capacidad_baja", $data, "Capacidad baja en $tipoEntidad $entidad->nombre");
							LogController::mensaje("Capacidad baja enviada");
						}
						break;
					case ALARMA_SILOS_CAPACIDAD_ALTA:
					LogController::mensaje("Alerta capacidad alta");
						if ($this->porcentaje <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"nombreEntidad" => $entidad->nombre,
								"capacidad" => $this->porcentaje,
								"titulo" => "Alerta por alta ocupación",
								"parametros" => $alerta->parametros,
								"idTipo" => SENSOR_SILO
							];
							LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".capacidad_alta", $data, "Capacidad alta en $tipoEntidad $entidad->nombre");
						}
						break;
					case ALARMA_SILOS_RECEPCION:
					LogController::mensaje("Alarma recepción");
						$data = [
							"nombre" => $alerta->nombreEmail,
							"fecha" => $this->fecha,
							"idSensor" => $this->idSensor,
							"nombreSensor" => $alerta->nombreSensor,
							"nombreEntidad" => $entidad->nombre,
							"capacidad" => $this->porcentaje,
							"titulo" => "Capacidad actualizada",
							"idTipo" => SENSOR_SILO
						];
						LogController::enviarEmail($alerta->email, "alertas." . $ruta . ".recepcion", $data, "Nuevo estado en $tipoEntidad $entidad->nombre");
						break;
					case ALARMA_SILOS_DESNIVEL:
						/*if ($capacidad <= $alerta->parametros)
						{
							app('App\Http\Controllers\AlertasController')->SILOS_capacidadBaja($alerta->email, $alerta->nombreEmail, $fecha, $idSensor, $alerta->nombreSensor, $capacidad);
						}
						break;
					LogController::mensaje("Fin de buscar alerta");
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores($this->idSensor . ": " . $e->getMessage());
		}
	}*/
}
