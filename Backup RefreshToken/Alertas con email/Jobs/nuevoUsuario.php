<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class nuevoUsuario implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $nombre;
	protected $email;
	protected $pass;

	public function __construct($nombre, $email, $pass)
	{
		$this->nombre = $nombre;
		$this->email = $email;
		$this->pass = $pass;
	}

	public function handle()
	{
		$datosEmail = ["user" => $this->email, "pass" => $this->pass, "nombre" => $this->nombre];
		LogController::enviarEmail($this->email, "alertas.email_cuenta", $datosEmail, "Nuevo usuario de Ventum IdC");
	}
}
