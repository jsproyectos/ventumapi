<?php

namespace App\Jobs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController; // Para la creación de logs y envíos por mail

class alertasSensoresPuertas extends alertasSensores
{
	protected $tipoApertura;

	public function __construct($idSensor, $fecha, $bateria, $bp, $snr, $rssi, $tipoApertura)
	{
		$this->idSensor = $idSensor;
		$this->fecha = $fecha;
		$this->bateria = $bateria;
		$this->bp = $bp;
		$this->snr = $snr;
		$this->rssi = $rssi;
		$this->tipoApertura = $tipoApertura;
	}

	public function handle()
	{
		try
		{
			// Una vez obtenidas las alertas activas, comprobamos las que se cumplen. Primero, se obtienen datos necesarios para trabajar con las alertas
			$alertas = $this->obtenerAlertasActivas($this->idSensor);

			foreach ($alertas as $alerta)
			{
				switch ($alerta->id_alerta)
				{
					case ALARMA_BATERIA_BAJA:
						if ($this->bp <= $alerta->parametros)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"bp" => $this->bp,
								"titulo" => "Alerta por batería baja",
								"idTipo" => SENSOR_PUERTA
							];
							$this->enviarAlerta($this->idSensor, ALARMA_BATERIA_BAJA, $alerta->id_usuario_a, $this->bp, $alerta->email, "alertas.bateria_baja", $data, "Bateria baja en sensor $alerta->nombreSensor");
						}
						break;
					case ALARMA_PUERTAS_APERTURA:
						if ($this->tipoApertura == PUERTA_ABIERTA)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"tipo" => PUERTA_ABIERTA,
								"titulo" => "Alerta por apertura de puerta",
								"idTipo" => SENSOR_PUERTA
							];
							$this->enviarAlerta($this->idSensor, ALARMA_PUERTAS_APERTURA, $alerta->id_usuario_a, null, $alerta->email, "alertas.sensoresPuertas.puertas", $data, "Apertura en sensor puerta: $alerta->nombreSensor");
						}
						break;
					case ALARMA_PUERTAS_CIERRE:
						if ($this->tipoApertura == PUERTA_CERRADA)
						{
							$data = [
								"nombre" => $alerta->nombreEmail,
								"fecha" => $this->fecha,
								"idSensor" => $this->idSensor,
								"nombreSensor" => $alerta->nombreSensor,
								"tipo" => PUERTA_CERRADA,
								"titulo" => "Alerta por cierre de puerta",
								"idTipo" => SENSOR_PUERTA
							];
						$this->enviarAlerta($this->idSensor, ALARMA_PUERTAS_CIERRE, $alerta->id_usuario_a, null, $alerta->email, "alertas.sensoresPuertas.puertas", $data, "Cierre en sensor puerta: $alerta->nombreSensor");
						}
						break;
				}
			}
		}
		catch (Exception $e)
		{
			LogController::errores("[Alertas] $this->idSensor" . ": " . $e->getMessage());
		}
	}
}
