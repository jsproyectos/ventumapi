@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, se ha detectado que el dispositivo <b>{{ $nombreSensor }}</b> ha detectado una temperatura inferior a la indicada ({{ $temperatura }}ºC).
@endSection