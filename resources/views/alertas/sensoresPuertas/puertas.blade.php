@extends('alertas.plantilla')

@section('alerta')
	@if($tipo == PUERTA_ABIERTA)
		Hola <b>{{ $nombre }}</b>, se ha detectado una apertura en la puerta <b>{{ $nombreSensor }}</b>. Si es el resultado esperado, puede ignorar este mensaje. De lo contrario, es posible que se deba a una acción no autoriada. Por favor, asegurese de que todo está corretamente.
	@else
		Hola <b>{{ $nombre }}</b>, se ha detectado un cierre en la puerta <b>{{ $nombreSensor }}</b>. Si es el resultado esperado, puede ignorar este mensaje. De lo contrario, es posible que se deba a una acción no autoriada. Por favor, asegurese de que todo está corretamente.
	@endif
@endSection