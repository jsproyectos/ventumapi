@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, se ha detectado que la puerta <b>{{ $nombreSensor }}</b> continua cerrada el tiempo indicado ({{ $parametros }} minutos). Si es el resultado esperado, puede ignorar este mensaje. De lo contrario, es posible que se deba a una acción no autoriada. Por favor, asegurese de que todo está corretamente.
@endSection