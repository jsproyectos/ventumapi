@extends('alertas.plantilla_admin')

@section('alerta')
	Los siguientes sensores no han emitido en su ciclo correcto:
	<ul class="lista">
		@foreach($sensores as $sensor)
		<li>
			@if($sensor->nombreEntidad)
				<strong>{{ $sensor->nombreSensor }}</strong>, asignado a <strong>{{ $sensor->nombreEntidad }}</strong> ({{ $sensor->nombreExplotacion }}).
			@else
				<strong>{{ $sensor->nombreSensor }}</strong>, sin asignar.
			@endif
			Ultima emisión: {{ $sensor->fechaUltimaEmision }}. Debería haber sido a las {{ $sensor->fechaNormal }} ({{ $sensor->tiempoTexto }})
		</li>
		@endforeach
	</ul>
@endSection