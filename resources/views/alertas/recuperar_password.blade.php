<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Recuperación de contraseña</title>
	<style>
		.lista { text-align: left; }
		.lista li { padding-left: 16px; }
	</style>
</head>
<body>
	<div style="text-align: center;">
		<div style="border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px;width: 60%;min-width: 700px;margin: 0 auto;" align="center">
			<div style="font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word">
					<div style="font-size:24px;margin-bottom: 10px;">Recuperación de contraseña</div>
					<div style="font-size:18px;margin-bottom: 10px; font-weight: bold;background-color:#dedede">Fecha: {{ date("d-m-Y H:i:s") }}</div>
					<p style="font-size:14px;margin-bottom: 5px;line-height:20px">
						<p>Hola {{ $nombre }}</p>

						<p style="text-align: left;">Si recibe este correo, es poque ha solicitado una recuperación de contraseña. Le indicamos que la nueva contraseña es:</p>

						<h3><b>{{ $password }}</b></h3>

						<p style="text-align: left;">Por seguridad y facilidad, le recomendamos que vuelva a cambiarla por una mas sencilla de recordar para usted a través del apartado <b>Perfil</b> en la aplicación Gavilán Control</p>
					</p>
			</div>
			<div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:14px;color:rgba(0,0,0,0.87);line-height:20px;padding-top:20px;text-align:left;text-align:center;">
				<img style="margin-bottom: 10px" src="http://clientes.ventumidc.es/img/logo_mini.png">
				<div style="font-family:'Roboto'">
				Este es un correo automático. Por favor, no responda al mismo</div>
			</div>
		</div>
	</div>
	<div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center">Ventum IDC, C\Miguel Alcantú 06510 Alburquerque (Badajoz) - www.ventumidc.es</div>
</body>
</html>