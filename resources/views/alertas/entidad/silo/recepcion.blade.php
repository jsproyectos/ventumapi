@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, le informamos que actualmente su silo <b>{{ $nombreEntidad }}</b> se encuentra con un porcentaje de capacidad del <b>{{ $capacidad }}%</b>.
@endSection