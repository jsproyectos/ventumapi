@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, le informamos que actualmente su silo <b>{{ $nombreEntidad }}</b> se encuentra con el sensor desnivelado. Esto provocará que las lecturas de capacidad no sean correctas. Esto se puede deber a un mal cierre de la tapa o que se encuentre abierta</b>.
@endSection