@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, le informamos que actualmente su tanque de leche <b>{{ $nombreEntidad }}</b> se encuentra con el sensor de capacidad desnivelado, el cual provocará que las lecturas de capacidad no sean correctas. Esto se puede deber a un mal cierre de la tapa o que se encuentre abierta</b>.
@endSection