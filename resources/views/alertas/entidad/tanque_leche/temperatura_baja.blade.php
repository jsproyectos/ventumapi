@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Le indicamos que la temperatura de su tanque de leche <b>{{ $nombreEntidad }}</b> se encuentra en <b>{{ $temperatura }} Cº</b>, por debajo del mínimo que nos indicó ({{ $parametros }} Cº)
@endSection