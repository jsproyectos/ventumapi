@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Le indicamos que la luminosidad detectada en la celda <b>{{ $nombreEntidad }}</b> se es de <b>{{ $luminosidad }} lux</b>, por encima del máximo que nos indicó ({{ $parametros }} lux)
@endSection