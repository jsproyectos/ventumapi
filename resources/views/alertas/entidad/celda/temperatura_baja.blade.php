@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Le indicamos que la temperatura de la celda <b>{{ $nombreEntidad }}</b> se encuentra en <b>{{ $temperatura }} Cº</b>, por debajo del mínimo que nos indicó ({{ $parametros }} Cº)
@endSection