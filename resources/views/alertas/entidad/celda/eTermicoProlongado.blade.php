@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, se ha detectado estés térmico prolongado y peligroso en la celda <b>{{ $nombreEntidad }}</b>. Le recomendamos ponerse en contacto con los encargados para relajar la situación.
@endSection