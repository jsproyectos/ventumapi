@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Le indicamos que la temperatura de su depósito de agua <b>{{ $nombreEntidad }}</b> se encuentra en <b>{{ $temperatura }} Cº</b>, por encima del máximo que nos indicó ({{ $parametros }} Cº)
@endSection