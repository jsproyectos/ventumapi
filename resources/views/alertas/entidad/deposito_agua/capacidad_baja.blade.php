@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Desde Ventum le indicamos que su depósito de agua <b>{{ $nombreEntidad }}</b> se encuentra con una capacidad del <b>{{ $capacidad }}%</b>, por debajo del mínimo que nos indicó ({{ $parametros }}%)
@endSection