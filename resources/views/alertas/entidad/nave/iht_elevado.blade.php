@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, le informamos que se ha detectado un IHT potencialmente peligroso en la nave <b>{{ $nombreEntidad }}</b>, con valores IHTMax {{ $IHTMax }} y un rango de {{ $rango }}. Le recomendamos ponerse en contacto con los encargados para relajar la situación.
@endSection