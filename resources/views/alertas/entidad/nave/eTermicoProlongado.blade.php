@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, se ha detectado estrés térmico prolongado y peligroso en la nave <b>{{ $nombreEntidad }}</b>. Le recomendamos ponerse en contacto con los encargados para relajar la situación.
@endSection