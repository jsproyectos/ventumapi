@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, se ha detectado que la batería del dispositivo <b>{{ $nombreSensor }}</b> se encuentra con baja capacidad de carga ({{ $bp }}%). Le recomendamos que cargue su dispositivo cuanto antes o reemplace sus baterías en caso de ser posible. Para cualquier duda, puede contactar con nosotros
@endSection