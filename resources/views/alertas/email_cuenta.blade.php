<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema de alertas Ventum IDC</title>
</head>
<body>
	<div style="text-align: center;">
		<div style="border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px;width: 60%;min-width: 700px;margin: 0 auto;" align="center">
			<div style="font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word">
					<p style="font-size:14px;margin-bottom: 5px;line-height:20px; text-align: left;">Hola <b>{{ $nombre }}</b></p>
					<p style="font-size:14px;margin-bottom: 40px;line-height:20px; text-align: left;">Desde Ventum IdC, le indicamos que su cuenta de usuario para la plataforma Ventum Monitor acaba de ser creada. Las credenciales con las que podrá hacer uso de nuestros servicios son las siguientes:</p>
					<p style="font-size:14px;margin-bottom: 5px;line-height:20px; text-align: left;"><b>Usuario</b>: {{ $user }}</p>
					<p style="font-size:14px;margin-bottom: 40px;line-height:20px; text-align: left;"><b>Contraseña</b>: {{ $pass }}</p>
					<p style="font-size:14px;margin-bottom: 5px;line-height:20px; text-align: left;">Puede acceder a nuestra plataforma a través de la siguiente dirección: <a href="http://clientes.ventumidc.es">http://clientes.ventumidc.es</a></p>
					<p style="font-size:14px;margin-bottom: 5px;line-height:20px"></p>
					<p style="font-size:14px;margin-bottom: 5px;line-height:20px; text-align: left;">Saludos cordiales</a></p>
			</div>
			<div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:14px;color:rgba(0,0,0,0.87);line-height:20px;padding-top:20px;text-align:left;text-align:center;">
				<img style="margin-bottom: 10px" src="http://clientes.ventumidc.es/img/logo_mini.png">
				<div style="font-family:'Roboto'">
				Sistema automático de alertas Ventum IDC. Si no desea recibir este tipo de alertas, puede anularlas.</div>
			</div>
		</div>
	</div>
	<div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center">Ventum IDC, C\Miguel Alcantú 06510 Alburquerque (Badajoz) - www.ventumidc.es</div>
</body>
</html>