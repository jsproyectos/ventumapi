@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, le informamos que el sensor de machos <b>{{ $nombreSensor }}</b> acaba de detectar una monta realizada. En total, el dispositivo ha registrado <b>{{ $montas }}</b> montas.
@endSection