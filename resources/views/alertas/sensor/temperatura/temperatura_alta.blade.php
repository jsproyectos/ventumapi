@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Le indicamos que la temperatura del dispositivo <b>{{ $nombreSensor }}</b> se encuentra en <b>{{ $temperatura }} Cº</b>, por encima del máximo que nos indicó ({{ $parametros }} Cº)
@endSection