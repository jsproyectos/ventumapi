@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Le indicamos que la humedad relativa de la nave <b>{{ $nombreEntidad }}</b> se encuentra en <b>{{ $humedad }}%</b>, por debajo del mínimo que nos indicó ({{ $parametros }}%)
@endSection