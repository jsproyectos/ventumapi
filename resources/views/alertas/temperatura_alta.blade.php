@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, se ha detectado que el dispositivo <b>{{ $nombreSensor }}</b> ha detectado una temperatura superior a la indicada ({{ $temperatura }}ºC).
@endSection