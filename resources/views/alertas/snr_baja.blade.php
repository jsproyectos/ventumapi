@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, se ha detectado que hay poca cobertura en la zona donde se encuentra el dispositivo <b>{{ $nombreSensor }}</b>. Si es la primera vez que le llega este mensaje, puede ignorarlo. Si ocurre con mucha frecuencia, contacte con nosotros para mejorar la calidad de su señal</p>
@endSection