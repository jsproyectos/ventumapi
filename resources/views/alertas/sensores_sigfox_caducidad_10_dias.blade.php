@extends('alertas.plantilla_admin')

@section('alerta')
	<p style="text-align: left;">Los siguientes sensores llevan tokens sigfox que caducan en 10 días ({{ date('d-m-Y', strtotime(date('Y-m-d') . ' + 10 day')) }})</p>
	<p style="text-align: left;">Se recomienda adquirir tokens para tenerlos preparados</p>
		<ul class="lista">
			@foreach($sensores as $sensor)
			<li>
				@if(!empty($sensor->nombreEntidad))
					<strong>{{ $sensor->nombreSensor }} [{{ $sensor->dispositivo }}]</strong>, asignado a <strong>{{ $sensor->nombreEntidad }}</strong> ({{ $sensor->nombreExplotacion }}).
				@else
					<strong>{{ $sensor->nombreSensor }} [{{ $sensor->dispositivo }}]</strong>, sin asignar.
				@endif
				Caducidad: {{ date('d-m-Y H:i:s', strtotime($sensor->finContrato)) }}
			</li>
			@endforeach
		</ul>
@endSection