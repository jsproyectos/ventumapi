@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>. Desde Ventum le indicamos que su sensor de nivel de llenado de silos <b>{{ $nombreSensor }}</b> ha marcado una distancia de <b>{{ $distancia }}cm</b>, por encima del máximo que nos indicó ({{ $parametros }}cm)
@endSection