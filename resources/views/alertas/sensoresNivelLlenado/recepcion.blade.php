@extends('alertas.plantilla')

@section('alerta')
	Hola <b>{{ $nombre }}</b>, le informamos que actualmente su sensor de nivel de llenado de silos <b>{{ $nombreSensor }}</b> se ha actualizado con una distancia de <b>{{ $distancia }}cm</b>.
@endSection