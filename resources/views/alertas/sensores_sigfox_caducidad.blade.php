@extends('alertas.plantilla_admin')

@section('alerta')
	<p style="text-align: left;">Los siguientes sensores llevan tokens sigfox que han caducado hoy</p>
	<p style="text-align: left;">Se recomienda renovar los tokens lo antes posible para no perder información</p>
		<ul class="lista">
			@foreach($sensores as $sensor)
			<li>
				@if(!empty($sensor->nombreEntidad))
					<strong>{{ $sensor->nombreSensor }} [{{ $sensor->dispositivo }}]</strong>, asignado a <strong>{{ $sensor->nombreEntidad }}</strong> ({{ $sensor->nombreExplotacion }}).
				@else
					<strong>{{ $sensor->nombreSensor }} [{{ $sensor->dispositivo }}]</strong>, sin asignar.
				@endif
			</li>
			@endforeach
		</ul>
@endSection