@extends('alertas.plantilla_admin')

@section('alerta')
	<p style="text-align: center;">Los siguientes sensores llevan tokens sigfox que caducan en menos de 10 días</p>
	@foreach($listaSensores as $dias => $sensores)
		@if ($dias == 0)
			<h4>Caducidad hoy</h4>
		@else
			<h4>Caducidad dentro de {{ $dias }} días</h4>
		@endif
			<ul class="lista">
				@foreach($sensores as $sensor)
				<li>
					@if(!empty($sensor['nombreEntidad']))
						<strong>{{ $sensor['nombreSensor'] }} [{{ $sensor['dispositivo'] }}]</strong>, asignado a <strong>{{ $sensor['nombreEntidad'] }}</strong> ({{ $sensor['nombreExplotacion'] }}).
					@else
						<strong>{{ $sensor['nombreSensor'] }} [{{ $sensor['dispositivo'] }}]</strong>, sin asignar.
					@endif
					Caducidad: {{ date('d-m-Y H:i:s', strtotime($sensor['finContrato'])) }}
				</li>
				@endforeach
			</ul>
	@endforeach
@endSection