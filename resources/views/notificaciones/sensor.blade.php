{
	"app_id": "{{ API_KEY_ONE_SIGNAL }}",
	"data": {
		"id_sensor": {{ $id_sensor }}, "notificacion":
		{
			"id": {{ $notificacion->id }},
			"fecha": "{{ $notificacion->fecha }}",
			"tipo": {{ $notificacion->tipo }},
			"id_tipo": {{ $notificacion->id_tipo }},
			"id_referencia": {{ $notificacion->id_referencia }},
			"id_alerta": {{ $notificacion->id_alerta }},
			"parametros": "{{ $notificacion->parametros }}",
			"leida": {{ $notificacion->leida == 1 ? 'true' : 'false' }},
			"nombre": "{{ $notificacion->nombre }}",
			"codigo": "{{ $notificacion->codigo }}",
			"texto": "{{ $notificacion->texto }}"
		}
	},
	"contents": {"en": "{{ $mensaje }}", "es": "{{ $mensaje }}"},
	"headings": {"en": "{{ TITULO_NOTIFICACIONES }}", "es": "{{ TITULO_NOTIFICACIONES }}"},
	"include_player_ids": ["{{ $service_id_ionic }}"]
}