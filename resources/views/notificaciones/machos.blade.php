{
	"app_id": "{{ API_KEY_ONE_SIGNAL }}",
	"data": {"id_entidad": {{ $id_entidad }}},
	"contents": {"en": "{{ $mensaje }}", "es": "{{ $mensaje }}"},
	"headings": {"en": "Ventun Machos", "es": "Ventum Machos"},
	"include_player_ids": ["{{ $service_id_ionic }}"]
}