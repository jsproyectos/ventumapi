{
	"name":
	{
		"value": "{{ $entidad->nombre }}",
		"type": "String"
	},
	"typeContainer":
	{
		"value": "{{ $entidad->tipo }}",
		"type": "String"
	},
	"code":
	{
		"value": "{{ $entidad->codigo }}",
		"type": "String"
	},
	"description":
	{
		"value": "{{ $entidad->descripcion }}",
		"type": "String"
	},
	"location":
	{
		"type": "geo:json",
		"value":
		{
			"type": "Point",
			"coordinates": [{{ $entidad->longitud }}, {{ $entidad->latitud }}]
		}
	},
	"owner":
	{
		"value": "{{ $entidad->nombreExplotacion }}",
		"type": "String"
	},
	"dateCreated":
	{
		"type": "DateTime",
		"value": "{{ date("c", strtotime($entidad->fechaAlta)) }}"
	},
	"dateDeleted":
	{
		"type": "DateTime",
		"value": "{{ date("c", strtotime($entidad->fechaBaja)) }}"
	},
	"height":
	{
		"value": "{{ $entidad->altura }}",
		"type": "Integer",
		"metadata": 
		{
			"description":
			{
				"type": "String",
				"value": "Altura del silo en centimetros"
			}
		}
	},
	"species" :
	{
		"value": "{{ $entidad->animal }}",
		"type": "String"
	},
	"feededAnimals":
	{
		"value": {{ $entidad->animales }},
		"type": "Integer",
		"metadata": 
		{
			"timestamp":
			{
				"type": "DateTime",
				"value": "{{ date("c", strtotime($entidad->fecha)) }}"
			}
		}
	},
	"maximumLoad":
	{
		"value": {{ $entidad->capacidad }},
		"type": "Integer"
	},
	"fillingLevel":
	{
		"value": {{ $entidad->porcentaje }},
		"type": "Integer",
		"metadata": 
		{
			"timestamp":
			{
				"type": "DateTime",
				"value": "{{ date("c", strtotime($entidad->fecha)) }}"
			}
		}
	},
	"temperature":
	{
		"value": {{ $entidad->temperatura }},
		"type": "Integer",
		"metadata": 
		{
			"timestamp":
			{
				"type": "DateTime",
				"value": "{{ date("c", strtotime($entidad->fecha)) }}"
			}
		}
	},
	"consuption" :
	{
		"value" : {{ $entidad->consumo }},
		"type": "Float",
		"metadata": 
		{
			"description": 
			{
				"type" : "String",
				"value" : "Kilos de consumo por día acumulado desde reinicio"
			},
			"timestamp":
			{
				"type": "DateTime",
				"value": "{{ date("c", strtotime($entidad->fecha)) }}"
			}
		}
	}
}