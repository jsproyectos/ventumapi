{
	"auth": {
		"identity": {
			"methods": [
				"password"
			],
			"password": {
				"user": {
					"domain": {
						"name": "{{ $service }}"
					},
					"name": "{{ $user }}",
					"password": "{{ $pass }}"
				}
			}
		},
		"scope": {
			"project": {
				"domain": {
					"name": "{{ $service }}"
				},
				"name": "{{ $subservice }}"
			}
		}
	}
}